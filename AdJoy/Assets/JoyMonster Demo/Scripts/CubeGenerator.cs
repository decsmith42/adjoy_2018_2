﻿using System.Collections;
using UnityEngine;

namespace JoyMonster.Demo
{
    public class CubeGenerator : MonoBehaviour
    {
        [SerializeField] private GameObject m_CubePrefab = null;

        void OnEnable()
        {
            GameManager.OnGameStart += OnGameStart;
            GameManager.OnGameEnd += OnGameEnd;
        }

        void OnDisable()
        {
            GameManager.OnGameStart -= OnGameStart;
            GameManager.OnGameEnd -= OnGameEnd;
        }

        private void OnGameStart()
        {
            StartCoroutine(GenerateCubeCoroutines());
        }

        private void OnGameEnd()
        {
            for (int i = 0; i < this.transform.childCount; i++)
            {
                Destroy(this.transform.GetChild(i).gameObject);
            }
        }

        private IEnumerator GenerateCubeCoroutines()
        {
            CreateCube();

            yield return new WaitForSeconds(Random.Range(0.5f, 1.5f));

            if (GameManager.IsGameRunning)
            {
                StartCoroutine(GenerateCubeCoroutines());
            }
        }

        private void CreateCube()
        {
            GameObject instance = GameObject.Instantiate(m_CubePrefab, Vector3.zero, Quaternion.identity);
            instance.transform.SetParent(this.transform);
            instance.transform.localPosition = new Vector3(Random.Range(GameManager.ScreenBounds.x + 1f, GameManager.ScreenBounds.z - 1f), 9, 0);
            instance.transform.localScale *= Random.Range(1.0f, 2.0f);
        }
    }
}
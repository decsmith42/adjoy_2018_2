﻿using System.Collections;
using TMPro;
using UnityEngine;

[ExecuteInEditMode]
public class TextAlign : MonoBehaviour
{
    private void OnEnable()
    {
        GetComponent<TextMeshProUGUI>().alignment = TextAlignmentOptions.Center;
    }
}

﻿using JoyMonster.API;
using MaterialUI_JoyMonster;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace JoyMonster.Demo
{
    public class GameUI : MonoBehaviour
    {
        [SerializeField] private Graphic m_Logo = null;
        [SerializeField] private Button m_StartButton = null;
        [SerializeField] private Button m_WalletButton = null;
        [SerializeField] private MaterialSwitch m_UseMockupButton = null;
        [SerializeField] private MaterialSwitch m_RandomErrors = null;
        [SerializeField] private TextMeshProUGUI m_HelpText = null;
        [SerializeField] private TextMeshProUGUI m_ScoreText = null;
        [SerializeField] private GameObject m_ScoreContainer = null;
        [SerializeField] private GameObject m_MenuBackground = null;
        [SerializeField] private Camera m_GameCamera = null;
        [SerializeField] private Canvas m_Canvas = null;

        [SerializeField] private TextMeshProUGUI m_LabelButtonStart;
        [SerializeField] private TextMeshProUGUI m_LabelButtonWallet;

        private void Awake()
        {
            // Change value here to modify promise result type at startup if needed
            JoyMonsterAPI.promiseResultType = JoyMonsterAPI.PromiseResultType.USE_REAL_API;

            JoyMonsterPlugin.onAfterUiShow.AddListener(OnJoyMonsterUiShow);
            JoyMonsterPlugin.onBeforeUiHide.AddListener(OnJoyMonsterUiHide);
        }

        private void OnJoyMonsterUiShow()
        {
            m_Canvas.enabled = false;
        }

        private void OnJoyMonsterUiHide()
        {
            m_Canvas.enabled = true;
        }

        private void OnEnable()
        {
            GameManager.OnGameStart += OnGameStart;
            GameManager.OnGameEnd += OnGameEnd;

            m_LabelButtonStart.alignment = TextAlignmentOptions.Center;
            m_LabelButtonWallet.alignment = TextAlignmentOptions.Center;
            m_HelpText.alignment = TextAlignmentOptions.Center;
            m_ScoreText.alignment = TextAlignmentOptions.Center;

            UpdateWalletButton();

            OnGameEnd();
        }

        private void OnDisable()
        {
            GameManager.OnGameStart -= OnGameStart;
            GameManager.OnGameEnd -= OnGameEnd;
        }

        private void OnGameStart()
        {
            m_Logo.gameObject.SetActive(false);
            m_StartButton.gameObject.SetActive(false);
            m_WalletButton.gameObject.SetActive(false);
            m_UseMockupButton.gameObject.SetActive(false);
            m_RandomErrors.gameObject.SetActive(false);
            m_HelpText.gameObject.SetActive(false);
            m_ScoreContainer.gameObject.SetActive(true);
            m_MenuBackground.gameObject.SetActive(false);
            m_GameCamera.enabled = true;
        }

        private void OnGameEnd()
        {
            m_Logo.gameObject.SetActive(true);
            m_StartButton.gameObject.SetActive(true);
            m_WalletButton.gameObject.SetActive(true);
            m_UseMockupButton.gameObject.SetActive(true);
            m_RandomErrors.gameObject.SetActive(true);
            m_HelpText.gameObject.SetActive(true);
            m_ScoreContainer.gameObject.SetActive(false);
            m_MenuBackground.gameObject.SetActive(true);
            m_GameCamera.enabled = false;

            UpdateMockupButton();
        }

        public void OnStartButtonClicked()
        {
            GameManager.TriggerGameStart();
        }

        public void OnWalletButtonClicked()
        {
            JoyMonsterPlugin.ShowWallet();
        }

        public void OnUseMockupButtonClicked(bool value)
        {
            JoyMonsterPlugin.ResetBoxCollectedStatus();
            JoyMonsterAPI.promiseResultType = value ? JoyMonsterAPI.PromiseResultType.MOCKUP_ALWAYS_RESOLVE : JoyMonsterAPI.PromiseResultType.USE_REAL_API;
            JoyMonsterPlugin.ForceWalletRefreshOnNextLoad();

            UpdateWalletButton();
            m_RandomErrors.GetComponent<CanvasGroup>().alpha = value ? 1f : 0.125f;
            m_RandomErrors.GetComponent<CanvasGroup>().interactable = value;
        }

        public void OnRandomErrorsButtonClicked(bool value)
        {
            JoyMonsterAPI.promiseResultType = value ? JoyMonsterAPI.PromiseResultType.MOCKUP_RANDOM : JoyMonsterAPI.PromiseResultType.MOCKUP_ALWAYS_RESOLVE;
            JoyMonsterPlugin.ForceWalletRefreshOnNextLoad();
        }

        private void UpdateMockupButton()
        {
            if (JoyMonsterAPI.promiseResultType == JoyMonsterAPI.PromiseResultType.USE_REAL_API)
            {
                m_UseMockupButton.TurnOff();
            }
            else
            {
                m_UseMockupButton.TurnOn();
            }
        }

        private void UpdateWalletButton()
        {
            JoyMonsterPlugin.GetOfferCount()
            .Then(offerCount =>
            {
                m_WalletButton.GetComponentInChildren<TextMeshProUGUI>().text = "Open Wallet" + (offerCount > 0 ? " (Offers Available)" : " (No Offers)");
            }).Catch(exception =>
            {
                Debug.LogError("An error occured checking if the wallet has offers: " + exception.Message);
            });
        }

        private int m_LastScore;
        void Update()
        {
            if (GameManager.IsGameRunning && GameManager.Score != m_LastScore)
            {
                m_ScoreText.text = GameManager.Score.ToString();
                m_LastScore = GameManager.Score;
            }
        }
    }
}
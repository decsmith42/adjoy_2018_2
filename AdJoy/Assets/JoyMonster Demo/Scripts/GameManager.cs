﻿using UnityEngine;

namespace JoyMonster.Demo
{
    public class GameManager : MonoBehaviour
    {
        public delegate void GameEvent();
        public static event GameEvent OnGameStart;
        public static event GameEvent OnGameEnd;

        [SerializeField] private Camera m_GameCamera = null;

        private static bool m_IsGameRunning = false;
        public static bool IsGameRunning
        {
            get { return m_IsGameRunning; }
        }

        private static Vector4 m_ScreenBounds;
        public static Vector4 ScreenBounds
        {
            get { return m_ScreenBounds; }
        }

        public static int Score;

        void Start()
        {
            Vector3 p = m_GameCamera.ViewportToWorldPoint(new Vector3(0, 0, m_GameCamera.transform.localPosition.z));
            m_ScreenBounds = new Vector4(-p.x, p.y, p.x, -p.y);
        }

        public static void TriggerGameStart()
        {
            Score = 0;
            m_IsGameRunning = true;

            JoyMonsterPlugin.ResetBoxCollectedStatus();

            if (OnGameStart != null)
            {
                OnGameStart();
            }
        }

        public static void TriggerGameEnd()
        {
            m_IsGameRunning = false;

            if (OnGameEnd != null)
            {
                OnGameEnd();
            }

            JoyMonsterPlugin.ShowRewardsIfAvailable();
        }
    }
}
﻿using UnityEngine;

namespace JoyMonster.Demo
{
    public class Bullet : MonoBehaviour
    {
        [SerializeField] private float m_Speed = 5f;

        void Update()
        {
            this.transform.Translate(Vector3.up * Time.deltaTime * m_Speed);

            if (this.transform.localPosition.y > GameManager.ScreenBounds.y + 1f)
            {
                Destroy(this.gameObject);
            }
        }
    }
}
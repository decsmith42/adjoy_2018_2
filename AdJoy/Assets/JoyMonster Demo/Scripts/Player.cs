﻿using MaterialUI_JoyMonster;
using UnityEngine;

namespace JoyMonster.Demo
{
    public class Player : MonoBehaviour
    {
        [SerializeField] private float m_Speed = 5.0f;
        [SerializeField] private GameObject m_BulletPrefab = null;
        [SerializeField] private UiTransformPosition m_ButtonShoot = null;

        void OnEnable()
        {
            GameManager.OnGameStart += OnGameStart;
            GameManager.OnGameEnd += OnGameEnd;

            m_ButtonShoot.SetDefaultPositionAndOffset(new Vector2(0f, 32f), new Vector2(0f, 40f + 32f + 8f));
            m_ButtonShoot.SetPosition(Direction.Down);
        }

        void OnDisable()
        {
            GameManager.OnGameStart -= OnGameStart;
            GameManager.OnGameEnd -= OnGameEnd;
        }

        private void OnGameStart()
        {
            this.GetComponent<MeshRenderer>().material.color = Color.white;
            m_ButtonShoot.TweenPosition(Direction.Down, Direction.Middle, 0.5f, 0f, Tween.TweenType.EaseOutQuint);
        }

        private void OnGameEnd()
        {
            this.GetComponent<MeshRenderer>().material.color = Color.red;
            m_ButtonShoot.TweenPosition(Direction.Down, 0.5f, 0f, Tween.TweenType.EaseInQuint);
        }

        void Update()
        {
            if (!GameManager.IsGameRunning)
            {
                return;
            }

            if (Input.GetKey(KeyCode.LeftArrow))
            {
                MoveLeft();
            }
            else if (Input.GetKey(KeyCode.RightArrow))
            {
                MoveRight();
            }

            if (Input.GetKeyDown(KeyCode.Space))
            {
                FireBullet();
            }

            if (Input.GetMouseButton(0))
            {
                if (Input.mousePosition.x < Screen.width * 0.4f)
                {
                    MoveLeft();
                }
                else if (Input.mousePosition.x > Screen.width * 0.6f)
                {
                    MoveRight();
                }
            }

            float maxLeft = GameManager.ScreenBounds.z - 0.5f;
            if (this.transform.localPosition.x > maxLeft)
            {
                this.transform.localPosition = new Vector3(maxLeft, this.transform.localPosition.y, this.transform.localPosition.z);
            }

            float maxRight = GameManager.ScreenBounds.x + 0.5f;
            if (this.transform.localPosition.x < maxRight)
            {
                this.transform.localPosition = new Vector3(maxRight, this.transform.localPosition.y, this.transform.localPosition.z);
            }
        }

        private void MoveLeft()
        {
            this.transform.Translate(Vector3.left * Time.deltaTime * m_Speed);
        }

        private void MoveRight()
        {
            this.transform.Translate(Vector3.right * Time.deltaTime * m_Speed);
        }

        public void FireBullet()
        {
            GameObject instance = GameObject.Instantiate(m_BulletPrefab, Vector3.zero, Quaternion.identity);
            instance.transform.localPosition = this.transform.localPosition;
            instance.name = "Bullet";
        }
    }
}
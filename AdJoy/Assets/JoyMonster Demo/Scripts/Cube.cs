﻿using JoyMonster.World;
using UnityEngine;

namespace JoyMonster.Demo
{
    public class Cube : MonoBehaviour
    {
        void OnEnable()
        {
            this.GetComponent<JoyMonsterBox>().AttachBox();
        }

        void OnTriggerEnter(Collider other)
        {
            if (other.gameObject.name.Equals("Player"))
            {
                GameManager.TriggerGameEnd();
            }

            if (other.gameObject.name.Equals("Bullet"))
            {
                GameManager.Score++;

                GetComponent<JoyMonsterBox>().CollectBox();

                Destroy(other.gameObject);

                this.GetComponent<Rigidbody>().useGravity = false;
                this.GetComponent<Rigidbody>().isKinematic = true;
                this.GetComponent<MeshRenderer>().enabled = false;
                this.GetComponent<Collider>().enabled = false;
            }
        }

        void Update()
        {
            if (this.transform.localPosition.y < GameManager.ScreenBounds.w)
            {
                Destroy(this.gameObject);
            }
        }

        void OnDestroy()
        {
            // We should ALWAYS release the box when we're done with the object that has it
            this.GetComponent<JoyMonsterBox>().ReleaseBox();
        }
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEditor;
using UnityEngine;

public class GameObjectCopy : EditorWindow
{
    private GameObject m_GameObjectCopy;
    private GameObject m_GameObjectPaste;

    // Add menu named "My Window" to the Window menu
    [MenuItem("Window/GameObjectCopy")]
    static void Init()
    {
        // Get existing open window or if none, make a new one:
        GameObjectCopy window = (GameObjectCopy)EditorWindow.GetWindow(typeof(GameObjectCopy));
        window.Show();
    }

    void OnGUI()
    {
        m_GameObjectCopy = (GameObject) EditorGUILayout.ObjectField(m_GameObjectCopy, typeof(GameObject), true);
        m_GameObjectPaste = (GameObject) EditorGUILayout.ObjectField(m_GameObjectPaste, typeof(GameObject), true);

        if (GUILayout.Button("Copy"))
        {
            StringBuilder log = new StringBuilder();

            SearchChildren(m_GameObjectCopy, log, 0, m_GameObjectPaste);

            Debug.Log(log.ToString());
        }
    }

    private void CopyGO(GameObject goCopy, GameObject goPaste)
    {
        foreach (var component in goCopy.GetComponents<Component>())
        {
            var componentType = component.GetType();
            if (componentType != typeof(Transform) &&
                componentType != typeof(MeshFilter) &&
                componentType != typeof(MeshRenderer)
            )
            {
                Debug.Log("Found a component of type " + component.GetType());
                UnityEditorInternal.ComponentUtility.CopyComponent(component);
                UnityEditorInternal.ComponentUtility.PasteComponentAsNew(goPaste);
                Debug.Log("Copied " + component.GetType() + " from " + goCopy.name + " to " + goPaste.name);
            }
        }
    }

    private void SearchChildren(GameObject go, StringBuilder log, int depth, GameObject pasteGo)
    {
        if (m_GameObjectCopy.GetComponentsInChildren<Transform>(true).Length == 0) return;

        for (int i = 0; i < depth; i++)
        {
            log.Append("-");
        }

        log.Append(go.name).Append("\n");

        GameObject newPasteGo = new GameObject(go.name);
        newPasteGo.transform.SetParent(pasteGo.transform);

        CopyGO(go, newPasteGo);

        foreach (Transform child in m_GameObjectCopy.GetComponentsInChildren<Transform>())
        {
            if(child.parent != go.transform) continue;

            SearchChildren(child.gameObject, log, depth++, newPasteGo);
        }
    }
}

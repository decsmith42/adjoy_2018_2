﻿using JoyMonster.Core;
using UnityEditor;

namespace JoyMonster
{
    [CustomEditor(typeof(JoyMonsterSettings))]
    internal class JoyMonsterSettingsEditorWrapper : JoyMonsterSettingsEditor { }
}
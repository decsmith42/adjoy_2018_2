﻿using JoyMonster.World;
using UnityEditor;

namespace JoyMonster.Editor
{
    [CustomEditor(typeof(JoyMonsterBox))]
    [CanEditMultipleObjects]
    public class JoyMonsterBoxEditorWrapper : JoyMonsterBoxEditor { }
}
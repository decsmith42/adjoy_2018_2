﻿using JoyMonster.Core;
using UnityEditor;
using UnityEngine;

public static class JoyMonsterEditor
{
    [MenuItem("Window/JoyMonster/Settings")]
    public static void ShowSettings()
    {
        JoyMonsterSettings currentSettingsFile = null;

        foreach (string asset in AssetDatabase.FindAssets("t:JoyMonsterSettings"))
        {
            currentSettingsFile = AssetDatabase.LoadAssetAtPath<JoyMonsterSettings>(AssetDatabase.GUIDToAssetPath(asset));
            break;
        }

        if (currentSettingsFile == null)
        {
            currentSettingsFile = ScriptableObject.CreateInstance<JoyMonsterSettings>();
        }

        Selection.activeObject = currentSettingsFile;
    }
}

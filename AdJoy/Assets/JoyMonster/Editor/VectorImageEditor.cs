﻿//  Copyright 2017 MaterialUI for Unity http://materialunity.com
//  Please see license file for terms and conditions of use, and more information.

using UnityEditor;

namespace MaterialUI_JoyMonster
{
    [CanEditMultipleObjects]
    [CustomEditor(typeof(VectorImage))]
    public class VectorImageEditor : MaterialBaseEditor
    {
        //  SerializedProperties
        private SerializedProperty m_Size;
        private SerializedProperty m_SizeMode;
        private SerializedProperty m_Sliced;
        private SerializedProperty m_CornerSizes;
        private SerializedProperty m_UseCorners2;
        private SerializedProperty m_Corner2Sizes;
        private SerializedProperty m_RotateMeshPos;
        private SerializedProperty m_RotateMeshUv;

        private SerializedProperty m_GradientColor;
        private SerializedProperty m_HorizontalGradient;
        private SerializedProperty m_GradientStart;
        private SerializedProperty m_GradientEnd;
        private SerializedProperty m_GradientBias1;
        private SerializedProperty m_GradientBias2;

        private SerializedProperty m_FillCenter;

        private SerializedProperty m_RaycastTarget;

        void OnEnable()
        {
            OnBaseEnable();
            m_Size = serializedObject.FindProperty("m_Size");
            m_SizeMode = serializedObject.FindProperty("m_SizeMode");
            m_Sliced = serializedObject.FindProperty("m_Sliced");
            m_CornerSizes = serializedObject.FindProperty("m_CornerSizes");
            m_UseCorners2 = serializedObject.FindProperty("m_UseCorners2");
            m_Corner2Sizes = serializedObject.FindProperty("m_Corner2Sizes");
            m_RotateMeshPos = serializedObject.FindProperty("m_RotateMeshPos");
            m_RotateMeshUv = serializedObject.FindProperty("m_RotateMeshUv");

            m_GradientColor = serializedObject.FindProperty("m_GradientColor");
            m_HorizontalGradient = serializedObject.FindProperty("m_HorizontalGradient");
            m_GradientStart = serializedObject.FindProperty("m_GradientStart");
            m_GradientEnd = serializedObject.FindProperty("m_GradientEnd");
            m_GradientBias1 = serializedObject.FindProperty("m_GradientBias1");
            m_GradientBias2 = serializedObject.FindProperty("m_GradientBias2");

            m_FillCenter = serializedObject.FindProperty("m_FillCenter");

            m_RaycastTarget = serializedObject.FindProperty("m_RaycastTarget");
        }

        void OnDisable()
        {
            OnBaseDisable();
        }

        public override void OnInspectorGUI()
        {
            serializedObject.Update();

            EditorGUILayout.PropertyField(m_RaycastTarget);

            EditorGUILayout.PropertyField(m_RotateMeshPos);

            EditorGUILayout.PropertyField(m_Sliced);

            if (m_Sliced.boolValue)
            {
                EditorGUILayout.PropertyField(m_Size);
                EditorGUILayout.PropertyField(m_UseCorners2);
                if (m_UseCorners2.boolValue)
                {
                    EditorGUILayout.PropertyField(m_Corner2Sizes, true);
                }
                else
                {
                    EditorGUILayout.PropertyField(m_CornerSizes, true);
                }
                EditorGUILayout.PropertyField(m_RotateMeshUv);

                EditorGUILayout.PropertyField(m_FillCenter);
            }
            else
            {
                EditorGUILayout.PropertyField(m_SizeMode);
                if (m_SizeMode.enumValueIndex == 0)
                {
                    EditorGUILayout.PropertyField(m_Size);
                }
            }

            InspectorFields.GraphicColorMultiField("Icon", gameObject => gameObject.GetComponent<VectorImage>());

            EditorGUILayout.PropertyField(m_GradientColor);

            if (m_GradientColor.boolValue)
            {
                EditorGUILayout.PropertyField(m_HorizontalGradient);
                EditorGUILayout.PropertyField(m_GradientStart);
                EditorGUILayout.PropertyField(m_GradientEnd);
                EditorGUILayout.PropertyField(m_GradientBias1);
                EditorGUILayout.PropertyField(m_GradientBias2);
            }

            serializedObject.ApplyModifiedProperties();
        }
    }
}
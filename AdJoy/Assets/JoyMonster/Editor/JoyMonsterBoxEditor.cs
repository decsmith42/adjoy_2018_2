﻿using JoyMonster.World;
using UnityEditor;
using UnityEngine;

namespace JoyMonster.Editor
{
    [CustomEditor(typeof(JoyMonsterBox))]
    [CanEditMultipleObjects]
    public class JoyMonsterBoxEditor : UnityEditor.Editor
    {
        private SerializedProperty useBoxGlobalSettings;
        private SerializedProperty boxPosition;
        private SerializedProperty boxScale;
        private SerializedProperty ignoreParentRotation;

        void OnEnable()
        {
            useBoxGlobalSettings = serializedObject.FindProperty("useBoxGlobalSettings");
            boxPosition = serializedObject.FindProperty("boxPosition");
            boxScale = serializedObject.FindProperty("boxScale");
            ignoreParentRotation = serializedObject.FindProperty("ignoreParentRotation");
        }

        public override void OnInspectorGUI()
        {
            serializedObject.Update();

            EditorGUILayout.PropertyField(useBoxGlobalSettings);

            if (!useBoxGlobalSettings.boolValue)
            {
                EditorGUILayout.PropertyField(boxPosition);
                EditorGUILayout.PropertyField(boxScale);
                EditorGUILayout.PropertyField(ignoreParentRotation);
            }
            else
            {
                GUI.enabled = false;
                EditorGUILayout.Vector3Field("Box Position", JoyMonsterPlugin.settings.boxPosition);
                EditorGUILayout.Vector3Field("Box Scale", JoyMonsterPlugin.settings.boxScale);
                EditorGUILayout.Toggle("Ignore Parent Rotation", JoyMonsterPlugin.settings.boxIgnoreParentRotation);
                GUI.enabled = true;
            }

            serializedObject.ApplyModifiedProperties();

            if (GUILayout.Button("Preview attach"))
            {
                JoyMonsterBox box = (JoyMonsterBox)serializedObject.targetObject;

                if (box.useBoxGlobalSettings)
                {
                    JoyMonsterBox.AttachBoxToObject(box.gameObject);
                }
                else
                {
                    JoyMonsterBox.AttachBoxToObject(box.gameObject, box.boxPosition, box.boxScale, box.ignoreParentRotation);
                }

                MaterialUI_JoyMonster.EditorCoroutine.Start(WaitAndDestroy(box));
            }
        }

        private System.Collections.IEnumerator WaitAndDestroy(JoyMonsterBox box)
        {
            float initialTime = Time.realtimeSinceStartup;

            while (Time.realtimeSinceStartup - initialTime < 2.0)
            {
                yield return null;
            }

            DestroyImmediate(box.transform.GetChild(0).gameObject);
        }
    }
}
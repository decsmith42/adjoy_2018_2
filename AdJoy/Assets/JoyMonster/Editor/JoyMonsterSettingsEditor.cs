﻿using JoyMonster.Core;
using UnityEditor;
using UnityEngine;

namespace JoyMonster
{
    [CustomEditor(typeof(JoyMonsterSettings))]
    public class JoyMonsterSettingsEditor : UnityEditor.Editor
    {
        private const float LOGO_W2H_RATIO = 1.74625f;

        private SerializedProperty m_ApiKey;
        private SerializedProperty m_PauseTimeWhenUiIsOpen;
        private SerializedProperty m_BoxPosition;
        private SerializedProperty m_BoxScale;
        private SerializedProperty m_BoxIgnoreParentRotation;
        private SerializedProperty m_EditorLogging;

        void OnEnable()
        {
            m_ApiKey = serializedObject.FindProperty("m_ApiKey");
            m_PauseTimeWhenUiIsOpen = serializedObject.FindProperty("m_PauseTimeWhenUiIsOpen");
            m_BoxPosition = serializedObject.FindProperty("m_BoxPosition");
            m_BoxScale = serializedObject.FindProperty("m_BoxScale");
            m_BoxIgnoreParentRotation = serializedObject.FindProperty("m_BoxIgnoreParentRotation");
            m_EditorLogging = serializedObject.FindProperty("m_EditorLogging");
        }

        public override void OnInspectorGUI()
        {
            serializedObject.Update();

            DrawLogo();

            EditorGUILayout.HelpBox("This is your JoyMonster API key. You need to fill this out for the plugin to work.",
                string.IsNullOrEmpty(m_ApiKey.stringValue)
                    ? MessageType.Warning
                    : MessageType.Info);
            EditorGUILayout.PropertyField(m_ApiKey);
            EditorGUILayout.Space();

            EditorGUILayout.HelpBox("Sets Time.timeScale to 0 whenever the JoyMonster UI overlay is active." +
                                    "This can help with performance.", MessageType.Info);
            EditorGUILayout.PropertyField(m_PauseTimeWhenUiIsOpen);
            EditorGUILayout.Space();

            EditorGUILayout.HelpBox("The position of a JoyMonster Box relative to its attached GameObject. " +
                                    "This global setting is used unless overridden on a per-box basis.", MessageType.Info);
            EditorGUILayout.PropertyField(m_BoxPosition);
            EditorGUILayout.Space();

            EditorGUILayout.HelpBox("The rotation of a JoyMonster Box relative to its attached GameObject. " +
                                    "This global setting is used unless overridden on a per-box basis.", MessageType.Info);
            EditorGUILayout.PropertyField(m_BoxScale);
            EditorGUILayout.Space();

            EditorGUILayout.HelpBox("Fixes rotation of a JoyMonster Box regardless of its attached GameObject's rotation. " +
                                    "This global setting is used unless overridden on a per-box basis.", MessageType.Info);
            EditorGUILayout.PropertyField(m_BoxIgnoreParentRotation);
            EditorGUILayout.Space();

            EditorGUILayout.PropertyField(m_EditorLogging, new GUIContent("Enable JoyMonster Logging"));

            serializedObject.ApplyModifiedProperties();
        }

        private void DrawLogo()
        {
            float maxWidth = EditorGUIUtility.currentViewWidth / 2f;

            EditorGUILayout.Space();
            EditorGUILayout.Space();
            EditorGUILayout.Space();

            GUILayout.Label("JOYMONSTER SETTINGS - " + JoyMonsterPlugin.JOYMONSTER_PLUGIN_VERSION,
                new GUIStyle
                {
                    fontSize = 18,
                    fontStyle = FontStyle.Bold
                });

            EditorGUILayout.Space();
        }
    }
}
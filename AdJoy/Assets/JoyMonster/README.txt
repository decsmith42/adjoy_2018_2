-----------------------
JoyMonster Unity Plugin
         v1.0.9
-----------------------

Welcome to the JoyMonster Plugin!

Please go to JoyMonster.com to create a developer account to start using the service.

You may review our in-depth documentation here:
https://joymonster.com/docs

Our HelpSuite is available for direct questions, here:
http://support.joymonster.com/support/home
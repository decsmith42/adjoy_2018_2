#import <CoreLocation/CoreLocation.h>
#import "NativeiOSLocationHelper.h"

@implementation NativeiOSLocationHelper

CLLocationManager* locationManager;

- (CLLocationManager*)getLocationManager
{
    if (locationManager == nil)
    {
        locationManager = [[CLLocationManager alloc] init];
    }
    
    return locationManager;
}

@end

static NativeiOSLocationHelper* delegateObject = nil;

extern "C"
{
    BOOL _IsLocationAuthorized()
    {
        if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusNotDetermined || [CLLocationManager authorizationStatus] == kCLAuthorizationStatusRestricted || [CLLocationManager authorizationStatus] == kCLAuthorizationStatusDenied)
        {
            return false;
        }
        else
        {
            return true;
        }
    }
    
    void _RequestWhenInUseAuthorization()
    {
        delegateObject = [[NativeiOSLocationHelper alloc] init];
        CLLocationManager* locationManager = [delegateObject getLocationManager];
        [locationManager requestWhenInUseAuthorization];
    }
}

#import <CoreLocation/CoreLocation.h>

@interface NativeiOSLocationHelper : NSObject
- (CLLocationManager*)getLocationManager;
@end

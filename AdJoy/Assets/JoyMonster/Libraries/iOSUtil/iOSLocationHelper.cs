using UnityEngine;
using System.Runtime.InteropServices;

namespace iOSUtil
{
    public static class iOSLocationHelper
    {
        [DllImport("__Internal")]
        private static extern bool _IsLocationAuthorized();
        
        [DllImport("__Internal")]
        private static extern bool _RequestWhenInUseAuthorization();
        
        public static bool IsLocationAuthorized()
        {
            if (Application.isEditor)
            {
                return true;
            }
            else
            {
                return _IsLocationAuthorized();
            }
        }
        
        public static void RequestWhenInUseAuthorization()
        {
            if (!Application.isEditor)
            {
                _RequestWhenInUseAuthorization();
            }
        }
    }
}

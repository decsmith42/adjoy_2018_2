﻿using UnityEngine;

namespace AndroidUtil
{
    public static class AndroidLocationHelper
    {
#if UNITY_ANDROID
        private static AndroidJavaObject mLocationListener;
#endif

        public static void StartLocationListener()
        {
#if UNITY_ANDROID
            using (AndroidJavaClass unityPlayer = new AndroidJavaClass("com.unity3d.player.UnityPlayer"))
            {
                using (AndroidJavaObject unityActivity = unityPlayer.GetStatic<AndroidJavaObject>("currentActivity"))
                {
                    mLocationListener = new AndroidJavaObject("com.joymonster.location.NativeAndroidLocationHelper");
                    mLocationListener.Call("startLocationListener", unityActivity, (long)(10 * 60 * 1000)); // 10 minutes between location updates
                }
            }
#endif
        }

        // This method is never called, but Unity finds it, and automatically add the ACCESS_FINE_LOCATION permission
        private static void LocationPermissionInManifestHack()
        {
            Input.location.Start();
        }

        public static double GetLatestLatitude()
        {
            if (Application.isEditor)
            {
                return 35.241053f;
            }

#if UNITY_ANDROID
            if (mLocationListener == null)
            {
                return 0;
            }

            return mLocationListener.Call<double>("getLatestLatitude");
#else
            return 0;
#endif
        }

        public static double GetLatestLongitude()
        {
            if (Application.isEditor)
            {
                return -120.64248f;
            }

#if UNITY_ANDROID
            if (mLocationListener == null)
            {
                return 0;
            }

            return mLocationListener.Call<double>("getLatestLongitude");
#else
            return 0;
#endif
        }
    }
}
package com.joymonster.location;

import android.content.Context;
import android.os.Bundle;
import android.location.Location;
import android.location.LocationManager;
import android.location.LocationListener;

public class NativeAndroidLocationHelper implements LocationListener
{
    private static Location mLatestLocation;

    public void startLocationListener(Context context, long minTimeInMilliseconds)
    {
        LocationManager locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, minTimeInMilliseconds, 0, this);
        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, minTimeInMilliseconds, 0, this);
        locationManager.requestLocationUpdates(LocationManager.PASSIVE_PROVIDER, minTimeInMilliseconds, 0, this);
    }

    @Override
    public void onLocationChanged(android.location.Location location)
    {
        mLatestLocation = location;
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras)
    {
    }

    @Override
    public void onProviderEnabled(String provider)
    {
    }

    @Override
    public void onProviderDisabled(String provider)
    {
    }

    public double getLatestLatitude()
    {
        if (mLatestLocation == null)
        {
            return 0d;
        }
        else
        {
            return mLatestLocation.getLatitude();
        }
    }

    public double getLatestLongitude()
    {
        if (mLatestLocation == null)
        {
            return 0d;
        }
        else
        {
            return mLatestLocation.getLongitude();
        }
    }
}

﻿using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

public class PrefabUtil
{
    public static bool IsPrefab<T>(T obj) where T : Object
    {
#if UNITY_EDITOR
#if UNITY_2018_3_OR_NEWER
        return !Application.isPlaying && PrefabUtility.IsPartOfPrefabAsset(obj);
#else
        return !Application.isPlaying && PrefabUtility.GetPrefabType(obj) == PrefabType.Prefab;
#endif
#else
        return false;
#endif
    }
}
﻿#if UNITY_ANDROID && UNITY_2018_3_OR_NEWER
using UnityEngine.Android;
#endif
using iOSUtil;
using UnityEngine;

namespace JoyMonster.ExternalUtils
{
    public static class PermissionHelper
    {
        public static bool IsLocationPermissionAuthorized()
        {
#if UNITY_ANDROID && UNITY_2018_3_OR_NEWER
            return Permission.HasUserAuthorizedPermission(Permission.FineLocation);
#elif UNITY_ANDROID
            return Input.location.isEnabledByUser;
#else
            return iOSLocationHelper.IsLocationAuthorized();
#endif
        }

        public static void RequestLocationPermission()
        {
#if UNITY_ANDROID && UNITY_2018_3_OR_NEWER
            Permission.RequestUserPermission(Permission.FineLocation);
#elif UNITY_ANDROID
            return;
#else
            iOSLocationHelper.RequestWhenInUseAuthorization();
#endif
        }
    }
}
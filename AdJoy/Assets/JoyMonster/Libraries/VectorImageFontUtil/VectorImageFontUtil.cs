﻿using System.Collections.Generic;
using System.IO;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

public static class VectorImageFontUtil
{
    public const string materialUIIconsFontName = "MaterialUI Icons";
    public const string materialDesignIconsFontName = "Material Design Icons";
    public static List<string> GetVectorFontNames()
    {
        List<string> vectorFontPaths = new List<string>();

#if UNITY_EDITOR
        string[] fontPaths = AssetDatabase.FindAssets("t:Font");

        foreach (string fontPath in fontPaths)
        {
            string realFontPath = AssetDatabase.GUIDToAssetPath(fontPath).Replace("Assets", "");
            if (File.Exists(Application.dataPath + realFontPath.Replace(".ttf", ".json")))
            {
                string name = AssetDatabase.LoadAssetAtPath<Font>("Assets" + realFontPath).name;
                if (name != materialUIIconsFontName && name != materialDesignIconsFontName)
                {
                    vectorFontPaths.Add(name);
                }
            }
        }
#endif

        return vectorFontPaths;
    }


}

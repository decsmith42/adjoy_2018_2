﻿using JoyMonster.Ui;
using MaterialUI_JoyMonster;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

namespace JoyMonster.Core
{
    internal sealed class UiHandler : UIBehaviour
    {
        private static UiHandler instance
        {
            get { return JoyMonsterManager.uiHandler; }
        }

        private Canvas m_Canvas;

        private Canvas canvas
        {
            get
            {
                if (m_Canvas == null)
                {
                    m_Canvas = GetComponent<Canvas>();
                }

                return m_Canvas;
            }
        }

        [SerializeField] private GameObject[] m_ScreensToLoad = null;

        private Dictionary<int, UiScreen> m_Screens;

        private Dictionary<int, UiScreen> screens
        {
            get { return m_Screens; }
        }

        [SerializeField] private DynamicGraphic m_Background = null;

        public DynamicGraphic background
        {
            get { return m_Background; }
        }

        [SerializeField] private GameObject m_AppBarPrefab = null;
        private AppBarStyleBehaviour m_AppBar;

        public AppBarStyleBehaviour appBar
        {
            get { return m_AppBar; }
        }

        [SerializeField] private GameObject m_BottomBarPrefab = null;
        private BottomBarStyleBehaviour m_BottomBar;

        public BottomBarStyleBehaviour bottomBar
        {
            get { return m_BottomBar; }
        }

        [SerializeField] private GameObject m_AddPopupPrefab = null;
        private AddPopup m_AddPopup;

        public AddPopup addPopup
        {
            get { return m_AddPopup; }
        }

        [SerializeField] private GameObject m_DismissPopupPrefab = null;
        private DismissPopup m_DismissPopup;

        public DismissPopup dismissPopup
        {
            get { return m_DismissPopup; }
        }

        [SerializeField] private GameObject m_RedeemPopupPrefab = null;
        private RedeemPopup m_RedeemPopup;

        public RedeemPopup redeemPopup
        {
            get { return m_RedeemPopup; }
        }

        [SerializeField] private GameObject m_ExpiredPopupPrefab = null;
        private ExpiredPopup m_ExpiredPopup;

        public ExpiredPopup expiredPopup
        {
            get { return m_ExpiredPopup; }
        }

        private bool m_UiIsShowing;
        public bool uiIsShowing
        {
            get { return m_UiIsShowing; }
        }

        private UiScreen m_CurrentScreen;
        private List<UiScreen> m_PreviousScreens = new List<UiScreen>(3);

        private UnityEvent m_OnBeforeUiShow = new UnityEvent();

        public UnityEvent onBeforeUiShow
        {
            get { return m_OnBeforeUiShow; }
        }

        private UnityEvent m_OnAfterUiShow = new UnityEvent();

        public UnityEvent onAfterUiShow
        {
            get { return m_OnAfterUiShow; }
        }

        private UnityEvent m_OnBeforeUiHide = new UnityEvent();

        public UnityEvent onBeforeUiHide
        {
            get { return m_OnBeforeUiHide; }
        }

        private UnityEvent m_OnAfterUiHide = new UnityEvent();

        public UnityEvent onAfterUiHide
        {
            get { return m_OnAfterUiHide; }
        }

        private void ActivateUi()
        {
            m_PreviousScreens.Clear();

            canvas.enabled = true;
            m_UiIsShowing = true;
            JoyMonsterManager.OnUiShow();

            onBeforeUiShow.InvokeIfNotNull();

            bool refreshCanvas = false;

            if (m_Screens == null)
            {
                m_Screens = new Dictionary<int, UiScreen>(m_ScreensToLoad.Length);

                foreach (GameObject screenToLoad in m_ScreensToLoad)
                {
                    UiScreen screen = Instantiate(screenToLoad.gameObject).GetComponent<UiScreen>();
                    m_Screens.Add(screen.screenId, screen);
                    DontDestroyOnLoad(screen.gameObject);
                }

                refreshCanvas = true;

                m_ScreensToLoad = null;
            }

            if (m_AppBar == null)
            {
                m_AppBar = Instantiate(m_AppBarPrefab).GetComponent<AppBarStyleBehaviour>();
                DontDestroyOnLoad(m_AppBar.gameObject);
                m_AppBarPrefab = null;
            }

            if (m_BottomBar == null)
            {
                m_BottomBar = Instantiate(m_BottomBarPrefab).GetComponent<BottomBarStyleBehaviour>();
                DontDestroyOnLoad(m_BottomBar.gameObject);
                m_BottomBarPrefab = null;
                refreshCanvas = true;
            }

            if (m_AddPopup == null)
            {
                m_AddPopup = Instantiate(m_AddPopupPrefab).GetComponent<AddPopup>();
                DontDestroyOnLoad(m_AddPopup.gameObject);
                m_AddPopupPrefab = null;
                refreshCanvas = true;
            }

            if (m_DismissPopup == null)
            {
                m_DismissPopup = Instantiate(m_DismissPopupPrefab).GetComponent<DismissPopup>();
                DontDestroyOnLoad(m_DismissPopup.gameObject);
                m_DismissPopupPrefab = null;
                refreshCanvas = true;
            }

            if (m_RedeemPopup == null)
            {
                m_RedeemPopup = Instantiate(m_RedeemPopupPrefab).GetComponent<RedeemPopup>();
                DontDestroyOnLoad(m_RedeemPopup.gameObject);
                m_RedeemPopupPrefab = null;
                refreshCanvas = true;
            }

            if (m_ExpiredPopup == null)
            {
                m_ExpiredPopup = Instantiate(m_ExpiredPopupPrefab).GetComponent<ExpiredPopup>();
                DontDestroyOnLoad(m_ExpiredPopup.gameObject);
                m_ExpiredPopupPrefab = null;
                refreshCanvas = true;
            }

            if (refreshCanvas)
            {
                GetComponent<UiScaleHandler>().Refresh();
            }
        }

        private void DeactivateUi()
        {
            canvas.enabled = false;
            m_UiIsShowing = false;

            JoyMonsterManager.OnUiHide();

            TweenManager.TimedCallback(Values.Duration.SCREEN_TRANSITION, () => { onAfterUiHide.InvokeIfNotNull(); });
        }

        public void ShowWalletScreen()
        {
            if (!m_UiIsShowing)
            {
                ActivateUi();
            }

            appBar.SetVisible(false);
            bottomBar.SetVisible(false);
            m_Background.SetState(false);

            appBar.SetLeftButton(false, false);
            appBar.SetHelpButton(true, false);
            appBar.SetSearchButton(true, false);
            appBar.SetTitle(Values.Text.APPBAR_TITLE_WALLET);

            //appBar.TransitionVisible(true);
            bottomBar.TransitionVisible(true);

            //TweenManager.TimedCallback(Values.Duration.SHORTER, () =>
            //{
            m_Background.TransitionState(true, Values.Duration.SHORT, 0f);
            //});

            TweenManager.TimedCallback(Values.Duration.SHORT,
                () => { ShowScreenOverCurrent(Values.ScreenIds.WALLET); });

            TweenManager.TimedCallback(Values.Duration.SCREEN_TRANSITION, () => { onAfterUiShow.InvokeIfNotNull(); });
        }

        public void ShowRewardsScreen()
        {
            if (!m_UiIsShowing)
            {
                ActivateUi();
            }

            appBar.SetVisible(false);
            bottomBar.SetVisible(false);
            m_Background.SetState(false);

            TweenManager.TimedCallback(Values.Duration.SHORTER,
                () => { m_Background.TransitionState(true, Values.Duration.SHORT, 0f); });

            TweenManager.TimedCallback(Values.Duration.SHORT,
                () => { ShowScreenOverCurrent(Values.ScreenIds.CONGRATULATIONS); });
        }

        public void SetScreen(int screenId)
        {
            m_CurrentScreen = GetScreen(screenId);
        }

        public void ShowScreenOverCurrent(int screen, bool setCurrentAsPrevious = true)
        {
            UiScreen tempCurrentScreen = m_CurrentScreen;
            if (setCurrentAsPrevious)
            {
                m_PreviousScreens.Add(tempCurrentScreen);
            }

            m_CurrentScreen = GetScreen(screen);
            m_CurrentScreen.Show().Then(successful =>
            {
                if (!successful) return;
                if (tempCurrentScreen != null)
                {
                    tempCurrentScreen.SetTemporaryHiddenState(true);
                }
            });
        }

        public void TransitionToScreen(int screen, bool waitForHide = true, bool setCurrentAsPrevious = true)
        {
            UiScreen tempCurrentScreen = m_CurrentScreen;
            if (setCurrentAsPrevious)
            {
                m_PreviousScreens.Add(tempCurrentScreen);
            }

            m_CurrentScreen = GetScreen(screen);
            if (waitForHide)
            {
                tempCurrentScreen.Hide()
                    .Then(successful => m_CurrentScreen.Show());
            }
            else
            {
                tempCurrentScreen.Hide();
                m_CurrentScreen.Show();
            }
        }

        private void GoBack()
        {
            if (m_CurrentScreen == null || m_PreviousScreens.Count == 0) return;

            int lastScreenIndex = m_PreviousScreens.Count - 1;

            if (m_PreviousScreens[lastScreenIndex] == null)
            {
                GoBackToGame();
                return;
            }

            UiScreen tempCurrentScreen = m_CurrentScreen;
            m_CurrentScreen = m_PreviousScreens[lastScreenIndex];
            m_PreviousScreens.RemoveAt(lastScreenIndex);

            if (m_CurrentScreen.temporarilyHidden)
            {
                m_CurrentScreen.SetTemporaryHiddenState(false);
            }
            else
            {
                m_CurrentScreen.Show();
            }

            tempCurrentScreen.Hide();
        }

        private void GoBackToGame()
        {
            onBeforeUiHide.InvokeIfNotNull();

            foreach (UiScreen screen in m_PreviousScreens)
            {
                if (screen == null) continue;
                screen.Hide();
            }

            m_PreviousScreens.Clear();
            m_CurrentScreen.Hide().Then(successful => { });
            m_CurrentScreen = null;

            TweenManager.TimedCallback(Values.Duration.SHORTER, () =>
            {
                m_Background.TransitionState(false, Values.Duration.SHORT, Values.Duration.SHORT);
                appBar.TransitionVisible(false);
                bottomBar.TransitionVisible(false);

                TweenManager.TimedCallback(Values.Duration.MEDIUM, DeactivateUi);
            });
        }

        public void ClearScreenHistory()
        {
            m_PreviousScreens.Clear();
        }

        public void ClearHistoryAndSetPreviousScreen(int screenId)
        {
            ClearScreenHistory();
            m_PreviousScreens.Add(GetScreen(screenId));
        }

        public UiScreen GetScreen(int screenId)
        {
            return instance.screens[screenId];
        }

        internal void OnShowUi()
        {
            m_UiIsShowing = true;
        }

        internal void OnHideUi()
        {
            if (!m_UiIsShowing) return;

            m_UiIsShowing = false;
        }

        public void OnBackToGameButtonPressed()
        {
            if (m_CurrentScreen == null) return;

            if (m_CurrentScreen.OnBackToGameButtonPressed())
            {
                GoBackToGame();
            }
        }

        public void OnBackButtonPressed()
        {
            if (m_CurrentScreen == null) return;

            if (m_CurrentScreen.OnBackButtonPressed())
            {
                GoBack();
            }
        }

        public void OnHelpButtonPressed()
        {
            ShowScreenOverCurrent(Values.ScreenIds.FAQ);
        }

        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                OnBackButtonPressed();
            }

            if (Input.GetKeyDown(KeyCode.Q))
            {
                expiredPopup.Show();
            }
        }
    }
}
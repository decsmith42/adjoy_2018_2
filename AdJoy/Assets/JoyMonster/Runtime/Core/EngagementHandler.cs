﻿using JoyMonster.API;
using JoyMonster.API.Model;
using UnityEngine;

namespace JoyMonster.Core
{
    internal sealed class EngagementHandler : MonoBehaviour
    {
        private bool m_IsRequestingEngagement;
        private float m_RequestEngagementLastDate;

        private float m_ReceiveEngagementLastDate;

        private float m_Cooldown = 0f;
        public float cooldown
        {
            get { return m_Cooldown; }
            private set { m_Cooldown = value; }
        }

        private bool m_CooldownEnabled = false;
        public bool cooldownEnabled
        {
            get { return m_CooldownEnabled; }
        }

        private EngagementResult[] m_AvailableEngagementArray;
        public bool hasBoxBeenCollected { get; private set; }

        private void Start()
        {
            if (!Application.isEditor)
            {
                // We init this like this, so EngagementHandler can wait 20s before requesting for the first time
                // This will give time to the LocationHandler to initialize and get a location
                m_RequestEngagementLastDate = Time.realtimeSinceStartup + 12f;
            }
        }

        public bool IsAtLeastOneEngagementAvailable()
        {
            return m_AvailableEngagementArray != null && m_AvailableEngagementArray.Length > 0;
        }

        public EngagementResult[] GetCollectedEngagementArray()
        {
            if (hasBoxBeenCollected && IsAtLeastOneEngagementAvailable())
            {
                return m_AvailableEngagementArray;
            }
            else
            {
                return null;
            }
        }

        public void SetHasBoxBeenCollected()
        {
            hasBoxBeenCollected = true;
        }

        public void ViewEngagement(EngagementResult engagement)
        {
            //  This only gets called the first time an engagement is viewed in a session
            JoyMonsterAPI.ViewEngagement(engagement.offerUuid, JoyMonsterManager.locationHandler.lastLocation.latitude,
                JoyMonsterManager.locationHandler.lastLocation.longitude);
        }

        public void Reset()
        {
            hasBoxBeenCollected = false;
        }

        public void DismissEngagements()
        {
            m_IsRequestingEngagement = false;
            m_RequestEngagementLastDate = Time.realtimeSinceStartup - Values.Duration.ENGAGEMENT_TIMER;
            //cooldown = 0f;
        }

        public void ClearEngagements()
        {
            m_AvailableEngagementArray = null;
            DismissEngagements();
        }

        private void Update()
        {
            // We check if we need to request an offer every x seconds
            if (Time.realtimeSinceStartup - m_RequestEngagementLastDate > Values.Duration.ENGAGEMENT_TIMER && cooldown <= 0f)
            {
                m_RequestEngagementLastDate = Time.realtimeSinceStartup;

                if (!IsAtLeastOneEngagementAvailable())
                {
                    RequestEngagement();
                }
            }

            if (cooldown > 0f)
            {
                cooldown -= Time.unscaledDeltaTime;
            }
            else if (cooldown < 0f)
            {
                cooldown = 0f;
            }
        }

        private void RequestEngagement()
        {
            JoyMonsterAPI.DebugLog("Requesting Engagement...");

            if (m_IsRequestingEngagement)
            {
                JoyMonsterAPI.DebugLog("Already Requesting Engagement...");
                return;
            }

            if (IsAtLeastOneEngagementAvailable())
            {
                JoyMonsterAPI.DebugLog("An engagement is already available, no need to request once again");
                return;
            }

            m_IsRequestingEngagement = true;

            JoyMonsterAPI.RequestEngagement(JoyMonsterManager.locationHandler.lastLocation.latitude,
                    JoyMonsterManager.locationHandler.lastLocation.longitude)
                .Then((engagementResultList) =>
                {
                    if (!m_IsRequestingEngagement) return;

                    m_IsRequestingEngagement = false;

                    cooldown = engagementResultList.cooldownTimer;
                    m_CooldownEnabled = true;

                    if (engagementResultList.engagements == null ||
                        engagementResultList.engagements.Length == 0)
                    {
                        JoyMonsterAPI.DebugLog("No engagement received");
                    }
                    else
                    {
                        m_AvailableEngagementArray = engagementResultList.engagements;
                        JoyMonsterAPI.DebugLog("Received " + m_AvailableEngagementArray.Length +
                                               " engagements");
                    }
                })
                .Catch(exception =>
                {
                    m_IsRequestingEngagement = false;
                    JoyMonsterAPI.DebugLogError(exception);
                });
        }
    }
}
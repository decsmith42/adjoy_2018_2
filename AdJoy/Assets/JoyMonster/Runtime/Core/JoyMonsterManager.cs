﻿using JoyMonster.API;
using UnityEngine;

namespace JoyMonster.Core
{
    internal sealed class JoyMonsterManager : MonoBehaviour
    {
        public const string JOYMONSTER_MANAGER_PREFAB = "JoyMonsterManager";
        public const string SETTINGS_FILE = "JoyMonsterSettings";

        private static JoyMonsterManager s_Instance;
        internal static JoyMonsterManager instance
        {
            get
            {
                if (s_Instance == null)
                {
                    Initialize();
                }

                return s_Instance;
            }
        }

        private static JoyMonsterSettings s_Settings;

        public static JoyMonsterSettings settings
        {
            get
            {
                if (s_Settings == null)
                {
                    s_Settings = Resources.Load<JoyMonsterSettings>(SETTINGS_FILE);
                }

                if (s_Settings == null)
                {
                    s_Settings = ScriptableObject.CreateInstance<JoyMonsterSettings>();
                }

                return s_Settings;
            }
        }

        #region Handlers

        [SerializeField] private UiHandler m_UiHandler = null;

        internal static UiHandler uiHandler
        {
            get { return instance.m_UiHandler; }
        }

        [SerializeField] private WalletHandler m_WalletHandler = null;

        internal static WalletHandler walletHandler
        {
            get { return instance.m_WalletHandler; }
        }

        [SerializeField] private FaqHandler m_FaqHandler = null;

        internal static FaqHandler faqHandler
        {
            get { return instance.m_FaqHandler; }
        }

        [SerializeField] private RedeemHandler m_RedeemHandler = null;

        internal static RedeemHandler redeemHandler
        {
            get { return instance.m_RedeemHandler; }
        }

        [SerializeField] private EngagementHandler m_EngagementHandler = null;

        public static EngagementHandler engagementHandler
        {
            get { return instance.m_EngagementHandler; }
        }

        [SerializeField] private LocationHandler m_LocationHandler = null;

        internal static LocationHandler locationHandler
        {
            get { return instance.m_LocationHandler; }
        }

        [SerializeField] private WebTextureHandler m_WebTextureHandler = null;

        internal static WebTextureHandler webTextureHandler
        {
            get { return instance.m_WebTextureHandler; }
        }

        [SerializeField] private MapHandler m_MapHandler = null;

        internal static MapHandler mapHandler
        {
            get { return instance.m_MapHandler; }
        }

        #endregion

        private static float m_TimeScaleBeforeUiShown;

        [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.AfterSceneLoad)]
        private static void Initialize()
        {
            if (!Application.isPlaying) return;
            if (s_Instance != null) return;

            s_Instance = FindObjectOfType<JoyMonsterManager>();

            if (s_Instance == null)
            {
                s_Instance = Instantiate(Resources.Load<GameObject>(JOYMONSTER_MANAGER_PREFAB)
                    .GetComponent<JoyMonsterManager>());
            }

            JoyMonsterAPI.API_KEY = "Bearer " + settings.apiKey;
            JoyMonsterAPI.clearEngagementsEvent.AddListener(engagementHandler.ClearEngagements);
            JoyMonsterAPI.editorLogging = settings.editorLogging;

            JoyMonsterAPI.DebugLog("JoyMonster Manager Initialized.");

            DontDestroyOnLoad(instance.gameObject);
        }

        public static void ShowWallet()
        {
            if (uiHandler.uiIsShowing) return;

            instance.m_UiHandler.ShowWalletScreen();
        }

        public static void ShowRewards()
        {
            if (uiHandler.uiIsShowing || !engagementHandler.IsAtLeastOneEngagementAvailable() ||
                !engagementHandler.hasBoxBeenCollected) return;

            instance.m_UiHandler.ShowRewardsScreen();
        }

        public static void OnUiShow()
        {
            if (settings.pauseTimeWhenUiIsOpen)
            {
                m_TimeScaleBeforeUiShown = Time.timeScale;
                Time.timeScale = 0f;
            }
        }

        public static void OnUiHide()
        {
            if (settings.pauseTimeWhenUiIsOpen)
            {
                Time.timeScale = m_TimeScaleBeforeUiShown;
            }
        }
    }
}
﻿using RSG;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

namespace JoyMonster.Core
{
    internal sealed class WebTextureHandler : MonoBehaviour
    {
        private static WebTextureHandler m_Instance;
        public static WebTextureHandler instance { get { return JoyMonsterManager.webTextureHandler; } }

        [SerializeField] private Texture2D m_ErrorTexture = null;
        private Texture2D errorTexture
        {
            get
            {
                //if (m_ErrorTexture == null)
                //{
                //    m_ErrorTexture = Resources.Load<Texture2D>("MissingWebTexture.psd");
                //}
                return m_ErrorTexture;
            }
        }

        private readonly Dictionary<string, Promise<Texture2D>> m_WebTextures = new Dictionary<string, Promise<Texture2D>>();

        private Promise<Texture2D> DownloadNewTexture(string url)
        {
            Promise<Texture2D> promise = new Promise<Texture2D>();

            StartCoroutine(DownloadTextureCoroutine(url, promise));

            return promise;
        }

        private IEnumerator DownloadTextureCoroutine(string url, Promise<Texture2D> promise)
        {
            if (url == "")
            {
                promise.Resolve(errorTexture);
            }
            else
            {
                using (UnityWebRequest www = UnityWebRequestTexture.GetTexture(url, true))
                {
                    yield return www.SendWebRequest();

                    if (www.isHttpError || www.isNetworkError || DownloadHandlerTexture.GetContent(www) == null)
                    {
                        if (promise.CurState == PromiseState.Pending)
                        {
                            promise.Resolve(errorTexture);
                        }
                    }
                    else
                    {
                        Texture texture = DownloadHandlerTexture.GetContent(www);

                        // If the texture is 8x8, it's the Unity red question mark, and we don't want
                        // to display it, instead, we'll show our error texture
                        if (texture != null && texture.width != 8 && texture.height != 8)
                        {
                            if (promise.CurState == PromiseState.Pending)
                            {
                                promise.Resolve(DownloadHandlerTexture.GetContent(www));
                            }
                        }
                        else
                        {
                            if (promise.CurState == PromiseState.Pending)
                            {
                                promise.Resolve(errorTexture);
                            }
                        }
                    }
                }
            }
        }

        internal static Promise<Texture2D> GetTexture(string url)
        {
            if (string.IsNullOrEmpty(url))
            {
                url = "";
            }

            if (!instance.m_WebTextures.ContainsKey(url))
            {
                instance.m_WebTextures.Add(url, instance.DownloadNewTexture(url));
            }

            return instance.m_WebTextures[url];
        }

        internal bool IsErrorTexture(Texture texture)
        {
            return texture.Equals(errorTexture);
        }
    }
}
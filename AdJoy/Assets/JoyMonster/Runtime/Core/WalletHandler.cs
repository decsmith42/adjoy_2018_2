using JoyMonster.API;
using JoyMonster.API.Model;
using RSG;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace JoyMonster.Core
{
    internal sealed class WalletHandler : MonoBehaviour
    {
        private static WalletHandler instance
        {
            get { return JoyMonsterManager.walletHandler; }
        }

        private Promise<WalletList> m_CurrentWalletRefreshPromise;

        public Promise<WalletList> currentWalletRefreshPromise
        {
            get { return m_CurrentWalletRefreshPromise; }
        }

        private Promise<bool> m_CurrentWalletAddPromise;

        public Promise<bool> currentWalletAddPromise
        {
            get { return m_CurrentWalletAddPromise; }
        }

        private Offer m_CurrentOffer;

        public Offer currentOffer
        {
            get { return m_CurrentOffer; }
        }

        public bool addToWalletInProgress
        {
            get
            {
                return m_CurrentWalletAddPromise != null &&
                       m_CurrentWalletAddPromise.CurState == PromiseState.Pending;
            }
        }

        public void RefreshWalletList(bool force = true)
        {
            if (!force && m_CurrentWalletRefreshPromise != null &&
                (m_CurrentWalletRefreshPromise.CurState == PromiseState.Pending ||
                 m_CurrentWalletRefreshPromise.CurState == PromiseState.Resolved)) return;

            m_CurrentWalletRefreshPromise = new Promise<WalletList>();

            JoyMonsterAPI.GetWalletList(JoyMonsterManager.locationHandler.lastLocation.latitude,
                    JoyMonsterManager.locationHandler.lastLocation.longitude)
                .Then(walletList =>
                {
                    if (walletList == null || walletList.offers == null)
                    {
                        m_CurrentWalletRefreshPromise.Reject(new NullReferenceException("WalletList returned null"));
                    }

                    ProcessWalletList(ref walletList);
                    m_CurrentWalletRefreshPromise.Resolve(walletList);
                })
                .Catch(exception => m_CurrentWalletRefreshPromise.Reject(exception));
        }

        public void AddToWallet(string offerUuid)
        {
            JoyMonsterAPI.DebugLog("Adding offer to wallet...");

            if (m_CurrentWalletAddPromise != null && m_CurrentWalletAddPromise.CurState == PromiseState.Pending)
            {
                m_CurrentWalletAddPromise.Resolve(false);
                JoyMonsterAPI.DebugLogError("Already adding offer.");
            }

            m_CurrentWalletAddPromise = new Promise<bool>();

            JoyMonsterAPI.AddOfferToWallet(offerUuid)
                .Then(success =>
                {
                    JoyMonsterAPI.DebugLog("Added offer to wallet: " + success);
                    m_CurrentWalletAddPromise.Resolve(success);
                })
                .Catch(exception =>
                {
                    JoyMonsterAPI.DebugLogError("Error adding offer to wallet: " + exception);
                    m_CurrentWalletAddPromise.Resolve(false);
                });
        }

        public void SelectOffer(Offer offer)
        {
            m_CurrentOffer = offer;
        }

        public void MarkWalletDataDirty()
        {
            if (m_CurrentWalletRefreshPromise == null) return;

            if (m_CurrentWalletRefreshPromise.CurState == PromiseState.Pending)
            {
                m_CurrentWalletRefreshPromise.Reject(null);
            }

            m_CurrentWalletRefreshPromise = null;
        }

        private void ProcessWalletList(ref WalletList walletList)
        {
            //  Find partner
            List<Offer> processedList = new List<Offer>(walletList.offers.Length);
            Offer partnerOffer = null;

            for (int i = 0; i < walletList.offers.Length; i++)
            {
                Offer offer = walletList.offers[i];

                if (offer.isPartner)
                {
                    if (partnerOffer == null)
                    {
                        partnerOffer = offer;
                    }
                }
                else
                {
                    processedList.Add(offer);
                }
            }

            bool hasPartner = partnerOffer != null;

            int specialOfferCount = hasPartner ? 1 : 0;

            walletList.offers = new Offer[processedList.Count + specialOfferCount];

            if (hasPartner)
            {
                walletList.offers[0] = partnerOffer;
            }

            for (int i = 0; i < walletList.offers.Length - specialOfferCount; i++)
            {
                walletList.offers[i + specialOfferCount] = processedList[i];
            }
        }
    }
}
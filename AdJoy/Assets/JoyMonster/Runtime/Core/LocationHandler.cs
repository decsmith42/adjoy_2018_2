﻿using AndroidUtil;
using iOSUtil;
using JoyMonster.API;
using System.Collections;
using UnityEngine;

namespace JoyMonster.Core
{
    internal sealed class LocationHandler : MonoBehaviour
    {
        public class LocationData
        {
            public double latitude { get; private set; }
            public double longitude { get; private set; }

            public LocationData(double latitude, double longitude)
            {
                this.latitude = latitude;
                this.longitude = longitude;
            }
        }

        private LocationData m_LastLocation = new LocationData(0, 0);

        public LocationData lastLocation
        {
            get
            {
                if (Application.platform == RuntimePlatform.Android)
                {
                    if (!isEnabledByUser)
                    {
                        return m_LastLocation;
                    }

                    return new LocationData(AndroidLocationHelper.GetLatestLatitude(), AndroidLocationHelper.GetLatestLongitude());
                }

                return m_LastLocation;
            }
        }

        void Start()
        {
            StartCoroutine(StartLocationListenerCoroutine());
        }

        private IEnumerator StartLocationListenerCoroutine()
        {
            JoyMonsterAPI.DebugLog("Starting location listener");

            if (Application.isEditor)
            {
                m_LastLocation = new LocationData(35.241053f, -120.64248f);
                JoyMonsterAPI.DebugLog("Using fake editor location: 35.241053, -120.64248");
                yield break;
            }

            // If the user did not enable location, we retry every 30sc
            if (!isEnabledByUser)
            {
                yield return new WaitForSeconds(30);
                StartCoroutine(StartLocationListenerCoroutine());
                yield break;
            }

            if (Application.platform == RuntimePlatform.Android)
            {
                // On Android we use our custom location listener class instead of the Input.location from Unity
                // Because depending on the location settings (GPS Only, Battery saver) selected by the user
                // We might never get a location...
                AndroidLocationHelper.StartLocationListener();
            }
            else
            {
                // If the user did not authorized permission, we retry every 30sc
                if (!iOSLocationHelper.IsLocationAuthorized())
                {
                    yield return new WaitForSeconds(30);
                    StartCoroutine(StartLocationListenerCoroutine());
                    yield break;
                }

                // On iOS, we use the Input.location from Unity, refreshing the lat/lon manually every 10 minutes
                if (Input.location.status == LocationServiceStatus.Failed ||
                    Input.location.status == LocationServiceStatus.Stopped)
                {
                    JoyMonsterAPI.DebugLog("Starting location service");
                    Input.location.Start();
                }

                // Wait until service initializes
                int maxWait = 20;
                while (Input.location.status == LocationServiceStatus.Initializing && maxWait > 0)
                {
                    JoyMonsterAPI.DebugLog("Waiting for location to init...");
                    yield return new WaitForSeconds(1);
                    maxWait--;
                }

                if (Input.location.status == LocationServiceStatus.Running)
                {
                    m_LastLocation = new LocationData(Input.location.lastData.latitude, Input.location.lastData.longitude);
                    JoyMonsterAPI.DebugLog("Retrieved location: " + m_LastLocation.latitude + ", " + m_LastLocation.longitude);
                }

                // Stop the location service because we don't want constant update
                // (We manually request one location update every 10mn)
                JoyMonsterAPI.DebugLog("Stopping location service");
                Input.location.Stop();

                // Wait 10mn before requesting an updated location
                yield return new WaitForSeconds(10 * 60);

                StartCoroutine(StartLocationListenerCoroutine());
            }
        }

        public static bool isEnabledByUser
        {
            get { return Application.isEditor ? true : Input.location.isEnabledByUser; }
        }
    }
}
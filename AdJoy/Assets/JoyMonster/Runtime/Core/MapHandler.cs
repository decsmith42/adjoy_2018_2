﻿using RSG;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

namespace JoyMonster.Core
{
    internal sealed class MapHandler : MonoBehaviour
    {
        private const string MAP_URL = "https://maps.googleapis.com/maps/api/staticmap?&zoom=13&key=" + "AIzaSyBomoxqLNJDPDyN6VtqmtxtzOqsaydarHg";

        internal struct MapTextureRequest
        {
            public double latitude;
            public double longitude;
            public int textureWidth;
            public int textureHeight;

            public MapTextureRequest(double latitude, double longitude, int textureWidth, int textureHeight)
            {
                this.latitude = latitude;
                this.longitude = longitude;
                this.textureWidth = textureWidth;
                this.textureHeight = textureHeight;
            }

            public MapTextureRequest(double latitude, double longitude, Vector2 textureSize) :
                this(latitude, longitude, Mathf.RoundToInt(textureSize.x), Mathf.RoundToInt(textureSize.y))
            { }
        }

        private static MapHandler instance { get { return JoyMonsterManager.mapHandler; } }

        [SerializeField] private Texture2D m_ErrorTexture = null;
        public Texture2D errorTexture { get { return m_ErrorTexture; } }

        private readonly Dictionary<MapTextureRequest, Promise<Texture2D>> m_MapTextures = new Dictionary<MapTextureRequest, Promise<Texture2D>>();

        private Promise<Texture2D> DownloadNewTexture(MapTextureRequest mapTextureRequest)
        {
            Promise<Texture2D> promise = new Promise<Texture2D>();

            Vector2 fittedMapSize = FitSizeInBounds(
                new Vector2(mapTextureRequest.textureWidth, mapTextureRequest.textureHeight),
                new Vector2(640f, 640f));

            mapTextureRequest.textureHeight = Mathf.RoundToInt(fittedMapSize.x);
            mapTextureRequest.textureHeight = Mathf.RoundToInt(fittedMapSize.y);

            StartCoroutine(DownloadTextureCoroutine(mapTextureRequest, promise));

            return promise;
        }

        private IEnumerator DownloadTextureCoroutine(MapTextureRequest mapTextureRequest, Promise<Texture2D> promise)
        {
            string url = MAP_URL +
                         "&size=" + mapTextureRequest.textureWidth + "x" + mapTextureRequest.textureHeight +
                         "&center=" + mapTextureRequest.latitude + "," + mapTextureRequest.longitude +
                         "&markers=" + mapTextureRequest.latitude + "," + mapTextureRequest.longitude;

            using (UnityWebRequest www = UnityWebRequestTexture.GetTexture(url, true))
            {
                yield return www.SendWebRequest();

                if (www.isHttpError || www.isNetworkError || DownloadHandlerTexture.GetContent(www) == null)
                {
                    promise.Resolve(m_ErrorTexture);
                }
                else
                {
                    yield return new WaitForSecondsRealtime(Random.value * 3f);
                    promise.Resolve(DownloadHandlerTexture.GetContent(www));
                }
            }
        }

        public Promise<Texture2D> GetTexture(MapTextureRequest mapTextureRequest)
        {
            if (!instance.m_MapTextures.ContainsKey(mapTextureRequest))
            {
                instance.m_MapTextures.Add(mapTextureRequest, instance.DownloadNewTexture(mapTextureRequest));
            }

            return instance.m_MapTextures[mapTextureRequest];
        }

        private static Vector2 FitSizeInBounds(Vector2 sizeToFit, Vector2 bounds)
        {
            if (sizeToFit.x < bounds.x && sizeToFit.y < bounds.y)
            {
                return sizeToFit;
            }

            return sizeToFit.x / bounds.x > sizeToFit.y / bounds.y
                ? new Vector2(bounds.x, bounds.x / (sizeToFit.x / sizeToFit.y))
                : new Vector2(bounds.y * (sizeToFit.x / sizeToFit.y), bounds.y);
        }
    }
}
﻿using JoyMonster.API.Model;
using System;
using System.Globalization;

namespace JoyMonster.Core
{
    internal static class OfferFormatter
    {
        public static float CurrentValueFloat(Offer offer)
        {
            if (offer == null) return 0f;
            float value = 0f;

            float.TryParse(offer.currentValue, NumberStyles.Any, CultureInfo.InvariantCulture, out value);

            return value;
        }

        public static float MaxValueFloat(Offer offer)
        {
            if (offer == null) return 0f;
            float value = 0f;

            float.TryParse(offer.maxValue, NumberStyles.Any, null, out value);

            return value;
        }

        public static string DisplayDescription(Offer offer)
        {
            return offer == null ? "" :
                offer.isPartner ? offer.partnerDescription : offer.description;
        }

        public static string DisplayDistance(Offer offer)
        {
            return offer == null
                ? ""
                : offer.distance > 0f
                    ? Values.Text.DISTANCE.Replace(Values.Text.DISTANCE_REPLACE, offer.distance.ToString("F"))
                    : "";
        }

        public static string DisplayExpiry(Offer offer)
        {
            if (offer == null || string.IsNullOrEmpty(offer.endDate) || offer.endDate.ToLower() == "null") return "";

            DateTime endDate = DateTime.Parse(offer.endDate);

            if (endDate == default(DateTime)) return "";

            //  TODO: Check endDate timezone.

            int lifeSpan = (endDate - DateTime.Now).Days;

            return Values.Text.OFFER_EXPIRY.Replace(Values.Text.OFFER_EXPIRY_REPLACE, lifeSpan.ToString()) +
                   (lifeSpan == 1 ? Values.Text.ENGAGEMENT_EXPIRY_SUFFIX_SINGLE : Values.Text.ENGAGEMENT_EXPIRY_SUFFIX_MULTIPLE);
        }

        public static string DisplayLimit(Offer offer)
        {
            return offer == null
                ? ""
                : offer.canAggregate
                    ? Values.Text.OFFER_REDEEMABLE_THRESHOLD.Replace(Values.Text.OFFER_REDEEMABLE_THRESHOLD_REPLACE,
                        Values.Text.CURRENCY.Replace(Values.Text.CURRENCY_REPLACE, MaxValueFloat(offer).ToString("F")))
                    : Values.Text.OFFER_LIMIT;
        }

        public static string DisplayMinimum(Offer offer)
        {
            return offer == null
                ? "null"
                : Values.Text.OFFER_REDEEMABLE_WARNING.Replace(Values.Text.OFFER_REDEEMABLE_WARNING_REPLACE_CURRENT,
                    Values.Text.CURRENCY.Replace(Values.Text.CURRENCY_REPLACE, CurrentValueFloat(offer).ToString("F")))
                    .Replace(Values.Text.OFFER_REDEEMABLE_WARNING_REPLACE_MAX,
                    Values.Text.CURRENCY.Replace(Values.Text.CURRENCY_REPLACE, MaxValueFloat(offer).ToString("F")));
        }

        public static bool HasLocationCoords(Offer offer)
        {
            return offer != null &&
                   (offer.latitude > 0f || offer.longitude > 0f);
        }

        public static bool HasAddress(Offer offer)
        {
            return offer != null && !string.IsNullOrEmpty(offer.address1) && offer.address1.ToLower().Replace(" ", "") != Values.Text.LOCATION_ANY;
        }
    }
}
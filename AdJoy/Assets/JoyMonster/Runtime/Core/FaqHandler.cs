﻿using JoyMonster.API;
using JoyMonster.API.Model;
using RSG;
using UnityEngine;

namespace JoyMonster.Core
{
    internal sealed class FaqHandler : MonoBehaviour
    {
        private static FaqHandler instance
        {
            get { return JoyMonsterManager.faqHandler; }
        }

        private Promise<FaqList> m_CurentFaqPromise;
        public static Promise<FaqList> currentFaqPromise
        {
            get { return instance.m_CurentFaqPromise; }
        }

        public static void RefreshFaqList(bool force = false)
        {
            if (!force && instance.m_CurentFaqPromise != null &&
                (instance.m_CurentFaqPromise.CurState == PromiseState.Pending ||
                 instance.m_CurentFaqPromise.CurState == PromiseState.Resolved)) return;

            instance.m_CurentFaqPromise = new Promise<FaqList>();

            JoyMonsterAPI.GetFaqList()
                .Then(faqList => { instance.m_CurentFaqPromise.Resolve(faqList); })
                .Catch(exception => instance.m_CurentFaqPromise.Reject(exception));
        }
    }
}
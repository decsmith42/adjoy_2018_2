﻿using JoyMonster.API;
using JoyMonster.API.Model;
using RSG;
using System;
using System.Collections;
using UnityEngine;

namespace JoyMonster.Core
{
    internal sealed class RedeemHandler : MonoBehaviour
    {
        public struct Redemption
        {
            public enum RedemptionType
            {
                BadGeo,
                RedeemError,
                RedeemSuccess
            }

            public RedemptionType redemptionType;
            public WalletRedeem walletRedeem;

            public Redemption(RedemptionType redemptionType, WalletRedeem walletRedeem)
            {
                this.redemptionType = redemptionType;
                this.walletRedeem = walletRedeem;
            }
        }

        public class RedeemedOffer
        {
            public Offer offer;
            public WalletRedeem walletRedeem;
            public string walletUuid;

            public DateTime redemptionTime;
            public DateTime expiryTime;

            public bool isExpired
            {
                get { return DateTime.UtcNow > expiryTime; }
            }

            public TimeSpan timerStatus
            {
                get { return expiryTime - DateTime.UtcNow; }
            }

            public string timerStatusString
            {
                get
                {
                    TimeSpan timeSpan = expiryTime - DateTime.UtcNow;
                    return timeSpan.Minutes.ToString("00") + ":" + timeSpan.Seconds.ToString("00");
                }
            }

            public float timerStatusNormalized
            {
                get { return Linear(0f, 1f, DateTime.UtcNow.Ticks - redemptionTime.Ticks, expiryTime.Ticks - redemptionTime.Ticks); }
            }

            public bool hasTimer { get { return walletRedeem.timer > 0; } }

            public bool hasImageCode
            {
                get
                {
                    return (!string.IsNullOrEmpty(walletRedeem.imageUrl) && walletRedeem.imageUrl.ToLower() != "null");
                }
            }

            public bool hasCode
            {
                get
                {
                    return !(string.IsNullOrEmpty(walletRedeem.code) || walletRedeem.code.ToLower() == "null");
                }
            }

            public RedeemedOffer(WalletRedeem walletRedeem, Offer offer, string walletUuid)
            {
                this.walletRedeem = walletRedeem;
                this.offer = offer;
                this.walletUuid = walletUuid;

                redemptionTime = DateTime.UtcNow;
                expiryTime = redemptionTime + new TimeSpan(0, 0, walletRedeem.timer);
            }

            private float Linear(float startValue, float endValue, float time, float duration)
            {
                float differenceValue = endValue - startValue;
                time = Mathf.Clamp(time, 0f, duration);
                time /= duration;

                if (time == 0f)
                    return startValue;
                if (time == 1f)
                    return endValue;

                return differenceValue * time + startValue;
            }
        }

        public Promise<Redemption> currentRedeemResponse { get; private set; }

        public RedeemedOffer currentRedeemedOffer;

        public bool redeemInProgress
        {
            get
            {
                return currentRedeemResponse != null &&
                       currentRedeemResponse.CurState == PromiseState.Pending;
            }
        }

        public Promise<Redemption> RedeemOffer(Offer offer, bool geoOverride = false)
        {
            if (currentRedeemResponse != null)
            {
                if (currentRedeemResponse.CurState == PromiseState.Resolved)
                {
                    currentRedeemResponse.Then(redemption =>
                    {
                        switch (redemption.redemptionType)
                        {
                            case Redemption.RedemptionType.BadGeo:
                                CreateNewRedeem(offer, geoOverride);
                                break;
                            case Redemption.RedemptionType.RedeemError:
                                CreateNewRedeem(offer, geoOverride);
                                break;
                            case Redemption.RedemptionType.RedeemSuccess:
                                if (currentRedeemedOffer != null && currentRedeemedOffer.isExpired)
                                {
                                    CreateNewRedeem(offer, geoOverride);
                                }
                                break;
                        }
                    });
                }
            }
            else
            {
                CreateNewRedeem(offer, geoOverride);
            }

            return currentRedeemResponse;
        }

        private void CreateNewRedeem(Offer offer, bool geoOverride)
        {
            currentRedeemResponse = new Promise<Redemption>();

            JoyMonsterAPI.RedeemOffer(offer.walletUuid, geoOverride, JoyMonsterManager.locationHandler.lastLocation.latitude, JoyMonsterManager.locationHandler.lastLocation.longitude)
            .Then(redeem =>
            {
                if (!geoOverride && redeem.geo_error)
                {
                    JoyMonsterAPI.DebugLogError("BadGeo");
                    currentRedeemResponse.Resolve(new Redemption(Redemption.RedemptionType.BadGeo, null));
                }
                else
                {
                    currentRedeemedOffer = new RedeemedOffer(redeem, offer, offer.walletUuid);
                    if (redeem.timer > 0)
                    {
                        JoyMonsterManager.instance.StartCoroutine(Wait(redeem.timer, () =>
                        {
                            JoyMonsterManager.walletHandler.MarkWalletDataDirty();
                        }));
                    }
                    JoyMonsterAPI.DebugLogError("Success");
                    currentRedeemResponse.Resolve(new Redemption(Redemption.RedemptionType.RedeemSuccess, redeem));
                }
            })
            .Catch(exception =>
            {
                JoyMonsterAPI.DebugLogError("Error redeeming offer: " + exception.Message);
                currentRedeemResponse.Resolve(new Redemption(Redemption.RedemptionType.RedeemError, null));
            });
        }

        private IEnumerator Wait(float seconds, Action onComplete)
        {
            yield return new WaitForSecondsRealtime(seconds);
            if (onComplete != null)
            {
                onComplete();
            }
        }
    }
}
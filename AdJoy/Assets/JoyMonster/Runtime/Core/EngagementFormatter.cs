﻿using JoyMonster.API.Model;
using UnityEngine;

namespace JoyMonster.Core
{
    internal static class EngagementFormatter
    {
        public static string DisplayDistance(EngagementResult engagement)
        {
            if (engagement == null || engagement.distance <= 0.099f) return "";

            return Values.Text.DISTANCE.Replace(Values.Text.DISTANCE_REPLACE, engagement.distance.ToString("N1"));
        }

        public static string DisplayExpiry(EngagementResult engagement)
        {
            if (engagement == null || engagement.lifespan <= 0f) return "";

            return Values.Text.OFFER_EXPIRY.Replace(Values.Text.OFFER_EXPIRY_REPLACE, engagement.lifespan.ToString()) + (engagement.lifespan == 1 ? Values.Text.ENGAGEMENT_EXPIRY_SUFFIX_SINGLE : Values.Text.ENGAGEMENT_EXPIRY_SUFFIX_MULTIPLE);
        }

        public static bool HasCooldown()
        {
            return JoyMonsterManager.engagementHandler.cooldown > 59f && !JoyMonsterManager.engagementHandler.IsAtLeastOneEngagementAvailable();
        }

        public static string DisplayCooldown()
        {
            int minutes = Mathf.FloorToInt(JoyMonsterManager.engagementHandler.cooldown / 60f);
            int hours = minutes / 60;

            if (hours > 0)
            {
                return Values.Text.WALLET_COOLDOWN_H.Replace(Values.Text.WALLET_COOLDOWN_REPLACE, hours.ToString());
            }
            else
            {
                return Values.Text.WALLET_COOLDOWN_M.Replace(Values.Text.WALLET_COOLDOWN_REPLACE, minutes.ToString());
            }
        }
    }
}
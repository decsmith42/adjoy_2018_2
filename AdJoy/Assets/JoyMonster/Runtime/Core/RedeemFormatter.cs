﻿using JoyMonster.API.Model;

namespace JoyMonster.Core
{
    internal static class RedeemFormatter
    {
        public static bool IsUrlRedeem(WalletRedeem redeem)
        {
            return redeem != null && !string.IsNullOrEmpty(redeem.url) && redeem.url.ToLower() != "null";
        }
    }
}
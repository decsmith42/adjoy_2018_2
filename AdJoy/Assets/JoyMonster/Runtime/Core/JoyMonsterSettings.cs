﻿using UnityEngine;

namespace JoyMonster.Core
{
    [CreateAssetMenu(fileName = "JoyMonster Settings", menuName = "JoyMonster/JoyMonster Settings")]
    public sealed class JoyMonsterSettings : ScriptableObject
    {
        [SerializeField]
        private string m_ApiKey;
        public string apiKey
        {
            get { return m_ApiKey; }
            set { m_ApiKey = value; }
        }

        [SerializeField]
        private bool m_PauseTimeWhenUiIsOpen = false;
        public bool pauseTimeWhenUiIsOpen
        {
            get { return m_PauseTimeWhenUiIsOpen; }
            set { m_PauseTimeWhenUiIsOpen = value; }
        }

        [SerializeField]
        private Vector3 m_BoxPosition = Vector3.zero;
        public Vector3 boxPosition
        {
            get { return m_BoxPosition; }
            set { m_BoxPosition = value; }
        }

        [SerializeField]
        private Vector3 m_BoxScale = Vector3.one;
        public Vector3 boxScale
        {
            get { return m_BoxScale; }
            set { m_BoxScale = value; }
        }

        [SerializeField]
        private bool m_BoxIgnoreParentRotation = false;
        public bool boxIgnoreParentRotation
        {
            get { return m_BoxIgnoreParentRotation; }
            set { m_BoxIgnoreParentRotation = value; }
        }

        [SerializeField]
        private bool m_EditorLogging = false;
        public bool editorLogging
        {
            get { return m_EditorLogging; }
            set { m_EditorLogging = value; }
        }
    }
}
﻿using JoyMonster.API.Model;
using UnityEngine;

namespace JoyMonster.API.Tests
{
    public class JoyMonsterEngagementAPITest : MonoBehaviour
    {
        private string m_OfferUuidTextField = "offerUuid";

        void OnGUI()
        {
            if (GUI.Button(new Rect(125, 10, 150, 40), "Request Engagement"))
            {
                JoyMonsterAPI.RequestEngagement(35.241053f, -120.64248f)
                    .Then((engagementResultList) =>
                    {
                        if (engagementResultList.engagements == null || engagementResultList.engagements.Length == 0)
                        {
                            Debug.Log("No engagement received (cooldown: " + engagementResultList.cooldownTimer + ")");
                        }
                        else
                        {
                            foreach (EngagementResult engagementResult in engagementResultList.engagements)
                            {
                                Debug.Log(engagementResult);
                            }
                        }
                    })
                    .Catch((exception) => { Debug.LogError(exception); });
            }



            m_OfferUuidTextField = GUI.TextField(new Rect(125, 80, 150, 25), m_OfferUuidTextField);

            if (GUI.Button(new Rect(125, 110, 150, 40), "View Engagement"))
            {
                JoyMonsterAPI.ViewEngagement(m_OfferUuidTextField, 35.241053f, -120.64248f)
                    .Then((success) => { Debug.Log("success: " + success); })
                    .Catch((exception) => { Debug.LogError(exception); });
            }
        }
    }
}
﻿using JoyMonster.API.Model;
using UnityEngine;

namespace JoyMonster.API.Tests
{
    public class JoyMonsterWalletAPITest : MonoBehaviour
    {
        private string m_OfferUuidTextField1 = "offerUuid";
        private string m_WalletUuidTextField2 = "walletUuid";
        private string m_WalletUuidTextField3 = "walletUuid";

        void OnGUI()
        {
            m_OfferUuidTextField1 = GUI.TextField(new Rect(285, 12, 150, 25), m_OfferUuidTextField1);

            if (GUI.Button(new Rect(285, 40, 150, 40), "Add to wallet"))
            {
                JoyMonsterAPI.AddOfferToWallet(m_OfferUuidTextField1)
                    .Then((success) => { Debug.Log("success: " + success); })
                    .Catch((exception) => { Debug.LogError(exception); });
            }



            if (GUI.Button(new Rect(285, 110, 150, 40), "Get wallet list"))
            {
                JoyMonsterAPI.GetWalletList(0f, 0f)
                    .Then((walletList) =>
                    {
                        if (walletList.offers == null || walletList.offers.Length == 0)
                        {
                            Debug.Log("No offer inside the wallet");
                        }
                        else
                        {
                            foreach (Offer offer in walletList.offers)
                            {
                                Debug.Log(offer);
                            }
                        }
                    })
                    .Catch((exception) => { Debug.LogError(exception); });
            }



            if (GUI.Button(new Rect(285, 180, 150, 40), "Get offer count"))
            {
                JoyMonsterAPI.GetOfferCount()
                    .Then((offerCount) => { Debug.Log("Offer count: " + offerCount); })
                    .Catch((exception) => { Debug.LogError(exception); });
            }



            m_WalletUuidTextField2 = GUI.TextField(new Rect(285, 252, 150, 25), m_WalletUuidTextField2);

            if (GUI.Button(new Rect(285, 280, 150, 40), "Delete offer from wallet"))
            {
                JoyMonsterAPI.DeleteOfferFromWallet(m_WalletUuidTextField2)
                    .Then((success) => { Debug.Log("success: " + success); })
                    .Catch((exception) => { Debug.LogError(exception); });
            }



            m_WalletUuidTextField3 = GUI.TextField(new Rect(285, 357, 150, 25), m_WalletUuidTextField3);

            if (GUI.Button(new Rect(285, 390, 150, 40), "Redeem offer"))
            {
                JoyMonsterAPI.RedeemOffer(m_WalletUuidTextField3, false, 0f, 0f)
                    .Then((walletRedeem) => { Debug.Log(walletRedeem); })
                    .Catch((exception) => { Debug.LogError(exception); });
            }
        }
    }
}
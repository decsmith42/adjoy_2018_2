﻿using JoyMonster.API.Model;
using UnityEngine;

namespace JoyMonster.API.Tests
{
    public class JoyMonsterFaqAPITest : MonoBehaviour
    {
        void OnGUI()
        {
            if (GUI.Button(new Rect(10, 10, 100, 40), "Get Faq List"))
            {
                JoyMonsterAPI.GetFaqList()
                    .Then((faqList) =>
                    {
                        if (faqList.faqs.Length == 0)
                        {
                            Debug.Log("No faqs available");
                        }
                        else
                        {
                            foreach (Faq faq in faqList.faqs)
                            {
                                Debug.Log(faq);
                            }
                        }
                    })
                    .Catch((exception) => { Debug.LogError(exception); });
            }
        }
    }
}
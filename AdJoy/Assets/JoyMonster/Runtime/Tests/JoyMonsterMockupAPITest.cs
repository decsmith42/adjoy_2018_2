﻿using UnityEngine;

namespace JoyMonster.API.Tests
{
    public class JoyMonsterMockupAPITest : MonoBehaviour
    {
        [SerializeField] private JoyMonsterAPI.PromiseResultType m_PromiseResultType = JoyMonsterAPI.PromiseResultType.MOCKUP_ALWAYS_RESOLVE;
        [SerializeField] private JoyMonsterAPI.ResultType m_ResultType = JoyMonsterAPI.ResultType.WITH_DATA;

        void Awake()
        {
            JoyMonsterAPI.promiseResultType = m_PromiseResultType;
            JoyMonsterAPI.resultType = m_ResultType;
        }
    }
}
﻿using UnityEngine;
using UnityEngine.EventSystems;

namespace JoyMonster.Ui
{
    public class ExpiredPopupBlocker : MonoBehaviour, IPointerClickHandler
    {
        [SerializeField] private ExpiredPopup m_Popup = null;

        public void OnPointerClick(PointerEventData eventData)
        {
            m_Popup.OnOkPressed();
        }
    }
}
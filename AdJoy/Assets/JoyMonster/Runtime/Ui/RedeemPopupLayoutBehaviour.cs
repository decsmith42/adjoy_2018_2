﻿using MaterialUI_JoyMonster;
using TMPro;
using UnityEngine;

namespace JoyMonster.Ui
{
    public class RedeemPopupLayoutBehaviour : UiLayoutBehaviour
    {
        public Vector2 panelBasePositionOffset
        {
            get { return new Vector2(0f, 0f); }
        }

        private struct LayoutValues
        {
            public float panelHeight;
            public float labelTitlePositionY;
            public float labelTitleHeight;
            public float labelDescriptionPositionY;
            public float labelDescriptionHeight;
        }

        [SerializeField] private RectTransform m_PanelTransform = null;
        [SerializeField] private TextMeshProUGUI m_LabelTitle = null;
        [SerializeField] private TextMeshProUGUI m_LabelDescription = null;
        [SerializeField] private RectTransform m_SwipeButtonRedeem = null;
        [SerializeField] private RectTransform m_ProgressBar = null;
        [SerializeField] private RectTransform m_BarTransition = null;

        [SerializeField] private TextMeshProUGUI m_LabelButtonSwipe = null;

        private float m_SwipeButtonHeight = 56f;
        private float m_ProgressBarHeight = 24f;

        private int m_TweenIdPanelSize;
        private int m_TweenIdLabelTitleSize;
        private int m_TweenIdLabelTitlePosition;
        private int m_TweenIdLabelDescriptionSize;
        private int m_TweenIdLabelDescriptionPosition;

        public bool progressBarEnabled { get; set; }

        protected override void OnSetLayout()
        {
            EndTweens();

            m_LabelButtonSwipe.alignment = TextAlignmentOptions.Center;

            float padding = UiLayoutValueHandler.common.currentValue.padding;
            float width = m_PanelTransform.rect.width - padding * 2f;

            m_BarTransition.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Left, padding, width);
            m_ProgressBar.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Left, padding, width);
            m_SwipeButtonRedeem.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Left, padding, width);
            m_LabelDescription.rectTransform.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Left, padding, width);
            m_LabelTitle.rectTransform.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Left, padding, width);

            LayoutValues layoutValue = CalculateLayoutValues();
            m_LabelTitle.rectTransform.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Bottom, layoutValue.labelTitlePositionY, layoutValue.labelTitleHeight);
            m_LabelDescription.rectTransform.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Bottom, layoutValue.labelDescriptionPositionY, layoutValue.labelDescriptionHeight);
            m_PanelTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, layoutValue.panelHeight);

            m_SwipeButtonRedeem.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Bottom, padding, m_SwipeButtonHeight);
            m_ProgressBar.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Bottom, padding, m_ProgressBarHeight);

            //m_ProgressBar.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Bottom, padding, progressBarEnabled ? m_ProgressBarHeight : m_SwipeButtonHeight);
        }

        public void AnimatedAdjustLayout(float duration)
        {
            EndTweens();

            LayoutValues layoutValue = CalculateLayoutValues();

            m_TweenIdLabelTitleSize = TweenManager.TweenFloat(value => m_LabelTitle.rectTransform.SetSizeDeltaY(value),
                m_LabelTitle.rectTransform.sizeDelta.y, layoutValue.labelTitleHeight,
                duration * 0.25f, tweenType: Tween.TweenType.EaseInOutSept);

            m_TweenIdLabelTitlePosition = TweenManager.TweenFloat(value => m_LabelTitle.rectTransform.SetAnchoredPositionY(value),
                m_LabelTitle.rectTransform.anchoredPosition.y, layoutValue.labelTitlePositionY,
                duration * 0.875f, duration * 0.125f, tweenType: Tween.TweenType.SoftEaseOutQuint);

            m_TweenIdLabelDescriptionSize = TweenManager.TweenFloat(value => m_LabelTitle.rectTransform.SetSizeDeltaY(value),
                m_LabelTitle.rectTransform.sizeDelta.y, layoutValue.labelTitleHeight,
                duration * 0.25f, tweenType: Tween.TweenType.EaseInOutSept);

            m_TweenIdLabelDescriptionPosition = TweenManager.TweenFloat(value => m_LabelTitle.rectTransform.SetAnchoredPositionY(value),
                m_LabelTitle.rectTransform.anchoredPosition.y, layoutValue.labelTitlePositionY,
                duration * 0.875f, duration * 0.125f, tweenType: Tween.TweenType.SoftEaseOutQuint);

            m_TweenIdPanelSize = TweenManager.TweenFloat(value => m_PanelTransform.SetSizeDeltaY(value),
                m_PanelTransform.rect.height, layoutValue.panelHeight,
                duration * 0.875f, duration * 0.125f, tweenType: Tween.TweenType.SoftEaseOutQuint);

        }

        private LayoutValues CalculateLayoutValues()
        {
            float padding = UiLayoutValueHandler.common.currentValue.padding;

            LayoutValues layoutValues = new LayoutValues();

            layoutValues.panelHeight = padding + (progressBarEnabled ? m_ProgressBarHeight : m_SwipeButtonHeight) + padding;

            layoutValues.labelDescriptionPositionY = layoutValues.panelHeight;
            layoutValues.labelDescriptionHeight = m_LabelDescription.preferredHeight;
            layoutValues.panelHeight += layoutValues.labelDescriptionHeight + padding;

            layoutValues.labelTitlePositionY = layoutValues.panelHeight;
            layoutValues.labelTitleHeight = m_LabelTitle.preferredHeight;
            layoutValues.panelHeight += layoutValues.labelTitleHeight + padding * 3f;

            return layoutValues;
        }

        private void EndTweens()
        {
            TweenManager.EndTween(m_TweenIdPanelSize);
            TweenManager.EndTween(m_TweenIdLabelTitleSize);
            TweenManager.EndTween(m_TweenIdLabelTitlePosition);
            TweenManager.EndTween(m_TweenIdLabelDescriptionSize);
            TweenManager.EndTween(m_TweenIdLabelDescriptionPosition);
        }
    }
}
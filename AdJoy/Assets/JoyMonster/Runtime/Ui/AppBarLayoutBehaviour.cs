﻿using MaterialUI_JoyMonster;
using UnityEngine;

namespace JoyMonster.Ui
{
    [ExecuteInEditMode]
    public class AppBarLayoutBehaviour : UiLayoutBehaviour
    {
        [SerializeField] private RectTransform m_LabelTransform = null;
        [SerializeField] private RectTransform m_LeftIconTransform = null;
        [SerializeField] private RectTransform m_RightIconTransform = null;
        [SerializeField] private RectTransform m_SecondIconTransform = null;
        [SerializeField] private RectTransform m_SearchBarTransform = null;

        protected override void OnSetLayout()
        {
            AppBarWidthValue appBarWidthValue = UiLayoutValueHandler.appBarWidth.currentValue;
            AppBarHeightValue appBarHeightValue = UiLayoutValueHandler.appBarHeight.currentValue;

            rectTransform.SetSizeDeltaY(appBarHeightValue.appBarHeight);
            m_LabelTransform.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Bottom, appBarHeightValue.labelBottomPadding,
                m_LabelTransform.rect.height);

            float rightPadding = appBarWidthValue.rightIconRightPadding;

            m_LeftIconTransform.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Left, appBarWidthValue.leftIconLeftPadding, m_LeftIconTransform.rect.width);

            m_RightIconTransform.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Right, rightPadding, m_RightIconTransform.rect.width);
            rightPadding += m_RightIconTransform.rect.width + appBarWidthValue.spacing;

            m_SearchBarTransform.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Left, appBarWidthValue.labelLeftPadding,
                rectTransform.rect.width - appBarWidthValue.labelLeftPadding - rightPadding);

            m_SecondIconTransform.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Right, rightPadding, m_SecondIconTransform.rect.width);
            rightPadding += m_SecondIconTransform.rect.width + appBarWidthValue.spacing;

            if (appBarHeightValue.appBarHeight > 90f)
            {
                m_LabelTransform.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Left, appBarWidthValue.leftIconLeftPadding,
                    rectTransform.rect.width - appBarWidthValue.leftIconLeftPadding - rightPadding);
            }
            else
            {
                m_LabelTransform.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Left, appBarWidthValue.labelLeftPadding,
                    rectTransform.rect.width - appBarWidthValue.labelLeftPadding - rightPadding);
            }
        }
    }
}
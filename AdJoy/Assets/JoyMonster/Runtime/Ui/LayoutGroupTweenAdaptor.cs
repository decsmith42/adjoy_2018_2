﻿using MaterialUI_JoyMonster;
using System.Collections.Generic;
using UnityEngine.EventSystems;

namespace JoyMonster.Ui
{
    public class LayoutGroupTweenAdaptor : UIBehaviour
    {
        private HorizontalOrVerticalLayoutGroup m_LayoutGroup;

        private HorizontalOrVerticalLayoutGroup layoutGroup
        {
            get
            {
                if (m_LayoutGroup == null)
                {
                    m_LayoutGroup = GetComponent<HorizontalOrVerticalLayoutGroup>();
                }

                return m_LayoutGroup;
            }
        }

        private List<int> m_TweenIds = new List<int>();

        public int Tween(int tweenId)
        {
            layoutGroup.setPositions = false;
            m_TweenIds.Add(tweenId);
            return tweenId;
        }

        public void EndTween(int tweenId)
        {
            TweenManager.EndTween(tweenId);
            m_TweenIds.Remove(tweenId);

            OnTweenEnd();
        }

        public void EndAllTweens()
        {
            foreach (int tweenId in m_TweenIds)
            {
                TweenManager.EndTween(tweenId);
            }

            m_TweenIds.Clear();

            OnTweenEnd();
        }

        private void OnTweenEnd()
        {
            if (m_TweenIds.Count == 0)
            {
                layoutGroup.setPositions = true;
            }
        }
    }
}
﻿using UnityEngine;
using UnityEngine.EventSystems;

namespace JoyMonster.Ui
{
    public class AddPopupBlocker : MonoBehaviour, IPointerClickHandler
    {
        [SerializeField] private AddPopup m_AddPopup = null;

        public void OnPointerClick(PointerEventData eventData)
        {
            m_AddPopup.OnCancelPressed();
        }
    }
}
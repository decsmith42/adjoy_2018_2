﻿using MaterialUI_JoyMonster;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace JoyMonster.Ui
{
    public class IconLayoutBehaviour : UIBehaviour, ILayoutGroup
    {
        private VectorImage m_Icon;

        private VectorImage icon
        {
            get
            {
                if (m_Icon == null)
                {
                    m_Icon = GetComponent<VectorImage>();
                }

                return m_Icon;
            }
        }

        [SerializeField] private LabelLayoutBehaviour.LabelType m_IconType = LabelLayoutBehaviour.LabelType.Paragraph;

        [SerializeField] private float m_Scale = 1f;

        [SerializeField] private float m_Delta = 0f;

        public void SetLayoutHorizontal()
        {
            LayoutValue layoutValues = UiLayoutValueHandler.common.currentValue;

            float size = m_Scale;

            switch (m_IconType)
            {
                case LabelLayoutBehaviour.LabelType.Header:
                    size *= layoutValues.textHeaderFontSize;
                    break;
                case LabelLayoutBehaviour.LabelType.SubHeader:
                    size *= layoutValues.textSubHeaderFontSize;
                    break;
                case LabelLayoutBehaviour.LabelType.Button:
                    size *= layoutValues.textButtonFontSize;
                    break;
                case LabelLayoutBehaviour.LabelType.Paragraph:
                    size *= layoutValues.textParagraphFontSize;
                    break;
            }

            icon.size = size + m_Delta;
        }

        public void SetLayoutVertical()
        {
        }
    }
}
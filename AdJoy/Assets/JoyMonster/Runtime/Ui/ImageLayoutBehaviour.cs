﻿using UnityEngine;
using UnityEngine.UI;

namespace JoyMonster.Ui
{
    public class ImageLayoutBehaviour : GraphicLayoutBehaviour<Image>
    {
        #region old
        //private RectTransform m_RectTransform;

        //private RectTransform rectTransform
        //{
        //    get
        //    {
        //        if (m_RectTransform == null)
        //        {
        //            m_RectTransform = GetComponent<RectTransform>();
        //        }

        //        return m_RectTransform;
        //    }
        //}

        //private RectTransform m_ParentRectTransform;

        //private RectTransform parentRectTransform
        //{
        //    get
        //    {
        //        if (m_ParentRectTransform == null)
        //        {
        //            m_ParentRectTransform = rectTransform.parent.GetComponent<RectTransform>();
        //        }

        //        return m_ParentRectTransform;
        //    }
        //}

        //private RawImage m_Image;
        //private RawImage image
        //{
        //    get
        //    {
        //        if (m_Image == null)
        //        {
        //            m_Image = GetComponent<RawImage>();
        //        }

        //        return m_Image;
        //    }
        //}

        //[SerializeField] private Vector2 m_MinPadding;
        //[SerializeField] private Vector2 m_MaxSize;

        //public void SetTexture(Texture2D texture)
        //{
        //    if (image == null) return;

        //    image.texture = texture;

        //    SetLayoutHorizontal();
        //}

        //public void SetLayoutHorizontal()
        //{
        //    if (image == null || parentRectTransform == null) return;

        //    Vector2 bounds = parentRectTransform.rect.size - m_MinPadding * 2f;

        //    if (m_MaxSize.x > 0f)
        //    {
        //        bounds.x = Mathf.Min(bounds.x, m_MaxSize.x);
        //    }

        //    if (m_MaxSize.y > 0f)
        //    {
        //        bounds.y = Mathf.Min(bounds.y, m_MaxSize.y);
        //    }

        //    Vector2 imageFittedSize;

        //    if (image.texture == null)
        //    {
        //        imageFittedSize = UiUtils.FitToBounds(bounds, bounds);
        //    }
        //    else
        //    {
        //        imageFittedSize = UiUtils.FitToBounds(new Vector2(image.texture.width, image.texture.height), bounds);
        //    }

        //    rectTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, imageFittedSize.x);
        //    rectTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, imageFittedSize.y);
        //}

        //public void SetLayoutVertical() { }
        #endregion

        protected override Texture texture
        {
            get { return m_Graphic.sprite.texture; }
        }

        public void SetSprite(Sprite sprite)
        {
            if (m_Graphic == null) return;

            m_Graphic.sprite = sprite;

            SetLayoutHorizontal();
        }
    }
}
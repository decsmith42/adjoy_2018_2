﻿using MaterialUI_JoyMonster;
using UnityEngine;

namespace JoyMonster.Ui
{
    public class DynamicTransformRotation : MonoBehaviour
    {
        protected RectTransform m_RectTransform;
        public RectTransform rectTransform
        {
            get
            {
                if (m_RectTransform == null)
                {
                    m_RectTransform = GetComponent<RectTransform>();
                }

                return m_RectTransform;
            }
        }

        protected int m_TweenId;

        public void SetState(float rotation)
        {
            TweenManager.EndTween(m_TweenId);
            rectTransform.localEulerAngles = new Vector3(0f, 0f, rotation);
        }

        public void TransitionState(float rotation, float duration, float delay,
            Tween.TweenType tweenType = Tween.TweenType.EaseInOutQuint)
        {
            TransitionState(rectTransform.localEulerAngles.z, rotation, duration, delay, tweenType);
        }

        public void TransitionState(float from, float to, float duration, float delay,
            Tween.TweenType tweenType = Tween.TweenType.EaseInOutQuint)
        {
            TweenManager.EndTween(m_TweenId);

            Vector3 target = new Vector3(0f, 0f, to);

            m_TweenId = TweenManager.TweenVector3(
                value => rectTransform.localEulerAngles = value, new Vector3(0f, 0f, from), target, duration,
                delay,
                tweenType: tweenType);
        }
    }
}
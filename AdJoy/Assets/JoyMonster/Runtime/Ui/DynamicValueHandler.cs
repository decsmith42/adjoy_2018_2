﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace JoyMonster.Ui
{
    public abstract class DynamicValueHandler : MonoBehaviour
    {
    }

    public abstract class DynamicValueGroup : ScriptableObject
    {
        public abstract int GetValueIndex(float input);
    }

    public abstract class DynamicValueGroup<T> : DynamicValueGroup where T : DynamicValue, new()
    {
        [SerializeField] protected T[] m_Values;

        protected T m_CurrentValue;

        public T currentValue
        {
            get
            {
                if (m_CurrentValue == null)
                {
                    m_CurrentValue = new T();
                }

                return m_CurrentValue;
            }
        }

        public void UpdateValue(float input)
        {
            if (m_Values == null || m_Values.Length == 0) return;

            m_CurrentValue = m_Values[GetValueIndex(input)];
        }

        public override int GetValueIndex(float input)
        {
            int index = 0;

            for (int i = 0; i < m_Values.Length - 1; i++)
            {
                if (input < m_Values[i + 1].threshold)
                {
                    break;
                }

                index++;
            }

            return index;
        }

        protected virtual void OnEnable()
        {
            bool valuesNeedSorting = false;
            float currentThreshold = -1f;

            if (m_Values == null) return;

            for (int i = 0; i < m_Values.Length; i++)
            {
                float valueThreshold = m_Values[i].threshold;
                if (currentThreshold >= valueThreshold)
                {
                    valuesNeedSorting = true;
                    break;
                }

                currentThreshold = valueThreshold;
            }

            if (valuesNeedSorting)
            {
                List<T> valuesList = m_Values.ToList();
                valuesList.Sort((v1, v2) => v1.threshold == v2.threshold ? 0 : (v1.threshold > v2.threshold ? 1 : -1));
                m_Values = valuesList.ToArray();
                valuesList.Clear();
            }
        }
    }

    [Serializable]
    public abstract class DynamicValue
    {
        public float threshold;
    }
}
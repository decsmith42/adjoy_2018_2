﻿using JoyMonster.Core;
using MaterialUI_JoyMonster;
using UnityEngine;
using UnityEngine.EventSystems;

namespace JoyMonster.Ui
{
    public class DismissPopup : UIBehaviour
    {
        [SerializeField] private RectTransform m_Panel = null;
        [SerializeField] private DynamicCanvasGroup m_Background = null;
        [SerializeField] private DismissPopupLayoutBehaviour m_LayoutBehaviour = null;
        [SerializeField] private DynamicGraphic m_LabelTitleGraphic = null;
        [SerializeField] private Canvas m_Canvas = null;

        private int m_TweenId;

        public bool isShowing { get; private set; }

        public void Show()
        {
            TweenManager.EndTween(m_TweenId);

            isShowing = true;

            m_LabelTitleGraphic.SetState(true);

            m_Canvas.enabled = true;
            m_Panel.SetAnchoredPositionY(-m_Panel.rect.height * 1.25f);

            m_TweenId = TweenManager.TweenFloat(value => m_Panel.SetAnchoredPositionY(value),
                m_Panel.anchoredPosition.y, m_LayoutBehaviour.panelBasePositionOffset.y,
                Values.Duration.MEDIUM, Values.Duration.SHORTER);

            m_Background.TransitionState(true, Values.Duration.SHORT, Values.Duration.SHORTER);
        }

        private void Hide()
        {
            TweenManager.EndTween(m_TweenId);

            isShowing = false;

            m_Background.TransitionState(false, Values.Duration.SHORT, Values.Duration.SHORTER);

            m_TweenId = TweenManager.TweenFloat(value => m_Panel.SetAnchoredPositionY(value),
                m_Panel.anchoredPosition.y, -m_Panel.rect.height * 1.25f,
                Values.Duration.SHORT, Values.Duration.SHORTER,
                () => { m_Canvas.enabled = false; },
                tweenType: Tween.TweenType.EaseInQuint);
        }

        public void OnNoPressed()
        {
            if (JoyMonsterManager.walletHandler.addToWalletInProgress) return;
            Hide();
        }

        public void OnYesPressed()
        {
            if (JoyMonsterManager.walletHandler.addToWalletInProgress) return;
            JoyMonsterManager.uiHandler.OnBackToGameButtonPressed();
            Hide();
        }
    }
}
﻿using System;
using UnityEngine;

namespace JoyMonster.Ui
{
    [CreateAssetMenu(fileName = "ScreenSizeSmallest", menuName = "UI/DynamicValues/ScreenSizeSmallest")]
    public class LayoutValueGroup : DynamicValueGroup<LayoutValue> { }

    [Serializable]
    public class LayoutValue : DynamicValue
    {
        public float padding;
        public float spacing;
        public float textHeaderFontSize;
        public float textSubHeaderFontSize;
        public float textButtonFontSize;
        public float textParagraphFontSize;
    }
}
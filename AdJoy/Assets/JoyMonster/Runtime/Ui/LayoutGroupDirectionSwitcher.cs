﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using HorizontalOrVerticalLayoutGroup = MaterialUI_JoyMonster.HorizontalOrVerticalLayoutGroup;

namespace JoyMonster.Ui
{
    public class LayoutGroupDirectionSwitcher : UIBehaviour, ILayoutElement
    {
        private HorizontalOrVerticalLayoutGroup m_LayoutGroup;

        public HorizontalOrVerticalLayoutGroup layoutGroup
        {
            get
            {
                if (m_LayoutGroup == null)
                {
                    m_LayoutGroup = GetComponent<HorizontalOrVerticalLayoutGroup>();
                }

                return m_LayoutGroup;
            }
        }

        [SerializeField] private RectTransform[] items = null;

        public void CalculateLayoutInputHorizontal()
        {
        }

        public void CalculateLayoutInputVertical()
        {
            if (!isActiveAndEnabled) return;

            float width = layoutGroup.padding.left + layoutGroup.padding.right;

            for (int i = 0; i < items.Length; i++)
            {
                width += LayoutUtility.GetPreferredWidth(items[i]);

                if (i < items.Length - 1)
                {
                    width += layoutGroup.spacing;
                }
            }

            if (width > ((RectTransform)layoutGroup.transform).rect.width)
            {
                if (layoutGroup.direction == HorizontalOrVerticalLayoutGroup.Direction.Horizontal)
                {
                    layoutGroup.direction = HorizontalOrVerticalLayoutGroup.Direction.Vertical;
                }
            }
            else
            {
                if (layoutGroup.direction == HorizontalOrVerticalLayoutGroup.Direction.Vertical)
                {
                    layoutGroup.direction = HorizontalOrVerticalLayoutGroup.Direction.Horizontal;
                }
            }
        }

        public float minWidth
        {
            get { return layoutGroup.minWidth; }
        }

        public float preferredWidth
        {
            get { return layoutGroup.preferredWidth; }
        }

        public float flexibleWidth
        {
            get { return layoutGroup.flexibleWidth; }
        }

        public float minHeight
        {
            get { return layoutGroup.minHeight; }
        }

        public float preferredHeight
        {
            get { return layoutGroup.preferredHeight; }
        }

        public float flexibleHeight
        {
            get { return layoutGroup.flexibleHeight; }
        }

        public int layoutPriority
        {
            get { return layoutGroup.layoutPriority; }
        }
    }
}
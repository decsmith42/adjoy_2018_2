﻿using System;
using UnityEngine;

namespace JoyMonster.Ui
{
    [CreateAssetMenu(fileName = "AppBarWidth", menuName = "UI/DynamicValues/AppBarWidth")]
    public class AppBarWidthValueGroup : DynamicValueGroup<AppBarWidthValue>
    {
    }

    [Serializable]
    public class AppBarWidthValue : DynamicValue
    {
        public float leftIconLeftPadding;
        public float rightIconRightPadding;
        public float labelLeftPadding;
        public float spacing;
    }
}
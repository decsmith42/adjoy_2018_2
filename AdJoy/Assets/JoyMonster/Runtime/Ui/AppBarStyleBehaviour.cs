﻿using JoyMonster;
using JoyMonster.Core;
using JoyMonster.Ui;
using MaterialUI_JoyMonster;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;

public class AppBarStyleBehaviour : UIBehaviour
{
    [SerializeField]
    private Canvas m_Canvas = null;

    [SerializeField]
    private DynamicTransformPosition m_Position = null;

    [SerializeField]
    private DynamicGraphic m_LeftIconBox = null;

    [SerializeField]
    private DynamicCanvasGroup m_LeftButtonArrow = null;

    [SerializeField]
    private DynamicTransformScale m_LeftIconBoxScale = null;

    [SerializeField]
    private DynamicTransformScale m_LeftIconArrowScale = null;

    [SerializeField]
    private DynamicTransformRotation m_LeftIconBoxRotation = null;

    [SerializeField]
    private DynamicTransformRotation m_LeftIconArrowRotation = null;

    [SerializeField]
    private DynamicCanvasGroup m_ButtonHelpCanvasGroup = null;

    [SerializeField]
    private DynamicCanvasGroup m_ButtonSearchCanvasGroup = null;

    [SerializeField]
    private DynamicTransformScale m_ButtonHelpScale = null;

    [SerializeField]
    private DynamicTransformScale m_ButtonSearchScale = null;

    [SerializeField]
    private TextMeshProUGUI m_Label = null;

    [SerializeField]
    private DynamicGraphic m_LabelGraphic = null;

    [SerializeField]
    private SearchBar m_SearchBar = null;

    public SearchBar searchBar
    {
        get { return m_SearchBar; }
    }

    private bool m_IsVisible;
    private int m_TimerId;

    public void TransitionVisible(bool visible)
    {
        if (m_IsVisible == visible) return;
        m_IsVisible = visible;

        if (visible)
        {
            m_Canvas.enabled = true;
        }

        m_Position.TransitionPosition(
            m_IsVisible ? DynamicTransformPosition.Direction.Middle : DynamicTransformPosition.Direction.Up,
            Values.Duration.SHORT, Values.Duration.SHORTER,
            m_IsVisible ? Tween.TweenType.EaseOutQuint : Tween.TweenType.EaseInQuint);

        TweenManager.EndTween(m_TimerId);

        if (!visible)
        {
            m_TimerId = TweenManager.TimedCallback(Values.Duration.SHORT + Values.Duration.SHORTER,
                () => { m_Canvas.enabled = false; });
        }
    }

    public void SetVisible(bool visible)
    {
        m_IsVisible = visible;
        m_Canvas.enabled = visible;
        m_Position.SetPosition(m_IsVisible
            ? DynamicTransformPosition.Direction.Middle
            : DynamicTransformPosition.Direction.Up);
    }

    public void SetLeftButton(bool isArrow, bool animate = true)
    {
        if (!animate)
        {
            m_LeftIconBox.SetState(!isArrow);
            m_LeftIconBoxScale.SetState(!isArrow);
            m_LeftIconBoxRotation.SetState(!isArrow ? 0f : -180f);

            m_LeftButtonArrow.SetState(isArrow);
            m_LeftIconArrowScale.SetState(isArrow);
            m_LeftIconArrowRotation.SetState(isArrow ? -180f : 0f);
            return;
        }

        if (!isArrow)
        {
            m_LeftIconBoxRotation.TransitionState(180f, 0f, Values.Duration.SHORTER, Values.Duration.SHORTER, Tween.TweenType.EaseOutSept);
            m_LeftIconArrowRotation.TransitionState(0f, -180f, Values.Duration.SHORTER, 0f, Tween.TweenType.EaseInSept);

            m_LeftIconBoxScale.TransitionState(true, Values.Duration.SHORTER, Values.Duration.SHORTER, Tween.TweenType.EaseOutSept);
            m_LeftIconArrowScale.TransitionState(false, Values.Duration.SHORTER, 0f, Tween.TweenType.EaseInSept);

            m_LeftIconBox.TransitionState(true, Values.Duration.SHORT, 0f);
            m_LeftButtonArrow.TransitionState(false, Values.Duration.SHORT, 0f);
        }
        else
        {
            m_LeftIconBoxRotation.TransitionState(0f, -180f, Values.Duration.SHORTER, 0f, Tween.TweenType.EaseInSept);
            m_LeftIconArrowRotation.TransitionState(180f, 0f, Values.Duration.SHORTER, Values.Duration.SHORTER, Tween.TweenType.EaseOutSept);

            m_LeftIconBoxScale.TransitionState(false, Values.Duration.SHORTER, 0f, Tween.TweenType.EaseInSept);
            m_LeftIconArrowScale.TransitionState(true, Values.Duration.SHORTER, Values.Duration.SHORTER, Tween.TweenType.EaseOutSept);

            m_LeftIconBox.TransitionState(false, Values.Duration.SHORT, 0f);
            m_LeftButtonArrow.TransitionState(true, Values.Duration.SHORT, 0f);
        }
    }

    public void SetTitle(string text, bool animate = true)
    {
        m_LabelGraphic.TransitionState(false, Values.Duration.SHORTER, Values.Duration.PER_ELEMENT_DELAY);

        TweenManager.TimedCallback(Values.Duration.SHORTER + Values.Duration.PER_ELEMENT_DELAY, () =>
        {
            if (string.IsNullOrEmpty(text)) return;

            m_Label.text = text;
            m_LabelGraphic.TransitionState(true, Values.Duration.SHORTER, 0f);
        });
    }

    public void SetSearchButton(bool isEnabled, bool animate = true)
    {
        if (m_SearchBar.isOpen)
        {
            m_SearchBar.Close();
        }

        if (!animate)
        {
            m_ButtonSearchCanvasGroup.SetState(isEnabled);
            m_ButtonSearchScale.SetState(isEnabled);
            return;
        }

        m_ButtonSearchCanvasGroup.TransitionState(isEnabled, Values.Duration.SHORT,
            Values.Duration.PER_ELEMENT_DELAY * 2f);
        m_ButtonSearchScale.TransitionState(isEnabled, Values.Duration.SHORT, Values.Duration.PER_ELEMENT_DELAY * 2f);
    }

    public void SetHelpButton(bool isEnabled, bool animate = true)
    {
        if (!animate)
        {
            m_ButtonHelpCanvasGroup.SetState(isEnabled);
            m_ButtonHelpScale.SetState(isEnabled);
            return;
        }

        m_ButtonHelpCanvasGroup.TransitionState(isEnabled, Values.Duration.SHORT,
            Values.Duration.PER_ELEMENT_DELAY * 3f);
        m_ButtonHelpScale.TransitionState(isEnabled, Values.Duration.SHORT, Values.Duration.PER_ELEMENT_DELAY * 3f);
    }

    public void OnLayoutChanged()
    {
        m_Position.offset = new Vector4(0f, m_Position.rectTransform.rect.height * 1.05f, 0f, 0f);
        SetVisible(m_IsVisible);
    }

    public void OnBackButtonPressed()
    {
        JoyMonsterManager.uiHandler.OnBackButtonPressed();
    }

    public void OnSearchButtonPressed()
    {
        m_SearchBar.Open();
    }

    public void OnHelpButtonPressed()
    {
        JoyMonsterManager.uiHandler.OnHelpButtonPressed();
    }
}
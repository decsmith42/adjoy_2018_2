﻿using System.Collections.Generic;
using UnityEngine;

namespace JoyMonster.Ui
{
    public class ScreenManager : MonoBehaviour
    {
        private static ScreenManager m_Instance;

        private static ScreenManager instance
        {
            get
            {
                if (m_Instance == null)
                {
                    m_Instance = FindObjectOfType<ScreenManager>();
                }

                return m_Instance;
            }
        }

        [SerializeField] private UiScreen[] m_ScreensToLoad;

        private Dictionary<int, UiScreen> m_Screens = new Dictionary<int, UiScreen>();
        protected UiScreen m_CurrentScreen;
        protected Stack<UiScreen> m_PreviousScreens = new Stack<UiScreen>();

        private void Awake()
        {
            m_Screens.Clear();

            foreach (UiScreen screen in m_ScreensToLoad)
            {
                m_Screens.Add(screen.screenId, screen);
            }

            m_ScreensToLoad = null;

            m_CurrentScreen = null;
        }

        public static void TransitionForward(int screenId)
        {
            instance.TransitionForwardInternal(screenId);
        }

        private void TransitionForwardInternal(int screenId)
        {
            if (m_CurrentScreen != null)
            {
                m_PreviousScreens.Push(m_CurrentScreen);
            }

            m_CurrentScreen = GetScreen(screenId);

            m_CurrentScreen.Show().Then(completed =>
            {
                if (m_PreviousScreens.Count > 0)
                {
                    m_PreviousScreens.Peek().HideImmediate();
                }
            });
        }

        public static void TransitionBack()
        {
            instance.TransitionBackInternal();
        }

        private void TransitionBackInternal()
        {
            if (m_PreviousScreens.Count == 0)
            {
                TransitionExitInternal();
                return;
            }

            m_PreviousScreens.Peek().ShowImmediate();

            m_CurrentScreen.Hide().Then(completed => { m_CurrentScreen = m_PreviousScreens.Pop(); });
        }

        public static void TransitionExit()
        {
            instance.TransitionExitInternal();
        }

        public void TransitionExitInternal()
        {
            m_CurrentScreen.Hide();
            m_CurrentScreen = null;
            m_PreviousScreens.Clear();
        }

        public static UiScreen GetScreen(int screenId)
        {
            return instance.m_Screens[screenId];
        }
    }
}
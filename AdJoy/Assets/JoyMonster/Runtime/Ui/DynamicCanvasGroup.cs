﻿using MaterialUI_JoyMonster;
using UnityEngine;

namespace JoyMonster.Ui
{
    public class DynamicCanvasGroup : MonoBehaviour
    {
        protected CanvasGroup m_CanvasGroup;

        public CanvasGroup canvasGroup
        {
            get
            {
                if (m_CanvasGroup == null)
                {
                    m_CanvasGroup = GetComponent<CanvasGroup>();
                }

                return m_CanvasGroup;
            }
        }

        protected int m_TweenId;

        public void SetState(bool visible)
        {
            TweenManager.EndTween(m_TweenId);
            canvasGroup.alpha = visible ? 1f : 0f;
            canvasGroup.interactable = visible;
            canvasGroup.blocksRaycasts = visible;
        }

        public void DelayedSetState(bool visible, float delay)
        {
            TweenManager.EndTween(m_TweenId);
            m_TweenId = TweenManager.TimedCallback(delay, () => SetState(visible));
        }

        public void TransitionState(bool visible, float duration, float delay)
        {
            TweenManager.EndTween(m_TweenId);

            canvasGroup.interactable = visible;
            canvasGroup.blocksRaycasts = visible;

            float target = visible ? 1f : 0f;

            m_TweenId = TweenManager.TweenFloat(
                value => canvasGroup.alpha = value, canvasGroup.alpha, target, duration, delay,
                tweenType: Tween.TweenType.EaseInOutQuint);
        }

        public void TransitionState(bool from, bool to, float duration, float delay)
        {
            SetState(from);
            TransitionState(to, duration, delay);
        }
    }
}
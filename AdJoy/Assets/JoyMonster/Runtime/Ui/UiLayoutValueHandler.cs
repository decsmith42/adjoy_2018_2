﻿using MaterialUI_JoyMonster;
using UnityEngine;

namespace JoyMonster.Ui
{
    [ExecuteInEditMode]
    public class UiLayoutValueHandler : DynamicValueHandler
    {
        private static UiLayoutValueHandler m_Instance;

        private static UiLayoutValueHandler instance
        {
            get
            {
                if (m_Instance == null)
                {
                    m_Instance = FindObjectOfType<UiLayoutValueHandler>();
                }

                return m_Instance;
            }
        }

        [SerializeField] private AppBarHeightValueGroup m_AppBarHeight = null;

        public static AppBarHeightValueGroup appBarHeight
        {
            get
            {
                if (m_Instance == null || m_Instance.m_AppBarWidth == null)
                {
                    return new AppBarHeightValueGroup();
                }
                return instance.m_AppBarHeight;
            }
        }

        [SerializeField] private AppBarWidthValueGroup m_AppBarWidth = null;

        public static AppBarWidthValueGroup appBarWidth
        {
            get
            {
                if (m_Instance == null || m_Instance.m_AppBarWidth == null)
                {
                    return new AppBarWidthValueGroup();
                }
                return instance.m_AppBarWidth;
            }
        }

        [SerializeField] private LayoutValueGroup m_Common = null;

        public static LayoutValueGroup common
        {
            get
            {
                if (m_Instance == null || m_Instance.m_Common == null)
                {
                    return new LayoutValueGroup();
                }
                return instance.m_Common;
            }
        }

        public void OnEnable()
        {

            UiScaleController.onRefreshedEarly += UpdateScalerDependentGroups;
            UpdateScalerDependentGroups();
        }

        public void OnDisable()
        {
            if (PrefabUtil.IsPrefab(gameObject)) return;

            UiScaleController.onRefreshedEarly -= UpdateScalerDependentGroups;
        }

        public void UpdateScalerDependentGroups()
        {
            if (instance == null) return;

            m_AppBarHeight.UpdateValue(UiScaleController.screenSizeDp.y);
            m_AppBarWidth.UpdateValue(UiScaleController.screenSizeDp.x);
            m_Common.UpdateValue(UiScaleController.screenSizeDp.x < UiScaleController.screenSizeDp.y
                ? (UiScaleController.screenSizeDp.x * 2f + UiScaleController.screenSizeDp.y) / 3f
                : (UiScaleController.screenSizeDp.x + UiScaleController.screenSizeDp.y * 2f) / 3f);

        }
    }
}
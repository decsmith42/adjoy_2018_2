﻿using MaterialUI_JoyMonster;
using TMPro;
using UnityEngine;

namespace JoyMonster.Ui
{
    public class AddPopupLayoutBehaviour : UiLayoutBehaviour
    {
        public Vector2 panelBasePositionOffset
        {
            //get { return new Vector2(0f, Values.Layout.BOTTOM_BAR_HEIGHT); }
            get { return new Vector2(0f, 0f); }
        }

        [SerializeField] private RectTransform m_PanelTransform = null;
        [SerializeField] private TextMeshProUGUI m_LabelTitle = null;
        [SerializeField] private RectTransform m_ButtonAdd = null;
        [SerializeField] private RectTransform m_ButtonCancel = null;
        [SerializeField] private RectTransform m_ProgressBar = null;

        private float m_ButtonHeight = 40f;
        private float m_ProgressBarHeight = 12f;

        private bool m_ProgressBarEnabled;
        public bool progressBarEnabled
        {
            get { return m_ProgressBarEnabled; }
            set { m_ProgressBarEnabled = value; }
        }

        private struct LayoutValues
        {
            public float panelHeight;
            public float labelTitlePositionY;
            public float labelTitleHeight;
        }

        private int m_TweenIdPanelSize;
        private int m_TweenIdLabelTitleSize;
        private int m_TweenIdLabelTitlePosition;

        protected override void OnSetLayout()
        {
            TweenManager.EndTween(m_TweenIdPanelSize);
            TweenManager.EndTween(m_TweenIdLabelTitleSize);
            TweenManager.EndTween(m_TweenIdLabelTitlePosition);

            float padding = UiLayoutValueHandler.common.currentValue.padding * 2f;

            float buttonWidth = (m_PanelTransform.rect.width - padding * 3f) / 2f;
            m_ButtonCancel.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Left, padding, buttonWidth);
            m_ButtonAdd.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Left, padding + buttonWidth + padding, buttonWidth);
            m_LabelTitle.rectTransform.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Left, padding, m_PanelTransform.rect.width - padding * 2f);

            m_ButtonCancel.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Bottom, padding, m_ButtonHeight);
            m_ButtonAdd.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Bottom, padding, m_ButtonHeight);
            m_ProgressBar.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Bottom, padding + m_ButtonHeight + padding, m_ProgressBarHeight);

            LayoutValues layoutValue = CalculateLayoutValues();
            m_LabelTitle.rectTransform.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Bottom, layoutValue.labelTitlePositionY, layoutValue.labelTitleHeight);
            m_PanelTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, layoutValue.panelHeight);
        }

        public void AnimatedAdjustLayout(float duration)
        {
            TweenManager.EndTween(m_TweenIdPanelSize);
            TweenManager.EndTween(m_TweenIdLabelTitleSize);
            TweenManager.EndTween(m_TweenIdLabelTitlePosition);

            LayoutValues layoutValue = CalculateLayoutValues();

            m_TweenIdLabelTitleSize = TweenManager.TweenFloat(value => m_LabelTitle.rectTransform.SetSizeDeltaY(value),
                m_LabelTitle.rectTransform.sizeDelta.y, layoutValue.labelTitleHeight,
                duration * 0.25f, tweenType: Tween.TweenType.EaseInOutSept);

            m_TweenIdLabelTitlePosition = TweenManager.TweenFloat(value => m_LabelTitle.rectTransform.SetAnchoredPositionY(value),
                m_LabelTitle.rectTransform.anchoredPosition.y, layoutValue.labelTitlePositionY,
                duration * 0.875f, duration * 0.125f, tweenType: Tween.TweenType.SoftEaseOutQuint);

            m_TweenIdPanelSize = TweenManager.TweenFloat(value => m_PanelTransform.SetSizeDeltaY(value),
                m_PanelTransform.rect.height, layoutValue.panelHeight,
                duration * 0.875f, duration * 0.125f, tweenType: Tween.TweenType.SoftEaseOutQuint);
        }

        private LayoutValues CalculateLayoutValues()
        {
            float padding = UiLayoutValueHandler.common.currentValue.padding * 2f;

            LayoutValues layoutValues = new LayoutValues();

            layoutValues.panelHeight = padding + m_ButtonHeight + padding;

            if (m_ProgressBarEnabled)
            {
                layoutValues.panelHeight += m_ProgressBarHeight + padding;
            }

            layoutValues.labelTitlePositionY = layoutValues.panelHeight;
            layoutValues.labelTitleHeight = m_LabelTitle.preferredHeight;

            layoutValues.panelHeight += layoutValues.labelTitleHeight + padding;

            return layoutValues;
        }
    }
}
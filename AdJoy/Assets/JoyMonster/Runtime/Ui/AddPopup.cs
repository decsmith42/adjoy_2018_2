﻿using JoyMonster.Core;
using MaterialUI_JoyMonster;
using RSG;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;

namespace JoyMonster.Ui
{
    internal class AddPopup : UIBehaviour
    {
        [SerializeField] private RectTransform m_Panel = null;
        [SerializeField] private DynamicCanvasGroup m_Background = null;
        [SerializeField] private ProgressIndicator m_ProgressBar = null;
        [SerializeField] private CanvasGroup m_ButtonCancel = null;
        [SerializeField] private CanvasGroup m_ButtonAdd = null;
        [SerializeField] private AddPopupLayoutBehaviour m_LayoutBehaviour = null;
        [SerializeField] private TextMeshProUGUI m_LabelTitle = null;
        [SerializeField] private DynamicGraphic m_LabelTitleGraphic = null;
        [SerializeField] private Canvas m_Canvas = null;

        private int m_TweenId;

        private Promise<bool> m_ConfirmationPromise;

        private string m_OfferUuid;

        public Promise<bool> Show(string offerUuid)
        {
            m_OfferUuid = offerUuid;

            TweenManager.EndTween(m_TweenId);

            GetComponent<AddPopupLayoutBehaviour>().RefreshLayout();

            m_LabelTitleGraphic.SetState(true);
            m_LabelTitle.text = Values.Text.ADD_TO_WALLET_PROMPT;
            m_ProgressBar.Hide();

            SetButtonsInteractableState(true);

            m_Canvas.enabled = true;
            m_Panel.SetAnchoredPositionY(-m_Panel.rect.height * 1.25f);

            m_TweenId = TweenManager.TweenFloat(value => m_Panel.SetAnchoredPositionY(value),
                m_Panel.anchoredPosition.y, m_LayoutBehaviour.panelBasePositionOffset.y,
                Values.Duration.MEDIUM, Values.Duration.SHORTER,
                tweenType: Tween.TweenType.EaseOutQuint);

            m_Background.TransitionState(true, Values.Duration.SHORT, Values.Duration.SHORTER);

            if (m_ConfirmationPromise != null && m_ConfirmationPromise.CurState == PromiseState.Pending)
            {
                m_ConfirmationPromise.Resolve(false);
            }
            m_ConfirmationPromise = new Promise<bool>();
            return m_ConfirmationPromise;
        }

        private void Hide(bool isTransition = false)
        {
            TweenManager.EndTween(m_TweenId);

            m_Background.TransitionState(false, Values.Duration.SHORT, Values.Duration.SHORTER);

            m_ConfirmationPromise.ResolveIfPending(false);

            if (isTransition)
            {
                m_TweenId = TweenManager.TweenFloat(value => m_Panel.SetSizeDeltaY(value),
                    m_Panel.sizeDelta.y, UiScaleController.screenSizeDp.y,
                    Values.Duration.MEDIUM, Values.Duration.SHORTER,
                    () => { m_Canvas.enabled = false; },
                    tweenType: Tween.TweenType.EaseInQuint);

                m_LabelTitleGraphic.TransitionState(false, Values.Duration.SHORT, 0f);
            }
            else
            {
                m_TweenId = TweenManager.TweenFloat(value => m_Panel.SetAnchoredPositionY(value),
                    m_Panel.anchoredPosition.y, -m_Panel.rect.height * 1.25f,
                    Values.Duration.SHORT, Values.Duration.SHORTER,
                    () => { m_Canvas.enabled = false; },
                    tweenType: Tween.TweenType.EaseInQuint);
            }
        }

        public void OnCancelPressed()
        {
            if (JoyMonsterManager.walletHandler.addToWalletInProgress) return;
            Hide();
        }

        public void OnAddButtonPressed()
        {
            if (JoyMonsterManager.walletHandler.addToWalletInProgress) return;
            AddReward();
        }

        private void AddReward()
        {
            SetButtonsInteractableState(false);

            m_ProgressBar.Show();
            m_LayoutBehaviour.progressBarEnabled = true;

            m_LabelTitleGraphic.TransitionState(false, Values.Duration.SHORTER, 0f);
            m_LabelTitleGraphic.DelayedTransitionState(true, Values.Duration.SHORTER, Values.Duration.SHORT);

            TweenManager.TimedCallback(Values.Duration.SHORTER, () =>
                {
                    m_LabelTitle.text = Values.Text.ADD_TO_WALLET_ADDING;
                    m_LayoutBehaviour.AnimatedAdjustLayout(Values.Duration.SHORT);
                });

            JoyMonsterManager.walletHandler.AddToWallet(m_OfferUuid);

            JoyMonsterManager.walletHandler.currentWalletAddPromise
                .Then(success =>
                {
                    m_ProgressBar.Hide();
                    m_LayoutBehaviour.progressBarEnabled = false;

                    if (success)
                    {
                        m_ConfirmationPromise.ResolveIfPending(true);
                        //ResolveIfPending(true);
                        Hide(true);
                    }
                    else
                    {
                        SetButtonsInteractableState(true);
                        m_LabelTitleGraphic.TransitionState(false, Values.Duration.SHORTER, 0f);
                        m_LabelTitleGraphic.DelayedTransitionState(true, Values.Duration.SHORTER, Values.Duration.SHORT);
                        TweenManager.TimedCallback(Values.Duration.SHORTER, () =>
                        {
                            m_LabelTitle.text = Values.Text.ADD_TO_WALLET_ERROR;
                            m_LayoutBehaviour.AnimatedAdjustLayout(Values.Duration.SHORT);
                        });
                    }
                });
        }

        private void SetButtonsInteractableState(bool state)
        {
            float alpha = state ? 1f : 0.25f;

            m_ButtonCancel.alpha = alpha;
            m_ButtonCancel.interactable = state;

            m_ButtonAdd.alpha = alpha;
            m_ButtonAdd.interactable = state;
        }
    }
}
﻿using MaterialUI_JoyMonster;
using System;
using UnityEngine;
using UnityEngine.EventSystems;
using HorizontalOrVerticalLayoutGroup = MaterialUI_JoyMonster.HorizontalOrVerticalLayoutGroup;

namespace JoyMonster.Ui
{
    [ExecuteInEditMode]
    public class LayoutGroupValueSwitcher : UIBehaviour
    {
        [Serializable]
        public enum Orientation
        {
            Both,
            LandscapeOnly,
            PortraitOnly
        }

        public float scale = 1f;

        public bool modifyPaddingLeft;
        public bool modifyPaddingRight;
        public bool modifyPaddingTop;
        public bool modifyPaddingBottom;

        public bool modifySpacing;

        public Orientation m_Orientation = Orientation.Both;

        private HorizontalOrVerticalLayoutGroup m_LayoutGroup;

        private HorizontalOrVerticalLayoutGroup layoutGroup
        {
            get
            {
                if (m_LayoutGroup == null)
                {
                    m_LayoutGroup = GetComponent<HorizontalOrVerticalLayoutGroup>();
                }

                return m_LayoutGroup;
            }
        }

        protected override void OnEnable()
        {
            UiScaleController.onRefreshed += Refresh;
        }

        protected override void OnDisable()
        {
            UiScaleController.onRefreshed -= Refresh;
        }

        protected virtual void Refresh()
        {
            if ((m_Orientation == Orientation.LandscapeOnly &&
                 UiScaleController.orientation == UiScaleController.Orientation.Portrait) ||
                (m_Orientation == Orientation.PortraitOnly &&
                 UiScaleController.orientation == UiScaleController.Orientation.Landscape))
            {
                return;
            }

            LayoutValue values = UiLayoutValueHandler.common.currentValue;

            RectOffset padding = new RectOffset(
                modifyPaddingLeft ? Mathf.RoundToInt(values.padding * scale) : layoutGroup.padding.left,
                modifyPaddingRight ? Mathf.RoundToInt(values.padding * scale) : layoutGroup.padding.right,
                modifyPaddingTop ? Mathf.RoundToInt(values.padding * scale) : layoutGroup.padding.top,
                modifyPaddingBottom ? Mathf.RoundToInt(values.padding * scale) : layoutGroup.padding.bottom
            );

            layoutGroup.padding = padding;

            if (modifySpacing)
            {
                layoutGroup.spacing = values.spacing * scale;
            }
        }
    }
}
﻿using MaterialUI_JoyMonster;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace JoyMonster.Ui
{
    public abstract class GraphicLayoutBehaviour<T> : UIBehaviour, ILayoutGroup
    {
        [SerializeField]
        private RectTransform m_RectTransform;
        protected RectTransform rectTransform
        {
            get
            {
                if (m_RectTransform == null)
                {
                    m_RectTransform = GetComponent<RectTransform>();
                }

                return m_RectTransform;
            }
        }

        [SerializeField]
        private RectTransform m_ParentRectTransform;
        protected RectTransform parentRectTransform
        {
            get
            {
                if (m_ParentRectTransform == null)
                {
                    m_ParentRectTransform = rectTransform.parent.GetComponent<RectTransform>();
                }

                return m_ParentRectTransform;
            }
        }

        [SerializeField]
        private float m_TexturelessRatio = 0f;
        public float texturelessRatio
        {
            get { return m_TexturelessRatio; }
            set { m_TexturelessRatio = value; }
        }

        [SerializeField]
        protected T m_Graphic;
        protected abstract Texture texture { get; }

        [SerializeField] protected Vector2 m_MinPadding;
        [SerializeField] protected Vector2 m_MaxSize;

        public void SetLayoutHorizontal()
        {
            if (m_Graphic == null || parentRectTransform == null) return;

            Vector2 bounds = parentRectTransform.rect.size - m_MinPadding * 2f;

            if (m_MaxSize.x > 0f)
            {
                bounds.x = Mathf.Min(bounds.x, m_MaxSize.x);
            }

            if (m_MaxSize.y > 0f)
            {
                bounds.y = Mathf.Min(bounds.y, m_MaxSize.y);
            }

            Vector2 imageFittedSize;

            imageFittedSize = texture == null ?
                UiUtils.FitToBounds(m_TexturelessRatio == 0f ? bounds : new Vector2(m_TexturelessRatio, 1f), bounds) :
                UiUtils.FitToBounds(new Vector2(texture.width, texture.height), bounds);

            rectTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, imageFittedSize.x);
            rectTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, imageFittedSize.y);
        }

        public void SetLayoutVertical() { }
    }
}
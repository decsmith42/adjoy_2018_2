﻿using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace JoyMonster.Ui
{
    public class LabelLayoutBehaviour : UIBehaviour, ILayoutGroup
    {
        public enum LabelType
        {
            Header,
            SubHeader,
            Button,
            Paragraph
        }

        private TextMeshProUGUI m_Label;

        private TextMeshProUGUI label
        {
            get
            {
                if (m_Label == null)
                {
                    m_Label = GetComponent<TextMeshProUGUI>();
                }

                return m_Label;
            }
        }

        [SerializeField] private LabelType m_LabelType = LabelType.Paragraph;

        [SerializeField] private float m_Scale = 1f;

        [SerializeField] private float m_Delta = 0f;

        public void SetLayoutHorizontal()
        {
            LayoutValue values = UiLayoutValueHandler.common.currentValue;

            float size = m_Scale;

            switch (m_LabelType)
            {
                case LabelType.Header:
                    size *= values.textHeaderFontSize;
                    break;
                case LabelType.SubHeader:
                    size *= values.textSubHeaderFontSize;
                    break;
                case LabelType.Button:
                    size *= values.textButtonFontSize;
                    break;
                case LabelType.Paragraph:
                    size *= values.textParagraphFontSize;
                    break;
            }

            label.fontSize = size + m_Delta;
        }

        public void SetLayoutVertical()
        {

        }
    }
}
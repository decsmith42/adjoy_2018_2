﻿using MaterialUI_JoyMonster;
using UnityEngine;
using UnityEngine.EventSystems;

namespace JoyMonster.Ui
{
    public class ExpiredPopup : UIBehaviour
    {
        [SerializeField] protected Canvas m_Canvas = null;
        [SerializeField] protected ExpiredPopupLayoutBehaviour m_LayoutBehaviour = null;
        [SerializeField] private RectTransform m_Panel = null;
        [SerializeField] private DynamicCanvasGroup m_BackgroundCanvasGroup = null;
        [SerializeField] private DynamicCanvasGroup m_PanelCanvasGroup = null;
        [SerializeField] private DynamicCanvasGroup m_ContentCanvasGroup = null;

        private float m_PanelSizeSmall = 24f;

        public bool isShowing { get; private set; }

        private int m_TweenIdSizeX;
        private int m_TweenIdSizeY;

        public void Show()
        {
            TweenManager.EndTween(m_TweenIdSizeX);
            TweenManager.EndTween(m_TweenIdSizeY);

            isShowing = true;

            m_Panel.anchoredPosition = Vector2.zero;
            m_Panel.sizeDelta = new Vector2(m_PanelSizeSmall, m_PanelSizeSmall);

            m_ContentCanvasGroup.SetState(false);
            m_PanelCanvasGroup.SetState(false);

            m_Canvas.enabled = true;

            m_BackgroundCanvasGroup.TransitionState(true, Values.Duration.SHORT, Values.Duration.SHORTER);

            m_PanelCanvasGroup.TransitionState(true, Values.Duration.SHORTER, Values.Duration.SHORTER);

            m_TweenIdSizeX = TweenManager.TweenFloat(value => m_Panel.SetSizeDeltaX(value),
                m_Panel.sizeDelta.x, m_LayoutBehaviour.panelSize.x,
                Values.Duration.SHORT, Values.Duration.SHORTER, tweenType: Tween.TweenType.EaseOutQuint);

            m_TweenIdSizeY = TweenManager.TweenFloat(value => m_Panel.SetSizeDeltaY(value),
                m_Panel.sizeDelta.y, m_LayoutBehaviour.panelSize.y,
                Values.Duration.MEDIUM, Values.Duration.SHORTER, tweenType: Tween.TweenType.EaseOutQuint);

            m_ContentCanvasGroup.TransitionState(true, Values.Duration.SHORT, Values.Duration.SHORT);
        }

        private void Hide()
        {
            TweenManager.EndTween(m_TweenIdSizeX);
            TweenManager.EndTween(m_TweenIdSizeY);

            isShowing = false;

            m_ContentCanvasGroup.TransitionState(false, Values.Duration.SHORT, Values.Duration.SHORTER);

            m_TweenIdSizeX = TweenManager.TweenFloat(value => m_Panel.SetSizeDeltaX(value),
                m_Panel.sizeDelta.x, m_PanelSizeSmall,
                Values.Duration.MEDIUM, Values.Duration.SHORTER, tweenType: Tween.TweenType.EaseInQuint);

            m_TweenIdSizeY = TweenManager.TweenFloat(value => m_Panel.SetSizeDeltaY(value),
                m_Panel.sizeDelta.y, m_PanelSizeSmall,
                Values.Duration.MEDIUM, Values.Duration.SHORTER,
                callback: () => m_Canvas.enabled = false,
                tweenType: Tween.TweenType.EaseInQuint);

            m_BackgroundCanvasGroup.TransitionState(true, Values.Duration.SHORT, Values.Duration.SHORTER);

            m_PanelCanvasGroup.TransitionState(false, Values.Duration.SHORT, Values.Duration.SHORTER);
        }

        public void OnOkPressed()
        {
            if (isShowing) Hide();
        }
    }
}
﻿using JoyMonster.API;
using RSG;
using UnityEngine;
using UnityEngine.EventSystems;

namespace JoyMonster.Ui
{
    public abstract class UiScreen : UIBehaviour
    {
        public abstract int screenId { get; }

        protected Canvas m_Canvas;

        public Canvas canvas
        {
            get
            {
                if (m_Canvas == null)
                {
                    m_Canvas = GetComponent<Canvas>();
                }

                return m_Canvas;
            }
        }

        protected ScreenLayoutBehaviour m_LayoutBehaviour;

        public ScreenLayoutBehaviour layoutBehaviour
        {
            get
            {
                if (m_LayoutBehaviour == null)
                {
                    m_LayoutBehaviour = GetComponent<ScreenLayoutBehaviour>();
                }

                return m_LayoutBehaviour;
            }
        }

        protected bool m_IsShowing;
        public bool isShowing
        {
            get { return m_IsShowing; }
        }

        protected bool m_TemporarilyHidden;
        public bool temporarilyHidden
        {
            get { return m_TemporarilyHidden; }
        }

        [SerializeField] protected DynamicCanvasGroup m_DynamicCanvasGroup;
        [SerializeField] protected RectTransform m_Content;

        protected Promise<bool> m_ShowPromise;
        protected Promise<bool> m_HidePromise;

        public Promise<bool> Show()
        {
            OnBeforeShow();

            m_IsShowing = true;
            m_TemporarilyHidden = false;

            if (m_ShowPromise != null && m_ShowPromise.CurState == PromiseState.Pending)
            {
                m_ShowPromise.Resolve(false);
            }

            m_ShowPromise = new Promise<bool>();

            OnShow();

            m_ShowPromise.Then(completed =>
            {
                if (completed)
                {
                    OnAfterShow();
                }
            }).Catch(exception => JoyMonsterAPI.DebugLogError(exception));

            return m_ShowPromise;
        }

        public Promise<bool> Hide()
        {
            if (!m_TemporarilyHidden)
            {
                OnBeforeHide();
            }

            m_IsShowing = false;

            if (m_HidePromise != null && m_HidePromise.CurState == PromiseState.Pending)
            {
                m_HidePromise.Resolve(false);
            }

            m_HidePromise = new Promise<bool>();

            if (m_TemporarilyHidden)
            {
                m_HidePromise.Resolve(true);
                ResetScreen();
            }
            else
            {
                OnHide();

                m_HidePromise.Then(completed =>
                {
                    if (completed)
                    {
                        OnAfterHide();
                    }
                }).Catch(exception => JoyMonsterAPI.DebugLogError(exception));
            }

            return m_HidePromise;
        }

        protected virtual void OnBeforeShow()
        {
            canvas.enabled = true;
            SetAppBars();
        }

        protected virtual void OnBeforeHide()
        {
        }

        protected virtual void OnShow()
        {
            m_DynamicCanvasGroup.SetState(true);
            m_ShowPromise.Resolve(true);
        }

        protected virtual void OnHide()
        {
            m_DynamicCanvasGroup.SetState(false);
            m_HidePromise.Resolve(true);
        }

        protected virtual void OnAfterShow() { }

        protected virtual void OnAfterHide()
        {
            canvas.enabled = false;
            ResetScreen();
        }

        public virtual void ShowImmediate()
        {
            canvas.enabled = true;

            m_IsShowing = true;

            if (m_ShowPromise != null && m_ShowPromise.CurState == PromiseState.Pending)
            {
                m_ShowPromise.Resolve(false);
            }
        }

        public virtual void HideImmediate()
        {
            canvas.enabled = false;

            m_IsShowing = false;

            if (m_HidePromise != null && m_HidePromise.CurState == PromiseState.Pending)
            {
                m_HidePromise.Resolve(false);
            }
        }

        public void SetTemporaryHiddenState(bool hidden)
        {
            if (m_IsShowing)
            {
                m_TemporarilyHidden = hidden;
                canvas.enabled = !hidden;
                if (!hidden)
                {
                    SetAppBars();
                }
            }
        }

        protected virtual void ResetScreen() { }

        protected virtual void SetAppBars() { }

        public virtual bool OnBackButtonPressed()
        {
            return true;
        }

        public virtual bool OnBackToGameButtonPressed()
        {
            return true;
        }
    }

}
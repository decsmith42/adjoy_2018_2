﻿using MaterialUI_JoyMonster;
using System;
using UnityEngine;
using UnityEngine.EventSystems;

namespace JoyMonster.Ui
{
    public class DynamicTransformPosition : UIBehaviour
    {
        protected RectTransform m_RectTransform;

        public RectTransform rectTransform
        {
            get
            {
                if (m_RectTransform == null)
                {
                    m_RectTransform = GetComponent<RectTransform>();
                }

                return m_RectTransform;
            }
        }

        [SerializeField] protected Vector2 m_BasePosition;

        public Vector2 basePosition
        {
            get { return m_BasePosition; }
            set { m_BasePosition = value; }
        }

        [Serializable]
        public enum Direction
        {
            Up,
            Down,
            Left,
            Right,
            Middle
        }

        [SerializeField] protected Direction m_Direction = Direction.Middle;

        public Direction direction
        {
            get { return m_Direction; }
            set { m_Direction = value; }
        }

        [SerializeField] private Vector4 m_Offset = new Vector4(24f, 24f, 24f, 24f);

        public Vector4 offset
        {
            get { return m_Offset; }
            set { m_Offset = value; }
        }

        protected int m_TweenId;

        public void TransitionPosition(float duration, float delay, Tween.TweenType tweenType)
        {
            m_TweenId = TweenManager.TweenVector2(value => rectTransform.anchoredPosition = value,
                rectTransform.anchoredPosition, DirectionToPosition(m_Direction), duration, delay, tweenType: tweenType);
        }

        public void DelayedTransitionPosition(float duration, float delay, Tween.TweenType tweenType)
        {
            TweenManager.TimedCallback(delay, () => { TransitionPosition(duration, 0f, tweenType); });
        }

        public void TransitionPosition(Direction direction, float duration, float delay, Tween.TweenType tweenType)
        {
            m_Direction = direction;
            TransitionPosition(duration, delay, tweenType);
        }

        public void DelayedTransitionPosition(Direction direction, float duration, float delay, Tween.TweenType tweenType)
        {
            TweenManager.TimedCallback(delay, () => { TransitionPosition(direction, duration, 0f, tweenType); });
        }

        public void TransitionPosition(Direction startDirection, Direction endDirection, float duration, float delay,
            Tween.TweenType tweenType)
        {
            SetPosition(startDirection);
            TransitionPosition(endDirection, duration, delay, tweenType);
        }

        public void DelayedTransitionPosition(Direction startDirection, Direction endDirection, float duration, float delay,
            Tween.TweenType tweenType)
        {
            TweenManager.TimedCallback(delay,
                () => { TransitionPosition(startDirection, endDirection, duration, 0f, tweenType); });
        }

        public void SetPosition()
        {
            rectTransform.anchoredPosition = DirectionToPosition(m_Direction);
        }

        public void MarkPositionAsBase()
        {
            m_BasePosition = rectTransform.anchoredPosition;
        }

        public void SetPosition(Direction direction)
        {
            m_Direction = direction;
            SetPosition();
        }

        public Vector2 DirectionToPosition(Direction direction)
        {
            Vector2 position = m_BasePosition;

            switch (direction)
            {
                case Direction.Up:
                    position.y += offset.y;
                    break;
                case Direction.Down:
                    position.y -= offset.w;
                    break;
                case Direction.Left:
                    position.x -= offset.x;
                    break;
                case Direction.Right:
                    position.x += offset.z;
                    break;
            }

            return position;
        }
    }
}
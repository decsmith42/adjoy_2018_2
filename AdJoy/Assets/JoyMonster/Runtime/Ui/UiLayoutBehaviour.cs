﻿using JoyMonster.Core;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

namespace JoyMonster.Ui
{
    [ExecuteInEditMode]
    public abstract class UiLayoutBehaviour : UIBehaviour
    {
        [SerializeField] protected bool m_SetValues = true;

        public bool setValues
        {
            get { return m_SetValues; }
            set { m_SetValues = value; }
        }

        [SerializeField] protected UnityEvent m_OnSetLayout;

        protected RectTransform m_RectTransform;

        public RectTransform rectTransform
        {
            get
            {
                if (m_RectTransform == null)
                {
                    m_RectTransform = GetComponent<RectTransform>();
                }

                return m_RectTransform;
            }
        }

        void Update()
        {
            if (Application.isEditor && !Application.isPlaying)
            {
                RefreshLayout();
            }
        }

        public void RefreshLayout()
        {
            if (PrefabUtil.IsPrefab(gameObject)) return;

            if (JoyMonsterManager.instance == null) return;

            if (m_SetValues)
            {
                OnSetLayout();
            }

            if (m_OnSetLayout != null)
            {
                m_OnSetLayout.Invoke();
            }
        }

        protected abstract void OnSetLayout();
    }
}
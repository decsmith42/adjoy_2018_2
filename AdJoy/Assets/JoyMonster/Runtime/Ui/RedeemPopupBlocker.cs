﻿using UnityEngine;
using UnityEngine.EventSystems;

namespace JoyMonster.Ui
{
    public class RedeemPopupBlocker : MonoBehaviour, IPointerClickHandler
    {
        [SerializeField] private RedeemPopup m_RedeemPopup = null;

        public void OnPointerClick(PointerEventData eventData)
        {
            m_RedeemPopup.OnCancelPressed();
        }
    }
}
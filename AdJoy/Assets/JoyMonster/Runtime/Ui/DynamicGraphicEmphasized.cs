﻿using MaterialUI_JoyMonster;
using UnityEngine;

namespace JoyMonster.Ui
{
    public class DynamicGraphicEmphasized : DynamicGraphic
    {
        private Color m_ColorUpper;
        private Color m_ColorLower;
        private float m_CycleDuration;

        private static readonly Color m_DefaultColorUpper = MaterialColor.disabledDark;
        private static readonly Color m_DefaultColorLower = MaterialColor.dividerDark;

        public void EmphasizeContinuous(Color colorUpper, Color colorLower, float cycleDuration = Values.Duration.LONG, float delay = 0f)
        {
            TweenManager.EndTween(m_TweenId);

            m_ColorUpper = colorUpper;
            m_ColorLower = colorLower;
            m_CycleDuration = cycleDuration;

            graphic.enabled = true;

            m_TweenId = TweenManager.TweenColor(value => m_Graphic.color = value, m_Graphic.color, m_ColorUpper,
                cycleDuration / 2f, delay, () => EmphasisCycle(false), tweenType: Tween.TweenType.SoftEaseOutQuint);
        }

        public void EmphasizeContinuous(float cycleDuration = Values.Duration.LONG, float delay = 0f)
        {
            EmphasizeContinuous(m_DefaultColorUpper, m_DefaultColorLower, cycleDuration, delay);
        }

        private void EmphasisCycle(bool cycleToUpperAlpha)
        {
            m_TweenId = TweenManager.TweenColor(value => m_Graphic.color = value, m_Graphic.color, cycleToUpperAlpha ? m_ColorUpper : m_ColorLower,
                m_CycleDuration / 2f, 0f, () => EmphasisCycle(!cycleToUpperAlpha), tweenType: Tween.TweenType.EaseInOutCubed);
        }
    }
}
﻿using MaterialUI_JoyMonster;
using UnityEngine;

namespace JoyMonster.Ui.Lists
{
    public class ListFaqErrorExtension : ListErrorExtension
    {
        [SerializeField] private DynamicCanvasGroup m_CanvasGroup = null;

        [SerializeField] private DynamicGraphic m_LabelGraphic = null;
        [SerializeField] private DynamicTransformPosition m_LabelPosition = null;

        [SerializeField] private DynamicCanvasGroup m_ButtonCanvasGroup = null;
        [SerializeField] private DynamicTransformPosition m_ButtonPosition = null;

        protected override void OnShowError()
        {
            m_CanvasGroup.SetState(true);

            m_LabelGraphic.TransitionState(false, true, Values.Duration.SHORT, Values.Duration.SHORTER);
            m_LabelPosition.TransitionPosition(DynamicTransformPosition.Direction.Down, DynamicTransformPosition.Direction.Middle,
                Values.Duration.SHORT, Values.Duration.SHORTER, Tween.TweenType.EaseOutQuint);

            m_ButtonCanvasGroup.TransitionState(false, true, Values.Duration.SHORT, Values.Duration.SHORTER + Values.Duration.PER_ELEMENT_DELAY);
            m_ButtonPosition.TransitionPosition(DynamicTransformPosition.Direction.Down, DynamicTransformPosition.Direction.Middle,
                Values.Duration.SHORT, Values.Duration.SHORTER + Values.Duration.PER_ELEMENT_DELAY, Tween.TweenType.EaseOutQuint);
        }

        protected override void OnHideError()
        {

            m_LabelGraphic.TransitionState(false, Values.Duration.SHORT, Values.Duration.PER_ELEMENT_DELAY * 2f);
            m_LabelPosition.TransitionPosition(DynamicTransformPosition.Direction.Up,
                Values.Duration.SHORT, Values.Duration.SHORTER, Tween.TweenType.EaseInQuint);

            m_ButtonCanvasGroup.TransitionState(false, Values.Duration.SHORT, Values.Duration.PER_ELEMENT_DELAY * 3f);
            m_ButtonPosition.TransitionPosition(DynamicTransformPosition.Direction.Up,
                Values.Duration.SHORT, Values.Duration.SHORTER + Values.Duration.PER_ELEMENT_DELAY, Tween.TweenType.EaseInQuint);

            m_CanvasGroup.DelayedSetState(false, Values.Duration.SHORT + Values.Duration.SHORTER + Values.Duration.PER_ELEMENT_DELAY);
        }

        public void RefreshLayout()
        {
            LayoutValue values = UiLayoutValueHandler.common.currentValue;

            if (UiScaleController.orientation == UiScaleController.Orientation.Portrait)
            {
                m_LabelPosition.rectTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, rectTransform.rect.width - values.padding * 6f);
                m_LabelPosition.rectTransform.SetAnchoredPositionY(values.spacing * 4f);

                m_ButtonPosition.rectTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal,
                    Mathf.Min(rectTransform.rect.width - values.padding * 6f, 360f));
                m_ButtonPosition.rectTransform.SetAnchoredPositionY(-values.spacing * 4f);
            }
            else
            {
                m_LabelPosition.rectTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, rectTransform.rect.width - values.padding * 8f);
                m_LabelPosition.rectTransform.SetAnchoredPositionY(values.spacing * 2f);

                m_ButtonPosition.rectTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal,
                    Mathf.Min(rectTransform.rect.width - values.padding * 8f, 360f));
                m_ButtonPosition.rectTransform.SetAnchoredPositionY(-values.spacing * 2f);
            }

            m_LabelPosition.MarkPositionAsBase();
            m_LabelPosition.SetPosition(DynamicTransformPosition.Direction.Middle);

            m_ButtonPosition.MarkPositionAsBase();
            m_ButtonPosition.SetPosition(DynamicTransformPosition.Direction.Middle);
        }
    }
}
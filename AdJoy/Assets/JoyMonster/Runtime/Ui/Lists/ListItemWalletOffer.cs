﻿using JoyMonster.API.Model;
using JoyMonster.Core;
using MaterialUI_JoyMonster;
using RSG;
using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace JoyMonster.Ui.Lists
{
    public class ListItemWalletOffer : ListItemStandard
    {
        private Promise<Texture2D> m_ImagePromise;

        [SerializeField]
        private RectTransform m_ImageContainer = null;

        public RectTransform imageContainer
        {
            get { return m_ImageContainer; }
        }

        [SerializeField]
        private RectTransform m_InfoContainer = null;

        public RectTransform infoContainer
        {
            get { return m_InfoContainer; }
        }

        [SerializeField]
        private RectTransform m_MainInfoContainer = null;

        public RectTransform mainInfoContainer
        {
            get { return m_MainInfoContainer; }
        }

        [SerializeField]
        private RectTransform m_ExtraInfoContainer = null;

        public RectTransform extraInfoContainer
        {
            get { return m_ExtraInfoContainer; }
        }

        [SerializeField]
        private RectTransform m_Divider = null;

        public RectTransform divider
        {
            get { return m_Divider; }
        }

        [SerializeField]
        private TextMeshProUGUI m_LabelTitle = null;

        public TextMeshProUGUI labelTitle
        {
            get { return m_LabelTitle; }
        }

        [SerializeField]
        private TextMeshProUGUI m_LabelCompany = null;

        public TextMeshProUGUI labelCompany
        {
            get { return m_LabelCompany; }
        }

        [SerializeField]
        private VectorImage m_IconDistance = null;

        public VectorImage iconDistance
        {
            get { return m_IconDistance; }
        }

        [SerializeField]
        private TextMeshProUGUI m_LabelDistance = null;

        public TextMeshProUGUI labelDistance
        {
            get { return m_LabelDistance; }
        }

        [SerializeField]
        private VectorImage m_IconExpiry = null;

        public VectorImage iconExpiry
        {
            get { return m_IconExpiry; }
        }

        [SerializeField]
        private TextMeshProUGUI m_LabelExpiry = null;

        public TextMeshProUGUI labelExpiry
        {
            get { return m_LabelExpiry; }
        }

        [SerializeField]
        private DynamicCanvasGroup m_ExtraInfoCanvasGroup = null;
        public DynamicCanvasGroup extraInfoCanvasGroup
        {
            get { return m_ExtraInfoCanvasGroup; }
        }

        [SerializeField]
        private Graphic m_Background = null;
        public Graphic background
        {
            get { return m_Background; }
        }

        [SerializeField]
        private VectorImage m_Outline = null;
        public VectorImage outline
        {
            get { return m_Outline; }
        }

        [SerializeField]
        private DynamicGraphic m_DividerGraphic = null;
        public DynamicGraphic dividerGraphic
        {
            get { return m_DividerGraphic; }
        }

        [SerializeField]
        private GameObject m_IconNew = null;
        public GameObject iconNew
        {
            get { return m_IconNew; }
        }

        [SerializeField]
        private RectTransform m_ButtonView = null;
        public RectTransform buttonView
        {
            get { return m_ButtonView; }
        }

        [SerializeField]
        private TextMeshProUGUI m_LabelButtonView = null;
        public TextMeshProUGUI labelButtonView
        {
            get { return m_LabelButtonView; }
        }

        [SerializeField]
        private TextMeshProUGUI m_LabelPartnerExtraTitle = null;
        public TextMeshProUGUI labelPartnerExtraTitle
        {
            get { return m_LabelPartnerExtraTitle; }
        }

        [SerializeField]
        private DynamicGraphic m_ImageLogoGraphic = null;
        [SerializeField]
        private RawImageLayoutBehaviour m_ImageLogoLayout = null;
        [SerializeField]
        private MaterialRipple m_BaseRipple = null;
        [SerializeField]
        private MaterialRipple m_ButtonRipple = null;

        public override Type matchingDataType
        {
            get { return typeof(ListItemDataWalletOffer); }
        }

        public void LoadAndSetImage(string url, bool isPartner)
        {
            m_ImageLogoLayout.SetTexture(null);
            m_ImageLogoGraphic.graphic.color = Color.white;

            m_ImagePromise = new Promise<Texture2D>((resolve, reject) => { WebTextureHandler.GetTexture(url).Then(resolve); });

            if (m_ImagePromise.CurState == PromiseState.Resolved)
            {
                m_ImagePromise.Then(texture =>
                {
                    m_ImageLogoLayout.SetTexture(texture);
                    if (JoyMonsterManager.webTextureHandler.IsErrorTexture(texture))
                    {
                        m_ImageLogoGraphic.SetToCustom(MaterialColor.dividerDark);
                    }
                    else
                    {
                        m_ImageLogoGraphic.SetState(true);
                    }
                });
            }
            else
            {
                m_ImagePromise.Then(texture =>
                {
                    m_ImageLogoLayout.SetTexture(texture);
                    if (JoyMonsterManager.webTextureHandler.IsErrorTexture(texture))
                    {
                        m_ImageLogoGraphic.TransitionToCustom(MaterialColor.dividerDark, Values.Duration.SHORT, 0f);
                    }
                    else
                    {
                        m_ImageLogoGraphic.TransitionState(true, Values.Duration.SHORT, 0f);
                    }
                });
            }

            m_BaseRipple.SetGraphicColor(isPartner ? new Color(1f, 0.936f, 0.76f, 1f) : Color.white, false);
            m_BaseRipple.rippleColor = isPartner ? outline.color : outline.gradientEnd;
            m_BaseRipple.highlightColor =
                MaterialColor.HighlightColor(isPartner ? new Color(1f, 0.936f, 0.76f, 1f) : Color.white, m_BaseRipple.rippleColor, 0.5f);
        }

        public override void OnItemReleaseToPool()
        {
            m_BaseRipple.InstantDestroyAllRipples(true);
            m_ButtonRipple.InstantDestroyAllRipples(true);

            if (m_ImagePromise != null)
            {
                if (m_ImagePromise.CurState == PromiseState.Pending)
                {
                    m_ImagePromise.Reject(null);
                }

                m_ImagePromise = null;
            }
        }

        public void OnButtonViewPressed()
        {
            listView.interactionController.OnItemInteraction(ListInteraction.pointerClick, null, this, 1);
        }
    }

    public class ListItemDataWalletOffer : ListItemDataStandard
    {
        public Offer offer;

        public override ListItemController GetNewController()
        {
            return new ListItemControllerWalletOffer();
        }
    }

    public class ListItemControllerWalletOffer : ListItemController
    {
        public override string itemPrefabPath
        {
            get { return null; }
        }

        public override Type matchingDataType
        {
            get { return typeof(ListItemDataWalletOffer); }
        }

        public override float minItemWidth
        {
            get { return 300f; }
        }

        public override float minItemHeight
        {
            get { return 115.89f; }
        }

        public override void FormatItemWithData(ListItem item, ListItemData data)
        {
            ListItemWalletOffer itemWalletOffer = (ListItemWalletOffer)item;
            ListItemDataWalletOffer dataWalletOffer = (ListItemDataWalletOffer)data;
            Offer offer = dataWalletOffer.offer;

            itemWalletOffer.labelCompany.text = offer.company;
            itemWalletOffer.labelTitle.text = offer.title;
            itemWalletOffer.labelDistance.text = OfferFormatter.DisplayDistance(offer);
            itemWalletOffer.labelExpiry.text = OfferFormatter.DisplayExpiry(offer);

            if (offer.isPartner)
            {
                itemWalletOffer.iconDistance.enabled = false;
                itemWalletOffer.labelDistance.enabled = false;
                itemWalletOffer.iconExpiry.enabled = false;
                itemWalletOffer.labelExpiry.enabled = false;

                itemWalletOffer.dividerGraphic.SetState(false);
                itemWalletOffer.background.color = new Color(1f, 0.936f, 0.76f, 1f);
                itemWalletOffer.outline.gradientColor = false;
                itemWalletOffer.labelPartnerExtraTitle.enabled = true;
                SetLayoutPartner(itemWalletOffer, m_ItemSizes[dataWalletOffer]);
            }
            else
            {
                if (itemWalletOffer.labelDistance.text == "")
                {
                    itemWalletOffer.iconDistance.enabled = false;
                    itemWalletOffer.labelDistance.enabled = false;
                }
                else
                {
                    itemWalletOffer.iconDistance.enabled = true;
                    itemWalletOffer.labelDistance.enabled = true;
                }

                if (dataWalletOffer.offer.endDate == "")
                {
                    itemWalletOffer.iconExpiry.enabled = false;
                    itemWalletOffer.labelExpiry.enabled = false;
                }
                else
                {
                    itemWalletOffer.iconExpiry.enabled = true;
                    itemWalletOffer.labelExpiry.enabled = true;
                }

                itemWalletOffer.dividerGraphic.SetState(true);
                itemWalletOffer.background.color = Color.white;
                itemWalletOffer.outline.gradientColor = true;
                itemWalletOffer.labelPartnerExtraTitle.enabled = false;
                SetLayout(itemWalletOffer, m_ItemSizes[dataWalletOffer]);
            }

            itemWalletOffer.LoadAndSetImage(offer.brandLogoUrl, offer.isPartner);
        }

        private void SetLayout(ListItemWalletOffer item, Vector2 size)
        {
            ListItemDataWalletOffer data = (ListItemDataWalletOffer)item.currentData;
            LayoutValue values = UiLayoutValueHandler.common.currentValue;

            item.labelButtonView.alignment = TextAlignmentOptions.Center;

            item.iconNew.SetActive(data.offer.isNew);
            item.labelCompany.margin = new Vector4(data.offer.isNew ? 43.7f + values.spacing : 0f, 0f, 0f, 0f);

            item.labelCompany.fontSize = values.textParagraphFontSize;
            item.labelCompany.fontSizeMax = values.textParagraphFontSize;
            item.labelTitle.fontSize = values.textSubHeaderFontSize;
            item.labelTitle.fontSizeMax = values.textSubHeaderFontSize;
            item.labelDistance.fontSize = values.textParagraphFontSize - 3f;
            item.iconDistance.size = values.textParagraphFontSize - 2f;
            item.labelExpiry.fontSize = values.textParagraphFontSize - 3f;
            item.iconExpiry.size = values.textParagraphFontSize - 2f;
            item.labelButtonView.fontSize = values.textParagraphFontSize - 2f;

            float imageContainerSize = size.y - values.padding * 2f;

            item.background.rectTransform.sizeDelta = Vector2.zero;

            item.imageContainer.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Left, values.padding, imageContainerSize);
            item.imageContainer.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, imageContainerSize);

            item.infoContainer.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Right, values.padding, size.x - imageContainerSize - values.padding * 3f);
            item.infoContainer.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Top, values.padding, imageContainerSize);

            item.extraInfoContainer.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Bottom, 0f,
                Mathf.Max(item.labelExpiry.preferredHeight, item.labelDistance.preferredHeight));

            float posX = 0f;

            if (item.labelDistance.text != "")
            {
                item.iconDistance.rectTransform.SetSizeDeltaX(values.textParagraphFontSize);
                posX += item.iconDistance.rectTransform.rect.width + values.spacing / 2f;

                item.labelDistance.rectTransform.SetSizeDeltaX(item.labelDistance.preferredWidth);
                item.labelDistance.rectTransform.SetAnchoredPositionX(posX);
                posX += item.labelDistance.rectTransform.rect.width + values.spacing;
            }

            if (item.labelExpiry.text != "")
            {
                item.iconExpiry.rectTransform.SetSizeDeltaX(values.textParagraphFontSize);
                item.iconExpiry.rectTransform.SetAnchoredPositionX(posX);
                posX += item.iconExpiry.rectTransform.rect.width + values.spacing / 2f;

                item.labelExpiry.rectTransform.SetSizeDeltaX(item.labelExpiry.preferredWidth);
                item.labelExpiry.rectTransform.SetAnchoredPositionX(posX);
                posX += item.labelExpiry.preferredWidth + values.spacing;
            }

            float freeSpaceX = item.extraInfoContainer.rect.width - posX;

            float buttonWidth = item.labelButtonView.preferredWidth;

            if (freeSpaceX > buttonWidth + values.spacing * 3f)
            {
                buttonWidth += values.spacing * 3f;
            }
            else if (freeSpaceX > buttonWidth + values.spacing * 2f)
            {
                buttonWidth += values.spacing * 2f;
            }
            else
            {
                buttonWidth += values.spacing;
            }

            item.buttonView.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Right, 0f, buttonWidth);
            item.buttonView.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical,
                Mathf.Min(item.labelButtonView.preferredHeight + values.spacing * 1.5f, item.extraInfoContainer.rect.height + values.spacing));
            item.buttonView.SetAnchoredPositionY(0f);

            if (freeSpaceX < buttonWidth)
            {
                float delta = buttonWidth - freeSpaceX;

                item.labelDistance.rectTransform.SetAnchoredPositionX(item.labelDistance.rectTransform.anchoredPosition.x - delta / 6f);
                item.iconExpiry.rectTransform.SetAnchoredPositionX(item.iconExpiry.rectTransform.anchoredPosition.x - delta / 6f - delta / 3f);
                item.labelExpiry.rectTransform.SetAnchoredPositionX(item.labelExpiry.rectTransform.anchoredPosition.x - delta / 6f - delta / 3f - delta / 6f);
            }

            item.divider.SetAnchoredPositionY(item.extraInfoContainer.rect.height + values.spacing);

            item.labelTitle.rectTransform.SetAnchoredPositionY(0f);
            item.labelTitle.rectTransform.SetSizeDeltaY(TextTester.GetPreferredHeight(TextTester.Font.Semibold, item.labelTitle.fontSize, 2));

            item.labelCompany.rectTransform.SetAnchoredPositionY(-item.labelTitle.rectTransform.rect.height - values.spacing);
            item.labelCompany.rectTransform.SetSizeDeltaY(TextTester.GetPreferredHeight(TextTester.Font.Normal, item.labelCompany.fontSize, 1));

            item.mainInfoContainer.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical,
                item.labelTitle.rectTransform.rect.height + values.spacing + item.labelCompany.rectTransform.rect.height);
            item.mainInfoContainer.SetAnchoredPositionY(item.extraInfoContainer.rect.height + values.spacing * 2f + 2f);

            if (item.labelTitle.preferredWidth < item.labelTitle.rectTransform.rect.width)
            {
                item.labelCompany.rectTransform.SetAnchoredPositionY(item.labelCompany.rectTransform.anchoredPosition.y + values.spacing / 2f);
            }
        }

        private void SetLayoutPartner(ListItemWalletOffer item, Vector2 size)
        {
            item.labelButtonView.alignment = TextAlignmentOptions.Center;

            ListItemDataWalletOffer data = (ListItemDataWalletOffer)item.currentData;
            LayoutValue values = UiLayoutValueHandler.common.currentValue;

            item.iconNew.SetActive(data.offer.isNew);
            item.labelCompany.margin = new Vector4(data.offer.isNew ? 43.7f + values.spacing : 0f, 0f, 0f, 0f);

            item.labelPartnerExtraTitle.fontSize = values.textParagraphFontSize - 2f;
            item.labelCompany.fontSize = values.textParagraphFontSize;
            item.labelCompany.fontSizeMax = values.textParagraphFontSize;
            item.labelTitle.fontSize = values.textSubHeaderFontSize;
            item.labelTitle.fontSizeMax = values.textSubHeaderFontSize;
            item.labelDistance.fontSize = values.textParagraphFontSize - 2f;
            item.iconDistance.size = values.textParagraphFontSize - 2f;
            item.labelExpiry.fontSize = values.textParagraphFontSize - 2f;
            item.iconExpiry.size = values.textParagraphFontSize - 2f;
            item.labelButtonView.fontSize = values.textParagraphFontSize - 2f;

            float imageContainerSize = size.y - values.padding * 2f;

            item.background.rectTransform.sizeDelta = Vector2.zero;

            item.imageContainer.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Right, values.padding, imageContainerSize);
            item.imageContainer.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, imageContainerSize);

            item.infoContainer.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Left, values.padding, size.x - imageContainerSize - values.padding * 3f);
            item.infoContainer.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Top, values.padding, imageContainerSize);

            item.extraInfoContainer.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Bottom, 0f,
                TextTester.GetPreferredHeight(TextTester.Font.Normal, values.textParagraphFontSize, 1));

            float mainInfoHeight = 0f;
            item.labelPartnerExtraTitle.rectTransform.SetAnchoredPositionY(-mainInfoHeight);
            item.labelPartnerExtraTitle.rectTransform.SetSizeDeltaY(item.labelPartnerExtraTitle.preferredHeight);
            mainInfoHeight += item.labelPartnerExtraTitle.rectTransform.rect.height + values.spacing * 0.75f;

            item.labelTitle.rectTransform.SetAnchoredPositionY(-mainInfoHeight);
            item.labelTitle.rectTransform.SetSizeDeltaY(item.labelTitle.preferredHeight);
            mainInfoHeight += item.labelTitle.rectTransform.rect.height + values.spacing / 2f;

            item.labelCompany.rectTransform.SetAnchoredPositionY(-mainInfoHeight);
            item.labelCompany.rectTransform.SetSizeDeltaY(item.labelCompany.preferredHeight);
            mainInfoHeight += item.labelCompany.rectTransform.rect.height;

            item.mainInfoContainer.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, mainInfoHeight);
            item.mainInfoContainer.SetAnchoredPositionY((item.infoContainer.rect.height - mainInfoHeight) + (string.IsNullOrEmpty(item.labelCompany.text)
                ? values.spacing / 2f
                : 0f));

            float buttonWidth = item.labelButtonView.preferredWidth + values.spacing * 3f;

            item.buttonView.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Left, 0f, buttonWidth);
            item.buttonView.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, item.labelButtonView.preferredHeight + values.spacing);
            item.buttonView.SetAnchoredPositionY(string.IsNullOrEmpty(item.labelCompany.text)
                ? (item.buttonView.rect.height - item.extraInfoContainer.rect.height) / 2f
                : (item.buttonView.rect.height - item.extraInfoContainer.rect.height) / 2f - values.spacing / 3f);
        }

        public override void CalculateItemHeight(ListItemData itemData, float availableHeight, float width)
        {
            if (itemData.hidden)
            {
                SetItemHeight(itemData, 0f);
                return;
            }

            LayoutValue values = UiLayoutValueHandler.common.currentValue;

            float vSpace = values.padding * 2f + values.spacing * 3;
            float companyHeight = TextTester.GetPreferredHeight(TextTester.Font.Normal, values.textParagraphFontSize, 1);
            float titleHeight = TextTester.GetPreferredHeight(TextTester.Font.Semibold, values.textSubHeaderFontSize, 2);
            float dividerHeight = 2f;
            float extraInfoHeight = companyHeight;

            SetItemHeight(itemData, vSpace + companyHeight + titleHeight + dividerHeight + extraInfoHeight);
        }
    }
}
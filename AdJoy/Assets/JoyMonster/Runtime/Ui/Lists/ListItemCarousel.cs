﻿using JoyMonster.API.Model;
using JoyMonster.Core;
using MaterialUI_JoyMonster;
using RSG;
using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace JoyMonster.Ui.Lists
{
    public class ListItemCarousel : ListItemStandard
    {
        [SerializeField] private RectTransform m_ImageContainer = null;
        public RectTransform imageContainer { get { return m_ImageContainer; } }

        [SerializeField] private RectTransform m_InfoContainer = null;
        public RectTransform infoContainer { get { return m_InfoContainer; } }

        [SerializeField] private RectTransform m_MainInfoContainer = null;
        public RectTransform mainInfoContainer { get { return m_MainInfoContainer; } }

        [SerializeField] private RectTransform m_ExtraInfoContainer = null;
        public RectTransform extraInfoContainer { get { return m_ExtraInfoContainer; } }

        [SerializeField] private TextMeshProUGUI m_LabelCompany = null;
        public TextMeshProUGUI labelCompany { get { return m_LabelCompany; } }

        [SerializeField] private TextMeshProUGUI m_LabelTitle = null;
        public TextMeshProUGUI labelTitle { get { return m_LabelTitle; } }

        [SerializeField] private TextMeshProUGUI m_LabelDescription = null;
        public TextMeshProUGUI labelDescription { get { return m_LabelDescription; } }

        [SerializeField] private VectorImage m_IconDistance = null;
        public VectorImage iconDistance { get { return m_IconDistance; } }

        [SerializeField] private TextMeshProUGUI m_LabelDistance = null;
        public TextMeshProUGUI labelDistance { get { return m_LabelDistance; } }

        [SerializeField] private VectorImage m_IconExpiry = null;
        public VectorImage iconExpiry { get { return m_IconExpiry; } }

        [SerializeField] private TextMeshProUGUI m_LabelExpiry = null;
        public TextMeshProUGUI labelExpiry { get { return m_LabelExpiry; } }

        [SerializeField] private RawImageLayoutBehaviour m_ImageLogo = null;
        public RawImageLayoutBehaviour imageLogo { get { return m_ImageLogo; } }

        [SerializeField] private Graphic m_DividerCompany = null;
        public Graphic dividerCompany { get { return m_DividerCompany; } }

        [SerializeField] private Graphic m_DividerExtraInfo = null;
        public Graphic dividerExtraInfo { get { return m_DividerExtraInfo; } }

        [SerializeField] private RectTransform m_ButtonAdd = null;
        public RectTransform buttonAdd { get { return m_ButtonAdd; } }

        [SerializeField] private VectorImage m_IconButtonAdd = null;
        public VectorImage iconButtonAdd { get { return m_IconButtonAdd; } }

        [SerializeField] private TextMeshProUGUI m_LabelButtonAdd = null;
        public TextMeshProUGUI labelButtonAdd { get { return m_LabelButtonAdd; } }

        [SerializeField] private DynamicGraphic m_ImageLogoGraphic = null;
        [SerializeField] private RawImageLayoutBehaviour m_ImageLogoLayout = null;

        private Promise<Texture2D> m_ImagePromise;

        public override Type matchingDataType
        {
            get { return typeof(ListItemDataCarousel); }
        }

        public void LoadAndSetImage(string url)
        {
            m_ImageLogoLayout.SetTexture(null);
            m_ImageLogoGraphic.graphic.color = MaterialColor.dividerDark;

            m_ImagePromise = new Promise<Texture2D>((resolve, reject) =>
            {
                WebTextureHandler.GetTexture(url).Then(resolve);
            });

            if (m_ImagePromise.CurState == PromiseState.Resolved)
            {
                m_ImagePromise.Then(texture =>
                {
                    m_ImageLogoLayout.SetTexture(texture);
                    if (JoyMonsterManager.webTextureHandler.IsErrorTexture(texture))
                    {
                        m_ImageLogoGraphic.SetToCustom(MaterialColor.dividerDark);
                    }
                    else
                    {
                        m_ImageLogoGraphic.SetState(true);
                    }
                });
                return;
            }

            m_ImagePromise.Then(texture =>
            {
                m_ImageLogoLayout.SetTexture(texture);
                if (JoyMonsterManager.webTextureHandler.IsErrorTexture(texture))
                {
                    m_ImageLogoGraphic.TransitionToCustom(MaterialColor.dividerDark, Values.Duration.SHORT, 0f);
                }
                else
                {
                    m_ImageLogoGraphic.TransitionState(true, Values.Duration.SHORT, 0f);
                }
            });
        }

        public void OnButtonAddClick()
        {
            listView.interactionController.OnItemInteraction(ListInteraction.pointerClick, null, this, 1);
        }
    }

    public class ListItemDataCarousel : ListItemDataStandard
    {
        public EngagementResult engagement;

        public override ListItemController GetNewController()
        {
            return new ListItemControllerCarousel();
        }
    }

    public class ListItemControllerCarousel : ListItemController
    {
        public override string itemPrefabPath { get { return null; } }

        public override Type matchingDataType
        {
            get { return typeof(ListItemDataCarousel); }
        }

        public override float minItemWidth
        {
            get { return 240f; }
        }

        public override float minItemHeight
        {
            get { return 240f; }
        }

        private float m_DividerHeight = 2f;

        public override void FormatItemWithData(ListItem item, ListItemData data)
        {
            ListItemCarousel itemCarousel = (ListItemCarousel)item;
            ListItemDataCarousel dataCarousel = (ListItemDataCarousel)data;
            EngagementResult engagement = dataCarousel.engagement;

            itemCarousel.labelCompany.text = engagement.company;
            itemCarousel.labelTitle.text = engagement.title;
            itemCarousel.labelDescription.text = engagement.description;
            itemCarousel.labelDistance.text = EngagementFormatter.DisplayDistance(engagement);
            itemCarousel.labelExpiry.text = EngagementFormatter.DisplayExpiry(engagement);

            if (itemCarousel.labelDistance.text == "")
            {
                itemCarousel.iconDistance.enabled = false;
                itemCarousel.labelDistance.enabled = false;
            }
            else
            {
                itemCarousel.iconDistance.enabled = true;
                itemCarousel.labelDistance.enabled = true;
            }

            if (itemCarousel.labelExpiry.text == "")
            {
                itemCarousel.iconExpiry.enabled = false;
                itemCarousel.labelExpiry.enabled = false;
            }
            else
            {
                itemCarousel.iconExpiry.enabled = true;
                itemCarousel.labelExpiry.enabled = true;
            }

            itemCarousel.LoadAndSetImage(engagement.bannerUrl);

            SetLayout(itemCarousel, dataCarousel, m_ItemSizes[dataCarousel]);
        }

        private void SetLayout(ListItemCarousel item, ListItemDataCarousel data, Vector2 size)
        {
            LayoutValue values = UiLayoutValueHandler.common.currentValue;

            bool isLandscape = layoutController.listView.rectTransform.rect.width >
                               layoutController.listView.rectTransform.rect.height * 1.5f;

            item.labelCompany.fontSize = values.textParagraphFontSize;
            item.labelCompany.fontSizeMax = values.textParagraphFontSize;
            item.labelTitle.fontSize = values.textSubHeaderFontSize;
            item.labelTitle.fontSizeMax = values.textSubHeaderFontSize;
            item.labelDescription.fontSize = values.textParagraphFontSize;
            item.labelDescription.fontSizeMax = values.textParagraphFontSize;
            item.labelDistance.fontSize = values.textParagraphFontSize - 2f;
            item.iconDistance.size = values.textParagraphFontSize - 2f;
            item.labelExpiry.fontSize = values.textParagraphFontSize - 2f;
            item.iconExpiry.size = values.textParagraphFontSize - 2f;
            item.labelButtonAdd.fontSize = values.textButtonFontSize - 2f;
            item.iconButtonAdd.size = values.textButtonFontSize - 2f;

            if (isLandscape)
            {
                float imageContainerWidth = Mathf.Min(size.y - values.padding * 2f, size.x / 2f);
                item.imageContainer.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Left, values.padding, imageContainerWidth);
                item.infoContainer.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Right, values.padding, size.x - imageContainerWidth - values.padding * 3f);
            }
            else
            {
                item.infoContainer.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Left, values.padding, size.x - values.padding * 2f);
                item.imageContainer.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Left, values.padding, size.x - values.padding * 2f);
            }

            float bottomPadding = 0f;
            item.buttonAdd.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Bottom, bottomPadding,
                TextTester.GetPreferredHeight(TextTester.Font.Semibold, values.textButtonFontSize, 1) + values.spacing * 1.5f);
            bottomPadding += item.buttonAdd.rect.height + values.padding;

            item.dividerExtraInfo.rectTransform.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Bottom, bottomPadding, m_DividerHeight);
            bottomPadding += m_DividerHeight + values.padding;

            float posX = 0f;

            if (item.labelDistance.text != "")
            {
                item.iconDistance.rectTransform.SetSizeDeltaX(values.textParagraphFontSize);
                posX = item.iconDistance.rectTransform.rect.width + values.spacing / 2f;
                item.labelDistance.rectTransform.SetSizeDeltaX(item.labelDistance.preferredWidth);
                item.labelDistance.rectTransform.SetAnchoredPositionX(posX);
                posX += item.labelDistance.rectTransform.rect.width + values.spacing;
            }

            item.iconExpiry.rectTransform.SetSizeDeltaX(values.textParagraphFontSize);
            item.iconExpiry.rectTransform.SetAnchoredPositionX(posX);
            posX += item.iconExpiry.rectTransform.rect.width + values.spacing / 2f;
            item.labelExpiry.rectTransform.SetSizeDeltaX(item.labelExpiry.preferredWidth);
            item.labelExpiry.rectTransform.SetAnchoredPositionX(posX);

            if (item.labelDistance.text != "" || item.labelExpiry.text != "")
            {
                item.extraInfoContainer.gameObject.SetActive(true);

                float extraInfoContainerHeight = TextTester.GetPreferredHeight(TextTester.Font.Normal, values.textParagraphFontSize - 2f, 1);
                item.extraInfoContainer.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Bottom, bottomPadding, extraInfoContainerHeight);
                bottomPadding += extraInfoContainerHeight + values.spacing;
            }
            else
            {
                item.extraInfoContainer.gameObject.SetActive(false);
            }

            float mainInfoContainerHeight = 0f;
            item.labelDescription.rectTransform.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Bottom, mainInfoContainerHeight,
                TextTester.GetPreferredHeight(TextTester.Font.Normal, values.textParagraphFontSize, isLandscape ? 1 : 3));
            mainInfoContainerHeight += item.labelDescription.rectTransform.rect.height + values.spacing / 2f;
            item.labelTitle.rectTransform.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Bottom, mainInfoContainerHeight,
                TextTester.GetPreferredHeight(TextTester.Font.Semibold, values.textSubHeaderFontSize, 1));
            mainInfoContainerHeight += item.labelTitle.rectTransform.rect.height + values.spacing;
            item.labelCompany.rectTransform.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Bottom, mainInfoContainerHeight,
                TextTester.GetPreferredHeight(TextTester.Font.Semibold, values.textParagraphFontSize, 1));
            mainInfoContainerHeight += item.labelCompany.rectTransform.rect.height;

            item.mainInfoContainer.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Bottom, bottomPadding, mainInfoContainerHeight);
            bottomPadding += mainInfoContainerHeight + values.padding;

            item.imageContainer.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Top, values.padding,
                isLandscape ? item.imageContainer.rect.width : size.y - bottomPadding - values.padding * 2f);

            item.dividerCompany.rectTransform.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Right, 0f,
                item.labelCompany.rectTransform.rect.width - item.labelCompany.preferredWidth - values.spacing / 2f);

            //  Adjustments

            if (!isLandscape && item.labelDescription.preferredHeight < item.labelDescription.fontSize * 2f)
            {
                item.labelTitle.rectTransform.SetAnchoredPositionY(item.labelTitle.rectTransform.anchoredPosition.y - values.spacing);
            }
        }

        public override void CalculateItemWidth(ListItemData itemData, float availableWidth, float height)
        {
            bool isLandscape = layoutController.listView.rectTransform.rect.width >
                                       layoutController.listView.rectTransform.rect.height * 1.5f;

            float maxWidth = 600f;

            float screenBasedWidth = UiScaleController.screenSizeDp.x -
                                     UiLayoutValueHandler.common.currentValue.padding *
                                     (isLandscape ? 8f : 8f);

            SetItemWidth(itemData, Mathf.Min(screenBasedWidth, maxWidth));
        }

        public override void CalculateItemHeight(ListItemData itemData, float availableHeight, float width)
        {
            if (width == 0f)
            {
                width = GetItemWidth(itemData);
            }

            LayoutValue values = UiLayoutValueHandler.common.currentValue;

            bool isLandscape = layoutController.listView.rectTransform.rect.width >
                                       layoutController.listView.rectTransform.rect.height * 1.5f;

            float buttonHeight = TextTester.GetPreferredHeight(TextTester.Font.Semibold, values.textButtonFontSize - 2f, 1) + values.spacing * 1.5f;
            float extraInfoheight = TextTester.GetPreferredHeight(TextTester.Font.Normal, values.textParagraphFontSize - 2f, 1);
            float descriptionHeight = TextTester.GetPreferredHeight(TextTester.Font.Normal, values.textParagraphFontSize, isLandscape ? 1 : 3);
            float titleHeight = TextTester.GetPreferredHeight(TextTester.Font.Semibold, values.textSubHeaderFontSize, 1);
            float companyHeight = TextTester.GetPreferredHeight(TextTester.Font.Normal, values.textParagraphFontSize, 1);
            float space = values.padding * 3f + values.spacing * 3.5f;

            float height = buttonHeight + extraInfoheight + descriptionHeight + titleHeight + companyHeight + space;

            if (!isLandscape)
            {
                height += values.padding * 1.5f;
                height += (width - values.padding * 2f) / 2f;
            }

            SetItemHeight(itemData, itemData.hidden ? 0f : Mathf.Min(height, availableHeight));
        }
    }

    public class ListAnimationControllerCarousel : ListAnimationControllerStandard
    {
        public override void AnimateItemShow(ListItem item, int displayedIndex, Action onCompleted)
        {
            float delay = GetDelayFromIndex(displayedIndex * 2, true);

            ListCarouselExtension carouselExtension = null;

            foreach (IListExtension extension in listView.listExtensions)
            {
                carouselExtension = extension as ListCarouselExtension;
                if (carouselExtension != null) break;
            }

            if (carouselExtension != null)
            {
                carouselExtension.OnContentChanged();
            }

            Vector2 endPos = item.rectTransform.anchoredPosition;
            Vector2 startPos = endPos;

            if (m_LayoutDirection == ListLayoutControllerStandard.LayoutDirection.Horizontal)
            {
                startPos.y -= listView.rectTransform.rect.height;
            }
            else
            {
                startPos.x -= 24f;
            }

            item.rectTransform.anchoredPosition = startPos;

            AddItemTween(TweenManager.TweenVector2(vector2 => item.rectTransform.anchoredPosition = vector2,
                    startPos, endPos, m_AnimationDuration, delay),
                item, onCompleted, () => item.rectTransform.anchoredPosition = endPos);

            item.canvasGroup.alpha = 0f;

            AddItemTween(TweenManager.TweenFloat(f => item.canvasGroup.alpha = f,
                    0f, 1f, 0.5f, delay),
                item, null, () => item.canvasGroup.alpha = 1f);
        }
    }
}
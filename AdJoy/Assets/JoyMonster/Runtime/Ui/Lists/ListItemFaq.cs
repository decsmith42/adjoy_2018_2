﻿using JoyMonster.API.Model;
using MaterialUI_JoyMonster;
using System;
using TMPro;
using UnityEngine;

namespace JoyMonster.Ui.Lists
{
    public class ListItemFaq : ListItemStandard
    {
        [SerializeField] private TextMeshProUGUI m_LabelQuestion = null;

        public TextMeshProUGUI labelQuestion
        {
            get { return m_LabelQuestion; }
        }

        [SerializeField] private TextMeshProUGUI m_LabelAnswer = null;

        public TextMeshProUGUI labelAnswer
        {
            get { return m_LabelAnswer; }
        }

        [SerializeField] private VectorImage m_IconExpand = null;

        public VectorImage iconExpand
        {
            get { return m_IconExpand; }
        }

        [SerializeField] private DynamicGraphic m_LabelAnswerDynamicGraphic = null;

        public DynamicGraphic labelAnswerDynamicGraphic
        {
            get { return m_LabelAnswerDynamicGraphic; }
        }

        public override Type matchingDataType
        {
            get { return typeof(ListItemDataFaq); }
        }

        public ListItemDataFaq currentDataFaq
        {
            get { return m_CurrentData as ListItemDataFaq; }
        }
    }

    public class ListItemDataFaq : ListItemDataStandard
    {
        public Faq faq;
        public bool isSelected;

        public override ListItemController GetNewController()
        {
            return new ListItemControllerFaq();
        }
    }

    public class ListItemControllerFaq : ListItemController
    {
        public override string itemPrefabPath
        {
            get { return "ListItemFaq"; }
        }

        public override Type matchingDataType
        {
            get { return typeof(ListItemDataFaq); }
        }

        public override float minItemWidth
        {
            get { return 240f; }
        }

        public override float minItemHeight
        {
            get { return 40f; }
        }

        public override void FormatItemWithData(ListItem item, ListItemData data)
        {
            ListItemFaq itemFaq = (ListItemFaq)item;
            ListItemDataFaq dataFaq = (ListItemDataFaq)data;

            itemFaq.labelQuestion.text = dataFaq.faq.question;
            itemFaq.labelAnswer.text = dataFaq.faq.answer;

            itemFaq.labelAnswerDynamicGraphic.SetState(dataFaq.isSelected);
            itemFaq.iconExpand.rectTransform.localEulerAngles = new Vector3(0f, 0f, dataFaq.isSelected ? 0f : 90f);

            SetLayout(itemFaq, m_ItemSizes[dataFaq]);
        }

        private void SetLayout(ListItemFaq item, Vector2 size)
        {
            AppBarWidthValue widthValues = UiLayoutValueHandler.appBarWidth.currentValue;
            LayoutValue commonValues = UiLayoutValueHandler.common.currentValue;
            //float textWidth = size.x - commonValues.padding * 2f - 24f - commonValues.padding * 4f;
            float textWidth = size.x - widthValues.labelLeftPadding - commonValues.padding;

            item.labelQuestion.fontSize = commonValues.textSubHeaderFontSize;
            item.labelAnswer.fontSize = commonValues.textParagraphFontSize;

            //item.iconExpand.rectTransform.anchoredPosition = new Vector2(commonValues.padding * 2f, -commonValues.padding * 2f);
            item.iconExpand.rectTransform.anchoredPosition =
                new Vector2(widthValues.leftIconLeftPadding + 12f, -commonValues.padding * 2f - 12f);

            //item.labelQuestion.rectTransform.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Left, commonValues.padding * 4f + 24f, textWidth);
            item.labelQuestion.rectTransform.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Left,
                widthValues.labelLeftPadding, textWidth);
            //item.labelQuestion.rectTransform.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Top, commonValues.padding * 2f, item.labelQuestion.preferredHeight);
            item.labelQuestion.rectTransform.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Top,
                commonValues.padding * 2f, item.labelQuestion.preferredHeight);

            //item.labelAnswer.rectTransform.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Left, commonValues.padding * 2f, textWidth);
            item.labelAnswer.rectTransform.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Left,
                widthValues.labelLeftPadding, textWidth);
            //item.labelAnswer.rectTransform.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Top, commonValues.padding * 2f +  + item.labelQuestion.rectTransform.rect.height + commonValues.spacing * 2f, item.labelAnswer.preferredHeight);
            item.labelAnswer.rectTransform.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Top,
                commonValues.padding * 2f + item.labelQuestion.preferredHeight + commonValues.spacing * 2f,
                item.labelAnswer.preferredHeight);
        }

        public override void CalculateItemHeight(ListItemData itemData, float availableHeight, float width)
        {
            AppBarWidthValue widthValues = UiLayoutValueHandler.appBarWidth.currentValue;
            LayoutValue commonValues = UiLayoutValueHandler.common.currentValue;

            //float ySpace = values.padding * 4f;
            float ySpace = commonValues.padding * 4f;
            //float textWidth = width - values.padding * 2f - 24f - values.padding * 4f;
            float textWidth = width - widthValues.labelLeftPadding - commonValues.padding;
            float textheight = TextTester.GetPreferredHeight(TextTester.Font.Semibold, commonValues.textSubHeaderFontSize,
                textWidth, ((ListItemDataFaq)itemData).faq.question);

            if (((ListItemDataFaq)itemData).isSelected)
            {
                textheight += TextTester.GetPreferredHeight(TextTester.Font.Normal, commonValues.textParagraphFontSize,
                    textWidth, ((ListItemDataFaq)itemData).faq.answer);
                ySpace += commonValues.spacing * 2f;
            }

            SetItemHeight(itemData, textheight + ySpace);
        }

        public override void OnLayoutChange()
        {
            //m_ItemSizes.Clear();
        }

        public void ClearSizes()
        {
            m_ItemSizes.Clear();
        }
    }

    public class ListAnimationControllerFaq : ListAnimationControllerStandard
    {
        public void SelectItem(ListItemFaq listItem)
        {
            ListLayoutControllerStandard layoutController = (ListLayoutControllerStandard)listView.layoutController;
            int[] firstAndLastItems = layoutController.GetFirstAndLastItemDataIndexes();
            ListItemFaq prevListItem = null;

            //  Check/set selection state
            foreach (ListItemData data in listView.listData.listItems)
            {
                ListItemDataFaq dataFaq = (ListItemDataFaq)data;

                if (dataFaq == listItem.currentDataFaq)
                {
                    dataFaq.isSelected = !dataFaq.isSelected;
                }
                else
                {
                    if (dataFaq.isSelected)
                    {
                        prevListItem = dataFaq.currentItem as ListItemFaq;
                    }

                    dataFaq.isSelected = false;
                }
            }

            //  Refresh list layout values to reflect new selection
            ((ListItemControllerFaq)listView.layoutController.GetItemController(listItem.currentData)).ClearSizes();
            Vector2 listPos = listView.listContent.rectTransform.anchoredPosition;
            listView.layoutController.CacheListData();
            listView.layoutController.SetContentSize();
            listView.listContent.rectTransform.anchoredPosition = listPos;

            //  Contract previous selected item if visible
            if (prevListItem != null)
            {
                prevListItem.labelAnswerDynamicGraphic.TransitionState(false, Values.Duration.SHORT, 0f);

                float previousSelectedItemPrevHeight = prevListItem.rectTransform.rect.height;
                float previousSelectedItemNewHeight = layoutController
                    .cachedItemSizes[layoutController.GetDataIndexFromDisplayedItemData(prevListItem.currentDataFaq)].y;

                AddItemTween(TweenManager.TweenFloat(value => prevListItem.rectTransform.SetSizeDeltaY(value),
                        previousSelectedItemPrevHeight, previousSelectedItemNewHeight, Values.Duration.SHORT,
                        Values.Duration.SHORTER),
                    prevListItem, null, () => prevListItem.rectTransform.SetSizeDeltaY(previousSelectedItemNewHeight));

                AddItemTween(TweenManager.TweenFloat(
                        value => prevListItem.iconExpand.rectTransform.localEulerAngles = new Vector3(0f, 0f, value),
                        0f, 90f, Values.Duration.SHORT, Values.Duration.SHORTER),
                    prevListItem, null,
                    () => prevListItem.iconExpand.rectTransform.localEulerAngles = new Vector3(0f, 0f, 0f));
            }

            //  Expand/contract new selected item (always visible)
            listItem.labelAnswerDynamicGraphic.TransitionState(listItem.currentDataFaq.isSelected,
                Values.Duration.SHORT,
                listItem.currentDataFaq.isSelected ? Values.Duration.SHORTER : 0f);

            float listItemPrevHeight = listItem.rectTransform.rect.height;
            float listItemNewHeight = layoutController
                .cachedItemSizes[layoutController.GetDataIndexFromDisplayedItemData(listItem.currentDataFaq)].y;

            AddItemTween(TweenManager.TweenFloat(value => listItem.rectTransform.SetSizeDeltaY(value),
                    listItemPrevHeight, listItemNewHeight, Values.Duration.SHORT,
                    Values.Duration.SHORTER),
                listItem, null, () => listItem.rectTransform.SetSizeDeltaY(listItemNewHeight));

            float newAngle = listItem.currentDataFaq.isSelected ? 0f : 90f;

            AddItemTween(TweenManager.TweenFloat(
                    value => listItem.iconExpand.rectTransform.localEulerAngles = new Vector3(0f, 0f, value),
                    listItem.iconExpand.rectTransform.localEulerAngles.z, newAngle, Values.Duration.SHORT,
                    Values.Duration.SHORTER),
                listItem, null, () => listItem.iconExpand.rectTransform.localEulerAngles = new Vector3(0f, 0f, newAngle));

            //  Move all other visible items to new positions
            for (int i = firstAndLastItems[0]; i <= firstAndLastItems[1]; i++)
            {
                if (listView.listData.listItems[i].currentItem == null) continue;

                ListItem item = listView.listData.listItems[i].currentItem;
                float oldPos = item.rectTransform.anchoredPosition.y;
                float newPos = layoutController.cachedItemPositions[i].y;

                AddItemTween(TweenManager.TweenFloat(value => item.rectTransform.SetAnchoredPositionY(value),
                        oldPos, newPos, Values.Duration.SHORT, Values.Duration.SHORTER),
                    item, null, () => item.rectTransform.SetAnchoredPositionY(newPos));
            }
        }
    }

}
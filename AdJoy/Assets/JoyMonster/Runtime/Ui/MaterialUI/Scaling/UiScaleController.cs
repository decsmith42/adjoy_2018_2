﻿using System;
using UnityEngine;

namespace MaterialUI_JoyMonster
{
    public static class UiScaleController
    {
        [Serializable]
        public enum Orientation
        {
            Portrait,
            Landscape
        }

        private static float m_Dpi;
        public static float dpi
        {
            get { return m_Dpi; }
        }

        private static Vector2 m_ScreenSizePx;
        public static Vector2 screenSizePx
        {
            get { return m_ScreenSizePx; }
        }

        private static Vector2 m_ScreenSizeDp;
        public static Vector2 screenSizeDp
        {
            get { return m_ScreenSizeDp; }
        }

        private static float m_ScreenPhysicalDiagonal;
        public static float screenPhysicalDiagonal
        {
            get { return m_ScreenPhysicalDiagonal; }
        }

        private static Orientation m_Orientation;
        public static Orientation orientation
        {
            get { return m_Orientation; }
        }

        private static float m_Scale;
        public static float scale
        {
            get { return m_Scale; }
        }

        private static Action m_OnRefreshedEarly;
        public static Action onRefreshedEarly
        {
            get { return m_OnRefreshedEarly; }
            set { m_OnRefreshedEarly = value; }
        }

        private static Action m_OnRefreshed;
        public static Action onRefreshed
        {
            get { return m_OnRefreshed; }
            set { m_OnRefreshed = value; }
        }

        public static void Refresh(UiScaleHandler.ScaleSettings scaleSettings, Display display)
        {
            if (scaleSettings == null || display == null) return;

            //  DPI

            m_Dpi = Screen.dpi;

            if (Application.isEditor && scaleSettings.editorForceDpi && scaleSettings.editorForceDpiValue > scaleSettings.baseDpi / 50f)
            {
                m_Dpi = scaleSettings.editorForceDpiValue;
            }

            if (m_Dpi == 0f)
            {
                m_Dpi = scaleSettings.fallbackDpi;
            }

            //  SCREEN SIZE PX

            m_ScreenSizePx.x = display.renderingWidth;
            m_ScreenSizePx.y = display.renderingHeight;

            //  SCREEN PHYSICAL DIAGONAL

            m_ScreenPhysicalDiagonal = Mathf.Sqrt(Mathf.Pow(m_ScreenSizePx.x, 2f) + Mathf.Pow(m_ScreenSizePx.y, 2f)) / m_Dpi;

            //  ORIENTATION

            m_Orientation = m_ScreenSizePx.x >= m_ScreenSizePx.y ? Orientation.Landscape : Orientation.Portrait;

            //  SCALE TO TARGET MODIFIER

            float scaleToTargetModifier = 1f;

            if (scaleSettings.scaleToTargetSize)
            {
                if (m_ScreenPhysicalDiagonal >= scaleSettings.targetSizeUpper)
                {
                    scaleToTargetModifier =
                        scaleSettings.scaleToTargetFactor * (m_ScreenPhysicalDiagonal / scaleSettings.targetSizeUpper) +
                        (1f - scaleSettings.scaleToTargetFactor);
                }
                else if (m_ScreenPhysicalDiagonal < scaleSettings.targetSizeLower)
                {
                    scaleToTargetModifier = m_ScreenPhysicalDiagonal / scaleSettings.targetSizeLower;
                }
            }

            //  SCALE

            m_Scale = (m_Dpi / scaleSettings.baseDpi) * scaleToTargetModifier;

            //  SCREEN SIZE DP

            m_ScreenSizeDp = m_ScreenSizePx / m_Scale;

            if (m_OnRefreshedEarly != null)
            {
                m_OnRefreshedEarly.InvokeIfNotNull();
            }

            if (m_OnRefreshed != null)
            {
                m_OnRefreshed.Invoke();
            }
        }
    }
}
﻿using System;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace MaterialUI_JoyMonster
{
    [ExecuteInEditMode]
    public class UiScaleHandler : UIBehaviour
    {
        [Serializable]
        public class ScaleSettings
        {
            public float baseDpi;
            public float fallbackDpi;

            public bool scaleToTargetSize;
            public float scaleToTargetFactor;
            public float targetSizeLower;
            public float targetSizeUpper;

            public bool editorForceDpi;
            public float editorForceDpiValue;
        }

        [SerializeField]
        private Canvas m_Canvas;
        public Canvas canvas
        {
            get
            {
                if (m_Canvas == null)
                {
                    m_Canvas = GetComponent<Canvas>();
                }
                return m_Canvas;
            }
        }

        [SerializeField] private bool m_UpdateScaler = false;
        [SerializeField] private ScaleSettings m_ScaleSettings = new ScaleSettings();

        [SerializeField] private UnityEvent m_OnUpdateCanvas;
        private bool m_CurrentlyModifyingCanvas;

        public UnityEvent onUpdateCanvas
        {
            get { return m_OnUpdateCanvas; }
            set { m_OnUpdateCanvas = value; }
        }

        protected override void OnEnable()
        {
            if (!Application.isPlaying) return;

            UiScaleController.onRefreshed += UpdateCanvas;
        }

        protected override void OnDisable()
        {
            if (!Application.isPlaying) return;


            UiScaleController.onRefreshed -= UpdateCanvas;
        }

        protected override void Start()
        {
            if (!Application.isPlaying) return;


            //  It just works...
            OnRectTransformDimensionsChange();
            LayoutRebuilder.MarkLayoutForRebuild(canvas.GetComponent<RectTransform>());
            OnRectTransformDimensionsChange();
        }

        public void UpdateCanvas()
        {
            m_CurrentlyModifyingCanvas = true;
            canvas.scaleFactor = UiScaleController.scale;
            m_CurrentlyModifyingCanvas = false;

            if (m_OnUpdateCanvas != null)
            {
                m_OnUpdateCanvas.Invoke();
            }
        }

        protected override void OnRectTransformDimensionsChange()
        {
            if (!isActiveAndEnabled) return;

            if (m_CurrentlyModifyingCanvas) return;

            Refresh();
        }

        public void Refresh()
        {
            if (m_UpdateScaler)
            {
                UiScaleController.Refresh(m_ScaleSettings, Display.displays[canvas.targetDisplay]);
            }
            else
            {
                if (m_OnUpdateCanvas != null)
                {
                    m_OnUpdateCanvas.Invoke();
                }
            }
        }
    }
}
//  Copyright 2017 MaterialUI for Unity http://materialunity.com
//  Please see license file for terms and conditions of use, and more information.

using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace MaterialUI_JoyMonster
{
    [ExecuteInEditMode]
    [AddComponentMenu("MaterialUI/Vector Image", 50)]
    public class VectorImage : Text
    {
        [Serializable]
        public struct Corners
        {
            public float bottomLeft;
            public float topLeft;
            public float topRight;
            public float bottomRight;

            public Corners(float size)
            {
                bottomLeft = size;
                topLeft = size;
                topRight = size;
                bottomRight = size;
            }
        }

        [Serializable]
        public struct Corners2
        {
            public Vector2 bottomLeft;
            public Vector2 topLeft;
            public Vector2 topRight;
            public Vector2 bottomRight;

            public Corners2(Vector2 size)
            {
                bottomLeft = size;
                topLeft = size;
                topRight = size;
                bottomRight = size;
            }

            public Corners2(Corners corners)
            {
                bottomLeft = new Vector2(corners.bottomLeft, corners.bottomLeft);
                topLeft = new Vector2(corners.topLeft, corners.topLeft);
                topRight = new Vector2(corners.topRight, corners.topRight);
                bottomRight = new Vector2(corners.bottomRight, corners.bottomRight);
            }
        }

        [SerializeField] private Corners m_CornerSizes;
        public Corners cornerSizes
        {
            get { return m_CornerSizes; }
            set
            {
                m_CornerSizes = value;
                SetVerticesDirty();
            }
        }

        [SerializeField] private Corners2 m_Corner2Sizes;
        public Corners2 corner2Sizes
        {
            get { return m_Corner2Sizes; }
            set
            {
                m_Corner2Sizes = value;
                SetVerticesDirty();
            }
        }



        [SerializeField] private bool m_UseCorners2 = false;
        public bool useCorners2
        {
            get { return m_UseCorners2; }
            set
            {
                m_UseCorners2 = value;
                SetVerticesDirty();
            }
        }

        [SerializeField] private bool m_Sliced;
        public bool sliced
        {
            get { return m_Sliced; }
            set
            {
                m_Sliced = value;
                SetVerticesDirty();
            }
        }

        public enum SizeMode
        {
            Manual,
            MatchWidth,
            MatchHeight,
            MatchMin,
            MatchMax
        }

        [SerializeField]
        private float m_Size = 48;
        public float size
        {
            get { return m_Size; }
            set
            {
                m_Size = value;
                RefreshScale();
            }
        }

        [SerializeField]
        private float m_ScaledSize;
        public float scaledSize
        {
            get { return m_ScaledSize; }
        }

        [SerializeField]
        private SizeMode m_SizeMode = SizeMode.MatchMin;
        public SizeMode sizeMode
        {
            get { return m_SizeMode; }
            set
            {
                m_SizeMode = value;
                //m_Tracker.Clear();
                RefreshScale();
                SetLayoutDirty();
            }
        }

        [SerializeField] private float m_RotateMeshPos = 0f;
        public float rotateMeshPos
        {
            get { return m_RotateMeshPos; }
            set
            {
                m_RotateMeshPos = value;
                SetVerticesDirty();
            }
        }

        [SerializeField] private int m_RotateMeshUv = 0;
        public int rotateMeshUv
        {
            get { return m_RotateMeshUv; }
            set
            {
                m_RotateMeshUv = value;
                SetVerticesDirty();
            }
        }

        [SerializeField] private bool m_GradientColor;
        public bool gradientColor
        {
            get { return m_GradientColor; }
            set
            {
                m_GradientColor = value;
                SetVerticesDirty();
            }
        }

        [SerializeField] private bool m_HorizontalGradient;
        public bool horizontalGradient
        {
            get { return m_HorizontalGradient; }
            set
            {
                m_HorizontalGradient = value;
                SetVerticesDirty();
            }
        }

        [SerializeField] private Color m_GradientStart = MaterialColor.grey200;
        public Color gradientStart
        {
            get { return m_GradientStart; }
            set
            {
                m_GradientStart = value;
                SetVerticesDirty();
            }
        }

        [SerializeField] private Color m_GradientEnd = MaterialColor.grey800;
        public Color gradientEnd
        {
            get { return m_GradientEnd; }
            set
            {
                m_GradientEnd = value;
                SetVerticesDirty();
            }
        }

        [SerializeField]
        private Tween.TweenType m_GradientBias1 = Tween.TweenType.Linear;
        public Tween.TweenType gradientBias1
        {
            get { return m_GradientBias1; }
            set
            {
                m_GradientBias1 = value;
                SetVerticesDirty();
            }
        }

        [SerializeField]
        private Tween.TweenType m_GradientBias2 = Tween.TweenType.Linear;
        public Tween.TweenType gradientBias2
        {
            get { return m_GradientBias2; }
            set
            {
                m_GradientBias2 = value;
                SetVerticesDirty();
            }
        }

        [SerializeField] private bool m_FillCenter = true;
        public bool fillCenter
        {
            get { return m_FillCenter; }
            set
            {
                m_FillCenter = value;
                SetVerticesDirty();
            }
        }

        private Canvas m_RootCanvas;
        private Canvas rootCanvas
        {
            get
            {
                if (m_RootCanvas == null)
                {
                    UiScaleHandler scaleHandler = GetComponentInParent<UiScaleHandler>();
                    if (scaleHandler != null)
                    {
                        m_RootCanvas = scaleHandler.GetComponent<Canvas>();
                    }
                }

                if (m_RootCanvas == null)
                {
                    Canvas[] parentCanvasses = GetComponentsInParent<Canvas>();
                    foreach (Canvas parentCanvas in parentCanvasses)
                    {
                        if (parentCanvas.transform.parent == null)
                        {
                            m_RootCanvas = parentCanvas;
                            break;
                        }
                    }
                }

                return m_RootCanvas;
            }
        }

        [SerializeField]
        private VectorImageData m_VectorImageData = new VectorImageData();
        public VectorImageData vectorImageData
        {
            get { return m_VectorImageData; }
            set
            {
                m_VectorImageData = value;
                updateFontAndText();

                RefreshScale();
            }
        }

        private bool m_DisableDirty;
        private float m_LocalScaleFactor;
        private readonly UIVertex[] m_TempVerts = new UIVertex[4];

        public void Refresh()
        {
            updateFontAndText();
            RefreshScale();
        }

        #region TextUnicode

        protected override void OnPopulateMesh(VertexHelper vertexHelper)
        {
            if (fontSize == 0)
            {
                vertexHelper.Clear();
                return;
            }

            m_DisableDirty = true;

            if (font != null)
            {
                m_DisableFontTextureRebuiltCallback = true;
                cachedTextGenerator.Populate(IconDecoder.Decode(text), GetGenerationSettings(rectTransform.rect.size));

                if (m_Sliced)
                {
                    PopulateMeshSliced(vertexHelper);
                }
                else
                {
                    vertexHelper.Clear();
                    float scale = 1f / pixelsPerUnit;
                    IList<UIVertex> verts = cachedTextGenerator.verts;

                    for (int i = 0; i < 4; i++)
                    {
                        m_TempVerts[i] = verts[i];
                        m_TempVerts[i].position = PixelAdjustPoint(m_TempVerts[i].position * scale);

                        if (rotateMeshPos != 0f)
                        {
                            m_TempVerts[i].position = RotateAroundPoint(m_TempVerts[i].position, rectTransform.rect.center, m_RotateMeshPos);
                        }
                    }

                    GenerateQuad(vertexHelper,
                        m_TempVerts[0].position, m_TempVerts[1].position, m_TempVerts[2].position, m_TempVerts[3].position,
                        m_TempVerts[0].uv0, m_TempVerts[1].uv0, m_TempVerts[2].uv0, m_TempVerts[3].uv0);

                    vertexHelper.AddTriangle(0, 1, 2);
                    vertexHelper.AddTriangle(2, 3, 0);
                }

                m_DisableFontTextureRebuiltCallback = false;
            }

            m_DisableDirty = false;
        }

        private Color GetVertexColor(Vector2 position)
        {
            if (!m_GradientColor || m_GradientBias1 == Tween.TweenType.Custom) return color;

            float startPoint = m_HorizontalGradient ? rectTransform.rect.xMin : rectTransform.rect.yMin;
            float endPoint = m_HorizontalGradient ? rectTransform.rect.xMax : rectTransform.rect.yMax;
            float vertPoint = m_HorizontalGradient ? position.x : position.y;

            vertPoint = Tween.Evaluate(m_GradientBias1, 0f, 1f, vertPoint - startPoint, endPoint - startPoint);

            if (m_GradientBias2 != Tween.TweenType.Linear && m_GradientBias2 != Tween.TweenType.Custom)
            {
                vertPoint = Tween.Evaluate(m_GradientBias2, 0f, 1f, vertPoint, 1f);
            }

            return Tween.Linear(
                m_HorizontalGradient ? m_GradientStart : m_GradientEnd,
                m_HorizontalGradient ? m_GradientEnd : m_GradientStart,
                vertPoint, 1f);
        }

        public bool m_HalveUvVerticalTop = false;
        public bool m_HalveUvVerticalBottom = false;
        public int m_LimitToRectSize = 0;

        private void PopulateMeshSliced(VertexHelper vertexHelper)
        {
            vertexHelper.Clear();

            Vector2 vert0 = cachedTextGenerator.verts[0].uv0;
            Vector2 vert1 = cachedTextGenerator.verts[1].uv0;
            Vector2 vert2 = cachedTextGenerator.verts[2].uv0;
            Vector2 vert3 = cachedTextGenerator.verts[3].uv0;

            Rect transformRect = RectTransformUtility.PixelAdjustRect(rectTransform, rootCanvas);

            Corners2 insets = m_UseCorners2 ? m_Corner2Sizes : new Corners2(m_CornerSizes);

            if (m_LimitToRectSize == 1)
            {
                insets.bottomLeft.x = Mathf.Min(insets.bottomLeft.x, transformRect.width / 2f);
                insets.bottomLeft.y = Mathf.Min(insets.bottomLeft.y, transformRect.height / 2f);
                insets.topLeft.x = Mathf.Min(insets.topLeft.x, transformRect.width / 2f);
                insets.topLeft.y = Mathf.Min(insets.topLeft.y, transformRect.height / 2f);
                insets.bottomRight.x = Mathf.Min(insets.bottomRight.x, transformRect.width / 2f);
                insets.bottomRight.y = Mathf.Min(insets.bottomRight.y, transformRect.height / 2f);
                insets.topRight.x = Mathf.Min(insets.topRight.x, transformRect.width / 2f);
                insets.topRight.y = Mathf.Min(insets.topRight.y, transformRect.height / 2f);
            }
            else if (m_LimitToRectSize == 2)
            {
                float minSize = Mathf.Min(transformRect.width, transformRect.height) / 2f;

                insets.bottomLeft.x = Mathf.Min(insets.bottomLeft.x, minSize);
                insets.bottomLeft.y = Mathf.Min(insets.bottomLeft.y, minSize);
                insets.topLeft.x = Mathf.Min(insets.topLeft.x, minSize);
                insets.topLeft.y = Mathf.Min(insets.topLeft.y, minSize);
                insets.bottomRight.x = Mathf.Min(insets.bottomRight.x, minSize);
                insets.bottomRight.y = Mathf.Min(insets.bottomRight.y, minSize);
                insets.topRight.x = Mathf.Min(insets.topRight.x, minSize);
                insets.topRight.y = Mathf.Min(insets.topRight.y, minSize);
            }

            Vector2 posOuterBottomLeft = new Vector2(transformRect.xMin, transformRect.yMin);
            Vector2 posOuterTopLeft = new Vector2(transformRect.xMin, transformRect.yMax);
            Vector2 posOuterTopRight = new Vector2(transformRect.xMax, transformRect.yMax);
            Vector2 posOuterBottomRight = new Vector2(transformRect.xMax, transformRect.yMin);

            Vector2 posInnerBottomLeft = new Vector2(transformRect.xMin + insets.bottomLeft.x, transformRect.yMin + insets.bottomLeft.y);
            Vector2 posInnerTopLeft = new Vector2(transformRect.xMin + insets.topLeft.x, transformRect.yMax - insets.topLeft.y);
            Vector2 posInnerTopRight = new Vector2(transformRect.xMax - insets.topRight.x, transformRect.yMax - insets.topRight.y);
            Vector2 posInnerBottomRight = new Vector2(transformRect.xMax - insets.bottomRight.x, transformRect.yMin + insets.bottomRight.y);

            Vector2 posTopLeft = new Vector2(posInnerTopLeft.x, posOuterTopLeft.y);
            Vector2 posTopRight = new Vector2(posInnerTopRight.x, posOuterTopRight.y);
            Vector2 posRightTop = new Vector2(posOuterTopRight.x, posInnerTopRight.y);
            Vector2 posRightBottom = new Vector2(posOuterTopRight.x, posInnerBottomRight.y);

            Vector2 posBottomLeft = new Vector2(posInnerBottomLeft.x, posOuterBottomLeft.y);
            Vector2 posBottomRight = new Vector2(posInnerBottomRight.x, posOuterBottomRight.y);
            Vector2 posLeftTop = new Vector2(posOuterTopLeft.x, posInnerTopLeft.y);
            Vector2 posLeftBottom = new Vector2(posOuterBottomLeft.x, posInnerBottomLeft.y);

            Vector2 uvBottomLeft = vert0;
            Vector2 uvTopLeft = vert1;
            Vector2 uvTopRight = vert2;
            Vector2 uvBottomRight = vert3;

            for (int i = 0; i < m_RotateMeshUv; i++)
            {
                Vector2 temp = uvBottomLeft;
                uvBottomLeft = uvTopLeft;
                uvTopLeft = uvTopRight;
                uvTopRight = uvBottomRight;
                uvBottomRight = temp;
            }

            if (m_HalveUvVerticalBottom)
            {
                uvTopLeft.x = (uvTopLeft.x + uvBottomLeft.x) / 2f;
                uvTopRight.x = (uvTopRight.x + uvBottomRight.x) / 2f;
                uvTopLeft.x = (uvTopLeft.x + uvBottomLeft.x) / 2f;
                uvTopRight.x = (uvTopRight.x + uvBottomRight.x) / 2f;
            }

            Vector2 uvTop = new Vector2((uvTopLeft.x + uvTopRight.x) / 2f, (uvTopLeft.y + uvTopRight.y) / 2f);
            Vector2 uvRight = new Vector2((uvTopRight.x + uvBottomRight.x) / 2f, (uvTopRight.y + uvBottomRight.y) / 2f);
            Vector2 uvBottom = new Vector2((uvBottomLeft.x + uvBottomRight.x) / 2f, (uvBottomLeft.y + uvBottomRight.y) / 2f);
            Vector2 uvLeft = new Vector2((uvTopLeft.x + uvBottomLeft.x) / 2f, (uvTopLeft.y + uvBottomLeft.y) / 2f);

            Vector2 uvCenter = new Vector2((uvLeft.x + uvRight.x) / 2f, (uvTop.y + uvBottom.y) / 2f);

            if (m_RotateMeshPos != 0f)
            {
                posOuterBottomLeft = RotateAroundPoint(posOuterBottomLeft, transformRect.center, m_RotateMeshPos);
                posOuterTopLeft = RotateAroundPoint(posOuterTopLeft, transformRect.center, m_RotateMeshPos);
                posOuterTopRight = RotateAroundPoint(posOuterTopRight, transformRect.center, m_RotateMeshPos);
                posOuterBottomRight = RotateAroundPoint(posOuterBottomRight, transformRect.center, m_RotateMeshPos);

                posInnerBottomLeft = RotateAroundPoint(posInnerBottomLeft, transformRect.center, m_RotateMeshPos);
                posInnerTopLeft = RotateAroundPoint(posInnerTopLeft, transformRect.center, m_RotateMeshPos);
                posInnerTopRight = RotateAroundPoint(posInnerTopRight, transformRect.center, m_RotateMeshPos);
                posInnerBottomRight = RotateAroundPoint(posInnerBottomRight, transformRect.center, m_RotateMeshPos);

                posTopLeft = RotateAroundPoint(posTopLeft, transformRect.center, m_RotateMeshPos);
                posTopRight = RotateAroundPoint(posTopRight, transformRect.center, m_RotateMeshPos);
                posRightTop = RotateAroundPoint(posRightTop, transformRect.center, m_RotateMeshPos);
                posRightBottom = RotateAroundPoint(posRightBottom, transformRect.center, m_RotateMeshPos);

                posBottomLeft = RotateAroundPoint(posBottomLeft, transformRect.center, m_RotateMeshPos);
                posBottomRight = RotateAroundPoint(posBottomRight, transformRect.center, m_RotateMeshPos);
                posLeftTop = RotateAroundPoint(posLeftTop, transformRect.center, m_RotateMeshPos);
                posLeftBottom = RotateAroundPoint(posLeftBottom, transformRect.center, m_RotateMeshPos);
            }

            GenerateQuad(vertexHelper, posOuterBottomLeft, posLeftBottom, posInnerBottomLeft, posBottomLeft,
                uvBottomLeft, uvLeft, uvCenter, uvBottom);

            GenerateQuad(vertexHelper, posLeftTop, posOuterTopLeft, posTopLeft, posInnerTopLeft,
                uvLeft, uvTopLeft, uvTop, uvCenter);

            GenerateQuad(vertexHelper, posInnerTopRight, posTopRight, posOuterTopRight, posRightTop,
                uvCenter, uvTop, uvTopRight, uvRight);

            GenerateQuad(vertexHelper, posBottomRight, posInnerBottomRight, posRightBottom, posOuterBottomRight,
                uvBottom, uvCenter, uvRight, uvBottomRight);


            vertexHelper.AddTriangle(0, 1, 2);
            vertexHelper.AddTriangle(2, 3, 0);

            vertexHelper.AddTriangle(4, 5, 6);
            vertexHelper.AddTriangle(6, 7, 4);

            vertexHelper.AddTriangle(8, 9, 10);
            vertexHelper.AddTriangle(10, 11, 8);

            vertexHelper.AddTriangle(12, 13, 14);
            vertexHelper.AddTriangle(14, 15, 12);

            vertexHelper.AddTriangle(1, 4, 7);
            vertexHelper.AddTriangle(7, 2, 1);

            vertexHelper.AddTriangle(7, 6, 9);
            vertexHelper.AddTriangle(9, 8, 7);

            vertexHelper.AddTriangle(13, 8, 11);
            vertexHelper.AddTriangle(11, 14, 13);

            vertexHelper.AddTriangle(3, 2, 13);
            vertexHelper.AddTriangle(13, 12, 3);

            if (m_FillCenter)
            {
                vertexHelper.AddTriangle(2, 7, 8);
                vertexHelper.AddTriangle(8, 13, 2);
            }
        }

        private Vector2 RotateAroundPoint(Vector2 point, Vector2 origin, float angle)
        {
            float angleInRadians = angle * (Mathf.PI / 180f);
            float cosTheta = Mathf.Cos(angleInRadians);
            float sinTheta = Mathf.Sin(angleInRadians);
            return new Vector2
            {
                x = (cosTheta * (point.x - origin.x) - sinTheta * (point.y - origin.y) + origin.x),
                y = (sinTheta * (point.x - origin.x) + cosTheta * (point.y - origin.y) + origin.y)
            };
        }

        private void GenerateQuad(VertexHelper vertexHelper, Vector2 pos0, Vector2 pos1, Vector2 pos2, Vector2 pos3, Vector2 uv0, Vector2 uv1, Vector2 uv2, Vector2 uv3)
        {
            UIVertex vert = UIVertex.simpleVert;

            //  0 0 0 0
            //  0 0 0 0
            //  0 0 0 0
            //  1 0 0 0
            vert.position = pos0;
            vert.uv0 = uv0;
            vert.color = GetVertexColor(pos0);
            vertexHelper.AddVert(vert);

            //  0 0 0 0
            //  0 0 0 0
            //  1 0 0 0
            //  0 0 0 0
            vert.position = pos1;
            vert.uv0 = uv1;
            vert.color = GetVertexColor(pos1);
            vertexHelper.AddVert(vert);

            //  0 0 0 0
            //  0 0 0 0
            //  0 1 0 0
            //  0 0 0 0
            vert.position = pos2;
            vert.uv0 = uv2;
            vert.color = GetVertexColor(pos2);
            vertexHelper.AddVert(vert);

            //  0 0 0 0
            //  0 0 0 0
            //  0 0 0 0
            //  0 1 0 0
            vert.position = pos3;
            vert.uv0 = uv3;
            vert.color = GetVertexColor(pos3);
            vertexHelper.AddVert(vert);
        }

        public override void SetLayoutDirty()
        {
            if (m_DisableDirty) return;

            base.SetLayoutDirty();
        }

        public override void SetVerticesDirty()
        {
            if (m_DisableDirty) return;

            base.SetVerticesDirty();
        }

        public override void SetMaterialDirty()
        {
            if (m_DisableDirty) return;

            base.SetMaterialDirty();
        }

        #endregion

        protected override void OnEnable()
        {
            base.OnEnable();

            alignment = TextAnchor.MiddleCenter;
            horizontalOverflow = HorizontalWrapMode.Overflow;
            verticalOverflow = VerticalWrapMode.Overflow;

            updateFontAndText();

            SetAllDirty();
        }

        protected override void Start()
        {
            alignment = TextAnchor.MiddleCenter;
            horizontalOverflow = HorizontalWrapMode.Overflow;
            verticalOverflow = VerticalWrapMode.Overflow;

            updateFontAndText();

            SetAllDirty();
            UpdateMaterial();
            UpdateGeometry();
        }

        private void updateFontAndText()
        {
            if (vectorImageData != null)
            {
                font = vectorImageData.font;
                text = vectorImageData.glyph.unicode;
            }
        }

        private void RefreshScale()
        {
            if (PrefabUtil.IsPrefab(gameObject)) return;

            if (!enabled) return;

            if (m_Sliced)
            {
                m_SizeMode = SizeMode.Manual;
            }

            if (size == 0 && sizeMode == SizeMode.Manual)
            {
                fontSize = 0;
                return;
            }

            float tempSize = size;

            if (m_Sliced)
            {
                tempSize *= 2f;
            }

            if (sizeMode == SizeMode.MatchWidth)
            {
                m_ScaledSize = rectTransform.rect.width;
                tempSize = m_ScaledSize;
            }
            else if (sizeMode == SizeMode.MatchHeight)
            {
                m_ScaledSize = rectTransform.rect.height;
                tempSize = m_ScaledSize;
            }
            else if (sizeMode == SizeMode.MatchMin)
            {
                Vector2 tempVector2 = new Vector2(rectTransform.rect.width, rectTransform.rect.height);

                m_ScaledSize = Mathf.Min(tempVector2.x, tempVector2.y);
                tempSize = m_ScaledSize;
            }
            else if (sizeMode == SizeMode.MatchMax)
            {
                Vector2 tempVector2 = new Vector2(rectTransform.rect.width, rectTransform.rect.height);

                m_ScaledSize = Mathf.Max(tempVector2.x, tempVector2.y);
                tempSize = m_ScaledSize;
            }

            fontSize = Mathf.CeilToInt(tempSize);
        }

        public override void CalculateLayoutInputHorizontal()
        {
            RefreshScale();
        }

        public override void CalculateLayoutInputVertical()
        {
            RefreshScale();
        }

        protected override void OnRectTransformDimensionsChange()
        {
            base.OnRectTransformDimensionsChange();
            RefreshScale();
        }

        public override float preferredWidth { get { return size; } }
        public override float minWidth { get { return -1; } }
        public override float flexibleWidth { get { return -1; } }
        public override float preferredHeight { get { return size; } }
        public override float minHeight { get { return -1; } }
        public override float flexibleHeight { get { return -1; } }
    }
}
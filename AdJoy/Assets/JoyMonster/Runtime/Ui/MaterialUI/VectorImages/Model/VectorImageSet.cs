﻿//  Copyright 2017 MaterialUI for Unity http://materialunity.com
//  Please see license file for terms and conditions of use, and more information.

using System;
using System.Collections.Generic;
using UnityEngine;

namespace MaterialUI_JoyMonster
{
    [Serializable]
    public class VectorImageSet
    {
        [SerializeField]
        private List<Glyph> m_IconGlyphList;
        public List<Glyph> iconGlyphList
        {
            get { return m_IconGlyphList; }
            set { m_IconGlyphList = value; }
        }

        public VectorImageSet()
        {
            m_IconGlyphList = new List<Glyph>();
        }
    }
}

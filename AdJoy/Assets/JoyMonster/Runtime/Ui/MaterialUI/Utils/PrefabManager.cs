﻿//  Copyright 2017 MaterialUI for Unity http://materialunity.com
//  Please see license file for terms and conditions of use, and more information.

using System.Collections.Generic;
using UnityEngine;

namespace MaterialUI_JoyMonster
{
    /// <summary>
    /// Static class to help with the instantiation of MaterialUI objects.
    /// </summary>
    public static class PrefabManager
    {
        private static readonly Dictionary<string, GameObject> m_PrefabDictionary = new Dictionary<string, GameObject>();

        /// <summary>
        /// Finds the GameObject with the matching name in the object pool, or if not pooled, from the path.
        /// </summary>
        /// <param name="nameWithPath">The name of the prefab, including the path.</param>
        /// <returns>The uninstantiated GameObject that matches the path, if found. If no GameObject is found, returns null.</returns>
        public static GameObject GetGameObject(string nameWithPath)
        {
            GameObject gameObject = null;

            if (!m_PrefabDictionary.ContainsKey(nameWithPath))
            {
                gameObject = Resources.Load<GameObject>(nameWithPath);

                if (gameObject != null)
                {
                    m_PrefabDictionary.Add(nameWithPath, gameObject);
                }
            }
            else
            {
                gameObject = m_PrefabDictionary[nameWithPath];
            }

            return gameObject;
        }

        /// <summary>
        /// Finds the GameObject with the matching name in the object pool, or if not pooled, from the path, then instantiates it.
        /// </summary>
        /// <param name="nameWithPath">The name of the prefab, including the path.</param>
        /// <param name="parent">The transform to set the parent of the instantiated GameObject.</param>
        /// <returns>The instantiated GameObject that matches the path, if found. If no GameObject is found, returns null.</returns>
        public static GameObject InstantiateGameObject(string nameWithPath, Transform parent)
        {
            GameObject go = GetGameObject(nameWithPath);

            if (go == null)
            {
                return null;
            }

            go = Object.Instantiate(go);

            if (parent == null)
            {
                return go;
            }

            go.transform.SetParent(parent);
            go.transform.localScale = Vector3.one;
            go.transform.localEulerAngles = Vector3.zero;
            go.transform.localPosition = Vector3.zero;

            return go;
        }
    }
}
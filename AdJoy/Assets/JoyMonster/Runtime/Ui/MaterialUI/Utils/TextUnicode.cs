﻿//  Copyright 2017 MaterialUI for Unity http://materialunity.com
//  Please see license file for terms and conditions of use, and more information.

using System.Collections.Generic;
using System.Globalization;
using System.Text.RegularExpressions;

namespace MaterialUI_JoyMonster
{
    /// <summary>
    /// Static class to decode icon codes to display as icons.
    /// </summary>
    public static class IconDecoder
    {
        /// <summary>
        /// The reg expression used to decode the icon codes.
        /// </summary>
        private static Regex m_RegExpression = new Regex(@"\\u(?<Value>[a-zA-Z0-9]{4})");

        private static Dictionary<string, string> s_DecodeCache = new Dictionary<string, string>();

        /// <summary>
        /// Decodes the specified code.
        /// </summary>
        /// <param name="value">The icon code to decode.</param>
        /// <returns>A string that can be displayed as an icon (with the right font).</returns>
        public static string Decode(string value)
        {
            if (string.IsNullOrEmpty(value)) return "";

            if (s_DecodeCache.ContainsKey(value))
            {
                return s_DecodeCache[value];
            }

            s_DecodeCache.Add(value, DecodeString(value));
            return s_DecodeCache[value];
        }

        private static string DecodeString(string value)
        {
            return m_RegExpression.Replace(value, m => ((char)int.Parse(m.Groups["Value"].Value, NumberStyles.HexNumber)).ToString());
        }
    }
}
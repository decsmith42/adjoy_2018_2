﻿using System;
using UnityEngine;

namespace MaterialUI_JoyMonster
{
    #region CanvasGroupController

    [Serializable]
    public class UiCanvasGroup
    {
        private int m_TweenId;
        //private bool m_ValuesCached;
        private float m_DefaultAlpha;

        [SerializeField]
        private CanvasGroup m_CanvasGroup;
        public CanvasGroup canvasGroup
        {
            get { return m_CanvasGroup; }
            set
            {
                m_CanvasGroup = value;
                CacheValues();
            }
        }

        public UiCanvasGroup() { }

        public UiCanvasGroup(CanvasGroup canvasGroup)
        {
            m_CanvasGroup = canvasGroup;
            CacheValues();
        }

        public void SetDefaultAlpha(float alpha)
        {
            CacheValues();
            m_DefaultAlpha = alpha;
        }

        /// <summary>
        /// Sets the default alpha.
        /// </summary>
        public void SetAlpha()
        {
            SetAlpha(m_DefaultAlpha);
        }

        public void SetAlpha(float alpha)
        {
            m_CanvasGroup.alpha = alpha;
        }

        public void SetAlpha(float alpha, bool interactable, bool blockRaycasts)
        {
            SetAlpha(alpha);
            m_CanvasGroup.interactable = interactable;
            m_CanvasGroup.blocksRaycasts = blockRaycasts;
        }

        /// <summary>
        /// Tweens to the default alpha.
        /// </summary>
        public void TweenAlpha(float duration, float delay, Tween.TweenType tweenType, Action onDone = null)
        {
            TweenAlpha(m_DefaultAlpha, duration, delay, tweenType, onDone);
        }

        public void TweenAlpha(float alpha, float duration, float delay, Tween.TweenType tweenType, Action onDone = null)
        {
            TweenManager.EndTween(m_TweenId);

            m_TweenId = TweenManager.TweenFloat(value => m_CanvasGroup.alpha = value,
                m_CanvasGroup.alpha, alpha, duration, delay, onDone, false, tweenType);
        }

        public void TweenAlpha(float startAlpha, float endAlpha, float duration, float delay, Tween.TweenType tweenType, Action onDone = null)
        {
            TweenManager.EndTween(m_TweenId);

            SetAlpha(startAlpha);
            TweenAlpha(endAlpha, duration, delay, tweenType, onDone);
        }

        private void CacheValues()
        {
            //m_ValuesCached = true;
            m_DefaultAlpha = m_CanvasGroup.alpha;
        }
    }

    #endregion

    #region RectTransformPosition

    [Serializable]
    public enum Direction
    {
        Middle,
        Up,
        Right,
        Down,
        Left
    }

    [Serializable]
    public class UiTransformPosition
    {
        [SerializeField]
        private RectTransform m_RectTransform;
        public RectTransform rectTransform
        {
            get { return m_RectTransform; }
            set
            {
                m_RectTransform = value;
                CacheValues();
            }
        }

        private UiTransformPositionData m_PositionData;
        public UiTransformPositionData positionData
        {
            get
            {
                if (!m_ValuesCached)
                {
                    CacheValues();
                }

                return m_PositionData;
            }
        }


        private int m_TweenId;

        private bool m_ValuesCached;
        public bool valuesCached
        {
            get { return m_ValuesCached; }
        }

        public UiTransformPosition() { }

        public UiTransformPosition(RectTransform rectTransform)
        {
            m_RectTransform = rectTransform;
            CacheValues();
        }

        public void TweenPosition(Direction startDirection, Direction endDirection, float duration, float delay, Tween.TweenType tweenType, Action onDone = null)
        {
            if (!m_ValuesCached)
            {
                CacheValues();
            }

            SetPosition(startDirection);

            TweenPosition(endDirection, duration, delay, tweenType, onDone);
        }

        public void TweenPosition(Direction direction, float duration, float delay, Tween.TweenType tweenType, Action onDone = null)
        {
            if (!m_ValuesCached)
            {
                CacheValues();
            }

            TweenPosition(m_PositionData.GetPosition(direction), duration, delay, tweenType, onDone);
        }

        public void TweenPosition(Vector2 startPosition, Vector2 endPosition, float duration, float delay, Tween.TweenType tweenType, Action onDone = null)
        {
            if (!m_ValuesCached)
            {
                CacheValues();
            }

            SetPosition(startPosition);

            TweenPosition(endPosition, duration, delay, tweenType, onDone);
        }

        public void TweenPosition(Vector2 position, float duration, float delay, Tween.TweenType tweenType, Action onDone = null)
        {
            if (!m_ValuesCached)
            {
                CacheValues();
            }

            m_TweenId = TweenManager.TweenVector2(value => rectTransform.anchoredPosition = value,
                rectTransform.anchoredPosition, position,
                duration, delay, onDone, false, tweenType);
        }

        public void SetPosition(Direction direction)
        {
            if (!m_ValuesCached)
            {
                CacheValues();
            }

            SetPosition(m_PositionData.GetPosition(direction));
        }

        public void SetPosition(Vector2 position)
        {
            if (!m_ValuesCached)
            {
                CacheValues();
            }

            TweenManager.EndTween(m_TweenId);
            rectTransform.anchoredPosition = position;
        }

        public void SetDefaultPosition(Vector2 position, bool alsoSetPosition = true)
        {
            if (alsoSetPosition)
            {
                rectTransform.anchoredPosition = position;
                CacheValues();
            }
            else
            {
                Vector2 oldPosition = rectTransform.anchoredPosition;
                rectTransform.anchoredPosition = position;
                CacheValues();
                rectTransform.anchoredPosition = oldPosition;
            }
        }

        public void SetDefaultPositionAndOffset(Vector2 position, Vector2 offset, bool alsoSetPosition = true)
        {
            if (alsoSetPosition)
            {
                rectTransform.anchoredPosition = position;
                SetOffset(offset);
            }
            else
            {
                Vector2 oldPosition = rectTransform.anchoredPosition;
                rectTransform.anchoredPosition = position;
                SetOffset(offset);
                rectTransform.anchoredPosition = oldPosition;
            }
        }

        public void SetOffset(Vector2 offset)
        {
            m_ValuesCached = true;

            m_PositionData = new UiTransformPositionData(rectTransform, offset);
        }

        private void CacheValues()
        {
            m_ValuesCached = true;

            m_PositionData = new UiTransformPositionData(rectTransform, Vector2.zero);
        }
    }

    public struct UiTransformPositionData
    {
        private readonly Vector2[] m_Positions;
        public readonly Vector2 offset;

        public UiTransformPositionData(RectTransform rectTransform, Vector2 offset)
        {
            Canvas rootCanvas = rectTransform.GetRootCanvas();

            if (rootCanvas != null)
            {
                //rootCanvas.GetComponent<MaterialUIScaler>().Handle();
            }

            if (offset == Vector2.zero)
            {
                offset = rootCanvas.pixelRect.size / rootCanvas.scaleFactor;
            }

            this.offset = offset;

            m_Positions = new Vector2[5];

            m_Positions[0] = rectTransform.anchoredPosition;
            m_Positions[1] = new Vector2(m_Positions[0].x, m_Positions[0].y + offset.y);
            m_Positions[2] = new Vector2(m_Positions[0].x + offset.x, m_Positions[0].y);
            m_Positions[3] = new Vector2(m_Positions[0].x, m_Positions[0].y - offset.y);
            m_Positions[4] = new Vector2(m_Positions[0].x - offset.x, m_Positions[0].y);
        }

        public Vector2 GetPosition(Direction direction)
        {
            switch (direction)
            {
                case Direction.Middle:
                    return m_Positions[0];
                case Direction.Up:
                    return m_Positions[1];
                case Direction.Right:
                    return m_Positions[2];
                case Direction.Down:
                    return m_Positions[3];
                case Direction.Left:
                    return m_Positions[4];
            }

            return Vector2.zero;
        }
    }

    #endregion

    #region Extensions

    public static class UiExtensions
    {
        #region RectTransform

        public static void SetAnchoredPositionX(this RectTransform rectTransform, float position, bool resetY = false)
        {
            rectTransform.anchoredPosition = new Vector2
            {
                x = position,
                y = resetY ? 0f : rectTransform.anchoredPosition.y
            };
        }

        public static void SetAnchoredPositionY(this RectTransform rectTransform, float position, bool resetX = false)
        {
            rectTransform.anchoredPosition = new Vector2
            {
                x = resetX ? 0f : rectTransform.anchoredPosition.x,
                y = position
            };
        }

        public static Vector2 GetPivotOffset(this RectTransform rectTransform)
        {
            Vector2 offset = Vector2.zero;
            Vector2 pivot = rectTransform.pivot;
            Vector2 size = rectTransform.rect.size;

            if (rectTransform.pivot.x < 0.5f)
            {
                offset.x -= size.x * (0.5f - pivot.x);
            }
            else if (pivot.x > 0.5f)
            {
                offset.x += size.x * (pivot.x - 0.5f);
            }

            if (pivot.y < 0.5f)
            {
                offset.y -= size.y * (0.5f - pivot.y);
            }
            else if (pivot.y > 0.5f)
            {
                offset.y += size.y * (pivot.y - 0.5f);
            }

            return offset;
        }

        #endregion
    }

    #endregion

    #region Misc

    public static class UiUtils
    {
        public static Vector2 FitToBounds(Vector2 size, Vector2 bounds)
        {
            Vector2 outputSize = new Vector2();

            float inputSizeRatio = size.x / size.y;
            float boundsRatio = bounds.x / bounds.y;

            if (inputSizeRatio > boundsRatio)
            {
                outputSize.x = bounds.x;
                outputSize.y = outputSize.x / inputSizeRatio;
            }
            else
            {
                outputSize.y = bounds.y;
                outputSize.x = bounds.y * inputSizeRatio;
            }

            return outputSize;
        }
    }

    #endregion
}
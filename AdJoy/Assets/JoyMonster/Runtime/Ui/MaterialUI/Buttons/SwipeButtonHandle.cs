﻿using UnityEngine;
using UnityEngine.EventSystems;

namespace MaterialUI_JoyMonster
{
    public class SwipeButtonHandle : UIBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler
    {
        [HideInInspector]
        [SerializeField]
        private RectTransform m_RectTransform;
        public RectTransform rectTransform
        {
            get
            {
                if (m_RectTransform == null)
                {
                    m_RectTransform = transform as RectTransform;
                }
                return m_RectTransform;
            }
        }

        [HideInInspector]
        [SerializeField]
        private SwipeButton m_SwipeButton;
        public SwipeButton swipeButton
        {
            get
            {
                if (m_SwipeButton == null)
                {
                    m_SwipeButton = GetComponentInParent<SwipeButton>();
                }
                return m_SwipeButton;
            }
        }

        //[HideInInspector]
        //[SerializeField]
        //private MaterialUIScaler m_Scaler;
        //public MaterialUIScaler scaler
        //{
        //    get
        //    {
        //        if (m_Scaler == null)
        //        {
        //            m_Scaler = MaterialUIScaler.GetRootScaler(transform);
        //        }
        //        return m_Scaler;
        //    }
        //}

        private float maxPos;
        private int m_HandleTweenIndex = -1;

        public void OnBeginDrag(PointerEventData eventData)
        {
            TweenManager.EndTween(m_HandleTweenIndex);
        }

        public void OnDrag(PointerEventData eventData)
        {
            ApplyDrag(eventData.delta.x);
        }

        public void OnEndDrag(PointerEventData eventData)
        {
            ApplyDrag(eventData.delta.x);

            bool success = false;

            if (rectTransform.anchoredPosition.x >= maxPos)
            {
                swipeButton.onSuccessfulSwipe.InvokeIfNotNull();
                success = true;
                TweenManager.TweenColor(color => swipeButton.backgroundColor = color, swipeButton.fillColor, swipeButton.backgroundColor, 0.5f, tweenType: Tween.TweenType.EaseInQuint);
            }

            m_HandleTweenIndex = TweenManager.TweenVector2(
                vector2 =>
                {
                    rectTransform.anchoredPosition = vector2;
                    swipeButton.RefreshFillAmount();
                },
                rectTransform.anchoredPosition, Vector2.zero, 0.5f,
                tweenType: success ? Tween.TweenType.EaseInOutQuint : Tween.TweenType.EaseOutQuint);
        }

        public void ResetSwipe()
        {
            TweenManager.EndTween(m_HandleTweenIndex);
            rectTransform.anchoredPosition = Vector2.zero;
        }

        private void ApplyDrag(float delta)
        {
            //float newPos = rectTransform.anchoredPosition.x + delta / scaler.scaleFactor;
            float newPos = rectTransform.anchoredPosition.x + delta / UiScaleController.scale;
            maxPos = swipeButton.rectTransform.rect.width - rectTransform.rect.width;
            rectTransform.anchoredPosition = new Vector2(Mathf.Clamp(newPos, 0f, maxPos), 0f);
            swipeButton.RefreshFillAmount();
        }
    }
}

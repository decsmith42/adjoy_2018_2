﻿using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace MaterialUI_JoyMonster
{
    [ExecuteInEditMode]
    public class SwipeButton : UIBehaviour
    {
        [SerializeField]
        private RectTransform m_RectTransform;
        public RectTransform rectTransform
        {
            get
            {
                if (m_RectTransform == null)
                {
                    m_RectTransform = transform as RectTransform;
                }
                return m_RectTransform;
            }
        }

        [SerializeField]
        private bool m_Interactable = true;
        public bool interactable
        {
            get { return m_Interactable; }
            set
            {
                m_Interactable = value;
                canvasGroup.interactable = value;
                canvasGroup.blocksRaycasts = value;
                canvasGroup.alpha = interactable ? 1f : 0.5f;
            }
        }

        public Color backgroundColor
        {
            get
            {
                if (m_Background != null)
                {
                    return m_Background.color;
                }

                return Color.black;
            }
            set
            {
                if (m_Background != null)
                {
                    m_Background.color = value;
                }
            }
        }

        public Color fillColor
        {
            get
            {
                if (m_Fill != null)
                {
                    return m_Fill.color;
                }

                return Color.black;
            }
            set
            {
                if (m_Fill != null)
                {
                    m_Fill.color = value;
                }
            }
        }

        [SerializeField]
        private CanvasGroup m_CanvasGroup;
        public CanvasGroup canvasGroup
        {
            get
            {
                if (m_CanvasGroup == null)
                {
                    m_CanvasGroup = gameObject.GetAddComponent<CanvasGroup>();
                }
                return m_CanvasGroup;
            }
        }

        [SerializeField]
        private Graphic m_Background;
        public Graphic background
        {
            get { return m_Background; }
            set { m_Background = value; }
        }

        [SerializeField]
        private Graphic m_Fill;
        public Graphic fill
        {
            get { return m_Fill; }
            set { m_Fill = value; }
        }

        [SerializeField]
        private RectTransform m_HandleTransform;
        public RectTransform handleTransform
        {
            get { return m_HandleTransform; }
            set { m_HandleTransform = value; }
        }

        [SerializeField]
        private Graphic m_HandleImage;
        public Graphic handleImage
        {
            get { return m_HandleImage; }
            set { m_HandleImage = value; }
        }

        [SerializeField]
        private Graphic m_HandleIcon;
        public Graphic handleIcon
        {
            get { return m_HandleIcon; }
            set { m_HandleIcon = value; }
        }

        [SerializeField]
        private TextMeshProUGUI m_Label;
        public TextMeshProUGUI label
        {
            get { return m_Label; }
            set { m_Label = value; }
        }

        [SerializeField]
        private Button.ButtonClickedEvent m_OnSuccessfulSwipe = new Button.ButtonClickedEvent();
        public Button.ButtonClickedEvent onSuccessfulSwipe
        {
            get { return m_OnSuccessfulSwipe; }
            set { m_OnSuccessfulSwipe = value; }
        }

        protected override void Start()
        {
            RefreshFillAmount();
        }

        protected override void OnRectTransformDimensionsChange()
        {
            RefreshFillAmount();
        }

        public void RefreshFillAmount()
        {
            fill.rectTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, m_HandleTransform.anchoredPosition.x + 56f);
        }
    }
}
﻿using System;
using UnityEngine;

namespace MaterialUI_JoyMonster
{
    public class ListAnimationControllerStandard : ListAnimationController
    {
        protected ListLayoutControllerStandard.LayoutDirection m_LayoutDirection;

        public override void AnimateListShow(Action onCompleted = null)
        {
            m_LayoutDirection = ((ListLayoutControllerStandard)listView.layoutController).layoutDirection;
            base.AnimateListShow(onCompleted);
        }

        public override void AnimateItemShow(ListItem item, int displayedIndex, Action onCompleted)
        {
            float delay = GetDelayFromIndex(displayedIndex, true);

            Vector2 endPos = item.rectTransform.anchoredPosition;
            Vector2 startPos = endPos;

            if (m_LayoutDirection == ListLayoutControllerStandard.LayoutDirection.Horizontal)
            {
                startPos.y -= 24f;
            }
            else
            {
                startPos.x -= 24f;
            }

            item.rectTransform.anchoredPosition = startPos;

            AddItemTween(TweenManager.TweenVector2(vector2 => item.rectTransform.anchoredPosition = vector2,
                    startPos, endPos, m_AnimationDuration, delay),
                item, onCompleted, () => item.rectTransform.anchoredPosition = endPos);

            item.canvasGroup.alpha = 0f;

            AddItemTween(TweenManager.TweenFloat(f => item.canvasGroup.alpha = f,
                    0f, 1f, 0.5f, delay),
                item, null, () => item.canvasGroup.alpha = 1f);
        }

        public override void AnimateItemHide(ListItem item, int displayedIndex, Action onCompleted = null)
        {
            float delay = GetDelayFromIndex(displayedIndex);

            Vector2 startPos = item.rectTransform.anchoredPosition;
            Vector2 endPos = startPos;

            if (m_LayoutDirection == ListLayoutControllerStandard.LayoutDirection.Horizontal)
            {
                endPos.y -= 24f;
            }
            else
            {
                endPos.x -= 24f;
            }

            AddItemTween(TweenManager.TweenVector2(vector2 => item.rectTransform.anchoredPosition = vector2,
                    startPos, endPos, m_AnimationDuration * 0.5f, delay,
                    tweenType: Tween.TweenType.EaseInQuint),
                item, onCompleted, null);

            AddItemTween(TweenManager.TweenFloat(f => item.canvasGroup.alpha = f,
                    1f, 0f, m_AnimationDuration * 0.5f, delay, tweenType: Tween.TweenType.EaseInQuint),
                item, null, () => item.canvasGroup.alpha = 1f);
        }

        public override void AnimateItemMove(ListItem item, Vector2 targetPosition, int displayedIndex, Action onCompleted)
        {
            Vector2 startPos = item.rectTransform.anchoredPosition;
            Vector2 endPos = targetPosition;
            AddItemTween(TweenManager.TweenVector2(vector2 => item.rectTransform.anchoredPosition = vector2,
                    startPos, endPos, m_AnimationDuration, GetDelayFromIndex(displayedIndex) / 2f,
                    tweenType: Tween.TweenType.EaseInOutQuint),
                item, onCompleted, null);
        }

        public override void AnimateItemEmphasis(ListItem item, int displayedIndex, Action onCompleted = null)
        {
            RippleData rippleData = new RippleData();
            rippleData.RippleParent = item.rectTransform;
            Ripple ripple = RippleManager.instance.GetRipple();
            ripple.Setup(rippleData, item.rectTransform.GetPositionRegardlessOfPivot(), null);
            ripple.Expand();
            TweenManager.TimedCallback(4f / rippleData.Speed, ripple.Contract);
        }
    }
}
﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Object = UnityEngine.Object;

namespace MaterialUI_JoyMonster
{
    [Serializable]
    public class ListLayoutControllerStandard : ListLayoutController
    {
        public enum LayoutDirection
        {
            Vertical,
            Horizontal
        }

        protected LayoutDirection m_LayoutDirection = LayoutDirection.Vertical;

        public LayoutDirection layoutDirection
        {
            get { return m_LayoutDirection; }
            set
            {
                m_LayoutDirection = value;

                if (itemsAreShown)
                {
                    Reset();
                    CacheListData();
                    Show();
                }
                else if (listView.listData != null)
                {
                    Reset();
                    CacheListData();
                }
            }
        }

        protected bool m_ForceSingleLine;

        public bool forceSingleLine
        {
            get { return m_ForceSingleLine; }
            set { m_ForceSingleLine = value; }
        }

        protected int m_CachedItemsPerLine;

        public int cachedItemsPerLine
        {
            get { return m_CachedItemsPerLine; }
        }

        protected List<int> m_CachedItemLines = new List<int>();

        public List<int> cachedItemLines
        {
            get { return m_CachedItemLines; }
        }

        private Vector2 m_ListPadding = new Vector2(8f, 8f);

        public Vector2 listPadding
        {
            get { return m_ListPadding; }
            set
            {
                m_ListPadding = value;
                //OnListDataChange();
            }
        }

        private Vector2 m_ListItemSpacing = new Vector2(8f, 8f);

        public Vector2 listItemSpacing
        {
            get { return m_ListItemSpacing; }
            set
            {
                m_ListItemSpacing = value;
                //OnListDataChange();
            }
        }

        public void Reset()
        {
            for (int i = 0; i < m_DisplayedItems.Count; i++)
            {
                Object.Destroy(m_DisplayedItems[i].gameObject);
            }

            itemPool.Cleanup(false);
            m_DisplayedItems.Clear();
            m_DisplayedItemDatas.Clear();
            m_ItemsAreShown = false;

            m_CachedListDataSize = listView.listData.listItems.Count;

            m_CachedItemSizes.Clear();
            m_CachedItemPositions.Clear();
            m_ListView.listData.listItems.Clear();

            m_ListView.listContent.rectTransform.anchoredPosition = Vector2.zero;
        }

        public override void CacheListSizesAndPositions()
        {
            if (listView.listData.listItems.Count == 0) return;

            bool isVertical = m_LayoutDirection == LayoutDirection.Vertical;

            Vector2 biggestItemMinSize = Vector2.zero;
            ListItemController itemController;

            for (int i = 0; i < listView.listData.listItems.Count; i++)
            {
                ListItemData itemData = listView.listData.listItems[i];
                ListItemDataStandard itemDataStandard = itemData as ListItemDataStandard;

                if (itemDataStandard != null && itemDataStandard.forceLayoutSingleLine) continue;

                itemController = GetItemController(itemData, this);
                itemController.CalculateItemWidth(itemData, itemController.minItemHeight, 0f);
                itemController.CalculateItemHeight(itemData, listView.rectTransform.rect.height, itemController.GetItemWidth(itemData));

                biggestItemMinSize.x = Mathf.Max(itemController.GetItemWidth(itemData), biggestItemMinSize.x);
                biggestItemMinSize.y = Mathf.Max(itemController.GetItemHeight(itemData), biggestItemMinSize.y);
            }

            Vector2 viewSize = listView.rectTransform.rect.size;
            Vector2 combinedPadding = 2f * m_ListPadding;

            m_CachedItemsPerLine = 1;

            if (!m_ForceSingleLine)
            {
                if (isVertical)
                {
                    for (int i = 1; i < 100; i++)
                    {
                        if (combinedPadding.x + ((i + 1) * biggestItemMinSize.x) + (i * m_ListItemSpacing.x) > viewSize.x)
                        {
                            m_CachedItemsPerLine = i;
                            break;
                        }
                    }
                }
                else
                {
                    for (int i = 1; i < 100; i++)
                    {
                        if (combinedPadding.y + ((i + 1) * biggestItemMinSize.y) + (i * m_ListItemSpacing.y) > viewSize.y)
                        {
                            m_CachedItemsPerLine = i;
                            break;
                        }
                    }
                }
            }

            Vector2 listSize = Vector2.zero;
            int lineCounter = 0;

            float availableItemSizePrimaryAxis = m_LayoutDirection == LayoutDirection.Vertical
                ? (viewSize.x - (combinedPadding.x + (m_ListItemSpacing.x * (m_CachedItemsPerLine - 1)))) /
                  m_CachedItemsPerLine
                : (viewSize.y - (combinedPadding.y + (m_ListItemSpacing.y * (m_CachedItemsPerLine - 1)))) /
                  m_CachedItemsPerLine;

            float availableItemSizePrimaryAxisForceSingle = m_LayoutDirection == LayoutDirection.Vertical
                ? (viewSize.x - combinedPadding.x)
                : (viewSize.y - combinedPadding.y);

            Vector2 itemSize = Vector2.zero;

            float[] linePositions = new float[m_CachedItemsPerLine];
            float lineBiggestItemLength = 0f;

            if (isVertical)
            {
                listSize.x = viewSize.x;
                listSize.y = m_ListPadding.y;

                for (int i = 0; i < m_CachedItemsPerLine; i++)
                {
                    linePositions[i] = m_ListPadding.x + (i * (availableItemSizePrimaryAxis + m_ListItemSpacing.x));
                }
            }
            else
            {
                listSize.x = m_ListPadding.x;
                listSize.y = viewSize.y;

                for (int i = 0; i < m_CachedItemsPerLine; i++)
                {
                    linePositions[i] = -m_ListPadding.y - (i * (availableItemSizePrimaryAxis + m_ListItemSpacing.y));
                }
            }

            m_CachedItemLines.Clear();

            for (int i = 0; i < m_CachedListDataSize; i++)
            {
                ListItemData itemData = m_ListView.listData.listItems[i];
                ListItemDataStandard itemDataStandard = itemData as ListItemDataStandard;
                bool itemForceSingleLine = itemDataStandard != null && itemDataStandard.forceLayoutSingleLine;

                if (itemForceSingleLine)
                {
                    if (i > 0)
                    {
                        if (isVertical)
                        {
                            listSize += new Vector2(0f, lineBiggestItemLength + m_ListItemSpacing.y);
                        }
                        else
                        {
                            listSize += new Vector2(lineBiggestItemLength + m_ListItemSpacing.x, 0f);
                        }
                    }

                    lineCounter = 0;
                    lineBiggestItemLength = 0f;
                }

                if (itemData.hidden)
                {
                    m_CachedItemSizes.Add(Vector2.zero);
                    m_CachedItemPositions.Add(isVertical
                        ? new Vector2(linePositions[lineCounter], -listSize.y)
                        : new Vector2(listSize.x, linePositions[lineCounter]));
                    m_CachedItemLines.Add(lineCounter);
                    continue;
                }

                float availableSize = itemForceSingleLine
                    ? availableItemSizePrimaryAxisForceSingle
                    : availableItemSizePrimaryAxis;

                itemController = GetItemController(itemData);

                if (isVertical)
                {
                    itemController.CalculateItemWidth(itemData, availableSize, 0f);
                    itemController.CalculateItemHeight(itemData, 0f, availableSize);
                }
                else
                {
                    itemController.CalculateItemWidth(itemData, 0f, availableSize);
                    itemController.CalculateItemHeight(itemData, availableSize, 0f);
                }

                itemSize.x = itemController.GetItemWidth(itemData);
                itemSize.y = itemController.GetItemHeight(itemData);

                lineBiggestItemLength = Mathf.Max(lineBiggestItemLength, isVertical ? itemSize.y : itemSize.x);

                m_CachedItemSizes.Add(itemSize);
                m_CachedItemPositions.Add(isVertical
                    ? new Vector2(linePositions[lineCounter], -listSize.y)
                    : new Vector2(listSize.x, linePositions[lineCounter]));
                m_CachedItemLines.Add(lineCounter);
                lineCounter++;

                if (lineCounter == m_CachedItemsPerLine || itemForceSingleLine)
                {
                    if (isVertical)
                    {
                        listSize += new Vector2(0f, lineBiggestItemLength + m_ListItemSpacing.y);
                    }
                    else
                    {
                        listSize += new Vector2(lineBiggestItemLength + m_ListItemSpacing.x, 0f);
                    }

                    lineCounter = 0;
                    lineBiggestItemLength = 0f;
                }
            }

            if (lineCounter > 0)
            {
                if (isVertical)
                {
                    listSize += itemSize + new Vector2(0f, m_ListItemSpacing.y);
                }
                else
                {
                    listSize += itemSize + new Vector2(m_ListItemSpacing.y, 0f);
                }
            }

            if (isVertical)
            {
                listSize += new Vector2(0f, m_ListPadding.y / 2f);
            }
            else
            {
                listSize += new Vector2(m_ListPadding.x / 2f, 0f);
            }

            m_CachedMinItemSize = itemSize;
            m_CachedListContentSize = listSize;
        }

        public override int[] GetFirstAndLastItemDataIndexes()
        {
            return m_LayoutDirection == LayoutDirection.Vertical
                ? GetFirstAndLastItemDataIndexesVertical()
                : GetFirstAndLastItemDataIndexesHorizontal();
        }

        private readonly int[] m_TempFirstAndLastItems = new int[2];

        protected int[] GetFirstAndLastItemDataIndexesVertical()
        {
            m_TempFirstAndLastItems[0] = -1;
            m_TempFirstAndLastItems[1] = -1;

            float startOfList = -listView.listContent.rectTransform.anchoredPosition.y;
            float endOfList = startOfList - listView.rectTransform.rect.height;

            for (int i = 0; i < m_CachedItemSizes.Count; i++)
            {
                if (m_CachedItemPositions[i].y - m_CachedItemSizes[i].y - m_ListItemSpacing.y <= startOfList &&
                    !listView.listData.listItems[i].hidden)
                {
                    m_TempFirstAndLastItems[0] = Mathf.Max(0, (i - m_CachedItemLines[i]));
                    break;
                }
            }

            if (m_TempFirstAndLastItems[0] == -1)
            {
                m_TempFirstAndLastItems[0] = 0;
            }

            for (int i = m_TempFirstAndLastItems[0]; i < m_CachedItemSizes.Count; i++)
            {
                if (m_CachedItemPositions[i].y + m_ListItemSpacing.y <= endOfList && !listView.listData.listItems[i].hidden)
                {
                    m_TempFirstAndLastItems[1] = Mathf.Min(m_CachedListDataSize - 1, (i - (m_CachedItemLines[i] + 1)));
                    break;
                }
            }

            if (m_TempFirstAndLastItems[1] == -1)
            {
                m_TempFirstAndLastItems[1] = listView.listData.listItems.Count - 1;
            }

            return m_TempFirstAndLastItems;
        }

        protected int[] GetFirstAndLastItemDataIndexesHorizontal()
        {
            int[] items = { -1, -1 };

            float startOfList = -listView.listContent.rectTransform.anchoredPosition.x;
            float endOfList = startOfList + listView.rectTransform.rect.width;

            for (int i = 0; i < m_CachedItemSizes.Count; i++)
            {
                if (m_CachedItemPositions[i].x + m_CachedItemSizes[i].x + m_ListItemSpacing.x >= startOfList &&
                    !listView.listData.listItems[i].hidden)
                {
                    items[0] = Mathf.Max(0, i);
                    break;
                }
            }

            if (items[0] == -1)
            {
                items[0] = 0;
            }

            for (int i = items[0]; i < m_CachedItemSizes.Count; i++)
            {
                if (m_CachedItemPositions[i].x - m_ListItemSpacing.x >= endOfList && !listView.listData.listItems[i].hidden)
                {
                    items[1] = Mathf.Min(m_CachedListDataSize - 1, i - 1);
                    break;
                }
            }

            if (items[1] == -1)
            {
                items[1] = listView.listData.listItems.Count - 1;
            }

            return items;
        }

        public override void SetContentSize()
        {
            if (m_LayoutDirection == LayoutDirection.Vertical)
            {
                listView.listContent.rectTransform.anchorMin = new Vector2(0f, 1f);
                listView.listContent.rectTransform.anchorMax = new Vector2(1f, 1f);
                listView.listContent.rectTransform.pivot = new Vector2(0.5f, 1f);
                listView.listContent.rectTransform.offsetMin =
                    new Vector2(0f, listView.listContent.rectTransform.offsetMin.y);
                listView.listContent.rectTransform.offsetMax =
                    new Vector2(0f, listView.listContent.rectTransform.offsetMax.y);
                listView.listContent.rectTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal,
                    listView.rectTransform.rect.width);
                listView.listContent.rectTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical,
                    m_CachedListContentSize.y);
            }
            else
            {
                listView.listContent.rectTransform.anchorMin = new Vector2(0f, 0f);
                listView.listContent.rectTransform.anchorMax = new Vector2(0f, 1f);
                listView.listContent.rectTransform.pivot = new Vector2(0f, 0.5f);
                listView.listContent.rectTransform.offsetMin =
                    new Vector2(listView.listContent.rectTransform.offsetMin.x, 0f);
                listView.listContent.rectTransform.offsetMax =
                    new Vector2(listView.listContent.rectTransform.offsetMax.x, 0f);
                listView.listContent.rectTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal,
                    m_CachedListContentSize.x);
                listView.listContent.rectTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical,
                    listView.rectTransform.rect.height);
            }

            //listView.overscroll.Setup();
        }

        public override void SetItemPosition(ListItem item, int dataIndex)
        {
            item.rectTransform.anchorMin = new Vector2(0f, 1f);
            item.rectTransform.anchorMax = new Vector2(0f, 1f);
            item.rectTransform.pivot = new Vector2(0f, 1f);
            item.rectTransform.sizeDelta = m_CachedItemSizes[dataIndex];
            item.rectTransform.anchoredPosition = m_CachedItemPositions[dataIndex];
        }

        protected override Comparison<ListItem> GetDisplayedItemSorter()
        {
            return (item1, item2) =>
            {
                if (m_LayoutDirection == LayoutDirection.Vertical)
                {
                    if (item1.rectTransform.anchoredPosition.y < item2.rectTransform.anchoredPosition.y)
                    {
                        return 1;
                    }

                    if (item1.rectTransform.anchoredPosition.y > item2.rectTransform.anchoredPosition.y)
                    {
                        return -1;
                    }

                    if (item1.rectTransform.anchoredPosition.x < item2.rectTransform.anchoredPosition.x)
                    {
                        return -1;
                    }

                    if (item1.rectTransform.anchoredPosition.x > item2.rectTransform.anchoredPosition.x)
                    {
                        return 1;
                    }
                }
                else
                {

                    if (item1.rectTransform.anchoredPosition.x < item2.rectTransform.anchoredPosition.x)
                    {
                        return -1;
                    }

                    if (item1.rectTransform.anchoredPosition.x > item2.rectTransform.anchoredPosition.x)
                    {
                        return 1;
                    }

                    if (item1.rectTransform.anchoredPosition.y < item2.rectTransform.anchoredPosition.y)
                    {
                        return 1;
                    }

                    if (item1.rectTransform.anchoredPosition.y > item2.rectTransform.anchoredPosition.y)
                    {
                        return -1;
                    }
                }

                return 0;
            };
        }
    }
}
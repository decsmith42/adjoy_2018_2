﻿namespace MaterialUI_JoyMonster
{
    public abstract class ListItemStandard : ListItem
    {
    }

    public abstract class ListItemDataStandard : ListItemData
    {
        protected bool m_ForceLayoutSingleLine;

        public bool forceLayoutSingleLine
        {
            get { return m_ForceLayoutSingleLine; }
            set { m_ForceLayoutSingleLine = value; }
        }
    }
}
﻿namespace MaterialUI_JoyMonster
{
    public static partial class ListExtensionMethods
    {
        public static ListLayoutControllerStandard LayoutControllerStandard(this ListView listView)
        {
            return listView.layoutController as ListLayoutControllerStandard;
        }

        public static void InitializeStandard(this ListView listView)
        {
            listView.InitializeList(new ListLayoutControllerStandard(), new ListAnimationControllerStandard(), new ListInteractionControllerStandard());
        }
    }
}
﻿using UnityEngine;

namespace MaterialUI_JoyMonster
{
    public class ListProgressExtension : MonoBehaviour, IListExtension
    {
        [SerializeField] private ProgressIndicator m_ProgressIndicator;

        public ProgressIndicator progessIndicator
        {
            get { return m_ProgressIndicator; }
            set { m_ProgressIndicator = value; }
        }

        [SerializeField] private bool m_HideProgressOnListShow = true;

        public bool hideProgressOnListShow
        {
            get { return m_HideProgressOnListShow; }
            set { m_HideProgressOnListShow = value; }
        }

        private ListView m_ListView;

        public void OnListInitialized(ListView listView)
        {
        }

        public void OnBeforeListDataChanged()
        {
        }

        public void OnAfterListDataChanged()
        {
        }

        public void OnBeforeListLayoutChanged()
        {
        }

        public void OnAfterListLayoutChanged()
        {
        }

        public void OnContentChanged()
        {
        }

        public void OnBeforeShow()
        {
        }

        public void OnShow()
        {
            if (m_HideProgressOnListShow)
            {
                m_ProgressIndicator.Hide();
            }
        }

        public void OnHide()
        {
        }

        public void ShowProgressIndeterminate()
        {
            m_ProgressIndicator.Show();
        }

        public void HideProgress()
        {
            m_ProgressIndicator.Hide();
        }
    }
}
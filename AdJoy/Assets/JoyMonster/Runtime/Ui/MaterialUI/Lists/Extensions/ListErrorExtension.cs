﻿using System;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

namespace MaterialUI_JoyMonster
{
    public abstract class ListErrorExtension : UIBehaviour, IListExtension
    {
        private RectTransform m_RectTransform;
        public RectTransform rectTransform
        {
            get
            {
                if (m_RectTransform == null)
                {
                    m_RectTransform = GetComponent<RectTransform>();
                }
                return m_RectTransform;
            }
        }

        [SerializeField] private bool m_EnableShowEmptyContent = false;

        [SerializeField] private bool m_HideOnButtonRefreshClick = true;
        public bool hideOnButtonRefreshClick
        {
            get { return m_HideOnButtonRefreshClick; }
            set { m_HideOnButtonRefreshClick = value; }
        }

        [SerializeField] private bool m_HideOnButtonOtherClick = true;
        public bool hideOnButtonOtherClick
        {
            get { return m_HideOnButtonOtherClick; }
            set { m_HideOnButtonOtherClick = value; }
        }

        [Serializable] public class ListErrorEvent : UnityEvent { }

        [SerializeField] private ListErrorEvent m_OnButtonRefreshClicked = new ListErrorEvent();
        public ListErrorEvent onButtonRefreshClicked
        {
            get { return m_OnButtonRefreshClicked; }
        }

        [SerializeField] private ListErrorEvent m_OnButtonOtherClicked = new ListErrorEvent();
        public ListErrorEvent onButtonOtherClicked
        {
            get { return m_OnButtonOtherClicked; }
        }

        public bool isShowing { get; protected set; }
        public bool isEmptyShowing { get; protected set; }

        public virtual void OnListInitialized(ListView listView)
        {
        }

        public virtual void OnBeforeListDataChanged()
        {
        }

        public virtual void OnAfterListDataChanged()
        {
        }

        public virtual void OnBeforeListLayoutChanged()
        {
        }

        public virtual void OnAfterListLayoutChanged()
        {
        }

        public virtual void OnContentChanged()
        {
        }

        public virtual void OnBeforeShow()
        {
        }

        public virtual void OnShow()
        {
        }

        public virtual void OnHide()
        {
        }

        public void ShowError()
        {
            if (isEmptyShowing)
            {
                HideEmpty();
            }

            isShowing = true;

            OnShowError();
        }

        protected virtual void OnShowError() { }

        public void HideError()
        {
            isShowing = false;

            OnHideError();
        }

        protected virtual void OnHideError() { }

        public void ShowEmpty()
        {
            if (!m_EnableShowEmptyContent) return;

            if (isShowing)
            {
                HideError();
            }

            isEmptyShowing = true;

            OnShowEmpty();
        }

        protected virtual void OnShowEmpty() { }

        public void HideEmpty()
        {
            if (!m_EnableShowEmptyContent) return;

            isEmptyShowing = false;

            OnHideEmpty();
        }

        protected virtual void OnHideEmpty() { }

        public void HideAll()
        {
            HideError();

            if (!m_EnableShowEmptyContent) return;

            HideEmpty();
        }

        public void OnButtonRefreshClicked()
        {
            if (m_HideOnButtonRefreshClick)
            {
                HideError();
            }

            m_OnButtonRefreshClicked.InvokeIfNotNull();
        }

        public void OnButtonOtherClicked()
        {
            if (m_HideOnButtonOtherClick)
            {
                HideError();
            }

            m_OnButtonOtherClicked.InvokeIfNotNull();
        }
    }
}
﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace MaterialUI_JoyMonster
{
    public class ListFilterExtension : MonoBehaviour, IListExtension
    {
        private ListView m_ListView;

        private List<bool> m_CachedHiddenItems = new List<bool>();

        public void OnListInitialized(ListView listView)
        {
            m_ListView = listView;
        }

        public void OnBeforeListDataChanged()
        {
        }

        public void OnAfterListDataChanged()
        {
        }

        public void OnBeforeListLayoutChanged()
        {
        }

        public void OnAfterListLayoutChanged()
        {
        }

        public void OnContentChanged()
        {
        }

        public void OnBeforeShow()
        {
        }

        public void OnShow()
        {
        }

        public void OnHide()
        {
        }

        public void FilterList(Func<ListItemData, bool> predicate)
        {
            if (m_ListView.listData == null ||
                m_ListView.listData.listItems == null ||
                m_ListView.listData.listItems.Count == 0)
            {
                return;
            }

            int numberOfItems = m_ListView.listData.listItems.Count;

            if (numberOfItems == 0) return;

            while (m_CachedHiddenItems.Count > numberOfItems)
            {
                m_CachedHiddenItems.RemoveAt(m_CachedHiddenItems.Count - 1);
            }

            while (m_CachedHiddenItems.Count < numberOfItems)
            {
                m_CachedHiddenItems.Add(false);
            }

            for (int i = 0; i < numberOfItems; i++)
            {
                m_CachedHiddenItems[i] = !predicate(m_ListView.listData.listItems[i]);
            }

            m_ListView.layoutController.UpdateMultipleHiddenItems(m_CachedHiddenItems.ToArray());
        }

        public void UnfilterList()
        {
            FilterList(data => true);
        }
    }
}
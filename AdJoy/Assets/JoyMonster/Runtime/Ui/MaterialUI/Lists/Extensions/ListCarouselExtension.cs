﻿using System.Collections;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace MaterialUI_JoyMonster
{
    public class ListCarouselExtension : MonoBehaviour, IListExtension, IBeginDragHandler, IEndDragHandler
    {
        public class IndicatorIconData
        {
            public IndicatorIconData(GameObject iconTemplate, RectTransform indicatorContainer)
            {
                vectorImage = Instantiate(iconTemplate, indicatorContainer, false).GetComponent<VectorImage>();
                isSelected = false;
                sizeTweenIndex = -1;
                colorTweenIndex = -1;
            }

            public VectorImage vectorImage;
            public bool isSelected;
            public int sizeTweenIndex;
            public int colorTweenIndex;
        }

        public class ItemCenteredEvent : UnityEvent<ListItemData>
        {
        }

        private ListLayoutControllerStandard m_LayoutController;

        private bool m_IsInitialized = false;

        [SerializeField]
        private bool m_CenterItems = false;

        [SerializeField]
        private GameObject m_IconTemplate = null;

        [SerializeField]
        private UiTransformPosition m_IndicatorContainerTransform = null;

        [SerializeField]
        private UiCanvasGroup m_IndicatorContainerCanvasGroup = null;

        [SerializeField]
        private VectorImageData m_IconGlyph = null;

        [SerializeField]
        private float m_IconSizeOn = 0f;

        [SerializeField]
        private float m_IconSizeOff = 0f;

        [SerializeField]
        private Color m_IconColorOff = MaterialColor.textHintDark;

        [SerializeField]
        private Color m_IconColorOn = MaterialColor.iconDark;

        [SerializeField]
        private ItemCenteredEvent m_OnItemCentered = new ItemCenteredEvent();

        public ItemCenteredEvent onItemCentered
        {
            get { return m_OnItemCentered; }
        }

        private IndicatorIconData[] m_IconDatas = new IndicatorIconData[0];

        private float m_ItemOffsetCompensation = 0f;

        private int m_CurrentlySelectedDataIndex = -1;

        public ListItemData currentlySelectedData
        {
            get
            {
                if (m_CurrentlySelectedDataIndex < 0 ||
                    m_CurrentlySelectedDataIndex >= m_LayoutController.listView.listData.listItems.Count)
                {
                    return null;
                }

                return m_LayoutController.listView.listData.listItems[m_CurrentlySelectedDataIndex];
            }
        }

        private bool m_IsDragging;
        private bool m_IsMoving;
        private bool m_WaitingForItemSelect;
        private bool m_IndicatorPositionSet;

        public void OnListInitialized(ListView listView)
        {
            m_LayoutController = listView.layoutController as ListLayoutControllerStandard;
            if (m_LayoutController != null)
            {
                listView.interactionController.onItemInteraction += OnItemInteraction;

                m_IndicatorContainerTransform.SetOffset(new Vector2(0f, 24f));

                m_LayoutController.onBeforeLayoutCalculate += () => SetLayoutSettings(false);
                m_LayoutController.onAfterLayoutCalculate += () =>
                {
                    SetLayoutSettings(true);
                    m_IsMoving = true;
                    OnContentChanged();
                };

                m_IsInitialized = true;
            }
        }

        private void OnItemInteraction(int interactionType, BaseEventData baseEventData, ListItem listItem,
            int subItemId)
        {
            if ((interactionType == ListInteraction.selectNonPointer ||
                 interactionType == ListInteraction.pointerClick) &&
                subItemId == 0)
            {
                OnItemSelected(listItem.currentData);
            }
        }

        private void OnItemSelected(ListItemData itemData, float duration = 0.5f)
        {
            m_LayoutController.lastSelectedItem = itemData;

            m_CurrentlySelectedDataIndex = m_LayoutController.GetDataIndexFromDisplayedItemData(itemData);

            Vector2 position = m_LayoutController.listView.listContent.rectTransform.TransformPoint(
                m_LayoutController.cachedItemPositions[m_CurrentlySelectedDataIndex] +
                m_LayoutController.cachedItemSizes[m_CurrentlySelectedDataIndex] / 2f);

            Vector2 size = m_LayoutController.cachedItemSizes[m_CurrentlySelectedDataIndex];

            m_LayoutController.CenterItemInListView(position, size, 0f, duration, duration == 0f);
            m_LayoutController.listView.scrollRect.velocity = Vector2.zero;

            for (int i = 0; i < m_LayoutController.listView.listData.listItems.Count; i++)
            {
                ListItemData iteratedItemData = m_LayoutController.listView.listData.listItems[i];
                if (m_IconDatas[i].isSelected && iteratedItemData != itemData)
                {
                    TransitionIcon(i, false);
                }
                else if (!m_IconDatas[i].isSelected && iteratedItemData == itemData)
                {
                    TransitionIcon(i, true);
                }
            }

            m_OnItemCentered.InvokeIfNotNull(itemData);
        }

        public void OnBeforeListDataChanged()
        {
        }

        public void OnAfterListDataChanged()
        {
            if (!m_IsInitialized) return;

            ListData listData = m_LayoutController.listView.listData;

            foreach (IndicatorIconData icon in m_IconDatas)
            {
                Destroy(icon.vectorImage.gameObject);
            }

            m_IconDatas = new IndicatorIconData[listData.listItems.Count];

            for (int i = 0; i < m_IconDatas.Length; i++)
            {
                m_IconDatas[i] = new IndicatorIconData(m_IconTemplate, m_IndicatorContainerTransform.rectTransform)
                {
                    vectorImage = { vectorImageData = m_IconGlyph, size = m_IconSizeOff, color = m_IconColorOff }
                };

                int index = i;
                m_IconDatas[i].vectorImage.GetComponent<Button>().onClick.AddListener(() => OnIconPressed(index));
            }
        }

        public void OnBeforeListLayoutChanged()
        {
        }

        public void OnAfterListLayoutChanged()
        {
        }

        public void OnIconPressed(int index)
        {
            StartCoroutine(SelectItemCoroutine(m_LayoutController.listView.listData.listItems[index]));
            OnItemSelected(m_LayoutController.listView.listData.listItems[index], 0.5f);
        }

        private void SetLayoutSettings(bool postLayoutCache)
        {
            bool layoutIsHorizontal =
                m_LayoutController.layoutDirection == ListLayoutControllerStandard.LayoutDirection.Horizontal;

            if (postLayoutCache)
            {
                float viewSize = layoutIsHorizontal
                    ? m_LayoutController.listView.rectTransform.rect.width
                    : m_LayoutController.listView.rectTransform.rect.height;

                float averageItemSize = 0f;

                if (layoutIsHorizontal)
                {
                    for (int i = 0; i < m_LayoutController.cachedItemSizes.Count; i++)
                    {
                        averageItemSize += m_LayoutController.cachedItemSizes[i].x;
                    }

                    averageItemSize /= m_LayoutController.cachedItemSizes.Count;
                }
                else
                {
                    for (int i = 0; i < m_LayoutController.cachedItemSizes.Count; i++)
                    {
                        averageItemSize += m_LayoutController.cachedItemSizes[i].y;
                    }

                    averageItemSize /= m_LayoutController.cachedItemSizes.Count;
                }

                float idealSpacing = viewSize / 2f - averageItemSize;
                float itemSpacing = idealSpacing;

                float minSpacing = 32f;
                float maxSpacing = 300f;

                if (idealSpacing < minSpacing)
                {
                    m_ItemOffsetCompensation = minSpacing - idealSpacing;
                    itemSpacing += m_ItemOffsetCompensation;
                }
                else if (idealSpacing > maxSpacing)
                {
                    m_ItemOffsetCompensation = idealSpacing - maxSpacing;
                    itemSpacing -= m_ItemOffsetCompensation;
                }

                if (layoutIsHorizontal)
                {
                    m_LayoutController.listItemSpacing = new Vector2(itemSpacing, 0f);

                    for (int i = 0; i < m_LayoutController.cachedItemPositions.Count; i++)
                    {
                        m_LayoutController.cachedItemPositions[i] += ((i + 1) * m_LayoutController.listItemSpacing);
                    }

                    m_LayoutController.cachedListContentSize +=
                        new Vector2((m_LayoutController.cachedItemPositions.Count) * idealSpacing, 0f);
                }
                else
                {
                    m_LayoutController.listItemSpacing = new Vector2(0f, idealSpacing);

                    for (int i = 0; i < m_LayoutController.cachedItemPositions.Count; i++)
                    {
                        m_LayoutController.cachedItemPositions[i] -= (i * m_LayoutController.listItemSpacing);
                    }

                    m_LayoutController.cachedListContentSize +=
                        new Vector2(0f, m_LayoutController.cachedItemPositions.Count * idealSpacing);
                }

                m_LayoutController.SetContentSize();
            }
            else
            {
                m_LayoutController.listItemSpacing = Vector2.zero;
            }

            if (m_LayoutController.layoutDirection == ListLayoutControllerStandard.LayoutDirection.Horizontal)
            {
                m_LayoutController.listPadding = new Vector2(2000f, m_LayoutController.listPadding.y);
                if (m_LayoutController.listView.listData.listItems.Count == 1)
                {
                    m_LayoutController.listView.scrollRect.horizontal = false;
                    m_LayoutController.listView.scrollRect.vertical = false;
                }
                else
                {
                    m_LayoutController.listView.scrollRect.horizontal = true;
                    m_LayoutController.listView.scrollRect.vertical = false;
                }
            }
            else
            {
                m_LayoutController.listPadding = new Vector2(m_LayoutController.listPadding.x, 2000f);
                if (m_LayoutController.listView.listData.listItems.Count == 1)
                {
                    m_LayoutController.listView.scrollRect.horizontal = false;
                    m_LayoutController.listView.scrollRect.vertical = false;
                }
                else
                {
                    m_LayoutController.listView.scrollRect.horizontal = false;
                    m_LayoutController.listView.scrollRect.vertical = true;
                }
            }
        }

        private float m_PreviousViewPosition;

        public void OnContentChanged()
        {
            if (!m_IsInitialized) return;

            bool layoutIsHorizontal =
                m_LayoutController.layoutDirection == ListLayoutControllerStandard.LayoutDirection.Horizontal;

            float viewPosition = layoutIsHorizontal
                ? -m_LayoutController.listView.listContent.rectTransform.anchoredPosition.x +
                  m_LayoutController.listView.rectTransform.rect.width / 2f
                : -m_LayoutController.listView.listContent.rectTransform.anchoredPosition.y -
                  m_LayoutController.listView.rectTransform.rect.height / 2f;

            if (m_PreviousViewPosition == viewPosition)
            {
                return;
            }

            m_PreviousViewPosition = viewPosition;

            float viewSize = layoutIsHorizontal
                ? m_LayoutController.listView.rectTransform.rect.width
                : m_LayoutController.listView.rectTransform.rect.height;

            //float itemSpacing = layoutIsHorizontal
            //    ? m_LayoutController.listItemSpacing.x
            //    : m_LayoutController.listItemSpacing.y;

            for (int i = 0; i < m_LayoutController.displayedItems.Count; i++)
            {
                int dataIndex =
                    m_LayoutController.GetDataIndexFromDisplayedItemData(m_LayoutController.displayedItems[i]
                        .currentData);

                if (dataIndex == -1) return;

                ListItem listItem = m_LayoutController.displayedItems[i];

                float itemPos = layoutIsHorizontal
                    ? m_LayoutController.cachedItemPositions[dataIndex].x +
                      m_LayoutController.cachedItemSizes[dataIndex].x / 2f
                    : m_LayoutController.cachedItemPositions[dataIndex].y -
                      m_LayoutController.cachedItemSizes[dataIndex].y / 2f;

                //float itemSize = layoutIsHorizontal
                //    ? m_LayoutController.cachedItemSizes[dataIndex].x
                //    : m_LayoutController.cachedItemSizes[dataIndex].y;

                float signedDistance = itemPos - viewPosition;
                float distance = Mathf.Abs(signedDistance);
                int inverter = signedDistance < 0 ? 1 : -1;

                float largestOffset = (viewSize / 2f) - (layoutIsHorizontal
                                          ? m_LayoutController.listItemSpacing.x
                                          : m_LayoutController.listItemSpacing.y);

                float offset = 0f;

                float maxDistance = viewSize / 4f;

                if (m_LayoutController.listView.rectTransform.rect.width > 600f)
                {
                    maxDistance *= 2f;
                }

                if (m_ItemOffsetCompensation < 0f)
                {
                    maxDistance -= m_ItemOffsetCompensation;
                }
                else
                {
                    float scale = (maxDistance - m_ItemOffsetCompensation) / maxDistance;
                    maxDistance /= scale;
                    largestOffset *= scale;
                }

                if (distance - maxDistance <= 0f)
                {
                    float offsetTweenValue = Tween.Linear(0f, 1f, distance, maxDistance);
                    if (offsetTweenValue < 0.5f)
                    {
                        offset = Tween.CubeInOut(0f, largestOffset / 2f, offsetTweenValue, 0.5f);
                    }
                    else
                    {
                        offset = Tween.Linear(0f, largestOffset / 2f, 1f - offsetTweenValue, 0.5f);
                    }
                }
                else
                {
                    float offsetTweenValue = Tween.Linear(0f, 1f, distance - maxDistance, maxDistance);

                    if (offsetTweenValue < 0.5f)
                    {
                        offset = Tween.Linear(0f, largestOffset / 2f, offsetTweenValue, 0.5f);
                    }
                    else
                    {
                        offset = Tween.Linear(0f, largestOffset / 2f, 1f - offsetTweenValue, 0.5f);
                    }
                }

                float posX = 0f;
                float posY = 0f;

                if (m_CenterItems)
                {
                    if (layoutIsHorizontal)
                    {
                        if (listItem.rectTransform.pivot.y != 0.5f)
                        {
                            listItem.rectTransform.pivot = new Vector2(listItem.rectTransform.pivot.x, 0.5f);
                        }

                        if (listItem.rectTransform.anchorMin.y != 0.5f || listItem.rectTransform.anchorMax.y != 0.5f)
                        {
                            listItem.rectTransform.SetAnchorY(0.5f, 0.5f);
                        }
                    }
                    else
                    {
                        if (listItem.rectTransform.pivot.x != 0.5f)
                        {
                            listItem.rectTransform.pivot = new Vector2(0.5f, listItem.rectTransform.pivot.y);
                        }

                        if (listItem.rectTransform.anchorMin.x != 0.5f || listItem.rectTransform.anchorMax.x != 0.5f)
                        {
                            listItem.rectTransform.SetAnchorX(0.5f, 0.5f);
                        }
                    }

                    posX = layoutIsHorizontal
                        ? itemPos - m_LayoutController.cachedItemSizes[dataIndex].x / 2f - (offset * inverter)
                        : Tween.CubeIn(0f, m_LayoutController.cachedListContentSize.x * 0.25f, distance, viewSize);

                    posY = layoutIsHorizontal
                        ? Tween.CubeIn(0f, m_LayoutController.cachedListContentSize.y * 0.25f, distance, viewSize)
                        : itemPos + m_LayoutController.cachedItemSizes[dataIndex].y / 2f + (offset * inverter);
                }
                else
                {
                    posX = layoutIsHorizontal
                        ? itemPos - m_LayoutController.cachedItemSizes[dataIndex].x / 2f - (offset * inverter)
                        : m_LayoutController.cachedItemPositions[dataIndex].x + Tween.CubeIn(0f,
                              m_LayoutController.cachedListContentSize.x * 0.25f, distance, viewSize);

                    posY = layoutIsHorizontal
                        ? m_LayoutController.cachedItemPositions[dataIndex].y + Tween.CubeIn(0f,
                              m_LayoutController.cachedListContentSize.y * 0.25f, distance, viewSize)
                        : itemPos + m_LayoutController.cachedItemSizes[dataIndex].y / 2f + (offset * inverter);
                }

                listItem.rectTransform.anchoredPosition = new Vector2(posX, posY);
            }

            if (!m_IsDragging && m_LayoutController.itemsAreShown && m_IsMoving)
            {
                OnEndScroll();
            }
        }

        public void OnBeforeShow()
        {
            StartCoroutine(SelectItemCoroutine(m_LayoutController.listView.listData.listItems[0]));
            OnItemSelected(m_LayoutController.listView.listData.listItems[0], 0f);
        }

        public void OnShow()
        {
            if (m_LayoutController.listView.listData.listItems.Count == 1) return;

            if (!m_IndicatorPositionSet)
            {
                m_IndicatorContainerTransform.SetDefaultPosition(m_IndicatorContainerTransform.rectTransform
                    .anchoredPosition);
                m_IndicatorPositionSet = true;
            }

            m_IndicatorContainerTransform.TweenPosition(Direction.Down, Direction.Middle, 0.25f, 0.2f,
                Tween.TweenType.EaseOutQuint);
            m_IndicatorContainerCanvasGroup.TweenAlpha(1f, 0.25f, 0.2f, Tween.TweenType.EaseOutQuint,
                () =>
                {
                    m_IndicatorContainerCanvasGroup.canvasGroup.interactable = true;
                    m_IndicatorContainerCanvasGroup.canvasGroup.blocksRaycasts = true;
                });
        }

        public void OnHide()
        {
            m_IndicatorContainerTransform.TweenPosition(Direction.Down, 0.15f, 0.2f, Tween.TweenType.EaseOutQuint);
            m_IndicatorContainerCanvasGroup.TweenAlpha(0f, 0.15f, 0.2f, Tween.TweenType.EaseOutQuint,
                () =>
                {
                    m_IndicatorContainerCanvasGroup.canvasGroup.interactable = true;
                    m_IndicatorContainerCanvasGroup.canvasGroup.blocksRaycasts = true;
                });
        }

        public void OnBeginDrag(PointerEventData eventData)
        {
            m_WaitingForItemSelect = false;
            m_IsDragging = true;
            m_IsMoving = true;
            m_LayoutController.EndItemCenterTween();
        }

        public void OnEndDrag(PointerEventData eventData)
        {
            m_IsDragging = false;
            if (eventData.delta.x <= 0.5f)
            {
                OnEndScroll();
            }
        }

        private void OnEndScroll()
        {
            m_IsMoving = false;

            if (!m_LayoutController.itemsAreShown) return;

            bool layoutIsHorizontal =
                m_LayoutController.layoutDirection == ListLayoutControllerStandard.LayoutDirection.Horizontal;

            float velocity = layoutIsHorizontal
                ? m_LayoutController.listView.scrollRect.velocity.x
                : -m_LayoutController.listView.scrollRect.velocity.y;

            float currentPosition = layoutIsHorizontal
                ? -m_LayoutController.listView.listContent.rectTransform.anchoredPosition.x +
                  (m_LayoutController.listView.rectTransform.rect.width * 0.5f)
                : -m_LayoutController.listView.listContent.rectTransform.anchoredPosition.y -
                  (m_LayoutController.listView.rectTransform.rect.height * 0.5f);

            float closestItemDistance = float.MaxValue;
            int closestItemIndex = 0;

            for (int i = 0; i < m_LayoutController.cachedItemSizes.Count; i++)
            {
                float distance = layoutIsHorizontal
                    ? Mathf.Abs((m_LayoutController.cachedItemPositions[i].x +
                                 (m_LayoutController.cachedItemSizes[i].x / 2f)) - currentPosition)
                    : Mathf.Abs((m_LayoutController.cachedItemPositions[i].y -
                                 (m_LayoutController.cachedItemSizes[i].y / 2f)) - currentPosition);

                if (distance < closestItemDistance)
                {
                    closestItemIndex = i;
                    closestItemDistance = distance;
                }
            }

            int targetIndex = closestItemIndex;
            float duration = 0.5f;

            float scaledVelocity =
                Mathf.Max(Mathf.Sqrt(Mathf.Abs(velocity / (m_LayoutController.listView.rectTransform.rect.width * 2f))),
                    0f);
            int strength = 0;

            if (scaledVelocity < 2f && scaledVelocity >= 1f)
            {
                strength = Mathf.RoundToInt(scaledVelocity);
            }
            else if (scaledVelocity >= 1f)
            {
                strength = Mathf.FloorToInt(scaledVelocity);
            }
            else if (scaledVelocity >= 0.35f)
            {
                strength = Mathf.CeilToInt(scaledVelocity);
            }

            strength *= (velocity > 0 ? 1 : -1);
            targetIndex = Mathf.Clamp(closestItemIndex - strength, 0, m_LayoutController.cachedItemSizes.Count - 1);
            duration += Mathf.Abs(strength * 0.3f);

            StartCoroutine(SelectItemCoroutine(m_LayoutController.listView.listData.listItems[targetIndex]));
            OnItemSelected(m_LayoutController.listView.listData.listItems[targetIndex], duration);
        }

        private void TransitionIcon(int index, bool isSelected)
        {
            m_IconDatas[index].isSelected = isSelected;

            IndicatorIconData iconData = m_IconDatas[index];

            TweenManager.EndTween(iconData.colorTweenIndex);
            TweenManager.EndTween(iconData.sizeTweenIndex);

            iconData.colorTweenIndex = TweenManager.TweenColor(value => iconData.vectorImage.color = value,
                iconData.vectorImage.color, isSelected ? m_IconColorOn : m_IconColorOff, 0.5f,
                tweenType: Tween.TweenType.SoftEaseOutSept);

            iconData.sizeTweenIndex = TweenManager.TweenFloat(value => iconData.vectorImage.size = value,
                iconData.vectorImage.size, isSelected ? m_IconSizeOn : m_IconSizeOff, 0.5f,
                tweenType: Tween.TweenType.SoftEaseOutSept);
        }

        private IEnumerator SelectItemCoroutine(ListItemData itemData)
        {
            m_WaitingForItemSelect = true;

            ListItem item = m_LayoutController.GetDisplayedItemFromData(itemData);

            while (m_WaitingForItemSelect)
            {
                if (item != null)
                {
                    item.SelectWithoutMessages();
                    m_WaitingForItemSelect = false;
                    yield return null;
                }

                yield return new WaitForEndOfFrame();

                item = m_LayoutController.GetDisplayedItemFromData(itemData);
            }
        }
    }
}
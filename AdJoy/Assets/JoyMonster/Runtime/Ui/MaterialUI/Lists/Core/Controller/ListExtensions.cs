﻿namespace MaterialUI_JoyMonster
{
    public interface IListExtension
    {
        void OnListInitialized(ListView listView);

        void OnBeforeListDataChanged();

        void OnAfterListDataChanged();

        void OnBeforeListLayoutChanged();

        void OnAfterListLayoutChanged();

        void OnContentChanged();

        void OnBeforeShow();

        void OnShow();

        void OnHide();
    }
}
﻿namespace MaterialUI_JoyMonster
{
    public static partial class ListInteraction
    {
        public static readonly int selectNonPointer = 1;
        public static readonly int selectPointer = 2;
        public static readonly int deselect = 3;
        public static readonly int pointerEnter = 4;
        public static readonly int pointerExit = 5;
        public static readonly int pointerDown = 6;
        public static readonly int pointerUp = 7;
        public static readonly int pointerClick = 8;
    }
}
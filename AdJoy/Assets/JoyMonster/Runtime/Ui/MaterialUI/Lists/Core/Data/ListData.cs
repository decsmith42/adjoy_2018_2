﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace MaterialUI_JoyMonster
{
    [Serializable]
    public class ListData
    {
        [SerializeField] private List<ListItemData> m_ListItems = new List<ListItemData>();

        public List<ListItemData> listItems
        {
            get { return m_ListItems; }
            set { m_ListItems = value; }
        }
    }
}
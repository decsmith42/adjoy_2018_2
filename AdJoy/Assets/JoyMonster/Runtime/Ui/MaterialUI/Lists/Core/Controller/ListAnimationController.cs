﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace MaterialUI_JoyMonster
{
    public abstract class ListAnimationController
    {
        public class ListTweenInfo
        {
            private readonly ListItem m_Item;

            public ListItem item
            {
                get { return m_Item; }
            }

            private readonly int m_TweenIndex;

            public int tweenIndex
            {
                get { return m_TweenIndex; }
            }

            private readonly Action m_OnInterrupted;

            public Action onInterrupted
            {
                get { return m_OnInterrupted; }
            }

            public ListTweenInfo(ListItem item, int tweenIndex, Action onInterrupted)
            {
                m_Item = item;
                m_TweenIndex = tweenIndex;
                m_OnInterrupted = onInterrupted;
            }
        }

        protected List<ListTweenInfo> m_ActiveTweens = new List<ListTweenInfo>();

        protected List<ListTweenInfo> activeTweens
        {
            get { return m_ActiveTweens; }
        }

        protected float m_AnimationDuration = 0.3f;

        public float animationDuration
        {
            get { return m_AnimationDuration; }
            set { m_AnimationDuration = value; }
        }

        protected ListView m_ListView;

        public ListView listView
        {
            get { return m_ListView; }
        }

        public virtual void Initialize(ListView listView)
        {
            m_ListView = listView;
        }

        public virtual void Cleanup()
        {
            for (int i = 0; i < m_ActiveTweens.Count; i++)
            {
                InterruptItemTween(m_ActiveTweens[i]);
            }

            m_ActiveTweens.Clear();
        }

        protected virtual void InterruptItemTween(ListTweenInfo listTweenInfo, bool callCallback = true)
        {
            m_ActiveTweens.Remove(listTweenInfo);
            TweenManager.EndTween(listTweenInfo.tweenIndex, callCallback);
            listTweenInfo.onInterrupted.InvokeIfNotNull();
        }

        public virtual void AddItemTween(int tweenId, ListItem item, Action onCompleted, Action onInterrupted)
        {
            AutoTween tween = TweenManager.GetAutoTween(tweenId);
            if (tween.callback != null)
            {
                Debug.LogException(
                    new Exception("Tween callbacks may not be used in ListItem tweens. Use onCompleted instead."),
                    listView);
            }

            ListTweenInfo info = new ListTweenInfo(item, tweenId, onInterrupted);
            tween.callback = onCompleted + (() => m_ActiveTweens.Remove(info));
            m_ActiveTweens.Add(info);
        }

        public virtual void InterruptItemTween(int tweenId, bool callCallback = true)
        {
            InterruptItemTween(m_ActiveTweens.Find(info => info.tweenIndex == tweenId), callCallback);
        }

        public virtual void InterruptItemTweens(ListItem item, bool callCallbacks = true)
        {
            for (int i = 0; i < m_ActiveTweens.Count; i++)
            {
                ListTweenInfo info = m_ActiveTweens[i];
                if (info.item == item)
                {
                    InterruptItemTween(info, callCallbacks);
                }
            }
        }

        public virtual void AnimateListShow(Action onCompleted = null)
        {
            List<ListItem> sortedItems = listView.layoutController.GetSortedDisplayedItems();
            int itemCount = sortedItems.Count;

            for (int i = 0; i < itemCount; i++)
            {
                AnimateItemShow(sortedItems[i], i, PassActionIfLastItem(i, itemCount, onCompleted));
            }
        }

        public virtual void AnimateListHide(Action onCompleted = null)
        {
            List<ListItem> sortedItems = listView.layoutController.GetSortedDisplayedItems();
            int itemCount = sortedItems.Count;

            for (int i = 0; i < itemCount; i++)
            {
                AnimateItemHide(sortedItems[i], i, i == itemCount - 1 ? onCompleted : null);
            }
        }

        public abstract void AnimateItemShow(ListItem item, int displayedIndex, Action onCompleted);

        public abstract void AnimateItemHide(ListItem item, int displayedIndex, Action onCompleted = null);

        public abstract void AnimateItemMove(ListItem item, Vector2 targetPosition, int displayedIndex, Action onCompleted);

        public abstract void AnimateItemEmphasis(ListItem item, int displayedIndex, Action onCompleted = null);

        public virtual float GetDelayFromIndex(int index, bool includeInitialDelay = false)
        {
            return (includeInitialDelay ? m_AnimationDuration : 0f) + index * m_AnimationDuration * 0.1f;
        }

        public virtual Action PassActionIfLastItem(int i, int count, Action action)
        {
            return i == count - 1 ? action : null;
        }
    }
}
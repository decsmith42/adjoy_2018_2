﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace MaterialUI_JoyMonster
{
    public class ListItemSelectable : Selectable, IPointerClickHandler
    {
        protected bool m_SelectionIsFromPointer;

        private RectTransform m_RectTransform;
        public RectTransform rectTransform
        {
            get
            {
                if (m_RectTransform == null)
                {
                    m_RectTransform = transform as RectTransform;
                }
                return m_RectTransform;
            }
        }

        private ListItem m_Item;
        public ListItem item
        {
            get
            {
                if (m_Item == null)
                {
                    m_Item = GetComponentInParent<ListItem>();
                }
                return m_Item;
            }
        }

        private ListView m_ListView;
        public ListView listView
        {
            get { return m_ListView; }
            set { m_ListView = value; }
        }

        public override void OnSelect(BaseEventData eventData)
        {
            base.OnSelect(eventData);

            if (!m_SelectionIsFromPointer)
            {
                OnSelectNonPointer(eventData);
            }
            else
            {
                OnSelectPointer(eventData);
            }
        }

        public virtual void OnSelectNonPointer(BaseEventData eventData)
        {
            listView.layoutController.CenterItemInListView(rectTransform, 0.5f);
            listView.layoutController.lastSelectedItem = listView.layoutController.GetDisplayedDataFromItem(item);

            InteractionSelectNonPointer(eventData);
        }

        public virtual void OnSelectPointer(BaseEventData eventData)
        {
            InteractionSelectPointer(eventData);
        }

        public override void OnDeselect(BaseEventData eventData)
        {
            base.OnDeselect(eventData);

            ListItemData data = listView.layoutController.GetDisplayedDataFromItem(item);
            if (data != null && listView.layoutController.lastSelectedItem == data)
            {
                listView.layoutController.lastSelectedItem = null;
            }

            InteractionDeselect(eventData);
        }

        public override void OnPointerDown(PointerEventData eventData)
        {
            m_SelectionIsFromPointer = true;
            base.OnPointerDown(eventData);

            listView.layoutController.InterruptCenterItemTween();

            InteractionPointerDown(eventData);
        }

        public override void OnPointerUp(PointerEventData eventData)
        {
            m_SelectionIsFromPointer = false;
            base.OnPointerUp(eventData);

            InteractionPointerUp(eventData);
        }

        public override void OnPointerEnter(PointerEventData eventData)
        {
            base.OnPointerEnter(eventData);

            InteractionPointerEnter(eventData);
        }

        public override void OnPointerExit(PointerEventData eventData)
        {
            m_SelectionIsFromPointer = false;
            base.OnPointerExit(eventData);

            InteractionPointerExit(eventData);
        }

        public void OnPointerClick(PointerEventData eventData)
        {
            InteractionPointerClick(eventData);
        }

        protected virtual void InteractionSelectNonPointer(BaseEventData eventData) { }
        protected virtual void InteractionSelectPointer(BaseEventData eventData) { }
        protected virtual void InteractionDeselect(BaseEventData eventData) { }
        protected virtual void InteractionPointerDown(BaseEventData eventData) { }
        protected virtual void InteractionPointerUp(BaseEventData eventData) { }
        protected virtual void InteractionPointerEnter(BaseEventData eventData) { }
        protected virtual void InteractionPointerExit(BaseEventData eventData) { }
        protected virtual void InteractionPointerClick(BaseEventData eventData) { }
    }
}
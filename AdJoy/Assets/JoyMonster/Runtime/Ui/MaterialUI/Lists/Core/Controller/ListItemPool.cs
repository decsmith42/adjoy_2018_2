﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace MaterialUI_JoyMonster
{
    public class ListItemPool : MonoBehaviour
    {
        private ListLayoutController m_LayoutController;

        public ListLayoutController layoutController
        {
            get { return m_LayoutController; }
        }

        private Dictionary<Type, GameObject> m_ItemTemplates = new Dictionary<Type, GameObject>();

        public Dictionary<Type, GameObject> itemTemplates
        {
            get { return m_ItemTemplates; }
            set { m_ItemTemplates = value; }
        }

        private Dictionary<Type, List<ListItem>> m_ItemPoolDictionary = new Dictionary<Type, List<ListItem>>();

        public Dictionary<Type, List<ListItem>> itemPoolDictionary
        {
            get { return m_ItemPoolDictionary; }
            set { m_ItemPoolDictionary = value; }
        }

        private List<ListItem> m_TempPool = new List<ListItem>();

        public List<ListItem> tempPool
        {
            get { return m_TempPool; }
            set { m_TempPool = value; }
        }

        public static ListItemPool CreatePool(ListLayoutController layoutController)
        {
            ListItemPool pool = new GameObject("List Item Pool").AddComponent<ListItemPool>();
            pool.m_LayoutController = layoutController;
            pool.gameObject.SetActive(false);
            pool.transform.localScale = Vector3.one;
            pool.transform.position = Vector3.zero;
            return pool;
        }

        public void Cleanup(bool alsoDestroy)
        {
            FlushTempPool();
            m_ItemPoolDictionary.Clear();

            RectTransform[] children = GetComponentsInChildren<RectTransform>();

            for (int i = 0; i < children.Length; i++)
            {
                Destroy(children[i].gameObject);
            }

            if (alsoDestroy)
            {
                Destroy(gameObject);
            }
        }

        public void FlushTempPool()
        {
            for (int i = 0; i < m_TempPool.Count; i++)
            {
                ListItem item = m_TempPool[i];
                m_TempPool.Remove(item);
                ReleaseItem(item);
            }
        }

        public void ReleaseItem(ListItem listItem, bool releaseToTempPool = false)
        {
            listItem.OnItemReleaseToPool();

            if (listItem.currentData != null)
            {
                listItem.currentData.currentItem = null;
                listItem.currentData = null;
            }

            layoutController.listView.animationController.InterruptItemTweens(listItem);

            if (releaseToTempPool)
            {
                m_TempPool.Add(listItem);
                return;
            }

            listItem.rectTransform.SetParentAndScale(transform, Vector3.one);

            Type itemDataType = listItem.matchingDataType;

            if (!m_ItemPoolDictionary.ContainsKey(itemDataType))
            {
                m_ItemPoolDictionary.Add(itemDataType, new List<ListItem>());
            }

            m_ItemPoolDictionary[itemDataType].Add(listItem);
        }

        public ListItem GetItem(ListItemData itemData, Transform parent, bool useTempPool = true)
        {
            ListItem item = null;
            Type itemDataType = itemData.GetType();

            if (useTempPool)
            {
                //Attempt to get from temp pool (faster)
                for (int i = 0; i < m_TempPool.Count; i++)
                {
                    if (item != null) continue;

                    if (m_TempPool[i].matchingDataType == itemDataType)
                    {
                        item = m_TempPool[i];
                        m_TempPool.Remove(item);
                    }
                }
            }

            //  Attempt to get from pool
            if (item == null)
            {
                if (m_ItemPoolDictionary.ContainsKey(itemDataType))
                {
                    if (m_ItemPoolDictionary[itemDataType].Count > 0)
                    {
                        item = m_ItemPoolDictionary[itemDataType][0];
                        m_ItemPoolDictionary[itemDataType].Remove(item);
                        item.rectTransform.SetParentAndScale(parent, Vector3.one);
                    }
                }
            }

            //  Create new item (slower)
            if (item == null)
            {
                item = CreateItem(itemData);
                item.rectTransform.SetParentAndScale(parent, Vector3.one);
            }

            item.currentData = itemData;
            itemData.currentItem = item;

            item.canvasGroup.alpha = 1f;

            return item;
        }

        public ListItem GetTemplatePrefab<T>(string itemPrefabPath) where T : ListItemData
        {
            Type itemDataType = typeof(T);

            if (!m_ItemTemplates.ContainsKey(itemDataType))
            {
                m_ItemTemplates.Add(itemDataType, Resources.Load<GameObject>(itemPrefabPath));
            }

            return m_ItemTemplates[itemDataType].GetComponent<ListItem>();
        }

        public ListItem GetTemplatePrefab(ListItemData itemData)
        {
            Type itemDataType = itemData.GetType();

            if (!m_ItemTemplates.ContainsKey(itemDataType))
            {
                m_ItemTemplates.Add(itemDataType,
                    Resources.Load<GameObject>(layoutController.GetItemController(itemData).itemPrefabPath));
            }

            return m_ItemTemplates[itemDataType].GetComponent<ListItem>();
        }

        public void SetTemplatePrefab(Type itemDataType, GameObject prefab)
        {
            if (!m_ItemTemplates.ContainsKey(itemDataType))
            {
                m_ItemTemplates.Add(itemDataType, prefab);
            }
            else
            {
                m_ItemTemplates[itemDataType] = prefab;
            }
        }

        private ListItem CreateItem(ListItemData itemData)
        {
            Type itemDataType = itemData.GetType();

            if (!m_ItemTemplates.ContainsKey(itemDataType))
            {
                m_ItemTemplates.Add(itemDataType,
                    Resources.Load<GameObject>(layoutController.GetItemController(itemData).itemPrefabPath));
            }

            return Instantiate(m_ItemTemplates[itemDataType]).GetComponent<ListItem>();
        }
    }
}
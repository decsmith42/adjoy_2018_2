﻿using UnityEngine;

namespace MaterialUI_JoyMonster
{
    public class ListContent : MonoBehaviour
    {
        /// <summary>
        /// The object's RectTransform component.
        /// </summary>
        private RectTransform m_RectTransform;

        /// <summary>
        /// The object's RectTransform component.
        /// </summary>
        public RectTransform rectTransform
        {
            get
            {
                if (m_RectTransform == null)
                {
                    m_RectTransform = transform as RectTransform;
                }

                return m_RectTransform;
            }
        }
    }
}
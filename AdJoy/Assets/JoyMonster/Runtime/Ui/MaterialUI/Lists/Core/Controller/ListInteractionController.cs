﻿using System;
using UnityEngine.EventSystems;

namespace MaterialUI_JoyMonster
{
    public abstract class ListInteractionController
    {
        protected ListView m_ListView;

        public ListView listView
        {
            get { return m_ListView; }
        }

        protected Action<int, BaseEventData, ListItem, int> m_OnItemInteraction;

        public Action<int, BaseEventData, ListItem, int> onItemInteraction
        {
            get { return m_OnItemInteraction; }
            set { m_OnItemInteraction = value; }
        }

        public virtual void Initialize(ListView listView)
        {
            m_ListView = listView;
        }

        public virtual void Cleanup()
        {
            m_OnItemInteraction = null;
        }

        public virtual void OnItemInteraction(int interactionType, BaseEventData eventData, ListItem listItem,
            int subItemId)
        {
            if (m_OnItemInteraction != null)
            {
                m_OnItemInteraction(interactionType, eventData, listItem, subItemId);
            }
        }
    }
}
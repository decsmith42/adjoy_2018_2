﻿using UnityEngine;

namespace MaterialUI_JoyMonster
{
    public class ListEmptyContent : MonoBehaviour
    {
        /// <summary>
        /// The object's RectTransform component.
        /// </summary>
        protected RectTransform m_RectTransform;

        /// <summary>
        /// The object's RectTransform component.
        /// </summary>
        public RectTransform rectTransform
        {
            get
            {
                if (m_RectTransform == null)
                {
                    m_RectTransform = transform as RectTransform;
                }

                return m_RectTransform;
            }
        }

        protected CanvasGroup m_CanvasGroup;

        public CanvasGroup canvasGroup
        {
            get
            {
                if (m_CanvasGroup == null)
                {
                    m_CanvasGroup = gameObject.GetAddComponent<CanvasGroup>();
                }

                return m_CanvasGroup;
            }
        }

        protected int m_AnimationTweenIndex;

        public virtual void ShowEmptyContent()
        {
            TweenManager.EndTween(m_AnimationTweenIndex);

            gameObject.SetActive(true);

            m_AnimationTweenIndex = TweenManager.TweenFloat(f => canvasGroup.alpha = f, canvasGroup.alpha, 1f, 0.5f);
        }

        public virtual void HideEmptyContent()
        {
            TweenManager.EndTween(m_AnimationTweenIndex);

            m_AnimationTweenIndex = TweenManager.TweenFloat(f => canvasGroup.alpha = f, canvasGroup.alpha, 0f, 0.5f, 0f,
                () => gameObject.SetActive(false));
        }
    }
}
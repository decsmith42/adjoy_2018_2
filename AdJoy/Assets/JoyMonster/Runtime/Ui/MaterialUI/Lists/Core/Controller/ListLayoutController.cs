﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace MaterialUI_JoyMonster
{
    public abstract class ListLayoutController
    {
        public enum ListTimedCallbackType
        {
            Custom,
            TrimList,
            RebuildListItems,
            ClearDisplayedItems,
            StopListContentVelocity
        }

        public class ListTimedCallbackInfo
        {
            private readonly int m_TimedCallbackIndex;

            public int timedCallbackIndex
            {
                get { return m_TimedCallbackIndex; }
            }

            private readonly string m_CustomCallbackType;

            public string customCallbackType
            {
                get { return m_CustomCallbackType; }
            }

            private readonly ListTimedCallbackType m_CallbackType;

            public ListTimedCallbackType callbackType
            {
                get { return m_CallbackType; }
            }

            public ListTimedCallbackInfo(int timedCallbackIndex, ListTimedCallbackType callbackType)
            {
                m_TimedCallbackIndex = timedCallbackIndex;
                m_CallbackType = callbackType;
            }

            public ListTimedCallbackInfo(int timedCallbackIndex, string customCallbackType)
            {
                m_TimedCallbackIndex = timedCallbackIndex;
                m_CallbackType = ListTimedCallbackType.Custom;
                m_CustomCallbackType = customCallbackType;
            }
        }

        private List<ListTimedCallbackInfo> m_TimedCallbackInfos = new List<ListTimedCallbackInfo>();

        public List<ListTimedCallbackInfo> timedCallbackInfos
        {
            get { return m_TimedCallbackInfos; }
        }

        protected ListView m_ListView;

        public ListView listView
        {
            get { return m_ListView; }
        }

        protected ListItemPool m_ItemPool;

        public ListItemPool itemPool
        {
            get { return m_ItemPool; }
        }

        protected Dictionary<Type, ListItemController> m_ItemControllers = new Dictionary<Type, ListItemController>();

        public Dictionary<Type, ListItemController> itemControllers
        {
            get { return m_ItemControllers; }
        }

        protected List<ListItem> m_DisplayedItems = new List<ListItem>();

        public List<ListItem> displayedItems
        {
            get { return m_DisplayedItems; }
        }

        protected List<ListItemData> m_DisplayedItemDatas = new List<ListItemData>();

        public List<ListItemData> displayedItemDatas
        {
            get { return m_DisplayedItemDatas; }
        }

        protected List<ListItemData> m_IgnoredItemDatas = new List<ListItemData>();

        public List<ListItemData> ignoredItemDatas
        {
            get { return m_IgnoredItemDatas; }
        }

        protected ListItemData m_LastSelectedItem;

        public ListItemData lastSelectedItem
        {
            get { return m_LastSelectedItem; }
            set { m_LastSelectedItem = value; }
        }

        protected bool m_RefreshOnScroll;

        public bool refreshOnScroll
        {
            get { return m_RefreshOnScroll; }
            set { m_RefreshOnScroll = value; }
        }

        protected int m_ItemCenterTweenIndex = -1;

        public int itemCenterTweenIndex
        {
            get { return m_ItemCenterTweenIndex; }
            set { m_ItemCenterTweenIndex = value; }
        }

        protected List<Vector2> m_CachedItemSizes = new List<Vector2>();

        public List<Vector2> cachedItemSizes
        {
            get { return m_CachedItemSizes; }
        }

        protected List<Vector2> m_CachedItemPositions = new List<Vector2>();

        public List<Vector2> cachedItemPositions
        {
            get { return m_CachedItemPositions; }
        }

        protected Vector2 m_CachedListContentSize;

        public Vector2 cachedListContentSize
        {
            get { return m_CachedListContentSize; }
            set { m_CachedListContentSize = value; }
        }

        protected Vector2 m_CachedMinItemSize;

        public Vector2 cachedMinItemSize
        {
            get { return m_CachedMinItemSize; }
        }

        protected int m_CachedListDataSize;

        public int cachedListDataSize
        {
            get { return m_CachedListDataSize; }
        }

        protected bool m_IsRefreshPass;

        public bool isRefreshPass
        {
            get { return m_IsRefreshPass; }
        }

        protected int m_RecentFirstItemIndex;

        public int recentFirstItemIndex
        {
            get { return m_RecentFirstItemIndex; }
        }

        protected int m_RecentLastItemIndex;

        public int recentLastItemIndex
        {
            get { return m_RecentLastItemIndex; }
        }

        protected Vector2 m_RecentRefreshedPosition;

        public Vector2 recentRefreshedPosition
        {
            get { return m_RecentRefreshedPosition; }
        }

        protected bool m_ItemsAreShown;

        public bool itemsAreShown
        {
            get { return m_ItemsAreShown; }
        }

        protected Comparison<ListItem> m_DisplayedItemSorter;

        public Comparison<ListItem> displayedItemSorter
        {
            get
            {
                if (m_DisplayedItemSorter == null)
                {
                    m_DisplayedItemSorter = GetDisplayedItemSorter();
                }

                return m_DisplayedItemSorter;
            }
        }

        protected abstract Comparison<ListItem> GetDisplayedItemSorter();

        protected Action m_OnListDataChange;

        public Action onListDataChanged
        {
            get { return m_OnListDataChange; }
            set { m_OnListDataChange = value; }
        }

        protected bool m_CachingListData;
        protected bool m_CachingLayout;

        public Action m_OnBeforeLayoutCalculate;

        public Action onBeforeLayoutCalculate
        {
            get { return m_OnBeforeLayoutCalculate; }
            set { m_OnBeforeLayoutCalculate = value; }
        }

        public Action m_OnAfterLayoutCalculate;

        public Action onAfterLayoutCalculate
        {
            get { return m_OnAfterLayoutCalculate; }
            set { m_OnAfterLayoutCalculate = value; }
        }

        public virtual void Initialize(ListView listView)
        {
            m_ListView = listView;
            if (m_ItemPool == null)
            {
                m_ItemPool = ListItemPool.CreatePool(this);
            }

            SetContentSize();
            listView.scrollRect.onValueChanged.AddListener(vector2 =>
            {
                if (m_RefreshOnScroll)
                {
                    OnContentPositionChanged();
                }
            });
        }

        public virtual void Show()
        {
            if (m_ItemsAreShown) return;
            m_ItemsAreShown = true;

            CancelTimedCallback(ListTimedCallbackType.ClearDisplayedItems);
            m_IsRefreshPass = false;
            RebuildListItems();
            listView.animationController.AnimateListShow();

            m_RefreshOnScroll = true;

            //listView.rootScaler.onCanvasAreaChanged.AddListener((scaleChanged, orientationChanged) => OnListLayoutChange());
        }

        public virtual void Hide()
        {
            if (!m_ItemsAreShown) return;

            refreshOnScroll = false;
            listView.animationController.AnimateListHide();

            float delay = listView.animationController.GetDelayFromIndex(m_DisplayedItems.Count, true);
            StartTimedCallback(delay, ListTimedCallbackType.ClearDisplayedItems);
            listView.scrollRect.velocity = Vector2.zero;

            m_ItemsAreShown = false;

            //listView.rootScaler.onCanvasAreaChanged.RemoveListener((scaleChanged, orientationChanged) =>
            //    OnListLayoutChange());
        }

        public virtual void Cleanup(bool destroyPool)
        {
            listView.scrollRect.onValueChanged.RemoveListener(vector2 =>
            {
                if (m_RefreshOnScroll)
                {
                    OnContentPositionChanged();
                }
            });
            ClearDisplayedItems();
            itemPool.Cleanup(destroyPool);
        }

        public abstract void SetContentSize();

        public virtual ListItemController GetItemController(ListItemData itemData,
            ListLayoutController layoutController = null)
        {
            if (!m_ItemControllers.ContainsKey(itemData.GetType()))
            {
                ListItemController controller = itemData.GetNewController();
                controller.layoutController = this;
                if (layoutController != null)
                {
                    controller.Initialize(layoutController);
                }

                m_ItemControllers.Add(itemData.GetType(), controller);
            }

            return m_ItemControllers[itemData.GetType()];
        }

        public virtual void OnListDataPartialChange(int firstChangedDataIndex, int lastChangedDataIndex,
            bool itemSizesChanged)
        {
            OnListDataChange();
        }

        public virtual void OnListDataChange(bool force = false)
        {
            if ((m_CachingListData || !itemsAreShown) && !force) return;

            m_CachingListData = true;

            foreach (IListExtension extension in listView.listExtensions)
            {
                extension.OnBeforeListDataChanged();
            }

            OnListLayoutChange();

            foreach (IListExtension extension in listView.listExtensions)
            {
                extension.OnAfterListDataChanged();
            }

            m_CachingListData = false;
        }

        public virtual void OnListLayoutChange()
        {
            if (m_CachingLayout || m_ListView.listData == null || m_ListView.listData.listItems.Count == 0) return;

            m_CachingLayout = true;

            foreach (IListExtension extension in listView.listExtensions)
            {
                extension.OnBeforeListLayoutChanged();
            }

            int centerItemIndex = 0;
            bool itemsWereShown = m_ItemsAreShown;

            if (itemsWereShown)
            {
                int[] firstAndLastIndexes = GetFirstAndLastItemDataIndexes();
                centerItemIndex = Mathf.RoundToInt((firstAndLastIndexes[0] + firstAndLastIndexes[1]) / 2f);
            }

            ClearDisplayedItems();
            CacheListData();
            SetContentSize();

            foreach (ListItemController itemController in itemControllers.Values)
            {
                itemController.OnLayoutChange();
            }

            if (itemsWereShown)
            {
                RebuildListItems();
            }

            foreach (IListExtension extension in listView.listExtensions)
            {
                extension.OnAfterListLayoutChanged();
            }

            if (itemsWereShown)
            {
                CenterItemInListView(listView.listData.listItems[centerItemIndex], 0f, 0f, true);
            }

            m_CachingLayout = false;
        }

        public virtual void ReorderItem(int oldIndex, int newIndex)
        {
            ClearDisplayedItems(true);

            ListItemData tempData = listView.listData.listItems[newIndex];
            listView.listData.listItems[newIndex] = listView.listData.listItems[oldIndex];
            listView.listData.listItems[oldIndex] = tempData;

            RebuildListItems();
            itemPool.FlushTempPool();
        }

        public virtual void AddListItems(ListItemData[] itemDatas, int index = -1)
        {
            ClearDisplayedItems(true);

            if (index == -1)
            {
                listView.listData.listItems.AddRange(itemDatas);
            }
            else
            {
                listView.listData.listItems.InsertRange(index, itemDatas);
            }

            CacheListData();
            SetContentSize();
            RefreshListItems();
        }

        public virtual void AddListItem(ListItemData itemData)
        {
            AddListItems(new[] { itemData });
        }

        public virtual void RefreshListItems()
        {
            int[] firstAndLastItems = GetFirstAndLastItemDataIndexes();
            m_RecentFirstItemIndex = firstAndLastItems[0];
            m_RecentLastItemIndex = firstAndLastItems[1];

            m_IsRefreshPass = true;
            TrimListItems();
            RebuildListItems();
            m_IsRefreshPass = false;
            itemPool.FlushTempPool();
        }

        public virtual void UpdateMultipleHiddenItems(bool[] hiddenItems)
        {
            if (hiddenItems.Length != m_CachedListDataSize)
            {
                Debug.LogWarning(
                    "Array argument 'hiddenItems' length doesn't match list data length. This may lead to errors.");
            }

            bool[] previouslyHidden = new bool[m_CachedListDataSize];

            for (int i = 0; i < m_CachedListDataSize; i++)
            {
                previouslyHidden[i] = listView.listData.listItems[i].hidden;
                listView.listData.listItems[i].hidden = hiddenItems[i];
            }

            if (!itemsAreShown) return;

            Vector3[] globalPositions = new Vector3[m_DisplayedItems.Count];

            for (int i = 0; i < m_DisplayedItems.Count; i++)
            {
                globalPositions[i] = m_DisplayedItems[i].transform.position;
            }

            m_CachedListDataSize = listView.listData.listItems.Count;
            m_CachedItemSizes.Clear();
            m_CachedItemPositions.Clear();

            CacheListSizesAndPositions();
            SetContentSize();

            listView.scrollRect.normalizedPosition = new Vector2(listView.scrollRect.normalizedPosition.x, 1f);

            for (int i = 0; i < m_DisplayedItems.Count; i++)
            {
                m_DisplayedItems[i].transform.position = globalPositions[i];
            }

            int[] firstAndLastItems = GetFirstAndLastItemDataIndexes();
            m_RecentFirstItemIndex = firstAndLastItems[0];
            m_RecentLastItemIndex = firstAndLastItems[1];

            for (int i = 0; i < m_CachedListDataSize; i++)
            {
                ListItemData data = listView.listData.listItems[i];

                int displayedIndex = GetDisplayedItemIndex(data);

                if (previouslyHidden[i])
                {
                    if (!data.hidden && i >= m_RecentFirstItemIndex && i <= m_RecentLastItemIndex)
                    {
                        FormatItem(i, false);
                        listView.animationController.AnimateItemShow(GetDisplayedItemFromData(data).item, displayedIndex + (m_RecentLastItemIndex - m_RecentFirstItemIndex), null);
                    }
                }
                else
                {
                    if (!data.hidden)
                    {
                        if (i >= m_RecentFirstItemIndex && i <= m_RecentLastItemIndex)
                        {
                            if (!m_DisplayedItemDatas.Contains(data))
                            {
                                FormatItem(i, false);
                                listView.animationController.AnimateItemShow(GetDisplayedItemFromData(data).item, displayedIndex + (m_RecentLastItemIndex - m_RecentFirstItemIndex), null);
                            }
                            else
                            {
                                listView.animationController.AnimateItemMove(GetDisplayedItemFromData(data).item, m_CachedItemPositions[i], displayedIndex + (m_RecentLastItemIndex - m_RecentFirstItemIndex) + 8, null);
                            }
                        }
                        else
                        {
                            if (m_DisplayedItemDatas.Contains(data))
                            {
                                listView.animationController.AnimateItemHide(GetDisplayedItemFromData(data).item, displayedIndex);
                            }
                        }
                    }
                    else
                    {
                        if (m_DisplayedItemDatas.Contains(data))
                        {
                            listView.animationController.AnimateItemHide(GetDisplayedItemFromData(data).item, displayedIndex);
                        }
                    }
                }
            }

            StartTimedCallback(listView.animationController.GetDelayFromIndex(m_CachedListDataSize - 1, true), ListTimedCallbackType.TrimList);
        }

        public virtual void StartTimedCallback(float duration, ListTimedCallbackType callbackType)
        {
            Action callback = null;

            switch (callbackType)
            {
                case ListTimedCallbackType.TrimList:
                    callback = TrimListItems;
                    break;
                case ListTimedCallbackType.RebuildListItems:
                    callback = RebuildListItems;
                    break;
                case ListTimedCallbackType.ClearDisplayedItems:
                    callback = () => ClearDisplayedItems();
                    break;
                case ListTimedCallbackType.StopListContentVelocity:
                    callback = () => listView.scrollRect.velocity = Vector2.zero;
                    break;
            }

            int timedCallbackIndex = TweenManager.TimedCallback(duration, callback);

            ListTimedCallbackInfo info = new ListTimedCallbackInfo(timedCallbackIndex, callbackType);

            TweenManager.GetAutoTween(timedCallbackIndex).callback += () => m_TimedCallbackInfos.Remove(info);

            m_TimedCallbackInfos.Add(info);
        }

        public virtual void StartTimedCallback(float duration, Action callback, string customCallbackType)
        {
            int timedCallbackIndex = TweenManager.TimedCallback(duration, callback);

            ListTimedCallbackInfo info = new ListTimedCallbackInfo(timedCallbackIndex, customCallbackType);

            TweenManager.GetAutoTween(timedCallbackIndex).callback += () => m_TimedCallbackInfos.Remove(info);

            m_TimedCallbackInfos.Add(info);
        }

        public virtual void CancelTimedCallback(ListTimedCallbackType callbackType)
        {
            for (int i = 0; i < m_TimedCallbackInfos.Count; i++)
            {
                ListTimedCallbackInfo info = m_TimedCallbackInfos[i];
                if (info.callbackType == callbackType)
                {
                    TweenManager.EndTween(info.timedCallbackIndex);
                    m_TimedCallbackInfos.Remove(info);
                }
            }
        }

        public virtual void CancelTimedCallback(string customCallbackType)
        {
            for (int i = 0; i < m_TimedCallbackInfos.Count; i++)
            {
                ListTimedCallbackInfo info = m_TimedCallbackInfos[i];
                if (info.callbackType == ListTimedCallbackType.Custom)
                {
                    if (info.customCallbackType == customCallbackType)
                    {
                        TweenManager.EndTween(info.timedCallbackIndex);
                        m_TimedCallbackInfos.Remove(info);
                    }
                }
            }
        }

        public virtual void InterruptCenterItemTween()
        {
            TweenManager.EndTween(m_ItemCenterTweenIndex);
        }

        public virtual List<ListItem> GetSortedDisplayedItems()
        {
            List<ListItem> items = new List<ListItem>();

            for (int i = 0; i < m_DisplayedItems.Count; i++)
            {
                items.Add(m_DisplayedItems[i]);
            }

            items.Sort(displayedItemSorter);

            return items;
        }

        public virtual ListItem GetDisplayedItemFromData(ListItemData data)
        {
            for (int i = 0; i < m_DisplayedItemDatas.Count; i++)
            {
                if (m_DisplayedItemDatas[i] == data)
                {
                    return m_DisplayedItems[i];
                }
            }

            return null;
        }

        public virtual ListItemData GetDisplayedDataFromItem(ListItem item)
        {
            for (int i = 0; i < m_DisplayedItems.Count; i++)
            {
                if (m_DisplayedItems[i] == item)
                {
                    return m_DisplayedItemDatas[i];
                }
            }

            return null;
        }

        public virtual int GetDisplayedItemIndex(ListItem item)
        {
            for (int i = 0; i < m_DisplayedItems.Count; i++)
            {
                if (m_DisplayedItems[i] == item)
                {
                    return i;
                }
            }

            return -1;
        }

        public virtual int GetDisplayedItemIndex(ListItemData data)
        {
            for (int i = 0; i < m_DisplayedItemDatas.Count; i++)
            {
                if (m_DisplayedItemDatas[i] == data)
                {
                    return i;
                }
            }

            return -1;
        }

        public virtual int GetDataIndexFromDisplayedItemData(ListItemData itemData)
        {
            for (int i = 0; i < listView.listData.listItems.Count; i++)
            {
                if (listView.listData.listItems[i] == itemData)
                {
                    return i;
                }
            }

            return -1;
        }

        public virtual void OnContentPositionChanged()
        {
            if (!m_ItemsAreShown || !m_RefreshOnScroll) return;

            Vector2 currentPos = listView.listContent.rectTransform.anchoredPosition;

            m_RecentRefreshedPosition = currentPos;

            int[] firstAndLastItems = GetFirstAndLastItemDataIndexes();
            int[] recentFirstAndLastItems = { m_RecentFirstItemIndex, m_RecentLastItemIndex };

            if (firstAndLastItems[0] != recentFirstAndLastItems[0] || firstAndLastItems[1] != recentFirstAndLastItems[1])
            {
                RefreshListItems();
            }

            listView.OnContentChanged();
        }

        public abstract int[] GetFirstAndLastItemDataIndexes();

        public virtual void CacheListData()
        {
            m_OnBeforeLayoutCalculate.InvokeIfNotNull();

            m_CachedListDataSize = listView.listData.listItems.Count;

            m_CachedItemSizes.Clear();
            m_CachedItemPositions.Clear();

            CacheListSizesAndPositions();

            m_OnAfterLayoutCalculate.InvokeIfNotNull();
        }

        public abstract void CacheListSizesAndPositions();

        public virtual void TrimListItems()
        {
            if (m_DisplayedItemDatas.Count == 0) return;

            List<ListItem> itemsToRemove = new List<ListItem>();
            List<ListItemData> itemDatasToRemove = new List<ListItemData>();

            int[] firstAndLastItemDataIndexes = m_IsRefreshPass
                ? new[] { m_RecentFirstItemIndex, m_RecentLastItemIndex }
                : GetFirstAndLastItemDataIndexes();
            for (int i = 0; i < m_DisplayedItemDatas.Count; i++)
            {
                int dataIndex = listView.listData.listItems.IndexOf(m_DisplayedItemDatas[i]);
                if (dataIndex < firstAndLastItemDataIndexes[0] || dataIndex > firstAndLastItemDataIndexes[1] ||
                    listView.listData.listItems[dataIndex].hidden)
                {
                    itemPool.ReleaseItem(m_DisplayedItems[i], m_IsRefreshPass);

                    itemsToRemove.Add(m_DisplayedItems[i]);
                    itemDatasToRemove.Add(m_DisplayedItemDatas[i]);
                }
            }

            for (int i = 0; i < itemsToRemove.Count; i++)
            {
                m_DisplayedItems.Remove(itemsToRemove[i]);
                m_DisplayedItemDatas.Remove(itemDatasToRemove[i]);
            }
        }

        public virtual void RebuildListItems()
        {
            m_ItemsAreShown = true;

            int[] firstAndLastItemDataIndexes = m_IsRefreshPass
                ? new[] { m_RecentFirstItemIndex, m_RecentLastItemIndex }
                : GetFirstAndLastItemDataIndexes();

            for (int i = firstAndLastItemDataIndexes[0]; i <= firstAndLastItemDataIndexes[1]; i++)
            {
                FormatItem(i);
            }
        }

        public virtual void FormatItem(int index, bool useTempPool = true, bool forceFormatIgnoredItem = false)
        {
            ListItemData data = listView.listData.listItems[index];

            if (!forceFormatIgnoredItem && m_IgnoredItemDatas.Contains(data))
            {
                return;
            }

            if (data.hidden)
            {
                if (data.currentItem != null)
                {
                    itemPool.ReleaseItem(data.currentItem);
                }

                return;
            }

            if (!m_DisplayedItemDatas.Contains(data))
            {
                ListItem item = itemPool.GetItem(data, listView.listContent.rectTransform, useTempPool);
                item.listView = listView;

                SetItemPosition(item, index);
                GetItemController(data).FormatItemWithData(item, data);

                m_DisplayedItemDatas.Add(data);
                m_DisplayedItems.Add(item);
            }
        }

        public abstract void SetItemPosition(ListItem item, int dataIndex);

        public virtual void ClearDisplayedItems(bool useTempPool = false, bool forceClearIgnoredItems = false)
        {
            for (int i = 0; i < m_DisplayedItems.Count; i++)
            {
                if (forceClearIgnoredItems || !m_IgnoredItemDatas.Contains(m_DisplayedItemDatas[i]))
                {
                    itemPool.ReleaseItem(m_DisplayedItems[i], useTempPool);
                }
            }

            m_DisplayedItems.Clear();
            m_DisplayedItemDatas.Clear();
            m_ItemsAreShown = false;
        }

        public virtual void CenterItemInListView(Vector2 itemPosition, Vector2 itemSize, float tolerance,
            float duration = 0.5f, bool instant = false)
        {
            tolerance *= 0.5f;
            itemSize *= tolerance;

            Vector2 viewSize = listView.rectTransform.rect.size;
            Vector2 centerOfView = listView.rectTransform.GetPositionRegardlessOfPivot();

            bool centerX = Mathf.Abs(itemPosition.x - centerOfView.x) - itemSize.x > viewSize.x * tolerance;
            bool centerY = Mathf.Abs(itemPosition.y - centerOfView.y) + itemSize.y > viewSize.y * tolerance;

            if (centerX || centerY)
            {
                TweenManager.EndTween(m_ItemCenterTweenIndex);

                RectTransform listContentTransform = listView.listContent.rectTransform;

                Vector2 listContentPosition = listContentTransform.anchoredPosition;

                Vector2 distanceToMove = new Vector2();
                Vector2 newPosition = new Vector2();
                Vector2 bounds = new Vector2();

                if (centerX)
                {
                    //distanceToMove.x = (Mathf.Abs(itemPosition.x - centerOfView.x) - viewSize.x * tolerance - itemSize.x) /
                    //                   listView.rootScaler.scaleFactor;
                    distanceToMove.x = (Mathf.Abs(itemPosition.x - centerOfView.x) - viewSize.x * tolerance - itemSize.x) /
                                       UiScaleController.scale;
                    newPosition.x = itemPosition.x < centerOfView.x
                        ? listContentPosition.x + distanceToMove.x
                        : listContentPosition.x - distanceToMove.x;
                    bounds.x = viewSize.x < listContentTransform.rect.width
                        ? listContentTransform.rect.width + viewSize.x
                        : 0f;
                    newPosition.x = Mathf.Clamp(newPosition.x, -bounds.x, 0f);
                }
                else
                {
                    newPosition.x = listContentPosition.x;
                }

                if (centerY)
                {
                    //distanceToMove.y = (Mathf.Abs(itemPosition.y - centerOfView.y) - viewSize.y * tolerance + itemSize.y) /
                    //                   listView.rootScaler.scaleFactor;
                    distanceToMove.y = (Mathf.Abs(itemPosition.y - centerOfView.y) - viewSize.y * tolerance + itemSize.y) /
                                       UiScaleController.scale;
                    newPosition.y = itemPosition.y > centerOfView.y
                        ? listContentPosition.y - distanceToMove.y
                        : listContentPosition.y + distanceToMove.y;
                    bounds.y = viewSize.y < listContentTransform.rect.height
                        ? listContentTransform.rect.height - viewSize.y
                        : 0f;
                    newPosition.y = Mathf.Clamp(newPosition.y, 0f, bounds.y);
                }
                else
                {
                    newPosition.y = listContentPosition.y;
                }

                if (instant)
                {
                    listContentTransform.anchoredPosition = newPosition;
                    OnContentPositionChanged();
                    return;
                }

                Action<Vector2> updateListContentPosition = vector2 =>
                {
                    listContentTransform.anchoredPosition = vector2;
                    OnContentPositionChanged();
                };

                m_ItemCenterTweenIndex =
                    TweenManager.TweenVector2(updateListContentPosition, listContentPosition, newPosition, duration);
            }
        }

        public virtual void CenterItemInListView(RectTransform item, float tolerance, float duration = 0.5f,
            bool instant = false)
        {
            CenterItemInListView(item.GetPositionRegardlessOfPivot(), item.rect.size, tolerance, duration, instant);
        }

        public virtual void CenterItemInListView(ListItemData itemData, float tolerance, float duration = 0.5f,
            bool instant = false)
        {
            for (int i = 0; i < listView.listData.listItems.Count; i++)
            {
                if (listView.listData.listItems[i] == itemData)
                {
                    ListItemController listItemController = GetItemController(itemData);
                    Vector2 itemSize = new Vector2();
                    itemSize.x = listItemController.GetItemWidth(itemData);
                    itemSize.y = listItemController.GetItemHeight(itemData);
                    CenterItemInListView(
                        listView.listContent.rectTransform.TransformPoint(m_CachedItemPositions[i] - itemSize / 2f),
                        itemSize, tolerance, duration, instant);
                    return;
                }
            }

            Debug.LogException(new Exception("Trying to center item that isn't in the list's data."), listView);
        }

        public void EndItemCenterTween()
        {
            TweenManager.EndTween(m_ItemCenterTweenIndex);
        }
    }
}
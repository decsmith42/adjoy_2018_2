﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace MaterialUI_JoyMonster
{
    public abstract class ListItemController
    {
        protected ListLayoutController m_LayoutController;

        public ListLayoutController layoutController
        {
            get { return m_LayoutController; }
            set { m_LayoutController = value; }
        }

        protected readonly Dictionary<ListItemData, Vector2> m_ItemSizes = new Dictionary<ListItemData, Vector2>();

        public abstract string itemPrefabPath { get; }
        public abstract Type matchingDataType { get; }

        public abstract float minItemWidth { get; }

        public abstract float minItemHeight { get; }

        protected bool m_IsInitialized;

        public virtual void Initialize(ListLayoutController layoutController)
        {
            m_LayoutController = layoutController;
            OnInitialized();
            m_IsInitialized = true;
        }

        protected virtual void OnInitialized() { }

        public virtual void CalculateItemWidth(ListItemData itemData, float availableWidth, float height)
        {
            SetItemWidth(itemData, itemData.hidden ? 0f : Mathf.Max(minItemWidth, availableWidth));
        }

        public virtual void CalculateItemHeight(ListItemData itemData, float availableHeight, float width)
        {
            SetItemHeight(itemData, itemData.hidden ? 0f : Mathf.Max(minItemHeight, availableHeight));
        }

        public abstract void FormatItemWithData(ListItem item, ListItemData data);

        public virtual float GetItemWidth(ListItemData itemData)
        {
            return m_ItemSizes.ContainsKey(itemData) ? m_ItemSizes[itemData].x : minItemWidth;
        }

        public virtual float GetItemHeight(ListItemData itemData)
        {
            return m_ItemSizes.ContainsKey(itemData) ? m_ItemSizes[itemData].y : minItemHeight;
        }

        protected virtual void SetItemWidth(ListItemData itemData, float width)
        {
            if (m_ItemSizes.ContainsKey(itemData))
            {
                m_ItemSizes[itemData] = new Vector2(width, m_ItemSizes[itemData].y);
            }
            else
            {
                m_ItemSizes.Add(itemData, new Vector2(width, minItemHeight));
            }
        }

        protected virtual void SetItemHeight(ListItemData itemData, float height)
        {
            if (m_ItemSizes.ContainsKey(itemData))
            {
                m_ItemSizes[itemData] = new Vector2(m_ItemSizes[itemData].x, height);
            }
            else
            {
                m_ItemSizes.Add(itemData, new Vector2(minItemWidth, height));
            }
        }

        public virtual void OnLayoutChange() { }
    }
}
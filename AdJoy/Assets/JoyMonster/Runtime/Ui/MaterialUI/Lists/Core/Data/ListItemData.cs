﻿using System;

namespace MaterialUI_JoyMonster
{
    [Serializable]
    public abstract class ListItemData
    {
        protected ListItem m_CurrentItem;

        public ListItem currentItem
        {
            get { return m_CurrentItem; }
            set { m_CurrentItem = value; }
        }

        protected bool m_Hidden;

        public bool hidden
        {
            get { return m_Hidden; }
            set { m_Hidden = value; }
        }

        public abstract ListItemController GetNewController();
    }
}
﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;

namespace MaterialUI_JoyMonster
{
    public abstract class ListItem : ListItemSelectable
    {
        protected CanvasGroup m_CanvasGroup;

        public CanvasGroup canvasGroup
        {
            get
            {
                if (m_CanvasGroup == null)
                {
                    m_CanvasGroup = gameObject.GetAddComponent<CanvasGroup>();
                }

                return m_CanvasGroup;
            }
            set { m_CanvasGroup = value; }
        }

        protected ListItemData m_CurrentData;

        public ListItemData currentData
        {
            get { return m_CurrentData; }
            set { m_CurrentData = value; }
        }

        public abstract Type matchingDataType { get; }

        protected bool m_IgnoreSelectMessages;

        public virtual void Cleanup()
        {
            listView.animationController.InterruptItemTweens(this);
        }

        public virtual void OnItemReleaseToPool()
        {
        }

        public virtual void SelectWithoutMessages()
        {
            m_IgnoreSelectMessages = true;
            Select();
        }

        public override void OnSelectNonPointer(BaseEventData eventData)
        {
            listView.layoutController.lastSelectedItem = listView.layoutController.GetDisplayedDataFromItem(item);

            if (!m_IgnoreSelectMessages)
            {
                InteractionSelectNonPointer(eventData);
            }

            m_IgnoreSelectMessages = false;
        }

        protected override void InteractionSelectNonPointer(BaseEventData eventData)
        {
            listView.interactionController.OnItemInteraction(ListInteraction.selectNonPointer, eventData, this, 0);
        }

        protected override void InteractionSelectPointer(BaseEventData eventData)
        {
            listView.interactionController.OnItemInteraction(ListInteraction.selectPointer, eventData, this, 0);
        }

        protected override void InteractionDeselect(BaseEventData eventData)
        {
            listView.interactionController.OnItemInteraction(ListInteraction.deselect, eventData, this, 0);
        }

        protected override void InteractionPointerDown(BaseEventData eventData)
        {
            listView.interactionController.OnItemInteraction(ListInteraction.pointerDown, eventData, this, 0);
        }

        protected override void InteractionPointerUp(BaseEventData eventData)
        {
            listView.interactionController.OnItemInteraction(ListInteraction.pointerUp, eventData, this, 0);
        }

        protected override void InteractionPointerEnter(BaseEventData eventData)
        {
            listView.interactionController.OnItemInteraction(ListInteraction.pointerEnter, eventData, this, 0);
        }

        protected override void InteractionPointerExit(BaseEventData eventData)
        {
            listView.interactionController.OnItemInteraction(ListInteraction.pointerExit, eventData, this, 0);
        }

        protected override void InteractionPointerClick(BaseEventData eventData)
        {
            listView.interactionController.OnItemInteraction(ListInteraction.pointerClick, eventData, this, 0);
        }
    }
}
﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace MaterialUI_JoyMonster
{
    public class ListView : MonoBehaviour, IEndDragHandler
    {
        private RectTransform m_RectTransform;

        public RectTransform rectTransform
        {
            get
            {
                if (m_RectTransform == null)
                {
                    m_RectTransform = (RectTransform)transform;
                }

                return m_RectTransform;
            }
        }

        private ListContent m_ListContent;

        public ListContent listContent
        {
            get
            {
                if (m_ListContent == null)
                {
                    ListContent[] contents = GetComponentsInChildren<ListContent>();
                    ListContent content = null;

                    for (int i = 0; i < contents.Length; i++)
                    {
                        if (contents[i].rectTransform.parent == rectTransform)
                        {
                            content = contents[i];
                        }
                    }

                    if (content == null)
                    {
                        content = new GameObject("ListContent", typeof(RectTransform)).AddComponent<ListContent>();
                        scrollRect.content = content.rectTransform;
                        content.rectTransform.SetParentAndScale(rectTransform, Vector3.one);
                    }

                    m_ListContent = content;
                }

                return m_ListContent;
            }
        }

        private ScrollRect m_ScrollRect;

        public ScrollRect scrollRect
        {
            get
            {
                if (m_ScrollRect == null)
                {
                    m_ScrollRect = GetComponent<ScrollRect>();
                }

                return m_ScrollRect;
            }
        }

        //private OverscrollConfig m_Overscroll;

        //public OverscrollConfig overscroll
        //{
        //    get
        //    {
        //        if (m_Overscroll == null)
        //        {
        //            m_Overscroll = gameObject.GetAddComponent<OverscrollConfig>();
        //        }

        //        return m_Overscroll;
        //    }
        //}

        private ListLayoutController m_LayoutController;

        public ListLayoutController layoutController
        {
            get { return m_LayoutController; }
        }

        private ListAnimationController m_AnimationController;

        public ListAnimationController animationController
        {
            get { return m_AnimationController; }
        }

        private ListInteractionController m_InteractionController;

        public ListInteractionController interactionController
        {
            get { return m_InteractionController; }
        }

        private ListData m_ListData;

        public ListData listData
        {
            get { return m_ListData; }
            set { m_ListData = value; }
        }

        private bool m_IsInitialized;

        public bool isInitialized
        {
            get { return m_IsInitialized; }
        }

        //private MaterialUIScaler m_RootScaler;

        //public MaterialUIScaler rootScaler
        //{
        //    get
        //    {
        //        if (m_RootScaler == null)
        //        {
        //            m_RootScaler = MaterialUIScaler.GetRootScaler(rectTransform);
        //        }

        //        return m_RootScaler;
        //    }
        //}

        private IListExtension[] m_ListExtensions;

        public IListExtension[] listExtensions
        {
            get { return m_ListExtensions; }
        }

        public void OnContentChanged()
        {
            foreach (IListExtension extension in m_ListExtensions)
            {
                extension.OnContentChanged();
            }
        }

        public void OnLayoutChanged()
        {
            if (m_LayoutController != null)
            {
                m_LayoutController.OnListLayoutChange();
            }
        }

        public void InitializeList(ListLayoutController layoutController, ListAnimationController animationController,
            ListInteractionController interactionController)
        {
            if (!Application.isPlaying) return;

            if (m_LayoutController != null)
            {
                m_LayoutController.Cleanup(true);
            }

            if (m_AnimationController != null)
            {
                m_AnimationController.Cleanup();
            }

            if (m_InteractionController != null)
            {
                m_InteractionController.Cleanup();
            }

            if (layoutController == null)
            {
                Debug.LogException(new Exception("Cannot initialize a list without a ListLayoutController."), this);
                return;
            }

            if (animationController == null)
            {
                Debug.LogException(new Exception("Cannot initialize a list without a ListAnimator."), this);
                return;
            }

            if (interactionController == null)
            {
                Debug.LogException(new Exception("Cannot initialize a list without a ListInteractionController."), this);
                return;
            }

            m_ListExtensions = GetComponents<IListExtension>();

            m_LayoutController = layoutController;
            m_AnimationController = animationController;
            m_InteractionController = interactionController;
            m_LayoutController.Initialize(this);
            m_AnimationController.Initialize(this);
            m_InteractionController.Initialize(this);

            foreach (IListExtension extension in m_ListExtensions)
            {
                extension.OnListInitialized(this);
            }

            m_IsInitialized = true;
        }

        public void SetListData(ListData listData)
        {
            if (layoutController == null || animationController == null)
            {
                Debug.LogException(new Exception("Cannot set list data without initializing first."), this);
                return;
            }

            m_ListData = listData;
            layoutController.OnListDataChange(true);
        }

        public void ShowList()
        {
            if (layoutController == null || animationController == null)
            {
                Debug.LogException(new Exception("Cannot show a list without initializing first."), this);
                return;
            }

            if (listData == null)
            {
                Debug.LogException(new Exception("Cannot show a list without any data."), this);
                return;
            }

            foreach (IListExtension extension in m_ListExtensions)
            {
                extension.OnBeforeShow();
            }

            layoutController.Show();

            foreach (IListExtension extension in m_ListExtensions)
            {
                extension.OnShow();
            }
        }

        public void HideList()
        {
            if (layoutController == null || animationController == null)
            {
                Debug.LogException(new Exception("Cannot hide a list without initializing first."), this);
                return;
            }

            if (listData == null)
            {
                Debug.LogException(new Exception("Cannot hide a list without any data."), this);
                return;
            }

            layoutController.Hide();

            foreach (IListExtension extension in m_ListExtensions)
            {
                extension.OnHide();
            }
        }

        public void OnEndDrag(PointerEventData eventData)
        {
            if (Mathf.Abs(eventData.delta.y) > 10f)
            {
                scrollRect.velocity = new Vector2(scrollRect.velocity.x, scrollRect.velocity.y * 2f);
            }
        }
    }
}
﻿//  Copyright 2017 MaterialUI for Unity http://materialunity.com
//  Please see license file for terms and conditions of use, and more information.

namespace MaterialUI_JoyMonster
{
    /// <summary>
    /// Implemented by an object that wants to be able to create and manage ripples.
    /// </summary>
    public interface IRippleCreator
    {
        /// <summary>
        /// Called when a ripple is created.
        /// </summary>
        void OnCreateRipple();
        /// <summary>
        /// Called when a ripple is destroyed.
        /// </summary>
        void OnDestroyRipple();

        /// <summary>
        /// Gets the ripple data.
        /// </summary>
        /// <value>
        /// The ripple data.
        /// </value>
        RippleData rippleData { get; }
    }
}
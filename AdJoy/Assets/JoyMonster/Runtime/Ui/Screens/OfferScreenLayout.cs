﻿using JoyMonster.API.Model;
using JoyMonster.Core;
using MaterialUI_JoyMonster;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace JoyMonster.Ui.Screens
{
    [ExecuteInEditMode]
    public class OfferScreenLayout : ScreenLayoutBehaviour
    {
        [SerializeField]
        private RectTransform m_MainContentTransform = null;

        [SerializeField]
        private RectTransform m_LeftContentTransform = null;

        [SerializeField]
        private Graphic m_LeftContentDivider = null;

        [SerializeField]
        private Graphic m_ExtraInfoDivider = null;

        [SerializeField]
        private RectTransform m_ContainerImage = null;

        [SerializeField]
        private RectTransform m_ContainerMainInfo = null;

        [SerializeField]
        private RectTransform m_ContainerExtraInfo = null;

        [SerializeField]
        private RectTransform m_ContainerRedeemableAt = null;

        [SerializeField]
        private RectTransform m_ContainerLocationExtraInfo = null;

        [SerializeField]
        private RectTransform m_ButtonRedeem = null;

        [SerializeField]
        private RectTransform m_ButtonMap = null;

        [SerializeField]
        private RectTransform m_ButtonMapContent = null;

        [SerializeField]
        private RectTransform m_ButtonCall = null;

        [SerializeField]
        private RectTransform m_ButtonCallContent = null;

        [SerializeField]
        private TextMeshProUGUI m_LabelCompany = null;

        [SerializeField]
        private TextMeshProUGUI m_LabelCity = null;

        [SerializeField]
        private TextMeshProUGUI m_LabelTitle = null;

        [SerializeField]
        private TextMeshProUGUI m_LabelDescription = null;

        [SerializeField]
        private RectTransform m_ContainerScrim = null;

        [SerializeField]
        private RectTransform m_ContainerMinumum = null;

        [SerializeField]
        private TextMeshProUGUI m_LabelMinimum = null;

        [SerializeField]
        private TextMeshProUGUI m_LabelButtonRedeem = null;

        [SerializeField]
        private TextMeshProUGUI m_LabelRedeemableAt = null;

        [SerializeField]
        private TextMeshProUGUI m_LabelLocation = null;

        [SerializeField]
        private VectorImage m_IconButtonMap = null;

        [SerializeField]
        private TextMeshProUGUI m_LabelButtonMap = null;

        [SerializeField]
        private VectorImage m_IconButtonCall = null;

        [SerializeField]
        private TextMeshProUGUI m_LabelButtonCall = null;

        [SerializeField]
        private VectorImage m_IconDistance = null;

        [SerializeField]
        private TextMeshProUGUI m_LabelDistance = null;

        [SerializeField]
        private VectorImage m_IconExpiry = null;

        [SerializeField]
        private TextMeshProUGUI m_LabelExpiry = null;

        [SerializeField]
        private TextMeshProUGUI m_LabelValue = null;

        private float m_DividerSize = 2f;

        protected override void OnSetLayout()
        {
            base.OnSetLayout();

            m_LabelDistance.alignment = TextAlignmentOptions.Center;
            m_LabelExpiry.alignment = TextAlignmentOptions.Center;

            bool isPartner = JoyMonsterManager.walletHandler.currentOffer != null &&
                             JoyMonsterManager.walletHandler.currentOffer.isPartner;
            bool isLandscape = UiScaleController.orientation == UiScaleController.Orientation.Landscape;
            LayoutValue values = UiLayoutValueHandler.common.currentValue;

            m_LeftContentTransform.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Left, 0f,
                m_ContentTransform.rect.width / 2f);
            m_LeftContentTransform.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Bottom, 0f,
                m_ContentTransform.rect.height);
            m_LeftContentTransform.gameObject.SetActive(isLandscape);

            m_MainContentTransform.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Right, 0f,
                isLandscape ? m_ContentTransform.rect.width / 2f : m_ContentTransform.rect.width);
            m_MainContentTransform.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Bottom, 0f,
                m_ContentTransform.rect.height);

            m_LeftContentDivider.gameObject.SetActive(isLandscape);

            m_ContainerImage.SetParentAndScale(isLandscape ? m_LeftContentTransform : m_MainContentTransform,
                Vector3.one);
            m_ContainerImage.SetAsFirstSibling();

            float paddedMainWidth = m_MainContentTransform.rect.width - values.padding * 2f;

            m_LabelCompany.fontSizeMax = values.textParagraphFontSize;
            m_LabelCity.fontSizeMax = values.textParagraphFontSize;
            m_LabelTitle.fontSizeMax = isLandscape ? values.textSubHeaderFontSize : values.textHeaderFontSize;
            m_LabelDescription.fontSizeMax = values.textParagraphFontSize;

            m_LabelMinimum.fontSizeMax = values.textParagraphFontSize;

            m_LabelButtonRedeem.fontSize = values.textButtonFontSize;

            m_LabelRedeemableAt.fontSize = values.textParagraphFontSize;

            m_LabelLocation.fontSizeMax = values.textParagraphFontSize;

            m_IconDistance.size = values.textParagraphFontSize;
            m_LabelDistance.fontSize = values.textParagraphFontSize - 1f;
            m_IconExpiry.size = values.textParagraphFontSize;
            m_LabelExpiry.fontSize = values.textParagraphFontSize - 1f;

            m_IconButtonMap.size = values.textParagraphFontSize;
            m_LabelButtonMap.fontSize = values.textParagraphFontSize - 1f;
            m_IconButtonCall.size = values.textParagraphFontSize;
            m_LabelButtonCall.fontSize = values.textParagraphFontSize - 1f;

            m_LabelValue.fontSize = values.textParagraphFontSize;
            float xPos = 0f;
            float maxWidth = 0f;

            float mainContentHeight = 0f;
            float extraInfoHeight = 0f;
            //if (isPartner)
            //{
            //m_ContainerExtraInfo.gameObject.SetActive(false);
            //}
            //else
            {
                m_ContainerExtraInfo.gameObject.SetActive(true);

                m_ContainerExtraInfo.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Left, 0f,
                    m_MainContentTransform.rect.width);
                m_LabelValue.rectTransform.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Left, values.padding,
                    paddedMainWidth);
                m_ExtraInfoDivider.rectTransform.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Left, values.padding,
                    paddedMainWidth);
                m_ContainerLocationExtraInfo.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Left, values.padding,
                    paddedMainWidth);
                m_LabelLocation.rectTransform.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Left, values.padding,
                    paddedMainWidth);
                m_ContainerRedeemableAt.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Left, 0f,
                    m_MainContentTransform.rect.width);

                extraInfoHeight += isLandscape ? values.spacing : values.padding;
                m_LabelValue.rectTransform.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Bottom, extraInfoHeight,
                    TextTester.GetPreferredHeight(TextTester.Font.Semibold, values.textParagraphFontSize, 1));
                extraInfoHeight += m_LabelValue.rectTransform.rect.height + values.spacing;

                m_ExtraInfoDivider.rectTransform.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Bottom,
                    extraInfoHeight, m_DividerSize);
                extraInfoHeight += m_DividerSize + values.spacing;

                Offer offer = JoyMonsterManager.walletHandler.currentOffer;

                bool mapEnabled = OfferFormatter.HasLocationCoords(offer) || OfferFormatter.HasAddress(offer);

                m_ButtonMap.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Left, xPos,
                    LayoutUtility.GetPreferredWidth(m_ButtonMapContent) + values.spacing * 2f);

                if (mapEnabled)
                {
                    xPos += m_ButtonMap.rect.width + values.spacing;
                    m_ButtonMap.localScale = Vector3.one;
                }
                else
                {
                    m_ButtonMap.localScale = Vector3.zero;
                }

                m_ButtonCall.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Left, xPos,
                    LayoutUtility.GetPreferredWidth(m_ButtonCallContent) + values.spacing * 2f);
                xPos += m_ButtonMap.rect.width + values.spacing;

                if (m_LabelDistance.text != "")
                {
                    m_IconDistance.enabled = true;
                    m_IconDistance.rectTransform.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Left, xPos,
                        values.textParagraphFontSize - 2f);
                    xPos += m_IconDistance.rectTransform.rect.width + values.spacing / 2f;

                    m_LabelDistance.enabled = true;
                    m_LabelDistance.rectTransform.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Left, xPos,
                        m_LabelDistance.preferredWidth);
                    xPos += m_LabelDistance.rectTransform.rect.width + values.spacing;
                }
                else
                {
                    m_IconDistance.enabled = false;
                    m_LabelDistance.enabled = false;
                }

                if (m_LabelExpiry.text != "")
                {
                    m_IconExpiry.enabled = true;
                    m_IconExpiry.rectTransform.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Left, xPos,
                        values.textParagraphFontSize - 2f);
                    xPos += m_IconExpiry.rectTransform.rect.width + values.spacing / 2f;

                    m_LabelExpiry.enabled = true;
                    m_LabelExpiry.rectTransform.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Left, xPos,
                        m_LabelExpiry.preferredWidth);
                    xPos += m_LabelExpiry.rectTransform.rect.width;
                }
                else
                {
                    m_IconExpiry.enabled = false;
                    m_LabelExpiry.enabled = false;
                }

                int itCount = 0;

                while (xPos > m_ContainerExtraInfo.rect.width - values.padding * 1.75f && itCount < 2)
                {
                    itCount++;

                    m_IconDistance.size -= 1f;
                    m_LabelDistance.fontSize -= 1f;
                    m_IconExpiry.size -= 1f;
                    m_LabelExpiry.fontSize -= 1f;

                    m_IconButtonMap.size -= 1f;
                    m_LabelButtonMap.fontSize -= 1f;
                    m_IconButtonCall.size -= 1f;
                    m_LabelButtonCall.fontSize -= 1f;

                    xPos = 0f;

                    m_ButtonMap.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Left, xPos,
                        LayoutUtility.GetPreferredWidth(m_ButtonMapContent) + values.spacing * 2f);

                    if (mapEnabled)
                    {
                        xPos += m_ButtonMap.rect.width + values.spacing;
                        m_ButtonMap.localScale = Vector3.one;
                    }
                    else
                    {
                        m_ButtonMap.localScale = Vector3.zero;
                    }

                    m_ButtonCall.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Left, xPos,
                        LayoutUtility.GetPreferredWidth(m_ButtonCallContent) + values.spacing * 2f);
                    xPos += m_ButtonMap.rect.width + values.spacing;

                    if (m_LabelDistance.text != "")
                    {
                        m_IconDistance.enabled = true;
                        m_IconDistance.rectTransform.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Left, xPos,
                            values.textParagraphFontSize - 2f);
                        xPos += m_IconDistance.rectTransform.rect.width + values.spacing * 0.25f;

                        m_LabelDistance.enabled = true;
                        m_LabelDistance.rectTransform.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Left, xPos,
                            m_LabelDistance.preferredWidth);
                        xPos += m_LabelDistance.rectTransform.rect.width + values.spacing * 0.5f;
                    }
                    else
                    {
                        m_IconDistance.enabled = false;
                        m_LabelDistance.enabled = false;
                    }

                    if (m_LabelExpiry.text != "")
                    {
                        m_IconExpiry.enabled = true;
                        m_IconExpiry.rectTransform.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Left, xPos,
                            values.textParagraphFontSize - 2f);
                        xPos += m_IconExpiry.rectTransform.rect.width + values.spacing * 0.25f;

                        m_LabelExpiry.enabled = true;
                        m_LabelExpiry.rectTransform.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Left, xPos,
                            m_LabelExpiry.preferredWidth);
                        xPos += m_LabelExpiry.rectTransform.rect.width;
                    }
                    else
                    {
                        m_IconExpiry.enabled = false;
                        m_LabelExpiry.enabled = false;
                    }
                }

                m_ContainerLocationExtraInfo.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Bottom, extraInfoHeight,
                    TextTester.GetPreferredHeight(TextTester.Font.Normal, values.textParagraphFontSize - 2f, 1) +
                    values.spacing * 1.5f);
                extraInfoHeight += m_ContainerLocationExtraInfo.rect.height + values.spacing;

                bool hasAddress = OfferFormatter.HasAddress(offer);

                if (hasAddress)
                {
                    m_LabelLocation.rectTransform.localScale = Vector3.one;
                    m_ContainerRedeemableAt.localScale = Vector3.one;

                    m_LabelLocation.rectTransform.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Bottom, extraInfoHeight,
                        TextTester.GetPreferredHeight(TextTester.Font.Semibold, values.textParagraphFontSize, 1));
                    extraInfoHeight += m_LabelLocation.rectTransform.rect.height + values.spacing;

                    m_ContainerRedeemableAt.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Bottom, extraInfoHeight,
                        TextTester.GetPreferredHeight(TextTester.Font.Normal, values.textParagraphFontSize - 2f, 1) +
                        values.spacing * 2f);
                    extraInfoHeight += m_ContainerRedeemableAt.rect.height;
                }
                else
                {
                    m_LabelLocation.rectTransform.localScale = Vector3.zero;
                    m_ContainerRedeemableAt.localScale = Vector3.zero;
                }

                m_ContainerExtraInfo.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Bottom, 0f, extraInfoHeight);

                m_ContainerMinumum.gameObject.SetActive(false);
            }

            mainContentHeight = extraInfoHeight + values.padding;

            if (JoyMonsterManager.walletHandler.currentOffer != null &&
                (JoyMonsterManager.walletHandler.currentOffer.isRedeemable ||
                 !JoyMonsterManager.walletHandler.currentOffer.canAggregate))
            {
                m_ButtonRedeem.gameObject.SetActive(true);
                m_ContainerMinumum.gameObject.SetActive(false);
                m_ContainerMinumum.localScale = Vector3.zero;

                m_ButtonRedeem.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Left, values.padding, paddedMainWidth);
                m_ButtonRedeem.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Bottom, mainContentHeight,
                    TextTester.GetPreferredHeight(TextTester.Font.Semibold, values.textButtonFontSize, 1) +
                    (isLandscape ? values.spacing : values.padding) * 2f);

                mainContentHeight += m_ButtonRedeem.rect.height + (isLandscape ? values.spacing : values.padding);
            }
            else
            {
                m_ButtonRedeem.gameObject.SetActive(false);
                m_ContainerMinumum.gameObject.SetActive(true);
                m_ContainerMinumum.localScale = Vector3.one;

                m_ContainerMinumum.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Left, values.padding,
                    paddedMainWidth);

                m_ContainerMinumum.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Bottom, mainContentHeight,
                    TextTester.GetPreferredHeight(TextTester.Font.Semibold, values.textParagraphFontSize, isLandscape ? 1 : 2) +
                    values.spacing * 2f);

                m_LabelMinimum.rectTransform.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Left, values.padding,
                    m_ContainerMinumum.rect.width - values.padding * 2f);

                mainContentHeight += m_ContainerMinumum.rect.height + (isLandscape ? values.spacing : values.padding);
            }

            m_ContainerMainInfo.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Left, values.padding, paddedMainWidth);

            xPos = 0f;
            maxWidth = (m_ContainerMainInfo.rect.width - values.spacing) * 0.65f;
            m_LabelCompany.rectTransform.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Left, xPos,
                Mathf.Min(m_LabelCompany.preferredWidth, maxWidth));
            xPos += m_LabelCompany.rectTransform.rect.width + (m_LabelCompany.text == "" ? 0f : values.spacing);
            maxWidth = (m_ContainerMainInfo.rect.width - values.spacing) * 0.35f;
            m_LabelCity.rectTransform.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Left, xPos,
                Mathf.Min(m_LabelCity.preferredWidth, maxWidth));

            float mainInfoHeight = 0f;
            m_LabelDescription.rectTransform.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Bottom, mainInfoHeight,
                isLandscape
                    ? TextTester.GetPreferredHeight(TextTester.Font.Normal, values.textParagraphFontSize,
                        2)
                    : m_LabelDescription.preferredHeight);
            mainInfoHeight += m_LabelDescription.rectTransform.rect.height + values.spacing / 2f;

            m_LabelTitle.rectTransform.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Bottom, mainInfoHeight,
                TextTester.GetPreferredHeight(TextTester.Font.Semibold, values.textSubHeaderFontSize, 1));
            mainInfoHeight += m_LabelTitle.rectTransform.rect.height +
                              (isLandscape ? values.spacing / 2f : values.spacing);

            m_LabelCompany.rectTransform.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Bottom, mainInfoHeight,
                TextTester.GetPreferredHeight(TextTester.Font.Semibold, values.textParagraphFontSize, 1));
            m_LabelCity.rectTransform.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Bottom, mainInfoHeight,
                m_LabelCompany.rectTransform.rect.height);
            mainInfoHeight += m_LabelCompany.rectTransform.rect.height;

            float freeSpace = 0f;

            if (isLandscape)
            {
                m_ContainerImage.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Left, values.padding,
                    m_LeftContentTransform.rect.width - values.padding * 2f);
                m_ContainerImage.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Bottom, values.padding,
                    m_LeftContentTransform.rect.height - values.padding * 2f);

                freeSpace = m_MainContentTransform.rect.height - mainContentHeight - values.padding - mainInfoHeight;

                if (freeSpace > values.spacing * 1.5f)
                {
                    m_LabelTitle.rectTransform.SetAnchoredPositionY(
                        m_LabelTitle.rectTransform.anchoredPosition.y + values.spacing * 0.5f);
                    m_LabelCompany.rectTransform.SetAnchoredPositionY(
                        m_LabelCompany.rectTransform.anchoredPosition.y + values.spacing);
                    m_LabelCity.rectTransform.SetAnchoredPositionY(
                        m_LabelCity.rectTransform.anchoredPosition.y + values.spacing);

                    freeSpace -= values.spacing;
                    mainInfoHeight += values.spacing;
                }

                m_ContainerMainInfo.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Bottom,
                    mainContentHeight + freeSpace / 2f, mainInfoHeight);
                m_ContainerScrim.gameObject.SetActive(false);
            }
            else
            {
                freeSpace = m_MainContentTransform.rect.height - mainContentHeight - values.padding * 2f -
                            mainInfoHeight;

                if (freeSpace > (values.textHeaderFontSize - values.textSubHeaderFontSize) * 1.5f)
                {
                    m_LabelTitle.fontSizeMax = values.textHeaderFontSize;
                    float sizeDelta = m_LabelTitle.rectTransform.rect.height;
                    m_LabelTitle.rectTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical,
                        TextTester.GetPreferredHeight(TextTester.Font.Semibold, values.textHeaderFontSize, 1));
                    sizeDelta = m_LabelTitle.rectTransform.rect.height - sizeDelta;
                    m_LabelTitle.rectTransform.SetAnchoredPositionY(
                        m_LabelTitle.rectTransform.anchoredPosition.y + sizeDelta / 2f);

                    m_LabelCompany.rectTransform.SetAnchoredPositionY(
                        m_LabelCompany.rectTransform.anchoredPosition.y + sizeDelta);
                    m_LabelCity.rectTransform.SetAnchoredPositionY(
                        m_LabelCity.rectTransform.anchoredPosition.y + sizeDelta);
                    mainInfoHeight += sizeDelta;

                    freeSpace -= sizeDelta;
                    mainInfoHeight += sizeDelta;
                }

                float imageHeightFactor = isPartner ? 0.65f : 0.85f;

                if (freeSpace > (paddedMainWidth + values.padding) * imageHeightFactor + values.spacing * 1.5f)
                {
                    m_LabelTitle.rectTransform.SetAnchoredPositionY(
                        m_LabelTitle.rectTransform.anchoredPosition.y + values.spacing * 0.5f);
                    m_LabelCompany.rectTransform.SetAnchoredPositionY(
                        m_LabelCompany.rectTransform.anchoredPosition.y + values.spacing);
                    m_LabelCity.rectTransform.SetAnchoredPositionY(
                        m_LabelCity.rectTransform.anchoredPosition.y + values.spacing);

                    freeSpace -= values.spacing;
                    mainInfoHeight += values.spacing;
                }

                freeSpace = Mathf.Max(0f, freeSpace - paddedMainWidth * imageHeightFactor);
                //}
                mainContentHeight += freeSpace;
                m_ContainerMainInfo.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Bottom, mainContentHeight,
                    mainInfoHeight);
                mainContentHeight += mainInfoHeight + values.padding * 2f;

                m_ContainerImage.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Left, values.padding,
                    paddedMainWidth);
                m_ContainerImage.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Bottom, mainContentHeight,
                    m_MainContentTransform.rect.height - mainContentHeight - values.padding);
                m_ContainerScrim.gameObject.SetActive(true);
                m_ContainerScrim.SetAnchoredPositionY(-values.padding);
                m_ContainerScrim.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal,
                    m_MainContentTransform.rect.width);
            }
        }
    }
}
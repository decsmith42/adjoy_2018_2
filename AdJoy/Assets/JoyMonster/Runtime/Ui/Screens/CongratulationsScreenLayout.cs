﻿using MaterialUI_JoyMonster;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace JoyMonster.Ui.Screens
{
    public class CongratulationsScreenLayout : ScreenLayoutBehaviour
    {
        [SerializeField] private RectTransform m_ChevronRectangle = null;
        [SerializeField] private RectTransform m_ChevronTriangle = null;
        [SerializeField] private RectTransform m_ChevronTriangle2 = null;
        [SerializeField] private RectTransform m_Title = null;
        [SerializeField] private RectTransform m_SubTitle = null;
        [SerializeField] private RectTransform m_Box = null;
        [SerializeField] private TextMeshProUGUI m_LabelTitle;
        [SerializeField] private TextMeshProUGUI m_LabelSubTitle;

        private Vector2 screenSize
        {
            get { return rectTransform.rect.size; }
        }

        protected override void OnSetLayout()
        {
            base.OnSetLayout();

            LayoutValue values = UiLayoutValueHandler.common.currentValue;


            m_LabelTitle.enabled = false;
            m_LabelTitle.enabled = true;

            m_LabelTitle.alignment = TextAlignmentOptions.Center;
            m_LabelSubTitle.alignment = TextAlignmentOptions.Center;

            if (UiScaleController.orientation == UiScaleController.Orientation.Portrait)
            {
                m_Title.anchorMin = new Vector2(0.5f, 0.75f);
                m_Title.anchorMax = new Vector2(0.5f, 0.75f);
                m_Title.sizeDelta = new Vector2(screenSize.x, LayoutUtility.GetPreferredHeight(m_Title));
                m_Title.anchoredPosition = Vector2.zero;

                m_SubTitle.anchorMin = new Vector2(0.5f, 0.675f);
                m_SubTitle.anchorMax = new Vector2(0.5f, 0.675f);
                m_SubTitle.sizeDelta = new Vector2(screenSize.x, LayoutUtility.GetPreferredHeight(m_SubTitle));
                m_SubTitle.anchoredPosition = Vector2.zero;

                m_Box.anchorMin = new Vector2(0.5f, 0.3f);
                m_Box.anchorMax = new Vector2(0.5f, 0.3f);
                m_Box.sizeDelta = new Vector2(300f, 300f);
                m_Box.anchoredPosition = Vector2.zero;

                m_ChevronRectangle.sizeDelta = screenSize;
                m_ChevronRectangle.pivot = new Vector2(0.5f, 0f);
                m_ChevronRectangle.anchoredPosition = new Vector2(0f, -screenSize.y * 0.2f + screenSize.x / 8f);

                m_ChevronTriangle.localEulerAngles = Vector3.zero;
                m_ChevronTriangle.anchorMin = new Vector2(0.5f, 0f);
                m_ChevronTriangle.anchorMax = new Vector2(0.5f, 0f);
                m_ChevronTriangle.sizeDelta = new Vector2(screenSize.x, screenSize.x / 8f);

                m_ChevronTriangle2.localEulerAngles = new Vector3(0f, 0f, 180f);
                m_ChevronTriangle2.anchorMin = new Vector2(0.5f, 1f);
                m_ChevronTriangle2.anchorMax = new Vector2(0.5f, 1f);
                m_ChevronTriangle2.sizeDelta = m_ChevronTriangle.sizeDelta;
            }
            else
            {
                m_Title.anchorMin = new Vector2(0.5f, 0.5f);
                m_Title.anchorMax = new Vector2(0.5f, 0.5f);
                m_Title.sizeDelta = new Vector2(LayoutUtility.GetPreferredWidth(m_Title), LayoutUtility.GetPreferredHeight(m_Title));
                m_Title.anchoredPosition = new Vector2((screenSize.x / 4f) - (screenSize.y / 8f) / 3f, m_Title.rect.height / 2f + values.spacing);

                m_SubTitle.anchorMin = new Vector2(0.5f, 0.5f);
                m_SubTitle.anchorMax = new Vector2(0.5f, 0.5f);
                m_SubTitle.sizeDelta = new Vector2(LayoutUtility.GetPreferredWidth(m_SubTitle), LayoutUtility.GetPreferredHeight(m_SubTitle));
                m_SubTitle.anchoredPosition = new Vector2((screenSize.x / 4f) - (screenSize.y / 8f) / 3f, -m_SubTitle.rect.height / 2f - values.spacing);

                m_Box.anchorMin = new Vector2(0f, 0.5f);
                m_Box.anchorMax = new Vector2(0f, 0.5f);
                m_Box.sizeDelta = new Vector2(300f, 300f);
                m_Box.anchoredPosition = new Vector2((screenSize.x / 4f) - (screenSize.y / 8f) / 2f, 0f);

                m_ChevronRectangle.sizeDelta = screenSize;
                m_ChevronRectangle.anchorMin = new Vector2(0.5f, 0.5f);
                m_ChevronRectangle.pivot = new Vector2(0f, 0.5f);
                m_ChevronRectangle.anchoredPosition = new Vector2(0f, 0f);

                m_ChevronTriangle.localEulerAngles = new Vector3(0f, 0f, -90f);
                m_ChevronTriangle.anchorMin = new Vector2(0f, 0.5f);
                m_ChevronTriangle.anchorMax = new Vector2(0f, 0.5f);
                m_ChevronTriangle.sizeDelta = new Vector2(screenSize.y, screenSize.y / 8f);

                m_ChevronTriangle2.localEulerAngles = new Vector3(0f, 0f, 90f);
                m_ChevronTriangle2.anchorMin = new Vector2(1f, 0.5f);
                m_ChevronTriangle2.anchorMax = new Vector2(1f, 0.5f);
                m_ChevronTriangle2.sizeDelta = m_ChevronTriangle.sizeDelta;
            }
        }
    }
}
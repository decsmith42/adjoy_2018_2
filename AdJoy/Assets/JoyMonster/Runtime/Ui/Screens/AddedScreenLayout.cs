﻿using MaterialUI_JoyMonster;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace JoyMonster.Ui.Screens
{
    public class AddedScreenLayout : ScreenLayoutBehaviour
    {
        [SerializeField] private RectTransform m_ChevronRectangle = null;
        [SerializeField] private RectTransform m_ChevronTriangle = null;
        [SerializeField] private RectTransform m_Title = null;
        [SerializeField] private RectTransform m_SubTitle = null;
        [SerializeField] private RectTransform m_Badge = null;
        [SerializeField] private RectTransform m_Button = null;

        [SerializeField] private TextMeshProUGUI m_LabelTitle;
        [SerializeField] private TextMeshProUGUI m_LabelSubTitle;

        private Vector2 screenSize
        {
            get { return m_ContentTransform.rect.size; }
        }

        protected override void OnSetLayout()
        {
            base.OnSetLayout();

            m_LabelTitle.alignment = TextAlignmentOptions.Center;
            m_LabelSubTitle.alignment = TextAlignmentOptions.Center;

            LayoutValue values = UiLayoutValueHandler.common.currentValue;

            if (UiScaleController.orientation == UiScaleController.Orientation.Portrait)
            {
                m_Title.anchorMin = new Vector2(0.5f, 0.8f);
                m_Title.anchorMax = new Vector2(0.5f, 0.8f);
                m_Title.sizeDelta = new Vector2(screenSize.x, LayoutUtility.GetPreferredHeight(m_Title));
                m_Title.anchoredPosition = Vector2.zero;

                m_SubTitle.anchorMin = new Vector2(0.5f, 0.6875f);
                m_SubTitle.anchorMax = new Vector2(0.5f, 0.6875f);
                m_SubTitle.sizeDelta = new Vector2(screenSize.x - values.padding * 2f, LayoutUtility.GetPreferredHeight(m_SubTitle));
                m_SubTitle.anchoredPosition = Vector2.zero;

                m_Badge.anchorMin = new Vector2(0.5f, 0.4f);
                m_Badge.anchorMax = new Vector2(0.5f, 0.4f);
                m_Badge.SetSizeDeltaY(screenSize.y * 0.35f);
                m_Badge.anchoredPosition = Vector2.zero;

                m_Button.anchorMin = new Vector2(0.5f, 0.1f);
                m_Button.anchorMax = new Vector2(0.5f, 0.1f);
                m_Button.sizeDelta = new Vector2(256f, 40f);
                m_Button.anchoredPosition = Vector2.zero;

                m_ChevronRectangle.sizeDelta = screenSize;
                m_ChevronRectangle.pivot = new Vector2(0.5f, 0f);
                m_ChevronRectangle.anchoredPosition = new Vector2(0f, -screenSize.y * 0.075f + screenSize.x / 8f);

                m_ChevronTriangle.localEulerAngles = Vector3.zero;
                m_ChevronTriangle.anchorMin = new Vector2(0.5f, 0f);
                m_ChevronTriangle.anchorMax = new Vector2(0.5f, 0f);
                m_ChevronTriangle.sizeDelta = new Vector2(screenSize.x, screenSize.x / 8f);
            }
            else
            {
                m_Title.anchorMin = new Vector2(0.5f, 0.5f);
                m_Title.anchorMax = new Vector2(0.5f, 0.5f);
                m_Title.sizeDelta = new Vector2(LayoutUtility.GetPreferredWidth(m_Title), LayoutUtility.GetPreferredHeight(m_Title));
                m_Title.anchoredPosition = new Vector2((screenSize.x / 4f) - (screenSize.y / 8f) / 3f, m_Title.rect.height / 2f + values.padding);

                m_SubTitle.anchorMin = new Vector2(0.5f, 0.5f);
                m_SubTitle.anchorMax = new Vector2(0.5f, 0.5f);
                m_SubTitle.sizeDelta = new Vector2(Mathf.Min(LayoutUtility.GetPreferredWidth(m_SubTitle), screenSize.x / 2f - values.padding * 2f), LayoutUtility.GetPreferredHeight(m_SubTitle));
                m_SubTitle.anchoredPosition = new Vector2((screenSize.x / 4f) - (screenSize.y / 8f) / 3f, -m_SubTitle.rect.height / 2f);

                m_Badge.anchorMin = new Vector2(0f, 0.5f);
                m_Badge.anchorMax = new Vector2(0f, 0.5f);
                m_Badge.SetSizeDeltaY(Mathf.Min(screenSize.y * 0.65f, (screenSize.x / 2f - screenSize.y / 8f) / 0.65f) - values.padding * 2f);
                m_Badge.anchoredPosition = new Vector2((screenSize.x / 4f) - (screenSize.y / 8f) / 2f, 0f);

                m_Button.anchorMin = new Vector2(0.5f, 0.5f);
                m_Button.anchorMax = new Vector2(0.5f, 0.5f);
                m_Button.sizeDelta = new Vector2(256f, 40f);
                m_Button.anchoredPosition = new Vector2((screenSize.x / 4f) - (screenSize.y / 8f) / 3f, -m_SubTitle.rect.height - 20f - values.padding - values.spacing);

                m_ChevronRectangle.sizeDelta = screenSize;
                m_ChevronRectangle.anchorMin = new Vector2(0.5f, 0.5f);
                m_ChevronRectangle.pivot = new Vector2(0f, 0.5f);
                m_ChevronRectangle.anchoredPosition = new Vector2(0f, 0f);

                m_ChevronTriangle.localEulerAngles = new Vector3(0f, 0f, -90f);
                m_ChevronTriangle.anchorMin = new Vector2(0f, 0.5f);
                m_ChevronTriangle.anchorMax = new Vector2(0f, 0.5f);
                m_ChevronTriangle.sizeDelta = new Vector2(screenSize.y, screenSize.y / 8f);
            }
        }
    }
}
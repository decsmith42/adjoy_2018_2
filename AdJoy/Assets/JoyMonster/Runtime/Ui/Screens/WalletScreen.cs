﻿using DuoVia.FuzzyStrings;
using JoyMonster.API;
using JoyMonster.API.Model;
using JoyMonster.Core;
using JoyMonster.Ui.Lists;
using MaterialUI_JoyMonster;
using System.Collections;
using JoyMonster.ExternalUtils;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace JoyMonster.Ui.Screens
{
    public class WalletScreen : UiScreen
    {
        public override int screenId
        {
            get { return Values.ScreenIds.WALLET; }
        }

        [SerializeField] private GameObject m_ListItemTemplate = null;
        [SerializeField] private ListView m_ListView = null;
        [SerializeField] private ListWalletErrorExtension m_ErrorExtension = null;
        [SerializeField] private ListProgressExtension m_ProgressExtension = null;
        [SerializeField] private ListFilterExtension m_FilterExtension = null;
        [SerializeField] private Graphic m_HeaderBackground = null;
        [SerializeField] private TextMeshProUGUI m_HeaderLabel = null;
        [SerializeField] private GameObject m_HeaderButton = null;

        [SerializeField] private DynamicTransformPosition m_TimerHeaderPosition = null;
        [SerializeField] private DynamicTransformPosition m_LocationHeaderPosition = null;

        private readonly Color m_ColorReady = new Color(0f, 0.4156863f, 0.7098039f, 1f);
        private readonly Color m_ColorWaiting = new Color(0.2980392f, 0.6862745f, 0.3137255f, 1f);

        private bool m_Init;

        protected override void OnBeforeShow()
        {
            if (EngagementFormatter.HasCooldown())
            {
                m_HeaderLabel.text = EngagementFormatter.DisplayCooldown();
                m_HeaderButton.SetActive(false);
                m_HeaderBackground.color = m_ColorWaiting;
            }
            else
            {
                m_HeaderLabel.text = Values.Text.WALLET_COOLDOWN_NONE;
                m_HeaderButton.SetActive(true);
                m_HeaderBackground.color = m_ColorReady;
            }

            JoyMonsterManager.walletHandler.RefreshWalletList();
            base.OnBeforeShow();
            layoutBehaviour.RefreshLayout();

            float offset = m_TimerHeaderPosition.rectTransform.rect.height +
                           m_LocationHeaderPosition.rectTransform.rect.height +
                           UiLayoutValueHandler.appBarHeight.currentValue.appBarHeight;

            if (JoyMonsterManager.engagementHandler.cooldownEnabled)
            {
                m_TimerHeaderPosition.rectTransform.gameObject.SetActive(true);
                m_TimerHeaderPosition.rectTransform.localScale = Vector3.one;
                m_TimerHeaderPosition.offset = new Vector4(offset, offset, offset, offset);
                m_TimerHeaderPosition.MarkPositionAsBase();
                m_TimerHeaderPosition.SetPosition(DynamicTransformPosition.Direction.Up);
            }
            else
            {
                m_TimerHeaderPosition.rectTransform.gameObject.SetActive(false);
                m_TimerHeaderPosition.rectTransform.localScale = Vector3.zero;
            }


            m_LocationHeaderPosition.offset = new Vector4(offset, offset, offset, offset);
            m_LocationHeaderPosition.MarkPositionAsBase();
            m_LocationHeaderPosition.SetPosition(DynamicTransformPosition.Direction.Up);
        }

        protected override void OnShow()
        {
            m_DynamicCanvasGroup.SetState(true);
            m_ProgressExtension.ShowProgressIndeterminate();
            TweenManager.TimedCallback(Values.Duration.SHORT, () => m_ShowPromise.Resolve(true));
            ShowListWhenReady();
        }

        protected override void OnAfterShow()
        {
            if (JoyMonsterManager.engagementHandler.cooldownEnabled)
            {
                m_TimerHeaderPosition.TransitionPosition(DynamicTransformPosition.Direction.Middle, Values.Duration.SHORT, 0f, Tween.TweenType.EaseOutQuint);
            }
            else
            {
                m_TimerHeaderPosition.rectTransform.gameObject.SetActive(false);
                m_TimerHeaderPosition.rectTransform.localScale = Vector3.zero;
            }

            m_LocationHeaderPosition.TransitionPosition(DynamicTransformPosition.Direction.Middle, Values.Duration.SHORT, Values.Duration.PER_ELEMENT_DELAY, Tween.TweenType.EaseOutQuint);
        }

        protected override void OnHide()
        {
            if (m_ListView.isInitialized)
            {
                m_ListView.HideList();
            }

            m_LocationHeaderPosition.TransitionPosition(DynamicTransformPosition.Direction.Up, Values.Duration.SHORT, 0f, Tween.TweenType.EaseInQuint);

            if (JoyMonsterManager.engagementHandler.cooldownEnabled)
            {
                m_TimerHeaderPosition.TransitionPosition(DynamicTransformPosition.Direction.Up, Values.Duration.SHORT, Values.Duration.PER_ELEMENT_DELAY,
                    Tween.TweenType.EaseInQuint);
            }

            m_DynamicCanvasGroup.TransitionState(false, Values.Duration.SHORT, Values.Duration.SHORT + Values.Duration.PER_ELEMENT_DELAY);
            TweenManager.TimedCallback(Values.Duration.MEDIUM, () => m_HidePromise.Resolve(true));
        }

        private void OnSearchUpdated()
        {
            m_FilterExtension.FilterList(ListItemIsHidden);
        }

        private bool ListItemIsHidden(ListItemData itemData)
        {
            ListItemDataWalletOffer dataOffer = itemData as ListItemDataWalletOffer;

            if (dataOffer == null)
            {
                return true;
            }

            string search = JoyMonsterManager.uiHandler.appBar.searchBar.inputField.text.ToLower();
            string title = dataOffer.offer.title.ToLower();
            string company = dataOffer.offer.company.ToLower();

            if (title.Contains(search) || company.Contains(search))
            {
                return true;
            }

            if (title.FuzzyEquals(search) || company.FuzzyEquals(search))
            {
                return true;
            }

            return false;
        }

        private void ShowListWhenReady()
        {
            JoyMonsterManager.walletHandler.currentWalletRefreshPromise
                .Then(list => SetListData(list))
                .Catch(exception =>
                {
                    JoyMonsterAPI.DebugLogError(exception);
                    m_ProgressExtension.HideProgress();
                    TweenManager.TimedCallback(Values.Duration.SHORT, () => m_ErrorExtension.ShowError());
                });
        }

        private void SetListData(WalletList walletList)
        {
            ListData listData = new ListData();

            foreach (Offer offer in walletList.offers)
            {
                listData.listItems.Add(new ListItemDataWalletOffer
                {
                    offer = offer
                });
            }

            if (!m_Init)
            {
                FirstTimeInit();
            }

            m_ListView.SetListData(listData);

            m_ProgressExtension.HideProgress();
            m_ErrorExtension.HideError();

            TweenManager.TimedCallback(Values.Duration.SHORT, () =>
            {
                if (m_ListView.listData.listItems.Count > 0)
                {
                    m_ListView.ShowList();
                }
                else
                {
                    m_ErrorExtension.ShowError();
                }
            });
        }

        private void FirstTimeInit()
        {
            m_Init = true;

            m_ListView.InitializeStandard();
            m_ListView.LayoutControllerStandard().forceSingleLine = true;
            float listPadding = UiLayoutValueHandler.common.currentValue.padding;
            m_ListView.LayoutControllerStandard().listPadding = new Vector2(listPadding, listPadding);
            m_ListView.LayoutControllerStandard().listItemSpacing = new Vector2(listPadding, listPadding);
            m_ListView.interactionController.onItemInteraction += OnListItemInteraction;
            m_ListView.layoutController.itemPool.SetTemplatePrefab(typeof(ListItemDataWalletOffer), m_ListItemTemplate);
            JoyMonsterManager.uiHandler.appBar.searchBar.onSearchUpdate.AddListener(OnSearchUpdated);
        }

        public void OnLocationPromptButtonClick()
        {
            if (!PermissionHelper.IsLocationPermissionAuthorized())
            {
                PermissionHelper.RequestLocationPermission();
            }

            StartCoroutine(WaitAndRefreshLocationBanner());
        }

        // After the user clicked on the location banner button, we try to refresh the banner
        private IEnumerator WaitAndRefreshLocationBanner()
        {
            yield return new WaitForSeconds(3f);

            WalletScreenLayout walletScreenLayout = ((WalletScreenLayout)layoutBehaviour);
            walletScreenLayout.RefreshLayout();
        }

        private void OnListItemInteraction(int interactionType, BaseEventData eventData, ListItem listItem, int subItemId)
        {
            if (interactionType == ListInteraction.pointerClick)
            {
                OnOfferSelected(listItem);
            }
        }

        private void OnOfferSelected(ListItem listItem)
        {
            if (JoyMonsterManager.redeemHandler.currentRedeemedOffer != null && JoyMonsterManager.redeemHandler.currentRedeemedOffer.offer.walletUuid ==
                ((ListItemDataWalletOffer)listItem.currentData).offer.walletUuid)
            {
                if (JoyMonsterManager.redeemHandler.currentRedeemedOffer.isExpired)
                {
                    JoyMonsterManager.uiHandler.expiredPopup.Show();
                    return;
                }
                else
                {
                    JoyMonsterManager.uiHandler.TransitionToScreen(Values.ScreenIds.MERCHANT);
                }
            }
            JoyMonsterManager.walletHandler.SelectOffer(((ListItemDataWalletOffer)listItem.currentData).offer);
            JoyMonsterManager.uiHandler.TransitionToScreen(Values.ScreenIds.OFFER_DETAILS);
        }

        public void OnErrorRefresh()
        {
            TweenManager.TimedCallback(Values.Duration.SHORT, () => m_ProgressExtension.ShowProgressIndeterminate());
            JoyMonsterManager.walletHandler.RefreshWalletList();
            ShowListWhenReady();
        }

        public void OnHeaderButtonClicked()
        {
            JoyMonsterManager.uiHandler.OnBackToGameButtonPressed();
        }

        protected override void SetAppBars()
        {
            TweenManager.TimedCallback(Values.Duration.PER_ELEMENT_DELAY, () =>
            {
                JoyMonsterManager.uiHandler.appBar.TransitionVisible(true);
                JoyMonsterManager.uiHandler.appBar.SetLeftButton(false);
                JoyMonsterManager.uiHandler.appBar.SetTitle(Values.Text.APPBAR_TITLE_WALLET);
                JoyMonsterManager.uiHandler.appBar.SetSearchButton(true);
                JoyMonsterManager.uiHandler.appBar.SetHelpButton(true);
            });
        }

        protected override void ResetScreen()
        {
            base.ResetScreen();
            m_ListView.layoutController.ClearDisplayedItems();
        }

        public override bool OnBackButtonPressed()
        {
            if (JoyMonsterManager.uiHandler.appBar.searchBar.isOpen)
            {
                JoyMonsterManager.uiHandler.appBar.searchBar.Close();
                return false;
            }

            return true;
        }
    }
}
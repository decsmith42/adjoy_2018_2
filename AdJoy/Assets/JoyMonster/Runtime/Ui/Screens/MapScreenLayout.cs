﻿using MaterialUI_JoyMonster;
using UnityEngine;

namespace JoyMonster.Ui.Screens
{
    public class MapScreenLayout : ScreenLayoutBehaviour
    {
        [SerializeField] private VectorImage m_IconLoading = null;

        protected override void OnSetLayout()
        {
            base.OnSetLayout();

            float padding = UiLayoutValueHandler.common.currentValue.padding * 32f;
            m_IconLoading.size = Mathf.Max(Mathf.Min(screenBaseSize.x - padding, screenBaseSize.y - padding, 512f), 64f);
        }
    }
}
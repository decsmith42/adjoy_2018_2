﻿using JoyMonster.Core;
using MaterialUI_JoyMonster;
using TMPro;
using UnityEngine;

namespace JoyMonster.Ui.Screens
{
    public class MerchantScreenLayout : ScreenLayoutBehaviour
    {
        [SerializeField] private RectTransform m_ContainerRedeemInfo = null;
        [SerializeField] private RectTransform m_ContainerCompanyInfo = null;

        [SerializeField] private RectTransform m_ContainerTimer = null;
        [SerializeField] private RectTransform m_ContainerImageCode = null;

        [SerializeField] private TextMeshProUGUI m_LabelCompany = null;
        [SerializeField] private TextMeshProUGUI m_LabelTitle = null;
        [SerializeField] private TextMeshProUGUI m_LabelDescription = null;
        [SerializeField] private TextMeshProUGUI m_LabelValue = null;
        [SerializeField] private TextMeshProUGUI m_LabelTimerTitle = null;
        [SerializeField] private TextMeshProUGUI m_LabelTimerCounter = null;
        [SerializeField] private TextMeshProUGUI m_LabelCode = null;

        public bool overrideHasTimer;
        public bool overrideHasImageCode;
        public bool overrideHasCode;

        protected override void OnSetLayout()
        {
            base.OnSetLayout();

            m_LabelTimerCounter.alignment = TextAlignmentOptions.Center;

            bool hasTimer = true;
            bool hasImageCode = true;
            bool hasCode = true;

            if (Application.isPlaying && JoyMonsterManager.redeemHandler.currentRedeemedOffer != null)
            {
                hasTimer = JoyMonsterManager.redeemHandler.currentRedeemedOffer.hasTimer;
                hasImageCode = JoyMonsterManager.redeemHandler.currentRedeemedOffer.hasImageCode;
                hasCode = JoyMonsterManager.redeemHandler.currentRedeemedOffer.hasCode;
            }
            else
            {
                hasTimer = overrideHasTimer;
                hasImageCode = overrideHasImageCode;
                hasCode = overrideHasCode;
            }

            bool isHorizontal = UiScaleController.orientation == UiScaleController.Orientation.Landscape && (hasTimer || hasImageCode || (screenBaseSize.x / screenBaseSize.y > 1.6f));
            LayoutValue values = UiLayoutValueHandler.common.currentValue;
            float padding = values.padding * 2f;
            float spacing = values.spacing * 2f;

            float redeemInfoHeight = 0f;

            m_ContainerTimer.gameObject.SetActive(hasTimer);
            m_LabelTimerTitle.gameObject.SetActive(hasTimer);
            m_ContainerImageCode.gameObject.SetActive(hasImageCode);
            m_LabelCode.gameObject.SetActive(hasCode);

            m_LabelCompany.fontSize = values.textSubHeaderFontSize;
            m_LabelTitle.fontSize = values.textHeaderFontSize;
            m_LabelDescription.fontSize = values.textParagraphFontSize;
            m_LabelValue.fontSize = values.textParagraphFontSize;
            m_LabelTimerTitle.fontSize = values.textSubHeaderFontSize;
            //m_LabelTimerCounter.fontSize = values.textSubHeaderFontSize;

            //m_LabelCode.enableAutoSizing = !(hasTimer || hasImageCode);
            //m_LabelCode.fontSize = values.textParagraphFontSize;

            if (isHorizontal)
            {
                float sectionWidth = (screenBaseSize.x - padding * 3f) / 2f;
                m_ContainerRedeemInfo.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Left, padding, sectionWidth);
                m_ContainerCompanyInfo.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Left, sectionWidth + padding * 2f, sectionWidth);
            }
            else
            {
                m_ContainerRedeemInfo.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Left, padding, screenBaseSize.x - padding * 2f);
                m_ContainerCompanyInfo.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Left, padding, screenBaseSize.x - padding * 2f);
            }

            float companyInfoHeight = 0f;
            m_LabelValue.rectTransform.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Bottom, companyInfoHeight, m_LabelValue.preferredHeight);
            companyInfoHeight += m_LabelValue.preferredHeight + spacing;
            m_LabelDescription.rectTransform.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Bottom, companyInfoHeight, m_LabelDescription.preferredHeight);
            companyInfoHeight += m_LabelDescription.preferredHeight + spacing;
            m_LabelTitle.rectTransform.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Bottom, companyInfoHeight, m_LabelTitle.preferredHeight);
            companyInfoHeight += m_LabelTitle.preferredHeight + spacing;
            m_LabelCompany.rectTransform.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Bottom, companyInfoHeight, m_LabelCompany.preferredHeight);
            companyInfoHeight += m_LabelCompany.preferredHeight;

            if (isHorizontal)
            {
                m_ContainerCompanyInfo.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Bottom, (screenBaseSize.y - companyInfoHeight) / 2f, companyInfoHeight);

                m_ContainerRedeemInfo.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Bottom, padding, screenBaseSize.y - padding * 2f);
            }
            else
            {
                m_ContainerCompanyInfo.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Bottom, padding, companyInfoHeight);

                m_ContainerRedeemInfo.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Bottom,
                    companyInfoHeight + padding * 2f, screenBaseSize.y - companyInfoHeight - padding * 3f);
            }

            redeemInfoHeight = m_ContainerRedeemInfo.rect.height;

            if (hasTimer)
            {
                m_LabelTimerTitle.rectTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, m_LabelTimerTitle.preferredHeight);
                redeemInfoHeight -= m_LabelTimerTitle.preferredHeight + spacing;
            }

            int numberOfSections = (hasTimer ? 1 : 0) + (hasCode ? 1 : 0) + (hasImageCode ? 1 : 0);
            float remainingRedeemInfoHeight = redeemInfoHeight;

            float codeHeight = 0f;
            if (hasCode)
            {
                m_LabelCode.rectTransform.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Left, padding, m_ContainerRedeemInfo.rect.width - padding * 2f);

                if (numberOfSections == 1)
                {
                    codeHeight = remainingRedeemInfoHeight - padding * 2f;
                    m_LabelCode.rectTransform.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Bottom, padding, codeHeight);
                }
                else
                {
                    codeHeight = m_LabelCode.preferredHeight;
                    m_LabelCode.rectTransform.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Bottom, 0f, codeHeight);
                }

                codeHeight += spacing;
                remainingRedeemInfoHeight -= codeHeight;
            }

            float imageCodeHeight = 0f;
            if (hasImageCode)
            {
                m_LabelCode.rectTransform.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Left, padding, m_ContainerRedeemInfo.rect.width - padding * 2f);

                if (numberOfSections == 1)
                {
                    imageCodeHeight = redeemInfoHeight - padding * 2f;
                    m_ContainerImageCode.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Bottom, padding, imageCodeHeight);
                }
                else if (numberOfSections == 2)
                {
                    imageCodeHeight = hasCode ? remainingRedeemInfoHeight : redeemInfoHeight / 2f;
                    m_ContainerImageCode.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Bottom, codeHeight, imageCodeHeight);
                }
                else
                {
                    imageCodeHeight = redeemInfoHeight / 3f;
                    m_ContainerImageCode.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Bottom, codeHeight, imageCodeHeight);
                }

                imageCodeHeight += spacing;
                remainingRedeemInfoHeight -= imageCodeHeight;
            }

            float timerHeight = 0f;
            if (hasTimer)
            {
                m_ContainerTimer.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Left, padding, m_ContainerRedeemInfo.rect.width - padding * 2f);

                if (numberOfSections == 1)
                {
                    timerHeight = redeemInfoHeight - padding * 2f;
                    m_ContainerTimer.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Bottom, padding, timerHeight);
                }
                else
                {
                    timerHeight = remainingRedeemInfoHeight;
                    m_ContainerTimer.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Bottom, codeHeight + imageCodeHeight, timerHeight);
                }
            }
        }
    }
}
﻿namespace JoyMonster.Ui.Screens
{
    public class DismissedScreen : UiScreen
    {
        public override int screenId
        {
            get { return Values.ScreenIds.DISMISSED; }
        }
    }
}
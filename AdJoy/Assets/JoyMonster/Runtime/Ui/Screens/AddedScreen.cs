﻿using JoyMonster.Core;
using MaterialUI_JoyMonster;
using TMPro;
using UnityEngine;

namespace JoyMonster.Ui.Screens
{
    public class AddedScreen : UiScreen
    {
        public override int screenId
        {
            get { return Values.ScreenIds.ADDED; }
        }

        [SerializeField] private DynamicTransformPosition m_ChevronPosition = null;
        [SerializeField] private DynamicTransformPosition m_LabelTitlePosition = null;
        [SerializeField] private TextMeshProUGUI m_LabelSubtitle = null;
        [SerializeField] private DynamicGraphic m_LabelSubTitleGraphic = null;
        [SerializeField] private RectTransform m_BadgeTransform = null;
        [SerializeField] private DynamicTransformPosition m_ButtonPosition = null;

        private bool m_ButtonPressed;

        protected override void OnBeforeShow()
        {
            m_DynamicCanvasGroup.SetState(false);

            base.OnBeforeShow();

            PrepareDynamicPosition(m_ChevronPosition);
            PrepareDynamicPosition(m_LabelTitlePosition);
            PrepareDynamicPosition(m_ButtonPosition);
            m_ButtonPosition.SetPosition(DynamicTransformPosition.Direction.Down);

            m_BadgeTransform.localScale = Vector3.zero;
            m_Content.SetAnchoredPositionY(-layoutBehaviour.screenBasePositionOffset.y);

            if (EngagementFormatter.HasCooldown())
            {
                m_LabelSubtitle.text = EngagementFormatter.DisplayCooldown();
            }
            else
            {
                m_LabelSubtitle.text = Values.Text.ENGAGEMENT_COOLDOWN_NONE;
            }

            m_LabelSubTitleGraphic.SetState(false);
            m_ButtonPressed = false;
        }

        protected override void OnAfterShow()
        {
            m_ChevronPosition.TransitionPosition(DynamicTransformPosition.Direction.Middle, Values.Duration.LONG,
                Values.Duration.MEDIUM, Tween.TweenType.Overshoot);

            m_ButtonPosition.TransitionPosition(DynamicTransformPosition.Direction.Middle,
                Values.Duration.MEDIUM, Values.Duration.LONG + Values.Duration.SHORT, Tween.TweenType.EaseOutQuint);

            TweenManager.TweenVector3(value => m_BadgeTransform.localScale = value, Vector3.zero, Vector3.one,
                Values.Duration.SHORT, Values.Duration.LONG + Values.Duration.SHORT,
                tweenType: Tween.TweenType.EaseOutSept);

            m_LabelTitlePosition.TransitionPosition(DynamicTransformPosition.Direction.Middle, Values.Duration.MEDIUM,
                Values.Duration.MEDIUM, Tween.TweenType.EaseOutQuint);
            m_LabelSubTitleGraphic.TransitionState(true, Values.Duration.MEDIUM, Values.Duration.LONG);
        }

        protected override void OnHide()
        {
            m_ChevronPosition.TransitionPosition(UiScaleController.orientation == UiScaleController.Orientation.Portrait ?
                    DynamicTransformPosition.Direction.Up :
                    DynamicTransformPosition.Direction.Right,
                Values.Duration.MEDIUM, 0f, Tween.TweenType.EaseInCubed);

            TweenManager.TweenVector3(value => m_BadgeTransform.localScale = value,
                Vector3.one, Vector3.zero,
                Values.Duration.SHORTER, 0f, tweenType: Tween.TweenType.EaseInCubed);

            m_LabelSubTitleGraphic.DelayedTransitionState(false, Values.Duration.SHORTER, Values.Duration.PER_ELEMENT_DELAY);

            m_ButtonPosition.TransitionPosition(UiScaleController.orientation == UiScaleController.Orientation.Portrait ?
                    DynamicTransformPosition.Direction.Down :
                    DynamicTransformPosition.Direction.Right,
                Values.Duration.SHORTER, Values.Duration.PER_ELEMENT_DELAY, Tween.TweenType.EaseInQuint);

            TweenManager.TimedCallback(Values.Duration.MEDIUM, () => m_HidePromise.Resolve(true));
        }

        private void PrepareDynamicPosition(DynamicTransformPosition dynamicTransformPosition)
        {
            dynamicTransformPosition.MarkPositionAsBase();
            Vector4 offset = dynamicTransformPosition.offset;
            offset.x = layoutBehaviour.screenBaseSize.x * 2f;
            offset.y = layoutBehaviour.screenBaseSize.y;
            offset.z = layoutBehaviour.screenBaseSize.x;
            offset.w = layoutBehaviour.screenBaseSize.y * 2f;
            dynamicTransformPosition.offset = offset;
            dynamicTransformPosition.SetPosition(UiScaleController.orientation == UiScaleController.Orientation.Portrait
                ? DynamicTransformPosition.Direction.Up
                : DynamicTransformPosition.Direction.Right);
        }


        public void OnWalletButtonPressed()
        {
            if (m_ButtonPressed) return;

            m_ButtonPressed = false;

            TweenManager.TimedCallback(Values.Duration.SHORT, () =>
            {
                JoyMonsterManager.uiHandler.ShowScreenOverCurrent(Values.ScreenIds.WALLET, false);
                JoyMonsterManager.uiHandler.ClearScreenHistory();
            });
        }
    }
}
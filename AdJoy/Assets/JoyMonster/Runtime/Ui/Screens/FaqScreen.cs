﻿using JoyMonster.API;
using JoyMonster.API.Model;
using JoyMonster.Core;
using JoyMonster.Ui.Lists;
using MaterialUI_JoyMonster;
using RSG;
using System.Text;
using UnityEngine;
using UnityEngine.EventSystems;
using Random = UnityEngine.Random;

namespace JoyMonster.Ui.Screens
{
    public class FaqScreen : UiScreen
    {
        public override int screenId
        {
            get { return Values.ScreenIds.FAQ; }
        }

        [SerializeField] private GameObject m_ListItemTemplate = null;
        [SerializeField] private ListView m_ListView = null;
        [SerializeField] private ListFaqErrorExtension m_ErrorExtension = null;
        [SerializeField] private DynamicTransformPosition m_DynamicPosition = null;
        [SerializeField] private ListProgressExtension m_ProgressExtension = null;

        private ListAnimationControllerFaq m_AnimationController;

        private bool m_Init;

        private string[] words =
        {
        "this", "elephants", "retailer", "overpowered", "apple", "pear", "YEE", "unsustainable", "black", "holes",
        "absolutely", "revelations"
        };

        protected override void OnBeforeShow()
        {
            FaqHandler.RefreshFaqList();
            base.OnBeforeShow();
            layoutBehaviour.RefreshLayout();
        }

        protected override void OnShow()
        {
            m_DynamicPosition.basePosition = new Vector2(layoutBehaviour.screenBasePositionOffset.x, -layoutBehaviour.screenBasePositionOffset.y);
            m_DynamicPosition.offset = new Vector4(0f, layoutBehaviour.screenBaseSize.y, 0f, layoutBehaviour.screenBaseSize.y);
            m_DynamicPosition.TransitionPosition(DynamicTransformPosition.Direction.Down, DynamicTransformPosition.Direction.Middle,
                Values.Duration.MEDIUM, 0f, Tween.TweenType.SoftEaseOutSept);
            m_DynamicCanvasGroup.TransitionState(false, true, Values.Duration.MEDIUM, 0f);
            TweenManager.TimedCallback(Values.Duration.SHORT, () => m_ShowPromise.Resolve(true));
            if (FaqHandler.currentFaqPromise == null || FaqHandler.currentFaqPromise.CurState != PromiseState.Resolved)
            {
                m_ProgressExtension.ShowProgressIndeterminate();
            }

            ShowListWhenReady();
        }

        protected override void OnAfterShow()
        {
        }

        protected override void OnHide()
        {
            m_ListView.HideList();
            m_DynamicCanvasGroup.TransitionState(false, Values.Duration.MEDIUM, Values.Duration.SHORT);
            TweenManager.TimedCallback(Values.Duration.MEDIUM + Values.Duration.SHORT, () => m_HidePromise.Resolve(true));
        }

        private void ShowListWhenReady()
        {
            FaqHandler.currentFaqPromise
                .Then(list => SetListData(list))
                .Catch(exception =>
                {
                    JoyMonsterAPI.DebugLogException(exception);
                    m_ProgressExtension.HideProgress();
                    TweenManager.TimedCallback(Values.Duration.SHORT, () => m_ErrorExtension.ShowError());
                });
        }

        private void SetListData(FaqList faqList)
        {
            if (m_ListView.listData == null || m_ListView.listData.listItems.Count != faqList.faqs.Length)
            {
                ListData listData = new ListData();

                foreach (Faq faq in faqList.faqs)
                {
                    listData.listItems.Add(new ListItemDataFaq
                    {
                        faq = faq
                    });
                }

                if (!m_Init)
                {
                    FirstTimeInit();
                }

                m_ListView.SetListData(listData);
            }

            if (m_ProgressExtension.progessIndicator.isAnimatingIndeterminate)
            {
                m_ProgressExtension.HideProgress();
                TweenManager.TimedCallback(Values.Duration.SHORT, () => m_ListView.ShowList());
            }
            else
            {
                m_ListView.ShowList();
            }
        }

        private void OnItemInteraction(int interactionType, BaseEventData eventData, ListItem listItem, int subItemId)
        {
            if (interactionType != ListInteraction.pointerClick) return;

            m_AnimationController.SelectItem((ListItemFaq)listItem);
        }

        private string GetRandomString(int maxWords)
        {
            int wordCount = Random.Range(5, maxWords);

            StringBuilder text = new StringBuilder();

            for (int i = 0; i < wordCount; i++)
            {
                text.Append(words[Random.Range(0, 11)]).Append(" ");
            }

            return text.ToString();
        }

        private void FirstTimeInit()
        {
            m_Init = true;

            ListLayoutControllerStandard layoutController = new ListLayoutControllerStandard
            {
                forceSingleLine = true,
                listPadding = Vector2.zero,
                listItemSpacing = Vector2.zero
            };
            m_AnimationController = new ListAnimationControllerFaq();
            ListInteractionControllerStandard interactionController = new ListInteractionControllerStandard();
            interactionController.onItemInteraction += OnItemInteraction;
            m_ListView.InitializeList(layoutController, m_AnimationController, interactionController);
            m_ListView.layoutController.itemPool.SetTemplatePrefab(typeof(ListItemDataFaq), m_ListItemTemplate);
        }

        public void OnErrorRefresh()
        {
            TweenManager.TimedCallback(Values.Duration.SHORT, () => m_ProgressExtension.ShowProgressIndeterminate());
            FaqHandler.RefreshFaqList();
            ShowListWhenReady();
        }

        protected override void SetAppBars()
        {
            JoyMonsterManager.uiHandler.appBar.SetLeftButton(true);
            JoyMonsterManager.uiHandler.appBar.SetTitle(Values.Text.APPBAR_TITLE_FAQ);
            JoyMonsterManager.uiHandler.appBar.SetSearchButton(false);
            JoyMonsterManager.uiHandler.appBar.SetHelpButton(false);
        }
    }
}
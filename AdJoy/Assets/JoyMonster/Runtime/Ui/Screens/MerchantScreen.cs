﻿using JoyMonster.Core;
using MaterialUI_JoyMonster;
using RSG;
using System.Globalization;
using JoyMonster.API.Model;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace JoyMonster.Ui.Screens
{
    public class MerchantScreen : UiScreen
    {
        public override int screenId
        {
            get { return Values.ScreenIds.MERCHANT; }
        }

        private Promise<Texture2D> m_ImagePromise;

        [SerializeField] private DynamicTransformPosition m_DynamicPosition = null;

        [SerializeField] private Image m_TimerMask = null;
        [SerializeField] private RawImageLayoutBehaviour m_ImageCodeLayout = null;
        [SerializeField] private DynamicGraphic m_ImageCodeGraphic = null;

        [SerializeField] private TextMeshProUGUI m_LabelTimerCounter = null;
        [SerializeField] private TextMeshProUGUI m_LabelCode = null;
        [SerializeField] private TextMeshProUGUI m_LabelCompany = null;
        [SerializeField] private TextMeshProUGUI m_LabelTitle = null;
        [SerializeField] private TextMeshProUGUI m_LabelDescription = null;
        [SerializeField] private TextMeshProUGUI m_LabelValue = null;

        private bool m_UpdateTimer;
        private float m_TimerLabelLastUpdate;

        private RedeemHandler.RedeemedOffer redeemedOffer { get { return JoyMonsterManager.redeemHandler.currentRedeemedOffer; } }

        protected override void OnBeforeShow()
        {
            SetData();
            m_DynamicCanvasGroup.SetState(false);
            base.OnBeforeShow();
            layoutBehaviour.RefreshLayout();
            m_DynamicPosition.basePosition = -layoutBehaviour.screenBasePositionOffset;
            m_DynamicPosition.offset = new Vector4(0f, 0f, 0f, layoutBehaviour.screenBaseSize.y * 1.25f);
            m_DynamicPosition.SetPosition(DynamicTransformPosition.Direction.Down);
        }

        protected override void OnShow()
        {
            m_UpdateTimer = true;

            m_DynamicPosition.TransitionPosition(DynamicTransformPosition.Direction.Middle, Values.Duration.SCREEN_TRANSITION, Values.Duration.SHORTER, Tween.TweenType.EaseOutQuint);
            m_DynamicCanvasGroup.TransitionState(true, Values.Duration.MEDIUM, Values.Duration.SHORTER);
            TweenManager.TimedCallback(Values.Duration.SCREEN_TRANSITION, () => m_ShowPromise.Resolve(true));
        }

        protected override void OnHide()
        {
            m_DynamicPosition.TransitionPosition(DynamicTransformPosition.Direction.Down, Values.Duration.SCREEN_TRANSITION, Values.Duration.SHORTER, Tween.TweenType.EaseInQuint);
            m_DynamicCanvasGroup.TransitionState(false, Values.Duration.SCREEN_TRANSITION, Values.Duration.SHORTER);
            TweenManager.TimedCallback(Values.Duration.SCREEN_TRANSITION + Values.Duration.SHORTER, () => m_HidePromise.Resolve(true));
        }

        protected override void OnAfterHide()
        {
            m_UpdateTimer = false;
            base.OnAfterHide();
        }

        protected override void SetAppBars()
        {
            JoyMonsterManager.uiHandler.appBar.SetSearchButton(false);
            JoyMonsterManager.uiHandler.appBar.SetHelpButton(true);
            JoyMonsterManager.uiHandler.appBar.SetLeftButton(true);
            JoyMonsterManager.uiHandler.appBar.SetTitle(Values.Text.APPBAR_TITLE_MERCHANT);
        }


        private void SetData()
        {
            if (redeemedOffer == null) return;

            LoadAndSetImage(redeemedOffer.walletRedeem.imageUrl);

            m_LabelCode.text = redeemedOffer.walletRedeem.code;
            m_LabelCompany.text = redeemedOffer.offer.company;
            m_LabelTitle.text = redeemedOffer.offer.title;
            m_LabelDescription.text = redeemedOffer.offer.description;

            m_LabelValue.text = OfferFormatter.DisplayLimit(JoyMonsterManager.walletHandler.currentOffer);

        }

        private void Update()
        {
            if (m_IsShowing && m_UpdateTimer && redeemedOffer.hasTimer)
            {
                UpdateTimer();
            }
        }

        private void UpdateTimer()
        {
            if (redeemedOffer.isExpired)
            {
                m_UpdateTimer = false;
                JoyMonsterManager.uiHandler.expiredPopup.Show();
                OnExpiryDialogClick();
            }

            m_TimerMask.fillAmount = redeemedOffer.timerStatusNormalized;

            m_TimerLabelLastUpdate -= Time.deltaTime;

            if (m_TimerLabelLastUpdate >= 0f) return;

            m_TimerLabelLastUpdate = 1f;

            m_LabelTimerCounter.text = redeemedOffer.timerStatusString;
        }

        private void OnExpiryDialogClick()
        {
            JoyMonsterManager.walletHandler.MarkWalletDataDirty();
            JoyMonsterManager.uiHandler.TransitionToScreen(Values.ScreenIds.WALLET);
            JoyMonsterManager.uiHandler.ClearHistoryAndSetPreviousScreen(Values.ScreenIds.GAME);
        }

        public void LoadAndSetImage(string url)
        {
            m_ImageCodeLayout.SetTexture(null);
            m_ImageCodeGraphic.graphic.color = MaterialColor.dividerDark;

            m_ImagePromise = new Promise<Texture2D>((resolve, reject) =>
            {
                WebTextureHandler.GetTexture(url).Then(resolve);
            });

            if (m_ImagePromise.CurState == PromiseState.Resolved)
            {
                m_ImagePromise.Then(texture =>
                {
                    m_ImageCodeLayout.SetTexture(texture);
                    if (JoyMonsterManager.webTextureHandler.IsErrorTexture(texture))
                    {
                        m_ImageCodeGraphic.SetToCustom(MaterialColor.dividerDark);
                    }
                    else
                    {
                        m_ImageCodeGraphic.SetState(true);
                    }
                });
                return;
            }

            m_ImagePromise.Then(texture =>
            {
                m_ImageCodeLayout.SetTexture(texture);
                if (JoyMonsterManager.webTextureHandler.IsErrorTexture(texture))
                {
                    m_ImageCodeGraphic.TransitionToCustom(MaterialColor.dividerDark, Values.Duration.SHORT, 0f);
                }
                else
                {
                    m_ImageCodeGraphic.TransitionState(true, Values.Duration.SHORT, 0f);
                }
            });
        }
    }
}
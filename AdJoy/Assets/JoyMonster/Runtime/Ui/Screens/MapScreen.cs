﻿using JoyMonster.Core;
using MaterialUI_JoyMonster;
using RSG;
using UnityEngine;

namespace JoyMonster.Ui.Screens
{
    public class MapScreen : UiScreen
    {
        public override int screenId
        {
            get { return Values.ScreenIds.MAP; }
        }

        [SerializeField] private DynamicTransformPosition m_DynamicPosition = null;
        [SerializeField] private DynamicGraphicEmphasized m_IconLoading = null;
        [SerializeField] private RawImageLayoutBehaviour m_ImageMap = null;
        [SerializeField] private DynamicGraphic m_ImageMapGraphic = null;
        private Promise<Texture2D> m_MapPromise;
        private double m_LocationToLoadLatitude;
        private double m_LocationToLoadLongitude;

        public void SetMapToLoad(double latitude, double longitude)
        {
            m_LocationToLoadLatitude = latitude;
            m_LocationToLoadLongitude = longitude;
        }

        protected override void OnBeforeShow()
        {
            m_ImageMapGraphic.SetState(false);
            m_DynamicCanvasGroup.SetState(false);
            m_IconLoading.SetState(false);
            base.OnBeforeShow();
            layoutBehaviour.RefreshLayout();
            m_DynamicPosition.basePosition = -layoutBehaviour.screenBasePositionOffset;
            m_DynamicPosition.offset = new Vector4(0f, 0f, 0f, layoutBehaviour.screenBaseSize.y * 1.25f);
            m_DynamicPosition.SetPosition(DynamicTransformPosition.Direction.Down);
            if (JoyMonsterManager.walletHandler.currentOffer != null)
            {
                m_LocationToLoadLatitude = JoyMonsterManager.walletHandler.currentOffer.latitude;
                m_LocationToLoadLongitude = JoyMonsterManager.walletHandler.currentOffer.longitude;
            }
        }

        protected override void OnShow()
        {
            LoadMap();

            m_DynamicPosition.TransitionPosition(DynamicTransformPosition.Direction.Middle, Values.Duration.SCREEN_TRANSITION, Values.Duration.SHORTER, Tween.TweenType.EaseOutQuint);
            m_DynamicCanvasGroup.TransitionState(true, Values.Duration.MEDIUM, Values.Duration.SHORTER);
            TweenManager.TimedCallback(Values.Duration.SCREEN_TRANSITION, () => m_ShowPromise.Resolve(true));
        }

        protected override void OnHide()
        {
            m_MapPromise.Discard();

            m_DynamicPosition.TransitionPosition(DynamicTransformPosition.Direction.Down, Values.Duration.SCREEN_TRANSITION, Values.Duration.SHORTER, Tween.TweenType.EaseInQuint);
            m_DynamicCanvasGroup.TransitionState(false, Values.Duration.SCREEN_TRANSITION, Values.Duration.SHORTER);
            TweenManager.TimedCallback(Values.Duration.SCREEN_TRANSITION + Values.Duration.SHORTER, () => m_HidePromise.Resolve(true));
        }

        protected override void SetAppBars()
        {
            JoyMonsterManager.uiHandler.appBar.SetSearchButton(false);
            JoyMonsterManager.uiHandler.appBar.SetHelpButton(true);
            JoyMonsterManager.uiHandler.appBar.SetLeftButton(true);
            JoyMonsterManager.uiHandler.appBar.SetTitle(Values.Text.APPBAR_TITLE_MAP);
        }

        public void OnLayoutChanged()
        {
            if (!isShowing) return;
            ReloadMap();
        }

        private void ReloadMap()
        {
            m_ImageMapGraphic.TransitionState(false, Values.Duration.SHORT, 0f);
            TweenManager.TimedCallback(Values.Duration.SHORT, () =>
            {
                m_ImageMap.SetTexture(null);
                LoadMap();
            });
        }

        private void LoadMap()
        {
            m_IconLoading.EmphasizeContinuous(delay: Values.Duration.MEDIUM);

            m_MapPromise = m_MapPromise.DiscardAndReplace();

            JoyMonsterManager.mapHandler.GetTexture(new MapHandler.MapTextureRequest(m_LocationToLoadLatitude, m_LocationToLoadLongitude, m_Content.rect.size))
                .Then(texture =>
                {
                    m_MapPromise.ResolveIfPending(texture);
                });

            TweenManager.TimedCallback(2f, () => { m_MapPromise.Then(texture => OnMapLoaded(texture)); });
        }

        private void OnMapLoaded(Texture2D mapTexture)
        {
            m_IconLoading.TransitionState(false, Values.Duration.SHORT, 0f);

            m_ImageMap.SetTexture(mapTexture);

            if (JoyMonsterManager.webTextureHandler.IsErrorTexture(mapTexture))
            {
                m_ImageMapGraphic.TransitionToCustom(MaterialColor.disabledDark, Values.Duration.SHORT, Values.Duration.SHORT);
            }
            else
            {
                m_ImageMapGraphic.TransitionState(true, Values.Duration.SHORT, Values.Duration.SHORT);
            }
        }
    }
}
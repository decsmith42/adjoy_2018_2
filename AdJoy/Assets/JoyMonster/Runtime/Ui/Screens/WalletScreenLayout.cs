﻿using JoyMonster.Core;
using JoyMonster.ExternalUtils;
using TMPro;
using UnityEngine;

namespace JoyMonster.Ui.Screens
{
    public class WalletScreenLayout : ScreenLayoutBehaviour
    {
        [SerializeField]
        private RectTransform m_ListView = null;

        [SerializeField]
        private RectTransform m_LocationPrompt = null;

        [SerializeField]
        private TextMeshProUGUI m_LocationPromptTitle = null;

        [SerializeField]
        private TextMeshProUGUI m_LocationPromptButtonLabel = null;

        [SerializeField]
        private RectTransform m_TimerHeader = null;

        [SerializeField]
        private TextMeshProUGUI m_TimerPromptLabel = null;

        [SerializeField]
        private TextMeshProUGUI m_TimerPromptButtonLabel = null;

        protected override void OnSetLayout()
        {
            base.OnSetLayout();

            m_LocationPromptTitle.alignment = TextAlignmentOptions.Left;
            m_LocationPromptButtonLabel.alignment = TextAlignmentOptions.Center;
            m_TimerPromptButtonLabel.alignment = TextAlignmentOptions.Center;
            m_TimerPromptLabel.alignment = TextAlignmentOptions.Left;

            bool showLocationPrompt = !LocationHandler.isEnabledByUser || !PermissionHelper.IsLocationPermissionAuthorized();

            float timerSize = JoyMonsterManager.engagementHandler.cooldownEnabled ? 40f : 0f;

            m_TimerHeader.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Top, 0f, 40f);

            m_TimerHeader.gameObject.SetActive(timerSize == 40f);// =  ? Vector3.one : Vector3.zero;

            if (showLocationPrompt)
            {
                if (!PermissionHelper.IsLocationPermissionAuthorized())
                {
                    m_LocationPrompt.localScale = Vector3.one;

                    m_LocationPromptTitle.text = Values.Text.PERMISSION_LOCATION_UNAUTHORIZED;
                    m_LocationPromptButtonLabel.text = Values.Text.SHARE_LOCATION;

                    m_LocationPrompt.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Top, timerSize, 56f);

                    m_ListView.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Top, (timerSize + 56f),
                        m_ContentTransform.rect.height - (timerSize + 56f));
                }
                else if (!LocationHandler.isEnabledByUser)
                {
                    m_LocationPrompt.localScale = Vector3.one;

                    m_LocationPromptTitle.text = Values.Text.LOCATION_DISABLED;
                    m_LocationPromptButtonLabel.text = Values.Text.OK;

                    m_LocationPrompt.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Top, timerSize, 56f);

                    m_ListView.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Top, (timerSize + 56f),
                        m_ContentTransform.rect.height - (timerSize + 56f));
                }
            }
            else
            {
                m_LocationPrompt.localScale = Vector3.zero;

                m_LocationPrompt.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Top, -timerSize - 56f, 56f);

                m_ListView.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Top, timerSize,
                    m_ContentTransform.rect.height - timerSize);
            }
        }
    }
}
﻿using JoyMonster.API.Model;
using JoyMonster.Core;
using MaterialUI_JoyMonster;
using RSG;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace JoyMonster.Ui.Screens
{
    public class OfferScreen : UiScreen
    {
        public override int screenId
        {
            get { return Values.ScreenIds.OFFER_DETAILS; }
        }

        private Promise<Texture2D> m_ImagePromise;

        [SerializeField] private DynamicTransformPosition m_DynamicPosition = null;

        [SerializeField] private RawImageLayoutBehaviour m_ImageLogoLayout = null;
        [SerializeField] private DynamicGraphic m_ImageLogoGraphic = null;
        [SerializeField] private TextMeshProUGUI m_LabelCompany = null;
        [SerializeField] private TextMeshProUGUI m_LabelCity = null;
        [SerializeField] private TextMeshProUGUI m_LabelTitle = null;
        [SerializeField] private TextMeshProUGUI m_LabelDescription = null;
        [SerializeField] private TextMeshProUGUI m_LabelLocation = null;
        [SerializeField] private TextMeshProUGUI m_LabelDistance = null;
        [SerializeField] private Graphic m_IconExpiry = null;
        [SerializeField] private TextMeshProUGUI m_LabelExpiry = null;
        [SerializeField] private TextMeshProUGUI m_LabelValue = null;
        [SerializeField] private TextMeshProUGUI m_LabelMinimum = null;

        protected override void OnBeforeShow()
        {
            SetData();
            m_DynamicCanvasGroup.SetState(false);
            base.OnBeforeShow();
            layoutBehaviour.RefreshLayout();
            m_DynamicPosition.basePosition = -layoutBehaviour.screenBasePositionOffset;
            m_DynamicPosition.offset = new Vector4(0f, 0f, 0f, layoutBehaviour.screenBaseSize.y * 1.25f);
            m_DynamicPosition.SetPosition(DynamicTransformPosition.Direction.Down);
        }

        protected override void OnShow()
        {
            m_DynamicPosition.TransitionPosition(DynamicTransformPosition.Direction.Middle, Values.Duration.SCREEN_TRANSITION, 0f, Tween.TweenType.EaseOutQuint);
            m_DynamicCanvasGroup.TransitionState(true, Values.Duration.MEDIUM, 0f);
            TweenManager.TimedCallback(Values.Duration.SCREEN_TRANSITION, () => m_ShowPromise.Resolve(true));
        }

        protected override void OnHide()
        {
            m_DynamicPosition.TransitionPosition(DynamicTransformPosition.Direction.Down, Values.Duration.SCREEN_TRANSITION, Values.Duration.SHORTER, Tween.TweenType.EaseInQuint);
            m_DynamicCanvasGroup.TransitionState(false, Values.Duration.SCREEN_TRANSITION, Values.Duration.SHORTER);
            TweenManager.TimedCallback(Values.Duration.SCREEN_TRANSITION + Values.Duration.SHORTER, () => m_HidePromise.Resolve(true));
        }

        protected override void SetAppBars()
        {
            JoyMonsterManager.uiHandler.appBar.SetSearchButton(false);
            JoyMonsterManager.uiHandler.appBar.SetHelpButton(true);
            JoyMonsterManager.uiHandler.appBar.SetLeftButton(true);
            JoyMonsterManager.uiHandler.appBar.SetTitle(Values.Text.APPBAR_TITLE_OFFER_DETAILS);
        }

        private void SetData()
        {
            Offer offer = JoyMonsterManager.walletHandler.currentOffer;

            if (offer == null) return;

            LoadAndSetImage(offer.brandLogoUrl);

            m_LabelCompany.text = offer.company;
            m_LabelCity.text = offer.city;
            m_LabelTitle.text = offer.title;
            m_LabelDescription.text = OfferFormatter.DisplayDescription(offer);
            m_LabelLocation.text = offer.address1;
            m_LabelDistance.text = OfferFormatter.DisplayDistance(offer);
            m_LabelExpiry.text = OfferFormatter.DisplayExpiry(offer);
            m_LabelValue.text = OfferFormatter.DisplayLimit(offer);
            m_LabelMinimum.text = OfferFormatter.DisplayMinimum(offer);

            if (m_LabelExpiry.text == "")
            {
                m_LabelExpiry.enabled = false;
                m_IconExpiry.enabled = false;
            }
            else
            {
                m_LabelExpiry.enabled = true;
                m_IconExpiry.enabled = true;
            }
        }

        public void LoadAndSetImage(string url)
        {
            m_ImageLogoLayout.SetTexture(null);
            m_ImageLogoGraphic.graphic.color = MaterialColor.dividerDark;

            m_ImagePromise = new Promise<Texture2D>((resolve, reject) =>
            {
                WebTextureHandler.GetTexture(url).Then(resolve);
            });

            if (m_ImagePromise.CurState == PromiseState.Resolved)
            {
                m_ImagePromise.Then(texture =>
                {
                    m_ImageLogoLayout.SetTexture(texture);
                    if (JoyMonsterManager.webTextureHandler.IsErrorTexture(texture))
                    {
                        m_ImageLogoGraphic.SetToCustom(MaterialColor.dividerDark);
                    }
                    else
                    {
                        m_ImageLogoGraphic.SetState(true);
                    }
                });
                return;
            }

            m_ImagePromise.Then(texture =>
            {
                m_ImageLogoLayout.SetTexture(texture);
                if (JoyMonsterManager.webTextureHandler.IsErrorTexture(texture))
                {
                    m_ImageLogoGraphic.TransitionToCustom(MaterialColor.dividerDark, Values.Duration.SHORT, 0f);
                }
                else
                {
                    m_ImageLogoGraphic.TransitionState(true, Values.Duration.SHORT, 0f);
                }
            });
        }

        public void OnRedeemButtonPressed()
        {
            JoyMonsterManager.uiHandler.redeemPopup.Show()
                .Then(success =>
                {
                    if (success)
                    {
                        JoyMonsterManager.walletHandler.MarkWalletDataDirty();

                        if (RedeemFormatter.IsUrlRedeem(JoyMonsterManager.redeemHandler.currentRedeemedOffer.walletRedeem))
                        {
                            Application.OpenURL(JoyMonsterManager.redeemHandler.currentRedeemedOffer.walletRedeem.url);
                            JoyMonsterManager.uiHandler.TransitionToScreen(Values.ScreenIds.WALLET);
                            JoyMonsterManager.uiHandler.ClearHistoryAndSetPreviousScreen(Values.ScreenIds.GAME);
                        }
                        else
                        {
                            JoyMonsterManager.uiHandler.TransitionToScreen(Values.ScreenIds.MERCHANT);
                            JoyMonsterManager.uiHandler.ClearHistoryAndSetPreviousScreen(Values.ScreenIds.WALLET);
                        }
                    }
                });
        }

        public void OnButtonMapPressed()
        {
            Offer offer = JoyMonsterManager.walletHandler.currentOffer;

            if (offer == null) return;

            string query = "";

            if (OfferFormatter.HasLocationCoords(offer))
            {
                query = offer.latitude + "," +
                        offer.longitude;
            }
            else if (OfferFormatter.HasAddress(offer))
            {
                query = JoyMonsterManager.walletHandler.currentOffer.GetFullAddress();
            }
            else
            {
                return;
            }

            Application.OpenURL("https://www.google.com/maps/search/?api=1&query=" + query);
        }

        public void OnButtonCallPressed()
        {
            Offer offer = JoyMonsterManager.walletHandler.currentOffer;

            if (offer == null) return;

            Application.OpenURL("tel:" + offer.phone);
        }

        public override bool OnBackButtonPressed()
        {
            bool redeemPopupShowing = JoyMonsterManager.uiHandler.redeemPopup.isShowing;

            if (redeemPopupShowing)
            {
                JoyMonsterManager.uiHandler.redeemPopup.OnCancelPressed();
            }

            return !redeemPopupShowing;
        }
    }
}
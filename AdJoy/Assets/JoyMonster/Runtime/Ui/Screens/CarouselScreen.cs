﻿using JoyMonster.API.Model;
using JoyMonster.Core;
using JoyMonster.Ui.Lists;
using MaterialUI_JoyMonster;
using System.Collections;
using System.Collections.Generic;
using JoyMonster.ExternalUtils;
using UnityEngine;
using UnityEngine.EventSystems;

namespace JoyMonster.Ui.Screens
{
    public class CarouselScreen : UiScreen
    {
        public override int screenId
        {
            get { return Values.ScreenIds.CAROUSEL; }
        }

        [SerializeField] private GameObject m_ListItemTemplate = null;
        [SerializeField] private ListView m_ListView = null;

        [SerializeField] private DynamicGraphic m_LabelTitleGraphic = null;
        [SerializeField] private DynamicGraphic m_LabelSubTitleGraphic = null;
        [SerializeField] private DynamicTransformPosition m_ChevronPosition = null;

        [SerializeField] private DynamicCanvasGroup m_PromptCanvasGroup = null;

        private bool m_Init;
        private bool m_DeferBackToPopup;
        private List<ListItemData> m_ViewedItems = new List<ListItemData>();

        protected override void OnBeforeShow()
        {
            m_DynamicCanvasGroup.SetState(false);
            m_LabelTitleGraphic.SetState(false);
            m_LabelSubTitleGraphic.SetState(false);

            //base.OnBeforeShow();

            canvas.enabled = true;

            //layoutBehaviour.setValues = true;
            layoutBehaviour.RefreshLayout();
            //layoutBehaviour.setValues = false;

            PrepareDynamicPosition(m_ChevronPosition);
            m_PromptCanvasGroup.SetState(false);

            m_ViewedItems.Clear();
        }

        protected override void OnShow()
        {
            TweenManager.TimedCallback(Values.Duration.LONG, () =>
            {
                JoyMonsterManager.uiHandler.bottomBar.TransitionVisible(true);
                JoyMonsterManager.uiHandler.background.TransitionToCustom(MaterialColor.grey200, Values.Duration.MEDIUM);
                m_DynamicCanvasGroup.SetState(true);
                m_ShowPromise.Resolve(true);
            });
        }

        protected override void OnAfterShow()
        {
            m_ChevronPosition.TransitionPosition(DynamicTransformPosition.Direction.Middle, Values.Duration.MEDIUM, 0f, Tween.TweenType.EaseOutQuint);
            m_LabelTitleGraphic.TransitionState(true, Values.Duration.SHORT, Values.Duration.SHORT);
            m_LabelSubTitleGraphic.TransitionState(true, Values.Duration.SHORT, Values.Duration.SHORT + Values.Duration.PER_ELEMENT_DELAY);
            m_PromptCanvasGroup.TransitionState(true, Values.Duration.SHORT, Values.Duration.SHORT + Values.Duration.PER_ELEMENT_DELAY * 2f);

            EngagementResult[] engagementResults = JoyMonsterManager.engagementHandler.GetCollectedEngagementArray();
            SetListData(engagementResults);
        }

        protected override void OnHide()
        {
            m_ListView.HideList();
            m_DynamicCanvasGroup.TransitionState(false, Values.Duration.MEDIUM, Values.Duration.SHORT);
            TweenManager.TimedCallback(Values.Duration.MEDIUM + Values.Duration.SHORTER, () => m_HidePromise.Resolve(true));
        }

        private void SetListData(EngagementResult[] engagementList)
        {
            ListData listData = new ListData();

            foreach (EngagementResult engagement in engagementList)
            {
                listData.listItems.Add(new ListItemDataCarousel
                {
                    engagement = engagement
                });
            }

            if (!m_Init)
            {
                FirstTimeInit();
            }
            m_ListView.SetListData(listData);


            TweenManager.TimedCallback(Values.Duration.SHORT, () =>
            {
                m_ListView.ShowList();
            });

            JoyMonsterManager.engagementHandler.ClearEngagements();
        }

        private void FirstTimeInit()
        {
            m_Init = true;

            m_ListView.InitializeList(new ListLayoutControllerStandard(), new ListAnimationControllerCarousel(), new ListInteractionControllerStandard());

            m_ListView.LayoutControllerStandard().layoutDirection = ListLayoutControllerStandard.LayoutDirection.Horizontal;
            m_ListView.LayoutControllerStandard().forceSingleLine = true;
            m_ListView.LayoutControllerStandard().listPadding = Vector2.zero;
            m_ListView.interactionController.onItemInteraction += OnItemInteraction;
            m_ListView.layoutController.itemPool.SetTemplatePrefab(typeof(ListItemDataCarousel), m_ListItemTemplate);
            m_ListView.GetComponent<ListCarouselExtension>().onItemCentered.AddListener(OnItemCentered);
        }

        public void OnLocationPromptButtonClick()
        {
            if (!PermissionHelper.IsLocationPermissionAuthorized())
            {
                PermissionHelper.RequestLocationPermission();
            }

            StartCoroutine(WaitAndRefreshLocationBanner());
        }

        // After the user clicked on the location banner button, we try to refresh the banner during 60s
        // We stop if we don't need to show the location prompt
        private IEnumerator WaitAndRefreshLocationBanner()
        {
            yield return new WaitForSeconds(3f);

            CarouselScreenLayout carouselScreenLayout = ((CarouselScreenLayout)layoutBehaviour);
            carouselScreenLayout.RefreshLayout();
        }

        private void OnItemCentered(ListItemData data)
        {
            if (data == null || m_ViewedItems.Contains(data)) return;

            m_ViewedItems.Add(data);
            JoyMonsterManager.engagementHandler.ViewEngagement(((ListItemDataCarousel)data).engagement);
        }

        private void OnItemInteraction(int interactionType, BaseEventData eventData, ListItem listItem, int subItemId)
        {
            if (interactionType == ListInteraction.pointerClick && subItemId == 1)
            {
                OnItemClick(listItem);
            }
        }

        private void OnItemClick(ListItem listItem)
        {
            m_DeferBackToPopup = true;

            JoyMonsterManager.uiHandler.addPopup.Show(((ListItemDataCarousel)listItem.currentData).engagement.offerUuid)
                .Then(confirmed =>
                {
                    m_DeferBackToPopup = false;

                    if (!confirmed) return;

                    JoyMonsterManager.uiHandler.TransitionToScreen(Values.ScreenIds.ADDED);
                });
        }

        protected override void SetAppBars()
        {
            JoyMonsterManager.uiHandler.appBar.SetLeftButton(false);
            JoyMonsterManager.uiHandler.appBar.SetTitle(Values.Text.APPBAR_TITLE_WALLET);
            JoyMonsterManager.uiHandler.appBar.SetSearchButton(true);
            JoyMonsterManager.uiHandler.appBar.SetHelpButton(true);
        }

        private void PrepareDynamicPosition(DynamicTransformPosition dynamicTransformPosition)
        {
            dynamicTransformPosition.MarkPositionAsBase();
            Vector4 offset = dynamicTransformPosition.offset;
            offset.x = layoutBehaviour.screenBaseSize.x;
            offset.y = layoutBehaviour.screenBaseSize.y;
            offset.z = layoutBehaviour.screenBaseSize.x;
            offset.w = layoutBehaviour.screenBaseSize.y;
            dynamicTransformPosition.offset = offset;
            dynamicTransformPosition.SetPosition(DynamicTransformPosition.Direction.Up);
        }

        public override bool OnBackButtonPressed()
        {
            if (!m_DeferBackToPopup)
            {
                if (JoyMonsterManager.uiHandler.dismissPopup.isShowing)
                {
                    return true;
                }

                JoyMonsterManager.uiHandler.dismissPopup.Show();
                return false;
            }

            JoyMonsterManager.uiHandler.addPopup.OnCancelPressed();
            return false;
        }

        public override bool OnBackToGameButtonPressed()
        {
            if (JoyMonsterManager.uiHandler.dismissPopup.isShowing)
            {
                return true;
            }

            JoyMonsterManager.uiHandler.dismissPopup.Show();
            return false;
        }
    }
}

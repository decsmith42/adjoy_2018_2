﻿using JoyMonster.Core;
using MaterialUI_JoyMonster;
using UnityEngine;

namespace JoyMonster.Ui.Screens
{
    public class CongratulationsScreen : UiScreen
    {
        public override int screenId
        {
            get { return Values.ScreenIds.CONGRATULATIONS; }
        }

        [SerializeField] private DynamicTransformPosition m_ChevronPosition = null;
        [SerializeField] private DynamicTransformPosition m_LabelTitlePosition = null;
        [SerializeField] private DynamicGraphic m_LabelSubTitleGraphic = null;
        [SerializeField] private DynamicTransformPosition m_BoxPosition = null;

        private int m_TimerId;

        protected override void OnBeforeShow()
        {
            m_DynamicCanvasGroup.SetState(false);

            base.OnBeforeShow();
            layoutBehaviour.RefreshLayout();

            PrepareDynamicPosition(m_ChevronPosition);
            PrepareDynamicPosition(m_LabelTitlePosition);
            PrepareDynamicPosition(m_BoxPosition);

            m_BoxPosition.rectTransform.localScale = Vector3.zero;
            m_Content.SetAnchoredPositionY(-layoutBehaviour.screenBasePositionOffset.y);

            m_LabelSubTitleGraphic.SetState(false);
        }

        protected override void OnShow()
        {
            m_DynamicCanvasGroup.TransitionState(true, Values.Duration.MEDIUM, Values.Duration.SHORTER);

            TweenManager.TimedCallback(Values.Duration.MEDIUM, () => m_ShowPromise.Resolve(true));
        }

        protected override void OnAfterShow()
        {
            TweenManager.EndTween(m_TimerId);

            m_ChevronPosition.TransitionPosition(DynamicTransformPosition.Direction.Middle, Values.Duration.LONG,
                Values.Duration.MEDIUM, Tween.TweenType.Overshoot);

            m_BoxPosition.SetPosition(DynamicTransformPosition.Direction.Middle);

            TweenManager.TweenVector3(value => m_BoxPosition.rectTransform.localScale = value, Vector3.zero, Vector3.one,
                Values.Duration.SHORT, Values.Duration.LONG + Values.Duration.SHORT,
                tweenType: Tween.TweenType.EaseOutSept);

            m_LabelTitlePosition.TransitionPosition(DynamicTransformPosition.Direction.Middle, Values.Duration.MEDIUM,
                Values.Duration.MEDIUM, Tween.TweenType.EaseOutQuint);
            m_LabelSubTitleGraphic.TransitionState(true, Values.Duration.MEDIUM, Values.Duration.LONG);

            m_TimerId = TweenManager.TimedCallback(Values.Duration.MEDIUM + Values.Duration.LONG + Values.Duration.CONGRATULATIONS_WAIT,
                () => JoyMonsterManager.uiHandler.TransitionToScreen(Values.ScreenIds.CAROUSEL, false, false));
        }

        protected override void OnHide()
        {
            TweenManager.EndTween(m_TimerId);

            m_ChevronPosition.TransitionPosition(
                UiScaleController.orientation == UiScaleController.Orientation.Portrait ?
                    DynamicTransformPosition.Direction.Down :
                    DynamicTransformPosition.Direction.Right,
                Values.Duration.MEDIUM, Values.Duration.PER_ELEMENT_DELAY * 2f, Tween.TweenType.EaseInCubed);

            TweenManager.TweenVector3(value => m_BoxPosition.rectTransform.localScale = value,
                Vector3.one, Vector3.zero,
                Values.Duration.SHORT, 0f, tweenType: Tween.TweenType.EaseInCubed);

            m_LabelSubTitleGraphic.DelayedTransitionState(false, Values.Duration.SHORT, Values.Duration.PER_ELEMENT_DELAY);

            m_TimerId = TweenManager.TimedCallback(Values.Duration.LONG, () => m_HidePromise.Resolve(true));
        }

        private void PrepareDynamicPosition(DynamicTransformPosition dynamicTransformPosition)
        {
            dynamicTransformPosition.MarkPositionAsBase();
            Vector4 offset = dynamicTransformPosition.offset;
            offset.x = layoutBehaviour.screenBaseSize.x * 2f;
            offset.y = layoutBehaviour.screenBaseSize.y;
            offset.z = layoutBehaviour.screenBaseSize.x;
            offset.w = layoutBehaviour.screenBaseSize.y * 2f;
            dynamicTransformPosition.offset = offset;
            dynamicTransformPosition.SetPosition(UiScaleController.orientation == UiScaleController.Orientation.Portrait
                ? DynamicTransformPosition.Direction.Up
                : DynamicTransformPosition.Direction.Right);
        }

        public override bool OnBackButtonPressed()
        {
            return false;
        }

        public override bool OnBackToGameButtonPressed()
        {
            return false;
        }
    }
}
﻿using JoyMonster.Core;
using JoyMonster.ExternalUtils;
using MaterialUI_JoyMonster;
using TMPro;
using UnityEngine;

namespace JoyMonster.Ui.Screens
{
    public class CarouselScreenLayout : ScreenLayoutBehaviour
    {
        [SerializeField]
        private RectTransform m_Chevron = null;

        [SerializeField]
        private RectTransform m_ChevronTriangle = null;

        [SerializeField]
        private TextMeshProUGUI m_Title = null;

        [SerializeField]
        private TextMeshProUGUI m_Subtitle = null;

        [SerializeField]
        private RectTransform m_ListView = null;

        [SerializeField]
        private RectTransform m_Indicators = null;

        [SerializeField]
        private RectTransform m_LocationPrompt = null;

        [SerializeField]
        private RectTransform m_LocationPromptButton = null;

        [SerializeField]
        private TextMeshProUGUI m_LocationPromptTitle = null;

        [SerializeField]
        private TextMeshProUGUI m_LocationPromptButtonLabel = null;

        protected override void OnSetLayout()
        {
            base.OnSetLayout();

            m_Title.alignment = TextAlignmentOptions.Center;
            m_Subtitle.alignment = TextAlignmentOptions.Center;

            bool showLocationPrompt = !LocationHandler.isEnabledByUser || !PermissionHelper.IsLocationPermissionAuthorized();

            bool isLandscape = rectTransform.rect.width > rectTransform.rect.height;

            LayoutValue values = UiLayoutValueHandler.common.currentValue;
            float padding = values.padding *
                            (UiScaleController.orientation == UiScaleController.Orientation.Portrait ? 1f : 0.75f);
            float spacing = values.spacing *
                            (UiScaleController.orientation == UiScaleController.Orientation.Portrait ? 1f : 0.75f);

            float totalHeight = m_ContentTransform.rect.height;

            if (isLandscape && showLocationPrompt)
            {
                m_Title.fontSize = values.textHeaderFontSize * 1.5f * 0.75f;
                m_Subtitle.fontSize = values.textSubHeaderFontSize * 1.25f * 0.75f;
            }
            else
            {
                m_Title.fontSize = values.textHeaderFontSize * 1.5f;
                m_Subtitle.fontSize = values.textSubHeaderFontSize * 1.25f;
            }

            float topPadding = padding;
            float titleHeight = m_Title.preferredHeight;
            float labelSpacing = spacing;
            float subtitleHeight = m_Subtitle.preferredHeight;
            float listTopPadding = padding;
            float listViewHeight = 200f;
            float listBottomPadding = padding;
            float indicatorHeight = 28f;
            float bottomPadding = padding;

            float locationPromptHeight = 0f;
            float locationPromptSpace = 0f;

            if (showLocationPrompt)
            {
                float mod = 1f;

                if (isLandscape)
                {
                    mod = 0.5f;

                    topPadding *= 0.5f;
                    //labelSpacing *= 0.5f;
                    //listTopPadding *= 0.5f;
                }

                m_LocationPromptButtonLabel.fontSize = values.textButtonFontSize * 0.75f * mod;
                m_LocationPromptTitle.fontSize = values.textSubHeaderFontSize * 0.85f * mod;

                m_LocationPromptButton.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Left, values.padding * mod,
                    m_LocationPromptButtonLabel.preferredWidth + values.padding * 1.5f * mod);
                m_LocationPromptButton.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Bottom, values.padding * mod,
                    m_LocationPromptButtonLabel.preferredHeight + values.padding * mod);

                locationPromptHeight += m_LocationPromptButton.rect.height + values.padding * mod + values.spacing * mod;

                m_LocationPrompt.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal,
                    Mathf.Min(rectTransform.rect.width - values.padding * 2f * mod, m_LocationPromptTitle.preferredWidth + values.padding * 2f * mod));

                m_LocationPromptTitle.rectTransform.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Left,
                    values.padding * mod, m_LocationPrompt.rect.width - values.padding * 2f * mod);
                m_LocationPromptTitle.rectTransform.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Bottom,
                    locationPromptHeight, m_LocationPromptTitle.preferredHeight);

                locationPromptHeight += m_LocationPromptTitle.rectTransform.rect.height + values.padding * mod;

                locationPromptSpace += values.spacing;

                if (!PermissionHelper.IsLocationPermissionAuthorized())
                {
                    m_LocationPromptTitle.text = Values.Text.PERMISSION_LOCATION_UNAUTHORIZED;
                    m_LocationPromptButtonLabel.text = Values.Text.SHARE_LOCATION;
                }
                else if (!LocationHandler.isEnabledByUser)
                {
                    m_LocationPromptTitle.text = Values.Text.LOCATION_DISABLED;
                    m_LocationPromptButtonLabel.text = Values.Text.OK;
                }
            }

            float height = totalHeight - topPadding -
                           titleHeight - labelSpacing - subtitleHeight - locationPromptHeight - locationPromptSpace -
                           listTopPadding - listViewHeight - listBottomPadding -
                           indicatorHeight - bottomPadding;

            height *= 0.66f;
            listViewHeight += height;

            height *= 0.5f;
            height *= 0.25f;
            topPadding += height;
            listTopPadding += height;
            listBottomPadding += height;
            bottomPadding += height;

            height = bottomPadding;
            //m_Indicators.SetAnchoredPositionY(height);
            m_Indicators.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Bottom, height, indicatorHeight);
            height += indicatorHeight;

            height += listBottomPadding;
            m_ListView.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Bottom, height, listViewHeight);
            height += listViewHeight;

            m_Chevron.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Bottom, height, totalHeight - height);
            m_ChevronTriangle.SetSizeDeltaY(m_Chevron.rect.width / 8f);

            if (showLocationPrompt)
            {
                height += isLandscape ? listTopPadding * 0.5f : listTopPadding;
                m_LocationPrompt.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Bottom, height, locationPromptHeight);
                height += locationPromptHeight + locationPromptSpace;

                m_LocationPrompt.gameObject.SetActive(true);
            }
            else
            {
                height += listTopPadding;
                m_LocationPrompt.gameObject.SetActive(false);
            }

            m_Subtitle.rectTransform.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Bottom, height, subtitleHeight);
            height += subtitleHeight;

            height += labelSpacing;
            m_Title.rectTransform.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Bottom, height, titleHeight);
        }
    }
}
﻿using MaterialUI_JoyMonster;

namespace JoyMonster.Ui
{
    public class DynamicCanvasGroupEmphasized : DynamicCanvasGroup
    {
        private float m_AlphaUpper;
        private float m_AlphaLower;
        private float m_CycleDuration;

        public void EmphasizeContinuous(float alphaBase = 0.5f, float alphaRange = 0.25f, float cycleDuration = Values.Duration.SHORT, float delay = 0f)
        {
            TweenManager.EndTween(m_TweenId);

            m_AlphaUpper = alphaBase + alphaRange;
            m_AlphaLower = alphaBase - alphaRange;
            m_CycleDuration = cycleDuration;

            m_TweenId = TweenManager.TweenFloat(value => m_CanvasGroup.alpha = value, m_CanvasGroup.alpha, m_AlphaUpper,
                cycleDuration / 2f, delay, () => EmphasisCycle(false), tweenType: Tween.TweenType.SoftEaseOutQuint);
        }

        private void EmphasisCycle(bool cycleToUpperAlpha)
        {
            m_TweenId = TweenManager.TweenFloat(value => m_CanvasGroup.alpha = value, m_CanvasGroup.alpha, cycleToUpperAlpha ? m_AlphaUpper : m_AlphaLower,
                m_CycleDuration / 2f, 0f, () => EmphasisCycle(!cycleToUpperAlpha), tweenType: Tween.TweenType.EaseInOutCubed);
        }
    }
}
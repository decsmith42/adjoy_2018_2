﻿using UnityEngine;

namespace JoyMonster.Ui
{
    public abstract class ScreenLayoutBehaviour : UiLayoutBehaviour
    {
        [SerializeField] protected RectTransform m_ContentTransform;

        [SerializeField] protected bool m_LeaveSpaceForAppBar;
        [SerializeField] protected bool m_LeaveSpaceForBottomBar;

        public Vector2 screenBasePositionOffset
        {
            get
            {
                return new Vector2(0f,
                    m_LeaveSpaceForAppBar ? UiLayoutValueHandler.appBarHeight.currentValue.appBarHeight : 0f);
            }
        }

        public Vector2 screenBaseSize
        {
            get
            {
                return new Vector2(rectTransform.rect.width,
                    rectTransform.rect.height -
                    (m_LeaveSpaceForAppBar ? UiLayoutValueHandler.appBarHeight.currentValue.appBarHeight : 0f) -
                    (m_LeaveSpaceForBottomBar ? Values.Layout.BOTTOM_BAR_HEIGHT : 0f));
            }
        }

        protected override void OnSetLayout()
        {
            m_ContentTransform.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Top, screenBasePositionOffset.y,
                screenBaseSize.y);
        }
    }
}
﻿using UnityEngine;
using UnityEngine.UI;

namespace JoyMonster.Ui
{
    public class RawImageLayoutBehaviour : GraphicLayoutBehaviour<RawImage>
    {
        protected override Texture texture
        {
            get { return m_Graphic.texture; }
        }

        public void SetTexture(Texture2D texture)
        {
            if (m_Graphic == null) return;

            m_Graphic.texture = texture;

            SetLayoutHorizontal();
        }
    }
}
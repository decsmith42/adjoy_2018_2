﻿using MaterialUI_JoyMonster;
using UnityEngine;
using UnityEngine.UI;

namespace JoyMonster.Ui
{
    public class DynamicGraphic : MonoBehaviour
    {
        protected Graphic m_Graphic;

        public Graphic graphic
        {
            get
            {
                if (m_Graphic == null)
                {
                    m_Graphic = GetComponent<Graphic>();
                }

                return m_Graphic;
            }
        }

        [SerializeField] private Color m_BaseValue;
        public Color baseValue
        {
            get { return m_BaseValue; }
            set { m_BaseValue = value; }
        }

        protected int m_TweenId;

        public void SetState(bool visible)
        {
            TweenManager.EndTween(m_TweenId);
            graphic.color = m_BaseValue.WithAlpha(visible ? m_BaseValue.a : 0f);
            graphic.enabled = visible;
        }

        public void SetToCustom(Color color)
        {
            TweenManager.EndTween(m_TweenId);
            graphic.color = color;
        }

        public void TransitionState(bool visible, float duration, float delay)
        {
            TweenManager.EndTween(m_TweenId);

            if (visible)
            {
                graphic.enabled = true;
            }

            Color target = m_BaseValue.WithAlpha(visible ? m_BaseValue.a : 0f);

            m_TweenId = TweenManager.TweenColor(
                value => graphic.color = value, graphic.color, target, duration, delay,
                () =>
                {
                    if (!visible)
                    {
                        graphic.enabled = false;
                    }
                }, tweenType: Tween.TweenType.EaseInOutQuint);
        }

        public void TransitionState(bool from, bool to, float duration, float delay)
        {
            SetState(from);
            TransitionState(to, duration, delay);
        }

        public void DelayedTransitionState(bool visible, float duration, float delay)
        {
            TweenManager.TimedCallback(delay, () =>
            {
                TweenManager.EndTween(m_TweenId);

                if (visible)
                {
                    graphic.enabled = true;
                }

                Color target = m_BaseValue.WithAlpha(visible ? m_BaseValue.a : 0f);

                m_TweenId = TweenManager.TweenColor(
                    value => graphic.color = value, graphic.color, target, duration, 0f,
                    () =>
                    {
                        if (!visible)
                        {
                            graphic.enabled = false;
                        }
                    }, tweenType: Tween.TweenType.EaseInOutQuint);
            });
        }

        public void TransitionToNewState(Color color, float duration, float delay)
        {
            m_BaseValue = color;
            TransitionState(true, duration, delay);
        }

        public void DelayedTransitionToNewState(Color color, float duration, float delay)
        {
            TweenManager.TimedCallback(delay, () =>
            {
                m_BaseValue = color;
                TransitionState(true, duration, 0f);
            });
        }

        public void TransitionToCustom(Color color, float duration, float delay = 0f)
        {
            TweenManager.EndTween(m_TweenId);

            m_TweenId = TweenManager.TweenColor(value => graphic.color = value,
                graphic.color, color, duration, delay, tweenType: Tween.TweenType.EaseInOutQuint);
        }
    }
}
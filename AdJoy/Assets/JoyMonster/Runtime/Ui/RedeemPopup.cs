﻿using JoyMonster.API;
using JoyMonster.API.Model;
using JoyMonster.Core;
using MaterialUI_JoyMonster;
using RSG;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;

namespace JoyMonster.Ui
{
    public class RedeemPopup : UIBehaviour
    {
        [SerializeField]
        private RectTransform m_Panel = null;

        [SerializeField]
        private DynamicCanvasGroup m_Background = null;

        [SerializeField]
        private RedeemPopupLayoutBehaviour m_LayoutBehaviour = null;

        [SerializeField]
        private Canvas m_Canvas = null;

        [SerializeField]
        private TextMeshProUGUI m_LabelTitle = null;

        [SerializeField]
        private DynamicGraphic m_LabelTitleGraphic = null;

        [SerializeField]
        private TextMeshProUGUI m_LabelDescription = null;

        [SerializeField]
        private DynamicGraphic m_LabelDescriptionGraphic = null;

        [SerializeField]
        private DynamicCanvasGroup m_SwipeButtonCanvasGroup = null;

        [SerializeField]
        private DynamicCanvasGroup m_ProgressBarCanvasGroup = null;

        [SerializeField]
        private ProgressIndicator m_ProgressBar = null;

        public bool isShowing { get; private set; }

        private const float m_CircleExtraHeight = 96f;

        private int m_TweenId;

        private bool m_GeoOverrideNextRedeem;

        private Promise<bool> m_RedeemPromise;

        public Promise<bool> Show()
        {
            TweenManager.EndTween(m_TweenId);

            Offer offer = JoyMonsterManager.walletHandler.currentOffer;

            m_LabelTitle.text = Values.Text.REDEEM_TITLE_NORMAL;
            m_LabelDescription.text = (offer.isPartner
                    ? Values.Text.REDEEM_DESCRIPTION_PARTNER
                    : Values.Text.REDEEM_DESCRIPTION_NORMAL)
                .Replace("[company]", offer.company);

            m_SwipeButtonCanvasGroup.SetState(true);
            m_ProgressBarCanvasGroup.SetState(false);

            m_Canvas.enabled = true;
            m_Panel.SetAnchoredPositionY(-m_Panel.rect.height - m_CircleExtraHeight);

            m_LayoutBehaviour.RefreshLayout();

            m_TweenId = TweenManager.TweenFloat(value => m_Panel.SetAnchoredPositionY(value),
                m_Panel.anchoredPosition.y, m_LayoutBehaviour.panelBasePositionOffset.y,
                Values.Duration.MEDIUM, Values.Duration.SHORTER,
                tweenType: Tween.TweenType.EaseOutQuint);

            m_Background.TransitionState(true, Values.Duration.SHORT, Values.Duration.SHORTER);

            m_GeoOverrideNextRedeem = offer.isPartner;

            isShowing = true;
            m_RedeemPromise = m_RedeemPromise.DiscardAndReplace();
            return m_RedeemPromise;
        }

        private void Hide(bool isTransition = false)
        {
            TweenManager.EndTween(m_TweenId);

            m_TweenId = TweenManager.TweenFloat(value => m_Panel.SetAnchoredPositionY(value),
                m_Panel.anchoredPosition.y, -m_Panel.rect.height - m_CircleExtraHeight,
                Values.Duration.SHORT, Values.Duration.SHORTER,
                () => { m_Canvas.enabled = false; },
                tweenType: Tween.TweenType.EaseInQuint);

            m_Background.TransitionState(false, Values.Duration.SHORT, Values.Duration.SHORTER);

            isShowing = false;
            m_RedeemPromise.ResolveIfPending(false);
        }

        public void OnCancelPressed()
        {
            if (JoyMonsterManager.redeemHandler.redeemInProgress) return;
            Hide();
        }

        public void OnRedeemButtonSwiped()
        {
            if (JoyMonsterManager.redeemHandler.redeemInProgress) return;

            Redeem();
        }

        private void Redeem()
        {
            //m_BarTransitionGraphic.TransitionState(true, Values.Duration.SHORTER, 0f);
            m_SwipeButtonCanvasGroup.TransitionState(false, Values.Duration.SHORTER, 0f);
            m_ProgressBarCanvasGroup.TransitionState(true, Values.Duration.SHORTER, Values.Duration.SHORTER);
            m_ProgressBar.Show();

            m_LabelDescriptionGraphic.TransitionState(false, Values.Duration.SHORTER, 0f);
            m_LabelDescriptionGraphic.DelayedTransitionState(true, Values.Duration.SHORTER, Values.Duration.SHORT);

            m_LabelTitleGraphic.TransitionState(false, Values.Duration.SHORTER, 0f);
            m_LabelTitleGraphic.DelayedTransitionState(true, Values.Duration.SHORTER, Values.Duration.SHORT);

            TweenManager.TimedCallback(Values.Duration.SHORTER, () =>
            {
                m_LabelTitle.text = Values.Text.REDEEM_TITLE_REDEEMING;
                m_LabelDescription.text = null;
                m_LayoutBehaviour.AnimatedAdjustLayout(Values.Duration.SHORT);
            });

            JoyMonsterManager.redeemHandler.RedeemOffer(JoyMonsterManager.walletHandler.currentOffer,
                m_GeoOverrideNextRedeem);

            JoyMonsterManager.redeemHandler.currentRedeemResponse
                .Then(redemption =>
                {
                    JoyMonsterAPI.DebugLog("Redemption type: " + redemption.redemptionType);

                    switch (redemption.redemptionType)
                    {
                        case RedeemHandler.Redemption.RedemptionType.BadGeo:
                            OnGeoError();
                            break;
                        case RedeemHandler.Redemption.RedemptionType.RedeemError:
                            OnRedeemError();
                            break;
                        case RedeemHandler.Redemption.RedemptionType.RedeemSuccess:
                            OnRedeemSuccess(redemption);
                            break;
                    }
                });
        }

        private void OnRedeemSuccess(RedeemHandler.Redemption redemption)
        {
            m_LabelDescriptionGraphic.TransitionState(false, Values.Duration.SHORTER, 0f);
            m_LabelDescriptionGraphic.DelayedTransitionState(true, Values.Duration.SHORTER, Values.Duration.SHORTER);

            m_LabelTitleGraphic.TransitionState(false, Values.Duration.SHORTER, 0f);
            m_LabelTitleGraphic.DelayedTransitionState(true, Values.Duration.SHORTER, Values.Duration.SHORTER);

            m_ProgressBarCanvasGroup.TransitionState(false, Values.Duration.SHORTER, 0f);
            m_ProgressBar.Hide();

            TweenManager.TimedCallback(Values.Duration.SHORTER, () =>
            {
                m_LabelTitle.text = Values.Text.REDEEM_TITLE_SUCCESS;
                m_LabelDescription.text = null;
            });

            TweenManager.TimedCallback(Values.Duration.SHORT, () =>
            {
                m_RedeemPromise.ResolveIfPending(true);
                Hide(true);
            });
        }

        private void OnGeoError()
        {
            m_ProgressBarCanvasGroup.TransitionState(false, Values.Duration.SHORTER, 0f);
            m_SwipeButtonCanvasGroup.TransitionState(true, Values.Duration.SHORTER, Values.Duration.SHORTER);
            m_ProgressBar.Hide();

            m_LabelDescriptionGraphic.TransitionState(false, Values.Duration.SHORTER, 0f);
            m_LabelDescriptionGraphic.DelayedTransitionState(true, Values.Duration.SHORTER, Values.Duration.SHORT);

            m_LabelTitleGraphic.TransitionState(false, Values.Duration.SHORTER, 0f);
            m_LabelTitleGraphic.DelayedTransitionState(true, Values.Duration.SHORTER, Values.Duration.SHORT);

            TweenManager.TimedCallback(Values.Duration.SHORTER, () =>
            {
                Offer offer = JoyMonsterManager.walletHandler.currentOffer;

                string company = offer == null ? "" : offer.company;

                m_LabelTitle.text = null;
                m_LabelDescription.text = Values.Text.REDEEM_DESCRIPTION_GEO_ERROR
                    .Replace(Values.Text.REDEEM_DESCRIPTION_NORMAL_REPLACE,
                        company);
                m_LayoutBehaviour.AnimatedAdjustLayout(Values.Duration.SHORT);
            });

            m_GeoOverrideNextRedeem = true;
        }

        private void OnRedeemError()
        {
            m_ProgressBarCanvasGroup.TransitionState(false, Values.Duration.SHORTER, 0f);
            m_SwipeButtonCanvasGroup.TransitionState(true, Values.Duration.SHORTER, Values.Duration.SHORTER);
            m_ProgressBar.Hide();

            m_LabelDescriptionGraphic.TransitionState(false, Values.Duration.SHORTER, 0f);
            m_LabelDescriptionGraphic.DelayedTransitionState(true, Values.Duration.SHORTER, Values.Duration.SHORT);

            m_LabelTitleGraphic.TransitionState(false, Values.Duration.SHORTER, 0f);
            m_LabelTitleGraphic.DelayedTransitionState(true, Values.Duration.SHORTER, Values.Duration.SHORT);

            TweenManager.TimedCallback(Values.Duration.SHORTER, () =>
            {
                m_LabelTitle.text = null;
                m_LabelDescription.text = Values.Text.REDEEM_DESCRIPTION_ERROR;
                m_LayoutBehaviour.AnimatedAdjustLayout(Values.Duration.SHORT);
            });
        }
    }
}
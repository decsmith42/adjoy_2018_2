﻿using MaterialUI_JoyMonster;
using System;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

namespace JoyMonster.Ui
{
    public class TextTester : MonoBehaviour
    {
        private static TextTester m_Instance;
        private static TextTester instance
        {
            get
            {
                if (m_Instance == null)
                {
                    m_Instance = FindObjectOfType<TextTester>();
                }

                return m_Instance;
            }
        }

        public enum Font
        {
            Normal,
            Semibold,
            Bold
        }

        private TextMeshProUGUI m_Label;
        public static TextMeshProUGUI label
        {
            get
            {
                if (instance.m_Label == null)
                {
                    instance.m_Label = instance.GetComponent<TextMeshProUGUI>();
                }

                return instance.m_Label;
            }
        }

        private struct LineHeight
        {
            public Font font;
            public float fontSize;
            public int lineCount;
        }

        [SerializeField] private TMP_FontAsset m_Normal = null;
        [SerializeField] private TMP_FontAsset m_Semibold = null;
        [SerializeField] private TMP_FontAsset m_Bold = null;

        private const string LINE_HEIGHT_TEST_1 = "lxy";
        private const string LINE_HEIGHT_TEST_2 = "lxy\nlxy";
        private const string LINE_HEIGHT_TEST_3 = "lxy\nlxy\nlxy";
        private const string LINE_HEIGHT_TEST_4 = "lxy\nlxy\nlxy\nlxy";
        private const string LINE_HEIGHT_TEST_5 = "lxy\nlxy\nlxy\nlxy\nlxy";

        private static Dictionary<LineHeight, float> m_CachedLineHeights = new Dictionary<LineHeight, float>();
        private static LineHeight m_TempLineHeight;

        public static float GetPreferredWidth(Font font, float fontSize, string text)
        {
            label.font = instance.GetFont(font);
            label.fontSize = fontSize;
            label.text = text;
            label.ForceMeshUpdate(true);
            return label.preferredWidth;
        }

        public static float GetPreferredHeight(Font font, float fontSize, float width, string text)
        {
            label.rectTransform.SetSizeDeltaX(width == 0f ? 10000f : width);
            label.font = instance.GetFont(font);
            label.fontSize = fontSize;
            label.text = text;
            label.ForceMeshUpdate(true);
            return label.preferredHeight;
        }

        public static float GetPreferredHeight(Font font, float fontSize, int lineCount)
        {
            m_TempLineHeight.font = font;
            m_TempLineHeight.fontSize = fontSize;
            m_TempLineHeight.lineCount = lineCount;

            if (m_CachedLineHeights.ContainsKey(m_TempLineHeight)) return m_CachedLineHeights[m_TempLineHeight];

            switch (lineCount)
            {
                case 1:
                    m_CachedLineHeights.Add(m_TempLineHeight, GetPreferredHeight(font, fontSize, 0f, LINE_HEIGHT_TEST_1));
                    break;
                case 2:
                    m_CachedLineHeights.Add(m_TempLineHeight, GetPreferredHeight(font, fontSize, 0f, LINE_HEIGHT_TEST_2));
                    break;
                case 3:
                    m_CachedLineHeights.Add(m_TempLineHeight, GetPreferredHeight(font, fontSize, 0f, LINE_HEIGHT_TEST_3));
                    break;
                case 4:
                    m_CachedLineHeights.Add(m_TempLineHeight, GetPreferredHeight(font, fontSize, 0f, LINE_HEIGHT_TEST_4));
                    break;
                case 5:
                    m_CachedLineHeights.Add(m_TempLineHeight, GetPreferredHeight(font, fontSize, 0f, LINE_HEIGHT_TEST_5));
                    break;
                default:
                    m_CachedLineHeights.Add(m_TempLineHeight, GetPreferredHeight(font, fontSize, 0f, LINE_HEIGHT_TEST_1));
                    break;
            }

            return m_CachedLineHeights[m_TempLineHeight];
        }

        private TMP_FontAsset GetFont(Font font)
        {
            switch (font)
            {
                case Font.Normal:
                    return m_Normal;
                case Font.Semibold:
                    return m_Semibold;
                case Font.Bold:
                    return m_Bold;
                default:
                    throw new ArgumentOutOfRangeException("font", font, null);
            }
        }
    }
}
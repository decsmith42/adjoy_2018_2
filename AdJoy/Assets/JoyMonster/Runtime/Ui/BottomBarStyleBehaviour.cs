﻿using JoyMonster;
using JoyMonster.Core;
using JoyMonster.Ui;
using MaterialUI_JoyMonster;
using UnityEngine;
using UnityEngine.EventSystems;

public class BottomBarStyleBehaviour : UIBehaviour
{
    [SerializeField] private Canvas m_Canvas = null;
    [SerializeField] private DynamicTransformPosition m_Position = null;

    private bool m_IsVisible;
    private int m_TimerId;

    public void TransitionVisible(bool visible)
    {
        if (m_IsVisible == visible) return;
        m_IsVisible = visible;

        if (visible)
        {
            m_Canvas.enabled = true;
        }

        m_Position.TransitionPosition(m_IsVisible ? DynamicTransformPosition.Direction.Middle : DynamicTransformPosition.Direction.Down,
            Values.Duration.SHORT, Values.Duration.SHORTER, m_IsVisible ? Tween.TweenType.EaseOutQuint : Tween.TweenType.EaseInQuint);

        TweenManager.EndTween(m_TimerId);

        if (!visible)
        {
            m_TimerId = TweenManager.TimedCallback(Values.Duration.SHORT + Values.Duration.SHORTER, () =>
            {
                m_Canvas.enabled = false;
            });
        }
    }

    public void SetVisible(bool visible)
    {
        m_IsVisible = visible;
        m_Canvas.enabled = visible;
        m_Position.SetPosition(m_IsVisible ? DynamicTransformPosition.Direction.Middle : DynamicTransformPosition.Direction.Down);
    }

    public void OnLayoutChanged()
    {
        m_Position.offset = new Vector4(0f, 0f, 0f, m_Position.rectTransform.rect.height * 1.05f);
        SetVisible(m_IsVisible);
    }

    public void OnBackToGameButtonPressed()
    {
        JoyMonsterManager.uiHandler.OnBackToGameButtonPressed();
    }
}

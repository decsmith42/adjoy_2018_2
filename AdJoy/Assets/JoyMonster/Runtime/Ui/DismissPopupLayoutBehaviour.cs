﻿using TMPro;
using UnityEngine;

namespace JoyMonster.Ui
{
    public class DismissPopupLayoutBehaviour : UiLayoutBehaviour
    {
        public Vector2 panelBasePositionOffset
        {
            get { return new Vector2(0f, 0f); }
        }

        [SerializeField] private RectTransform m_PanelTransform = null;
        [SerializeField] private TextMeshProUGUI m_LabelTitle = null;
        [SerializeField] private RectTransform m_ButtonAdd = null;
        [SerializeField] private RectTransform m_ButtonCancel = null;

        private float m_ButtonHeight = 40f;

        private struct LayoutValues
        {
            public float panelHeight;
            public float labelTitlePositionY;
            public float labelTitleHeight;
        }

        protected override void OnSetLayout()
        {
            float padding = UiLayoutValueHandler.common.currentValue.padding * 2f;

            float buttonWidth = (m_PanelTransform.rect.width - padding * 3f) / 2f;
            m_ButtonCancel.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Left, padding, buttonWidth);
            m_ButtonAdd.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Left, padding + buttonWidth + padding, buttonWidth);
            m_LabelTitle.rectTransform.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Left, padding, m_PanelTransform.rect.width - padding * 2f);

            m_ButtonCancel.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Bottom, padding, m_ButtonHeight);
            m_ButtonAdd.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Bottom, padding, m_ButtonHeight);

            LayoutValues layoutValue = CalculateLayoutValues();
            m_LabelTitle.rectTransform.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Bottom, layoutValue.labelTitlePositionY, layoutValue.labelTitleHeight);
            m_PanelTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, layoutValue.panelHeight);
        }

        private LayoutValues CalculateLayoutValues()
        {
            float padding = UiLayoutValueHandler.common.currentValue.padding * 2f;

            LayoutValues layoutValues = new LayoutValues();

            layoutValues.panelHeight = padding + m_ButtonHeight + padding;

            layoutValues.labelTitlePositionY = layoutValues.panelHeight;
            layoutValues.labelTitleHeight = m_LabelTitle.preferredHeight;

            layoutValues.panelHeight += layoutValues.labelTitleHeight + padding;

            return layoutValues;
        }
    }
}
﻿using System;
using UnityEngine;

namespace JoyMonster.Ui
{
    [CreateAssetMenu(fileName = "AppBarHeight", menuName = "UI/DynamicValues/AppBarHeight")]
    public class AppBarHeightValueGroup : DynamicValueGroup<AppBarHeightValue> { }

    [Serializable]
    public class AppBarHeightValue : DynamicValue
    {
        public float appBarHeight;
        public float labelBottomPadding;
    }
}
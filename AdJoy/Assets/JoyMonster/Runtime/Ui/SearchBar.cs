﻿using MaterialUI_JoyMonster;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

namespace JoyMonster.Ui
{
    public class SearchBar : UIBehaviour
    {
        private RectTransform m_RectTransform;
        public RectTransform rectTransform
        {
            get
            {
                if (m_RectTransform == null)
                {
                    m_RectTransform = GetComponent<RectTransform>();
                }
                return m_RectTransform;
            }
        }

        [SerializeField] private RectTransform m_BackgroundTransform = null;
        [SerializeField] private DynamicTransformScale m_BackgroundTransformScale = null;
        [SerializeField] private DynamicGraphic m_BackgroundGraphic = null;
        [SerializeField] private TMP_InputField m_InputField = null;
        public TMP_InputField inputField { get { return m_InputField; } }

        [SerializeField] private DynamicCanvasGroup m_DynamicCanvasGroup = null;
        [SerializeField] private DynamicCanvasGroup m_InputFieldCanvasGroup = null;
        [SerializeField] private DynamicCanvasGroup m_ButtonCloseCanvasGroup = null;

        [SerializeField] private UnityEvent m_OnSearchUpdate = null;
        public UnityEvent onSearchUpdate { get { return m_OnSearchUpdate; } }

        private bool m_IsOpen;
        public bool isOpen
        {
            get { return m_IsOpen; }
        }

        private float backgroundWidthClosed { get { return 36f; } }
        private float backgroundWidthOpen { get { return rectTransform.rect.width + 16f; } }

        private int m_TweenIdBackgroundSize;

        private float m_LastTextChange;
        private bool m_TextDirty;

        public void Open()
        {
            m_IsOpen = true;
            m_DynamicCanvasGroup.SetState(true);

            TweenManager.EndTween(m_TweenIdBackgroundSize);

            m_InputField.enabled = true;

            m_BackgroundGraphic.TransitionState(true, Values.Duration.PER_ELEMENT_DELAY, Values.Duration.PER_ELEMENT_DELAY);
            m_BackgroundTransformScale.TransitionState(true, Values.Duration.SHORT, Values.Duration.PER_ELEMENT_DELAY, Tween.TweenType.EaseOutQuint);

            m_TweenIdBackgroundSize = TweenManager.TweenFloat(value => m_BackgroundTransform.SetSizeDeltaX(value),
                m_BackgroundTransform.sizeDelta.x, backgroundWidthOpen,
                Values.Duration.SHORT, Values.Duration.SHORTER, () => m_InputField.Select(), tweenType: Tween.TweenType.SoftEaseOutQuint);

            m_ButtonCloseCanvasGroup.TransitionState(true, Values.Duration.SHORTER, Values.Duration.SHORT);

            m_InputFieldCanvasGroup.TransitionState(true, Values.Duration.SHORTER, Values.Duration.SHORT + Values.Duration.PER_ELEMENT_DELAY);
        }

        public void Close()
        {
            m_IsOpen = false;
            m_DynamicCanvasGroup.DelayedSetState(false, Values.Duration.SHORT);

            TweenManager.EndTween(m_TweenIdBackgroundSize);

            m_InputField.enabled = false;

            m_BackgroundGraphic.TransitionState(false, Values.Duration.SHORT, Values.Duration.SHORTER);
            m_BackgroundTransformScale.TransitionState(false, Values.Duration.SHORTER, Values.Duration.SHORTER + Values.Duration.PER_ELEMENT_DELAY);

            m_TweenIdBackgroundSize = TweenManager.TweenFloat(value => m_BackgroundTransform.SetSizeDeltaX(value),
                m_BackgroundTransform.sizeDelta.x, backgroundWidthClosed,
                Values.Duration.SHORT, Values.Duration.SHORTER, () =>
                {
                    m_InputField.text = "";
                    m_OnSearchUpdate.InvokeIfNotNull();
                }, tweenType: Tween.TweenType.SoftEaseOutSept);

            m_ButtonCloseCanvasGroup.TransitionState(false, Values.Duration.SHORTER, Values.Duration.SHORTER);

            m_InputFieldCanvasGroup.TransitionState(false, Values.Duration.SHORTER, Values.Duration.SHORTER);
        }

        public void OnLayoutRefresh()
        {
            m_BackgroundTransform.SetSizeDeltaX(m_IsOpen ? rectTransform.rect.width + 16f : 36f);
        }

        public void OnTextChanged()
        {
            m_TextDirty = true;
            m_LastTextChange = Time.realtimeSinceStartup;
        }

        private void Update()
        {
            if (!isOpen || !m_TextDirty || Time.realtimeSinceStartup - m_LastTextChange < Values.Duration.MEDIUM) return;

            m_TextDirty = false;
            m_OnSearchUpdate.InvokeIfNotNull();
        }
    }
}
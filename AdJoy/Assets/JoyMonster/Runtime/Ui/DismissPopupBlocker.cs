﻿using UnityEngine;
using UnityEngine.EventSystems;

namespace JoyMonster.Ui
{
    public class DismissPopupBlocker : MonoBehaviour, IPointerClickHandler
    {
        [SerializeField] private DismissPopup m_Popup = null;

        public void OnPointerClick(PointerEventData eventData)
        {
            m_Popup.OnNoPressed();
        }
    }
}
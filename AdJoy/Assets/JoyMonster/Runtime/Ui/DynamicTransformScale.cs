﻿using MaterialUI_JoyMonster;
using UnityEngine;

namespace JoyMonster.Ui
{
    public class DynamicTransformScale : MonoBehaviour
    {
        protected RectTransform m_RectTransform;

        public RectTransform rectTransform
        {
            get
            {
                if (m_RectTransform == null)
                {
                    m_RectTransform = GetComponent<RectTransform>();
                }

                return m_RectTransform;
            }
        }

        protected int m_TweenId;

        public void SetState(bool visible)
        {
            TweenManager.EndTween(m_TweenId);
            rectTransform.localScale = visible ? Vector3.one : Vector3.zero;
        }

        public void TransitionState(bool visible, float duration, float delay, Tween.TweenType tweenType = Tween.TweenType.EaseInOutQuint)
        {
            TweenManager.EndTween(m_TweenId);

            Vector3 target = visible ? Vector3.one : Vector3.zero;

            m_TweenId = TweenManager.TweenVector3(
                value => rectTransform.localScale = value, rectTransform.localScale, target, duration, delay,
                tweenType: tweenType);
        }
    }
}
﻿using MaterialUI_JoyMonster;
using UnityEngine;
using UnityEngine.EventSystems;

namespace JoyMonster.Ui
{
    public class DynamicVectorImageRotation : UIBehaviour
    {
        protected VectorImage m_VectorImage;
        public VectorImage vectorImage
        {
            get
            {
                if (m_VectorImage == null)
                {
                    m_VectorImage = GetComponent<VectorImage>();
                }

                return m_VectorImage;
            }
        }

        [SerializeField] private float m_BaseRotation = 0f;
        [SerializeField] private float m_AltRotation = 180f;

        private int m_TweenId;

        public void SetState(bool isBase)
        {
            TweenManager.EndTween(m_TweenId);

            vectorImage.rotateMeshPos = isBase ? m_BaseRotation : m_AltRotation;
        }

        public void TransitionState(bool isBase, float duration, float delay, Tween.TweenType tweenType)
        {
            TweenManager.EndTween(m_TweenId);

            m_TweenId = TweenManager.TweenFloat(value => m_VectorImage.rotateMeshPos = value,
                m_VectorImage.rotateMeshPos, isBase ? m_BaseRotation : m_AltRotation,
                duration, delay, tweenType: tweenType);
        }

        public void TransitionState(bool isFromBase, bool isToBase, float duration, float delay, Tween.TweenType tweenType)
        {
            TweenManager.EndTween(m_TweenId);

            m_TweenId = TweenManager.TweenFloat(value => m_VectorImage.rotateMeshPos = value,
                isFromBase ? m_BaseRotation : m_AltRotation, isToBase ? m_BaseRotation : m_AltRotation,
                duration, delay, tweenType: tweenType);
        }

        public void TransitionCustom(float targetValue, float duration, float delay, Tween.TweenType tweenType)
        {
            TweenManager.EndTween(m_TweenId);

            m_TweenId = TweenManager.TweenFloat(value => m_VectorImage.rotateMeshPos = value,
                m_VectorImage.rotateMeshPos, targetValue,
                duration, delay, tweenType: tweenType);
        }

        public void TransitionCustom(float startValue, float targetValue, float duration, float delay, Tween.TweenType tweenType)
        {
            TweenManager.EndTween(m_TweenId);

            m_TweenId = TweenManager.TweenFloat(value => m_VectorImage.rotateMeshPos = value,
                startValue, targetValue,
                duration, delay, tweenType: tweenType);
        }
    }
}
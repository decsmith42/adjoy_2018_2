﻿using TMPro;
using UnityEngine;

namespace JoyMonster.Ui
{
    public class ExpiredPopupLayoutBehaviour : UiLayoutBehaviour
    {
        public Vector2 panelBasePositionOffset
        {
            get { return new Vector2(0f, 0f); }
        }

        [SerializeField] private TextMeshProUGUI m_LabelTitle = null;
        [SerializeField] private RectTransform m_ButtonOk = null;
        [SerializeField] private TextMeshProUGUI m_LabelButtonOk = null;

        public Vector2 panelSize { get; private set; }

        protected override void OnSetLayout()
        {
            LayoutValue values = UiLayoutValueHandler.common.currentValue;

            m_LabelTitle.fontSize = values.textSubHeaderFontSize;
            m_LabelButtonOk.fontSize = values.textButtonFontSize;

            float minWidth = 320f;
            float maxWidth = rectTransform.rect.width - values.padding * 4f;

            float width = (minWidth + maxWidth) / 2f;

            float height = values.padding;
            m_ButtonOk.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Right, values.padding, m_LabelButtonOk.preferredWidth + values.padding * 2f);
            m_ButtonOk.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Bottom, height, m_LabelButtonOk.preferredHeight + values.spacing * 2f);
            height += m_ButtonOk.rect.height + values.spacing * 2f;

            m_LabelTitle.rectTransform.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Right, values.padding, width - values.padding * 2f);
            m_LabelTitle.rectTransform.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Bottom, height, m_LabelTitle.preferredHeight);
            height += m_LabelTitle.rectTransform.rect.height + values.padding;

            panelSize = new Vector2(width, height);
        }
    }
}
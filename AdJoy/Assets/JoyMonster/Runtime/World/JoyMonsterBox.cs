﻿using JoyMonster.API;
using JoyMonster.Core;
using UnityEngine;

namespace JoyMonster.World
{
    /// <summary>
    /// Attach to a GameObject to allow a JoyMonsterBox to be attached to this object.
    /// </summary>
    public class JoyMonsterBox : MonoBehaviour
    {
        private static bool m_IsBoxCurrentlyUsed = false;

        /// <summary>
        /// If false, the following settings will override the global behaviour for this specific box.
        /// </summary>
        public bool useBoxGlobalSettings = true;

        /// <summary>
        /// Position is local.
        /// </summary>
        public Vector3 boxPosition = Vector3.zero;

        /// <summary>
        /// Scale is local.
        /// </summary>
        public Vector3 boxScale = Vector3.one;

        /// <summary>
        /// Forces a (global) rotation of 0.
        /// </summary>
        public bool ignoreParentRotation = false;

        private GameObject m_BoxInstance;

        /// <summary>
        /// Attaches a Box to this GameObject object if there are any engagements available and a box hasn't already been collected in the current level instance.
        /// <para></para>
        /// Use this when you want to spawn a Box to this object.
        /// </summary>
        public void AttachBox()
        {
            if (m_IsBoxCurrentlyUsed || JoyMonsterManager.engagementHandler.hasBoxBeenCollected)
            {
                return;
            }

            if (!JoyMonsterManager.engagementHandler.IsAtLeastOneEngagementAvailable())
            {
                return;
            }

            JoyMonsterAPI.DebugLog("Attaching a JoyMonster Box to the object " + this.name, this);
            m_IsBoxCurrentlyUsed = true;

            if (useBoxGlobalSettings)
            {
                m_BoxInstance = AttachBoxToObject(this.gameObject);
            }
            else
            {
                m_BoxInstance = AttachBoxToObject(this.gameObject, boxPosition, boxScale, ignoreParentRotation);
            }
        }

        /// <summary>
        /// Call to collect the Box associated with this GameObject.
        /// <para></para>
        /// Use this when the player shoots/picks up/etc the object.
        /// </summary>
        public void CollectBox()
        {
            if (m_BoxInstance == null)
            {
                return;
            }

            JoyMonsterAPI.DebugLog("Collecting the JoyMonster Box");

            JoyMonsterManager.engagementHandler.SetHasBoxBeenCollected();

            m_BoxInstance.GetComponent<Animator>().speed = 1.0f;
            m_BoxInstance.GetComponent<Animator>().SetInteger("INDEX", 1);
            m_IsBoxCurrentlyUsed = false;
            Destroy(m_BoxInstance.gameObject, 1.2f);
        }

        /// <summary>
        /// Call this if the Box was not collected, but the object it is associated with is no longer available.
        /// <para></para>
        /// Eg. If the object has gone offscreen or despawns.
        /// </summary>
        public void ReleaseBox()
        {
            if (m_BoxInstance != null)
            {
                m_IsBoxCurrentlyUsed = false;
                Destroy(m_BoxInstance.gameObject);
            }
        }

        /// <summary>
        /// Internally used by the plugin.
        /// </summary>
        public static GameObject AttachBoxToObject(GameObject go)
        {
            return AttachBoxToObject(go, JoyMonsterManager.settings.boxPosition, JoyMonsterManager.settings.boxScale, JoyMonsterManager.settings.boxIgnoreParentRotation);
        }

        /// <summary>
        /// Internally used by the plugin.
        /// </summary>
        public static GameObject AttachBoxToObject(GameObject go, Vector3 position, Vector3 scale, bool boxIgnoreParentRotation)
        {
            GameObject boxGameObject = new GameObject("Box");
            boxGameObject.transform.SetParent(go.transform, true);
            boxGameObject.transform.localPosition = position;
            boxGameObject.transform.localScale = scale;
            boxGameObject.transform.localRotation = boxIgnoreParentRotation ? Quaternion.Euler(-go.transform.localRotation.eulerAngles.x, -go.transform.localRotation.eulerAngles.y, -go.transform.localRotation.eulerAngles.z) : Quaternion.identity;

            GameObject boxInstance = Instantiate(Resources.Load("JoyMonsterBox"), Vector3.zero, Quaternion.identity) as GameObject;
            boxInstance.transform.SetParent(boxGameObject.transform, true);
            boxInstance.transform.localPosition = Vector3.zero;
            boxInstance.transform.localScale = Vector3.one;
            return boxInstance;
        }
    }
}
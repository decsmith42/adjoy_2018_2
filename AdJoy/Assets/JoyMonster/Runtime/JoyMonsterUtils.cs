﻿using RSG;

namespace JoyMonster
{
    public static class JoyMonsterUtils
    {
        public static bool IsActiveAndPending(this Promise promise)
        {
            return promise != null && promise.CurState == PromiseState.Pending;
        }

        public static bool IsActiveAndPending<PromisedT>(this Promise<PromisedT> promise)
        {
            return promise != null && promise.CurState == PromiseState.Pending;
        }

        public static void ResolveIfPending(this Promise promise)
        {
            if (promise.IsActiveAndPending())
            {
                promise.Resolve();
            }
        }

        public static void ResolveIfPending<PromisedT>(this Promise<PromisedT> promise, PromisedT value)
        {
            if (promise.IsActiveAndPending())
            {
                promise.Resolve(value);
            }
        }

        public static void Discard(this Promise promise)
        {
            if (promise.IsActiveAndPending())
            {
                promise.Reject(null);
            }
        }

        public static void Discard<PromisedT>(this Promise<PromisedT> promise)
        {
            if (promise.IsActiveAndPending())
            {
                promise.Reject(null);
            }
        }

        public static Promise DiscardAndReplace(this Promise promise)
        {
            promise.Discard();
            return new Promise();
        }

        public static Promise<PromisedT> DiscardAndReplace<PromisedT>(this Promise<PromisedT> promise)
        {
            promise.Discard();
            return new Promise<PromisedT>();
        }
    }
}
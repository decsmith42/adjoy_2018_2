﻿namespace JoyMonster
{
    public static class Values
    {
        public static class ScreenIds
        {
            public const int GAME = 0;
            public const int CONGRATULATIONS = 10;
            public const int CAROUSEL = 11;
            public const int ADDED = 12;
            public const int DISMISSED = 13;
            public const int WALLET = 20;
            public const int OFFER_DETAILS = 30;
            public const int MERCHANT = 40;
            public const int FAQ = 50;
            public const int MAP = 60;
        }

        public static class Duration
        {
            public const float SCREEN_TRANSITION = 0.7f;
            public const float LONG = 1f;
            public const float MEDIUM = 0.5f;
            public const float SHORT = 0.3f;
            public const float SHORTER = 0.15f;
            public const float PER_ELEMENT_DELAY = 0.1f;
            public const float CONGRATULATIONS_WAIT = 2f;

            public const float ENGAGEMENT_TIMER = 10f;
        }

        public static class Layout
        {
            public const float BOTTOM_BAR_HEIGHT = 48f;
        }

        public static class Text
        {
            public const string APPBAR_TITLE_WALLET = "Wallet";
            public const string APPBAR_TITLE_FAQ = "FAQs";
            public const string APPBAR_TITLE_OFFER_DETAILS = "Offer Details";
            public const string APPBAR_TITLE_MERCHANT = "Show Cashier";
            public const string APPBAR_TITLE_MAP = "Map";

            public const string ADD_TO_WALLET_PROMPT = "Add reward to your wallet?";
            public const string ADD_TO_WALLET_ADDING = "Adding reward...";
            public const string ADD_TO_WALLET_ERROR = "There was a problem adding the reward. Try again?";

            public const string REDEEM_TITLE_NORMAL = "Redeem Now";
            public const string REDEEM_TITLE_REDEEMING = "Redeeming...";
            public const string REDEEM_DESCRIPTION_NORMAL = "You must be at [company] before you can successfully use this reward.\n\nOnce there, swipe to redeem.";
            public const string REDEEM_DESCRIPTION_NORMAL_REPLACE = "[company]";
            public const string REDEEM_DESCRIPTION_PARTNER = "Swipe to redeem this offer with [company].";
            public const string REDEEM_DESCRIPTION_GEO_ERROR = "There was a problem checking if you're close enough to [company] to use this reward. Redeem anyway?";
            public const string REDEEM_DESCRIPTION_ERROR = "There was a problem redeeming this offer. Try again?\n";
            public const string REDEEM_TITLE_SUCCESS = "Offer Redeemed.";

            public const string OFFER_EXPIRY = "Expires in [expiry]";
            public const string OFFER_EXPIRY_REPLACE = "[expiry]";
            public const string OFFER_REDEEMABLE_THRESHOLD = "Redeemable when value exceeds [value]";
            public const string OFFER_REDEEMABLE_THRESHOLD_REPLACE = "[value]";
            public const string OFFER_REDEEMABLE_WARNING = "You currently have [current] and need [max] to redeem this reward.";
            public const string OFFER_REDEEMABLE_WARNING_REPLACE_CURRENT = "[current]";
            public const string OFFER_REDEEMABLE_WARNING_REPLACE_MAX = "[max]";
            public const string OFFER_LIMIT = "Limit 1 per visit";
            public const string ENGAGEMENT_EXPIRY_SUFFIX_SINGLE = " day";
            public const string ENGAGEMENT_EXPIRY_SUFFIX_MULTIPLE = " days";

            public const string ENGAGEMENT_COOLDOWN_NONE = "Keep playing to earn more rewards";
            public const string ENGAGEMENT_COOLDOWN = "Eligible to earn another reward in [cooldown]";
            public const string ENGAGEMENT_COOLDOWN_REPLACE = "[cooldown]";
            public const string ENGAGEMENT_COOLDOWN_SUFFIX = " minutes";

            public const string WALLET_COOLDOWN_NONE = "Rewards available - Play now to earn!";
            public const string WALLET_COOLDOWN_H = "[cooldown] hours until you're eligible for another reward";
            public const string WALLET_COOLDOWN_M = "[cooldown] minutes until you're eligible for another reward";
            public const string WALLET_COOLDOWN_REPLACE = "[cooldown]";

            public const string LOCATION_DISABLED = "Enable GPS location in your device settings to start receiving rewards from local businesses!";
            public const string PERMISSION_LOCATION_UNAUTHORIZED = "Share your location to start receiving rewards from local businesses!";
            public const string SHARE_LOCATION = "Share location";
            public const string OK = "OK";

            public const string DISTANCE = "[distance]mi";
            public const string DISTANCE_REPLACE = "[distance]";

            public const string CURRENCY = "$[currency]";
            public const string CURRENCY_REPLACE = "[currency]";

            public const string LOCATION_ANY = "anyparticipatinglocation";
        }
    }
}
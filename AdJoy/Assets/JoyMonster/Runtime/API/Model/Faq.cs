﻿namespace JoyMonster.API.Model
{
    [System.Serializable]
    public sealed class Faq
    {
        public string question;
        public string answer;

        public override string ToString()
        {
            return string.Format("[Faq]"
                                 + "\nquestion:{0}"
                                 + "\nanswer:{1}"
                , question, answer);
        }
    }
}
﻿namespace JoyMonster.API.Model
{
    [System.Serializable]
    public sealed class WalletRedeem
    {
        public bool geo_error;
        public string code;
        public string url;
        public string imageUrl;
        public int timer;

        public override string ToString()
        {
            return string.Format("[WalletRedeem]"
                                 + "\ngeo_error:{0}"
                                 + "\ncode:{1}"
                                 + "\nurl:{2}"
                                 + "\nimageUrl:{3}"
                                 + "\ntimer:{4}"
                                 , geo_error, code, url, imageUrl, timer);
        }
    }
}

﻿namespace JoyMonster.API.Model
{
    [System.Serializable]
    internal sealed class ResultError
    {
        public string __message = "";

        public override string ToString()
        {
            return string.Format("[ResultError]"
                                 + "\n__message:{0}"
                                 , __message);
        }
    }
}

﻿namespace JoyMonster.API.Model
{
    [System.Serializable]
    public sealed class EngagementResultList
    {
        public int cooldownTimer = 0;
        public EngagementResult[] engagements;
    }
}
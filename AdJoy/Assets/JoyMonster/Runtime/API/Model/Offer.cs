using System.Collections.Generic;

namespace JoyMonster.API.Model
{
    [System.Serializable]
    public sealed class Offer
    {
        public string address1;
        public string address2;
        public string bannerUrl;
        public string brandLogoUrl;
        public string city;
        public string company;
        public string currentValue;
        public string description;
        public float distance;
        public string endDate;
        public bool isNew;
        public bool isRedeemable;
        public double latitude;
        public double longitude;
        public string maxValue;
        public string partnerDescription;
        public string phone;
        public string state;
        public string title;
        public string walletUuid;
        public string zip;
        public bool isPartner;
        public bool canAggregate;

        public override string ToString()
        {
            return string.Format("[Offer]"
                                 + "\nwalletUuid:{0}"
                                 + "\ntitle:{1}"
                                 + "\ncompany:{2}"
                                 + "\ndescription:{3}"
                                 + "\nbrandLogoUrl:{4}"
                                 + "\nbannerUrl:{5}"
                                 + "\nendDate:{6}"
                                 + "\ndistance:{7}"
                                 + "\nisRedeemable:{8}"
                                 + "\nlatitude:{9}"
                                 + "\nlongitude:{10}"
                                 + "\ncurrentValue:{11}"
                                 + "\nisNew:{12}"
                                 + "\naddress1:{13}"
                                 + "\naddress2:{14}"
                                 + "\ncity:{15}"
                                 + "\nstate:{16}"
                                 + "\nzip:{17}"
                                 + "\nphone:{18}"
                                 + "\nisPartner:{19}"
                                 + "\npartnerDescription:{20}"
                                 + "\ncanAggregate:{21}"
                                 , walletUuid, title, company, description, brandLogoUrl, bannerUrl, endDate, distance, isRedeemable, latitude, longitude, currentValue, isNew, address1, address2, city, state, zip, phone, isPartner, partnerDescription, canAggregate);
        }

        public string GetFullAddress()
        {
            List<string> fullAddressDataList = new List<string>();

            if (!string.IsNullOrEmpty(address1))
            {
                fullAddressDataList.Add(address1);
            }

            if (!string.IsNullOrEmpty(address2))
            {
                fullAddressDataList.Add(address2);
            }

            if (!string.IsNullOrEmpty(city))
            {
                fullAddressDataList.Add(city);
            }

            if (!string.IsNullOrEmpty(state))
            {
                fullAddressDataList.Add(state);
            }

            if (!string.IsNullOrEmpty(zip))
            {
                fullAddressDataList.Add(zip);
            }

            return string.Join(" ", fullAddressDataList.ToArray());
        }
    }
}

﻿namespace JoyMonster.API.Model.Input
{
    [System.Serializable]
    internal sealed class DeleteOfferInput
    {
        public string deviceUuid;
        public string walletUuid;

        public DeleteOfferInput(string deviceUuid, string walletUuid)
        {
            this.deviceUuid = deviceUuid;
            this.walletUuid = walletUuid;
        }

        public override string ToString()
        {
            return string.Format("[DeleteOfferInput]"
                                 + "\ndeviceUuid:{0}"
                                 + "\nwalletUuid:{1}"
                                 , deviceUuid, walletUuid);
        }
    }
}

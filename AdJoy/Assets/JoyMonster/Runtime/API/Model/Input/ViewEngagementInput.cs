﻿namespace JoyMonster.API.Model.Input
{
    [System.Serializable]
    internal sealed class ViewEngagementInput
    {
        public string deviceUuid;
        public string offerUuid;
        public double latitude;
        public double longitude;

        public ViewEngagementInput(string deviceUuid, string offerUuid, double latitude, double longitude)
        {
            this.deviceUuid = deviceUuid;
            this.offerUuid = offerUuid;
            this.latitude = latitude;
            this.longitude = longitude;
        }

        public override string ToString()
        {
            return string.Format("[ViewEngagementInput]"
                                 + "\ndeviceUuid:{0}"
                                 + "\nofferUuid:{1}"
                                 + "\nlatitude:{2}"
                                 + "\nlongitude:{3}"
                                 , deviceUuid, offerUuid, latitude, longitude);
        }
    }
}

﻿namespace JoyMonster.API.Model.Input
{
    [System.Serializable]
    internal sealed class OfferCountInput
    {
        public string deviceUuid;

        public OfferCountInput(string deviceUuid)
        {
            this.deviceUuid = deviceUuid;
        }

        public override string ToString()
        {
            return string.Format("[OfferCountInput]"
                                 + "\ndeviceUuid:{0}"
                                 , deviceUuid);
        }
    }
}

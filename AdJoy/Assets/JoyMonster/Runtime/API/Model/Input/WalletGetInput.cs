﻿namespace JoyMonster.API.Model.Input
{
    [System.Serializable]
    internal sealed class WalletGetInput
    {
        public string deviceUuid;
        public double latitude;
        public double longitude;

        public WalletGetInput(string deviceUuid, double latitude, double longitude)
        {
            this.deviceUuid = deviceUuid;
            this.latitude = latitude;
            this.longitude = longitude;
        }

        public override string ToString()
        {
            return string.Format("[WalletGetInput]"
                                 + "\ndeviceUuid:{0}"
                                 + "\nlatitude:{1}"
                                 + "\nlongitude:{2}"
                                 , deviceUuid, latitude, longitude);
        }
    }
}

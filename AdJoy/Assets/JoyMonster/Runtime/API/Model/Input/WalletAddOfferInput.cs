﻿namespace JoyMonster.API.Model.Input
{
    [System.Serializable]
    internal sealed class WalletAddOfferInput
    {
        public string deviceUuid;
        public string offerUuid;

        public WalletAddOfferInput(string deviceUuid, string offerUuid)
        {
            this.deviceUuid = deviceUuid;
            this.offerUuid = offerUuid;
        }

        public override string ToString()
        {
            return string.Format("[WalletAddOfferInput]"
                                 + "\ndeviceUuid:{0}"
                                 + "\nofferUuid:{1}"
                                 , deviceUuid, offerUuid);
        }
    }
}

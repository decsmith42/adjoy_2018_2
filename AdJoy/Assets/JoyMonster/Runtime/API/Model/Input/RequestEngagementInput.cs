﻿namespace JoyMonster.API.Model.Input
{
    [System.Serializable]
    internal sealed class RequestEngagementInput
    {
        public string deviceUuid;
        public double latitude;
        public double longitude;

        public RequestEngagementInput(string deviceUuid, double latitude, double longitude)
        {
            this.deviceUuid = deviceUuid;
            this.latitude = latitude;
            this.longitude = longitude;
        }

        public override string ToString()
        {
            return string.Format("[RequestEngagementInput]"
                                 + "\ndeviceUuid:{0}"
                                 + "\nlatitude:{1}"
                                 + "\nlongitude:{2}"
                                 , deviceUuid, latitude, longitude);
        }
    }
}

﻿namespace JoyMonster.API.Model.Input
{
    [System.Serializable]
    internal sealed class WalletRedeemInput
    {
        public string deviceUuid;
        public string walletUuid;
        public bool geo_override;
        public double latitude;
        public double longitude;

        public WalletRedeemInput(string deviceUuid, string walletUuid, bool geo_override, double latitude, double longitude)
        {
            this.deviceUuid = deviceUuid;
            this.walletUuid = walletUuid;
            this.geo_override = geo_override;
            this.latitude = latitude;
            this.longitude = longitude;
        }

        public override string ToString()
        {
            return string.Format("[WalletRedeemInput]"
                                 + "\ndeviceUuid:{0}"
                                 + "\nwalletUuid:{1}"
                                 + "\ngeo_override:{2}"
                                 + "\nlatitude:{3}"
                                 + "\nlongitude:{4}"
                                 , deviceUuid, walletUuid, geo_override, latitude, longitude);
        }
    }
}

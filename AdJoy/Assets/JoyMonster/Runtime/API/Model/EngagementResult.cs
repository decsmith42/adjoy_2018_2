﻿namespace JoyMonster.API.Model
{
    [System.Serializable]
    public sealed class EngagementResult
    {
        public string offerUuid;
        public string title;
        public string description;
        public string brandLogoUrl;
        public string bannerUrl;
        public float lifespan;
        public string company;
        public float distance;

        public override string ToString()
        {
            return string.Format("[EngagementResult]"
                                 + "\nofferUuid:{0}"
                                 + "\ntitle:{1}"
                                 + "\ndescription:{2}"
                                 + "\nbrandLogoUrl:{3}"
                                 + "\nbannerUrl:{4}"
                                 + "\nlifespan:{5}"
                                 + "\ncompany:{6}"
                                 + "\ndistance:{7}"
                , offerUuid, title, description, brandLogoUrl, bannerUrl, lifespan, company, distance);
        }
    }
}
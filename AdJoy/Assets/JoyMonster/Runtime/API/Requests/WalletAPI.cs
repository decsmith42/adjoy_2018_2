﻿using JoyMonster.API.Model;
using JoyMonster.API.Model.Input;
using RSG;
using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Networking;

namespace JoyMonster.API
{
    internal sealed class WalletAPI
    {
        public static IEnumerator AddOfferToWalletCoroutine(Promise<bool> promise, string offerUuid)
        {
            WalletAddOfferInput walletAddOfferInput = new WalletAddOfferInput(RequestHelper.GetDeviceUuid(), offerUuid);
            string bodyJson = JsonUtility.ToJson(walletAddOfferInput);

            using (UnityWebRequest www = new UnityWebRequest(JoyMonsterAPI.BASE_URL + "wallet/add", "POST"))
            {
                byte[] bodyRaw = System.Text.Encoding.UTF8.GetBytes(bodyJson);
                www.uploadHandler = new UploadHandlerRaw(bodyRaw);
                www.downloadHandler = new DownloadHandlerBuffer();

                www.SetRequestHeader("Authorization", JoyMonsterAPI.API_KEY);
                www.SetRequestHeader("Accept", "application/json");
                www.SetRequestHeader("Content-Type", "application/json");

                yield return www.SendWebRequest();

                RequestHelper.HandleResult(www, (bool isSuccess, string data) =>
                {
                    if (!isSuccess)
                    {
                        promise.Reject(new UnityException(data));
                    }
                    else
                    {
                        try
                        {
                            bool success = bool.Parse(data);
                            promise.Resolve(success);
                        }
                        catch (Exception)
                        {
                            promise.Reject(new UnityException("Data is not a bool: " + data));
                        }
                    }
                });
            }
        }

        public static IEnumerator GetWalletListCoroutine(Promise<WalletList> promise, double latitude, double longitude)
        {
            WalletGetInput walletGetInput = new WalletGetInput(RequestHelper.GetDeviceUuid(), latitude, longitude);
            string bodyJson = JsonUtility.ToJson(walletGetInput);

            using (UnityWebRequest www = new UnityWebRequest(JoyMonsterAPI.BASE_URL + "wallet/list", "POST"))
            {
                byte[] bodyRaw = System.Text.Encoding.UTF8.GetBytes(bodyJson);
                www.uploadHandler = new UploadHandlerRaw(bodyRaw);
                www.downloadHandler = new DownloadHandlerBuffer();

                www.SetRequestHeader("Authorization", JoyMonsterAPI.API_KEY);
                www.SetRequestHeader("Accept", "application/json");
                www.SetRequestHeader("Content-Type", "application/json");

                JoyMonsterAPI.DebugLog("Getting wallet list...");

                yield return www.SendWebRequest();

                RequestHelper.HandleResult(www, (bool isSuccess, string data) =>
                {
                    JoyMonsterAPI.DebugLog("Wallet list returned " + isSuccess);

                    if (!isSuccess)
                    {
                        promise.Reject(new UnityException(data));
                    }
                    else
                    {
                        WalletList walletList = JsonUtility.FromJson<WalletList>(data);
                        promise.Resolve(walletList);
                    }
                });
            }
        }

        public static IEnumerator GetOfferCountCoroutine(Promise<int> promise)
        {
            OfferCountInput offerCountInput = new OfferCountInput(RequestHelper.GetDeviceUuid());
            string bodyJson = JsonUtility.ToJson(offerCountInput);

            using (UnityWebRequest www = new UnityWebRequest(JoyMonsterAPI.BASE_URL + "wallet/offerCount", "POST"))
            {
                byte[] bodyRaw = System.Text.Encoding.UTF8.GetBytes(bodyJson);
                www.uploadHandler = new UploadHandlerRaw(bodyRaw);
                www.downloadHandler = new DownloadHandlerBuffer();

                www.SetRequestHeader("Authorization", JoyMonsterAPI.API_KEY);
                www.SetRequestHeader("Accept", "application/json");
                www.SetRequestHeader("Content-Type", "application/json");

                yield return www.SendWebRequest();

                RequestHelper.HandleResult(www, (bool isSuccess, string data) =>
                {
                    if (!isSuccess)
                    {
                        promise.Reject(new UnityException(data));
                    }
                    else
                    {
                        try
                        {
                            int offerCount = int.Parse(data);
                            promise.Resolve(offerCount);
                        }
                        catch (Exception)
                        {
                            promise.Reject(new UnityException("Data is not an int: " + data));
                        }
                    }
                });
            }
        }

        public static IEnumerator DeleteOfferFromWalletCoroutine(Promise<bool> promise, string walletUuid)
        {
            DeleteOfferInput deleteOfferInput = new DeleteOfferInput(RequestHelper.GetDeviceUuid(), walletUuid);
            string bodyJson = JsonUtility.ToJson(deleteOfferInput);

            using (UnityWebRequest www = new UnityWebRequest(JoyMonsterAPI.BASE_URL + "wallet/delete", "POST"))
            {
                byte[] bodyRaw = System.Text.Encoding.UTF8.GetBytes(bodyJson);
                www.uploadHandler = new UploadHandlerRaw(bodyRaw);
                www.downloadHandler = new DownloadHandlerBuffer();

                www.SetRequestHeader("Authorization", JoyMonsterAPI.API_KEY);
                www.SetRequestHeader("Accept", "application/json");
                www.SetRequestHeader("Content-Type", "application/json");

                yield return www.SendWebRequest();

                RequestHelper.HandleResult(www, (bool isSuccess, string data) =>
                {
                    if (!isSuccess)
                    {
                        promise.Reject(new UnityException(data));
                    }
                    else
                    {
                        try
                        {
                            bool success = bool.Parse(data);
                            promise.Resolve(success);
                        }
                        catch (Exception)
                        {
                            promise.Reject(new UnityException("Data is not a bool: " + data));
                        }
                    }
                });
            }
        }

        public static IEnumerator RedeemOfferCoroutine(Promise<WalletRedeem> promise, string walletUuid,
            bool geoOverride, double latitude, double longitude)
        {
            WalletRedeemInput walletRedeemInput = new WalletRedeemInput(RequestHelper.GetDeviceUuid(), walletUuid,
                geoOverride, latitude, longitude);
            string bodyJson = JsonUtility.ToJson(walletRedeemInput);

            using (UnityWebRequest www = new UnityWebRequest(JoyMonsterAPI.BASE_URL + "wallet/redeem", "POST"))
            {
                byte[] bodyRaw = System.Text.Encoding.UTF8.GetBytes(bodyJson);
                www.uploadHandler = new UploadHandlerRaw(bodyRaw);
                www.downloadHandler = new DownloadHandlerBuffer();

                www.SetRequestHeader("Authorization", JoyMonsterAPI.API_KEY);
                www.SetRequestHeader("Accept", "application/json");
                www.SetRequestHeader("Content-Type", "application/json");

                yield return www.SendWebRequest();

                RequestHelper.HandleResult(www, (bool isSuccess, string data) =>
                {
                    if (!isSuccess)
                    {
                        promise.Reject(new UnityException(data));
                    }
                    else
                    {
                        WalletRedeem walletRedeem = JsonUtility.FromJson<WalletRedeem>(data);
                        promise.Resolve(walletRedeem);
                    }
                });
            }
        }
    }
}
﻿using JoyMonster.API.Model;
using System;
using UnityEngine;
using UnityEngine.Networking;

namespace JoyMonster.API
{
    internal sealed class RequestHelper
    {
        public static string GetDeviceUuid()
        {
            return SystemInfo.deviceUniqueIdentifier;
        }

        public static void HandleResult(UnityWebRequest www, Action<bool, string> onDone)
        {
            if (www.isNetworkError)
            {
                onDone(false, www.error);
                return;
            }

            if (string.IsNullOrEmpty(www.downloadHandler.text))
            {
                onDone(false, "The server didn't return any data");
                return;
            }

            string json = www.downloadHandler.text;

            if (json.Contains("<!DOCTYPE html>") && json.Contains("<title>Error</title>"))
            {
                onDone(false, "The server returned an error");
                return;
            }

            if (www.responseCode != 200)
            {
                if (json.Contains("__message"))
                {
                    try
                    {
                        ResultError resultError = JsonUtility.FromJson<ResultError>(json);
                        onDone(false, resultError.__message);
                    }
                    catch (Exception e)
                    {
                        onDone(false, e.Message);
                    }
                }
                else
                {
                    onDone(false, "The server returned an error (" + json + ")");
                }

                return;
            }

            onDone(true, json);
        }
    }
}
﻿using JoyMonster.API.Model;
using JoyMonster.API.Model.Input;
using RSG;
using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Networking;

namespace JoyMonster.API
{
    internal sealed class EngagementAPI
    {
        public static IEnumerator RequestEngagementCoroutine(Promise<EngagementResultList> promise, double latitude, double longitude)
        {
            RequestEngagementInput requestEngagementInput = new RequestEngagementInput(RequestHelper.GetDeviceUuid(), latitude, longitude);
            string bodyJson = JsonUtility.ToJson(requestEngagementInput);

            using (UnityWebRequest www = new UnityWebRequest(JoyMonsterAPI.BASE_URL + "engagement/request", "POST"))
            {
                byte[] bodyRaw = System.Text.Encoding.UTF8.GetBytes(bodyJson);
                www.uploadHandler = new UploadHandlerRaw(bodyRaw);
                www.downloadHandler = new DownloadHandlerBuffer();

                www.SetRequestHeader("Authorization", JoyMonsterAPI.API_KEY);
                www.SetRequestHeader("Accept", "application/json");
                www.SetRequestHeader("Content-Type", "application/json");

                yield return www.SendWebRequest();

                RequestHelper.HandleResult(www, (bool isSuccess, string data) =>
                {
                    if (!isSuccess)
                    {
                        promise.Reject(new UnityException(data));
                    }
                    else
                    {
                        EngagementResultList engagementResultList = JsonUtility.FromJson<EngagementResultList>(data);
                        promise.Resolve(engagementResultList);
                    }
                });
            }
        }

        public static IEnumerator ViewEngagementCoroutine(Promise<bool> promise, string offerUuid, double latitude, double longitude)
        {
            ViewEngagementInput viewEngagementInput = new ViewEngagementInput(RequestHelper.GetDeviceUuid(), offerUuid, latitude, longitude);
            string bodyJson = JsonUtility.ToJson(viewEngagementInput);

            using (UnityWebRequest www = new UnityWebRequest(JoyMonsterAPI.BASE_URL + "engagement/view", "POST"))
            {
                byte[] bodyRaw = System.Text.Encoding.UTF8.GetBytes(bodyJson);
                www.uploadHandler = new UploadHandlerRaw(bodyRaw);
                www.downloadHandler = new DownloadHandlerBuffer();

                www.SetRequestHeader("Authorization", JoyMonsterAPI.API_KEY);
                www.SetRequestHeader("Accept", "application/json");
                www.SetRequestHeader("Content-Type", "application/json");

                yield return www.SendWebRequest();

                RequestHelper.HandleResult(www, (bool isSuccess, string data) =>
                {
                    if (!isSuccess)
                    {
                        promise.Reject(new UnityException(data));
                    }
                    else
                    {
                        try
                        {
                            bool success = bool.Parse(data);
                            promise.Resolve(success);
                        }
                        catch (Exception)
                        {
                            promise.Reject(new UnityException("Data is not a bool: " + data));
                        }
                    }
                });
            }
        }
    }
}
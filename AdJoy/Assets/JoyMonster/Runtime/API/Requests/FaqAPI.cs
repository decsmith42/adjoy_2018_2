﻿using JoyMonster.API.Model;
using RSG;
using System.Collections;
using UnityEngine;
using UnityEngine.Networking;

namespace JoyMonster.API
{
    internal sealed class FaqAPI
    {
        public static IEnumerator GetFaqListCoroutine(Promise<FaqList> promise)
        {
            using (UnityWebRequest www = UnityWebRequest.Post(JoyMonsterAPI.BASE_URL + "faq/list", ""))
            {
                www.SetRequestHeader("Authorization", JoyMonsterAPI.API_KEY);
                www.SetRequestHeader("Accept", "application/json");
                www.SetRequestHeader("Content-Type", "application/json");

                yield return www.SendWebRequest();

                RequestHelper.HandleResult(www, (bool isSuccess, string data) =>
                {
                    if (!isSuccess)
                    {
                        promise.Reject(new UnityException(data));
                    }
                    else
                    {
                        FaqList faqList = JsonUtility.FromJson<FaqList>(data);
                        promise.Resolve(faqList);
                    }
                });
            }
        }
    }
}
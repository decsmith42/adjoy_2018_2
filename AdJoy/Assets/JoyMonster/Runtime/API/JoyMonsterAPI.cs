using JoyMonster.API.Model;
using RSG;
using System;
using UnityEngine;
using UnityEngine.Events;
using Object = UnityEngine.Object;

//TODO: Add timeouts
namespace JoyMonster.API
{
    public sealed class JoyMonsterAPI
    {
        public enum PromiseResultType
        {
            USE_REAL_API,
            MOCKUP_ALWAYS_RESOLVE,
            MOCKUP_ALWAYS_EXCEPTION,
            MOCKUP_RANDOM,
        }

        public enum ResultType
        {
            WITH_DATA,
            WITHOUT_DATA,
            RANDOM
        }

        public static string API_KEY = "";
        public static bool editorLogging;

        public static readonly string BASE_URL = "https://unityapi.joymonster.com/data/v1/";

        private static PromiseResultType m_PromiseResultType = PromiseResultType.USE_REAL_API;
        public static PromiseResultType promiseResultType
        {
            get { return m_PromiseResultType; }
            set
            {
                m_PromiseResultType = value;
                if (clearEngagementsEvent != null)
                {
                    clearEngagementsEvent.Invoke();
                }
            }
        }

        public static ResultType resultType = ResultType.WITH_DATA;
        private static CoroutineRunner m_CoroutineRunner;
        internal static CoroutineRunner coroutineRunner
        {
            get
            {
                if (m_CoroutineRunner == null)
                {
                    m_CoroutineRunner = new GameObject("Coroutine Runner").AddComponent<CoroutineRunner>();
                    GameObject.DontDestroyOnLoad(m_CoroutineRunner.gameObject);
                }

                return m_CoroutineRunner;
            }
        }

        private static UnityEvent m_ClearEngagemntsEvent = new UnityEvent();
        internal static UnityEvent clearEngagementsEvent
        {
            get { return m_ClearEngagemntsEvent; }
        }

        internal static IPromise<int> GetOfferCount()
        {
            if (m_PromiseResultType != PromiseResultType.USE_REAL_API)
            {
                return JoyMonsterMockupAPI.GetOfferCount();
            }

            Promise<int> promise = new Promise<int>();
            coroutineRunner.StartCoroutine(WalletAPI.GetOfferCountCoroutine(promise));
            return promise;
        }

        internal static IPromise<EngagementResultList> RequestEngagement(double latitude, double longitude)
        {
            if (m_PromiseResultType != PromiseResultType.USE_REAL_API)
            {
                return JoyMonsterMockupAPI.RequestEngagement(latitude, longitude);
            }

            Promise<EngagementResultList> promise = new Promise<EngagementResultList>();
            coroutineRunner.StartCoroutine(
                EngagementAPI.RequestEngagementCoroutine(promise, latitude, longitude));
            return promise;
        }

        internal static IPromise<bool> ViewEngagement(string offerUuid, double latitude, double longitude)
        {
            if (m_PromiseResultType != PromiseResultType.USE_REAL_API)
            {
                return JoyMonsterMockupAPI.ViewEngagement(offerUuid);
            }

            Promise<bool> promise = new Promise<bool>();
            coroutineRunner.StartCoroutine(
                EngagementAPI.ViewEngagementCoroutine(promise, offerUuid, latitude, longitude));
            return promise;
        }

        internal static IPromise<bool> AddOfferToWallet(string offerUuid)
        {
            if (m_PromiseResultType != PromiseResultType.USE_REAL_API)
            {
                return JoyMonsterMockupAPI.AddOfferToWallet(offerUuid);
            }

            Promise<bool> promise = new Promise<bool>();
            coroutineRunner.StartCoroutine(WalletAPI.AddOfferToWalletCoroutine(promise, offerUuid));
            return promise;
        }

        internal static IPromise<WalletList> GetWalletList(double latitude, double longitude)
        {
            if (m_PromiseResultType != PromiseResultType.USE_REAL_API)
            {
                return JoyMonsterMockupAPI.GetWalletList(latitude, longitude);
            }

            Promise<WalletList> promise = new Promise<WalletList>();
            coroutineRunner.StartCoroutine(WalletAPI.GetWalletListCoroutine(promise, latitude, longitude));
            return promise;
        }

        internal static IPromise<bool> DeleteOfferFromWallet(string walletUuid)
        {
            if (m_PromiseResultType != PromiseResultType.USE_REAL_API)
            {
                return JoyMonsterMockupAPI.DeleteOfferFromWallet(walletUuid);
            }

            Promise<bool> promise = new Promise<bool>();
            coroutineRunner.StartCoroutine(WalletAPI.DeleteOfferFromWalletCoroutine(promise, walletUuid));
            return promise;
        }

        internal static IPromise<WalletRedeem> RedeemOffer(string walletUuid, bool geoOverride, double latitude,
            double longitude)
        {
            if (m_PromiseResultType != PromiseResultType.USE_REAL_API)
            {
                return JoyMonsterMockupAPI.RedeemOffer(walletUuid, geoOverride, latitude, longitude);
            }

            Promise<WalletRedeem> promise = new Promise<WalletRedeem>();
            coroutineRunner.StartCoroutine(WalletAPI.RedeemOfferCoroutine(promise, walletUuid, geoOverride,
                latitude, longitude));
            return promise;
        }

        internal static IPromise<FaqList> GetFaqList()
        {
            if (m_PromiseResultType != PromiseResultType.USE_REAL_API)
            {
                return JoyMonsterMockupAPI.GetFaqList();
            }

            Promise<FaqList> promise = new Promise<FaqList>();
            coroutineRunner.StartCoroutine(FaqAPI.GetFaqListCoroutine(promise));
            return promise;
        }

        internal static void DebugLog(object message, Object source = null)
        {
            if (!editorLogging) return;

            Debug.Log(message, source);
        }

        internal static void DebugLogError(object message, Object source = null)
        {
            if (!editorLogging) return;

            Debug.LogError(message, source);
        }

        internal static void DebugLogException(Exception exception)
        {
            if (!editorLogging) return;

            Debug.LogException(exception);
        }
    }
}
﻿using JoyMonster.API.Model;
using RSG;
using System;
using System.Collections;
using System.Text;
using UnityEngine;
using Random = UnityEngine.Random;

namespace JoyMonster.API
{
    internal sealed class JoyMonsterMockupAPI
    {
        private static readonly float m_MockupDelay = 2.0f;

        public static Promise<EngagementResultList> RequestEngagement(double latitude, double longitude)
        {
            Promise<EngagementResultList> promise = new Promise<EngagementResultList>();
            JoyMonsterAPI.coroutineRunner.StartCoroutine(Wait(m_MockupDelay, () =>
            {
                EngagementResultList engagementResultList = new EngagementResultList();

                bool withData = Random.value > 0.5f;
                if (JoyMonsterAPI.resultType == JoyMonsterAPI.ResultType.WITH_DATA ||
                    (JoyMonsterAPI.resultType == JoyMonsterAPI.ResultType.RANDOM && withData))
                {
                    engagementResultList.engagements = new EngagementResult[Random.Range(0, 10)];

                    for (int i = 0; i < engagementResultList.engagements.Length; i++)
                    {
                        engagementResultList.engagements[i] = GenerateRandomEngagement(i);
                    }
                }
                else
                {
                    engagementResultList.engagements = new EngagementResult[0];
                }

                ResolveOrRejectPromise(promise, engagementResultList);
            }));
            return promise;
        }

        private static EngagementResult GenerateRandomEngagement(int index)
        {
            return new EngagementResult
            {
                offerUuid = GenerateRandomWalletUuid(),
                title = Random.value > 0.5f
                    ? Random.value > 0.5f
                        ? "Offer"
                        : "Short Offer"
                    : Random.value > 0.5f
                        ? "Medium Length Offer Title"
                        : "Long Title Name For An Offer With A Long Title",
                description = Random.value > 0.5f
                    ? Random.value > 0.5f
                        ? "Description."
                        : "Description that is short."
                    : Random.value > 0.5f
                        ? "A slightly longer, medium sized description for an offer with a description."
                        : "The longest title for an offer. This offer has the longest title out of all possible titles, though some offers share a part of this title so maybe it's actually not that unique.",
                brandLogoUrl = "https://picsum.photos/300/300/?image=" + index,
                bannerUrl = "https://picsum.photos/500/" +
                            (Random.value > 0.5f ? "375" : (Random.value > 0.5f ? "250" : "500")) + "/?image=" + index,
                distance = Random.value * 100f,
                lifespan = (Random.value > 0.5f ? 0 : (Random.value > 0.5f ? 1 : 10)),
                company = Random.value > 0.5f
                    ? Random.value > 0.5f
                        ? "Company"
                        : "Short Company"
                    : Random.value > 0.5f
                        ? "Medium Length Company Name"
                        : "Long Company Name For An Offer With A Long Company Name"
            };
        }

        public static IPromise<bool> ViewEngagement(string offerUuid)
        {
            Promise<bool> promise = new Promise<bool>();
            JoyMonsterAPI.coroutineRunner.StartCoroutine(Wait(m_MockupDelay, () =>
            {
                bool result;

                bool withData = Random.value > 0.5f;
                if (JoyMonsterAPI.resultType == JoyMonsterAPI.ResultType.WITH_DATA ||
                    (JoyMonsterAPI.resultType == JoyMonsterAPI.ResultType.RANDOM && withData))
                {
                    result = true;
                }
                else
                {
                    result = false;
                }

                ResolveOrRejectPromise(promise, result);
            }));
            return promise;
        }

        public static IPromise<bool> AddOfferToWallet(string offerUuid)
        {
            Promise<bool> promise = new Promise<bool>();
            JoyMonsterAPI.coroutineRunner.StartCoroutine(Wait(m_MockupDelay, () =>
            {
                bool result;

                bool withData = Random.value > 0.5f;
                if (JoyMonsterAPI.resultType == JoyMonsterAPI.ResultType.WITH_DATA ||
                    (JoyMonsterAPI.resultType == JoyMonsterAPI.ResultType.RANDOM && withData))
                {
                    result = true;
                }
                else
                {
                    result = false;
                }

                ResolveOrRejectPromise(promise, result);
            }));
            return promise;
        }

        public static IPromise<WalletList> GetWalletList(double latitude, double longitude)
        {
            Promise<WalletList> promise = new Promise<WalletList>();
            JoyMonsterAPI.coroutineRunner.StartCoroutine(Wait(m_MockupDelay, () =>
            {
                WalletList walletList = new WalletList();

                bool withData = Random.value > 0.5f;
                if (JoyMonsterAPI.resultType == JoyMonsterAPI.ResultType.WITH_DATA ||
                    (JoyMonsterAPI.resultType == JoyMonsterAPI.ResultType.RANDOM && withData))
                {
                    walletList.offers = new Offer[20];

                    for (var i = 0; i < walletList.offers.Length; i++)
                    {
                        walletList.offers[i] = GenerateRandomOfferForWalletList(i);
                    }
                }
                else
                {
                    walletList.offers = new Offer[0];
                }

                ResolveOrRejectPromise(promise, walletList);
            }));
            return promise;
        }

        private static Offer GenerateRandomOfferForWalletList(int index)
        {
            bool partner = Random.value > 0.85f;

            //float logoRatio = (Random.value * 0.5f) + 0.9f;
            //int logoWidth = Mathf.RoundToInt(300 * logoRatio);
            int logoWidth = 300;
            int logoHeight = 300;

            int bannerWidth = 500;
            int bannerHeight = Random.Range(250, 500);

            return new Offer
            {
                walletUuid = GenerateRandomWalletUuid(),
                title = (Random.value > 0.5f ? "Short Offer #" : "Super Duper Very Long Offer Title #") + (index + 1),
                company = (Random.value > 0.5f ? "Company #" : "Super Duper Very Long Company Name #") + (index + 1),
                description = (Random.value > 0.5f
                                  ? "Short description."
                                  : "Super duper very incredibly long description that might possibly span multiple lines " +
                                    "and now I've run out of words but need more to test that should be enough words now."
                              )
                              + (index + 1),
                brandLogoUrl =
                    "https://picsum.photos/" + logoWidth + "/" + logoHeight + "/?image=" +
                    (10 + index), // + (Random.value > 0.5f ? "150" : "300") + "/" + (Random.value > 0.5f ? "150" : "300"),
                bannerUrl = "https://picsum.photos/" + bannerWidth + "/" + bannerHeight + "/?image=" +
                            (100 + index), // + ,
                endDate = (DateTime.Now + new TimeSpan(Random.Range(1, 200), 0, 0, 0)).ToString(),
                distance = (Random.value * 10000f) / 33.78f * ((Random.value + 0.5f) / 2f),
                isRedeemable = true,
                latitude = Random.Range(-75f, 75f),
                longitude = Random.Range(-180f, 180f),
                currentValue = "$" + (Random.value * 6f).ToString("#0.00"),
                isNew = Random.value >= 0.5f,
                address1 = (100 + index) + " 2nd Ave",
                address2 = "",
                city = "NewCity",
                state = "CA",
                zip = "93013",
                phone = "232-323-839" + index,
                isPartner = partner,
                partnerDescription = partner ? "Dine for less" : "",
                maxValue = "6"
            };
        }

        private static readonly char[] m_Characters =
        {
            'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U',
            'V', 'W', 'X', 'Y', 'Z',
            'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u',
            'v', 'w', 'x', 'y', 'z',
            '0', '1', '2', '3', '4', '5', '6', '7', '8', '9'
        };

        private static readonly int[] m_WalletUuidFormat =
        {
            8, 4, 4, 4, 12
        };

        private static string GenerateRandomWalletUuid()
        {
            StringBuilder sb = new StringBuilder(40);

            for (int i = 0; i < m_WalletUuidFormat.Length; i++)
            {
                if (i > 0)
                {
                    sb.Append('-');
                }

                for (int j = 0; j < m_WalletUuidFormat[i]; j++)
                {
                    sb.Append(Random.Range(0, m_Characters.Length));
                }
            }

            return sb.ToString();
        }

        public static IPromise<int> GetOfferCount()
        {
            Promise<int> promise = new Promise<int>();
            JoyMonsterAPI.coroutineRunner.StartCoroutine(Wait(m_MockupDelay, () =>
            {
                int offerCount;

                bool withData = Random.value > 0.5f;
                if (JoyMonsterAPI.resultType == JoyMonsterAPI.ResultType.WITH_DATA ||
                    (JoyMonsterAPI.resultType == JoyMonsterAPI.ResultType.RANDOM && withData))
                {
                    offerCount = 3;
                }
                else
                {
                    offerCount = 0;
                }

                ResolveOrRejectPromise(promise, offerCount);
            }));
            return promise;
        }

        public static IPromise<bool> DeleteOfferFromWallet(string walletUuid)
        {
            Promise<bool> promise = new Promise<bool>();
            JoyMonsterAPI.coroutineRunner.StartCoroutine(Wait(m_MockupDelay, () =>
            {
                bool result;

                bool withData = Random.value > 0.5f;
                if (JoyMonsterAPI.resultType == JoyMonsterAPI.ResultType.WITH_DATA ||
                    (JoyMonsterAPI.resultType == JoyMonsterAPI.ResultType.RANDOM && withData))
                {
                    result = true;
                }
                else
                {
                    result = false;
                }

                ResolveOrRejectPromise(promise, result);
            }));
            return promise;
        }

        public static IPromise<WalletRedeem> RedeemOffer(string walletUuid, bool geoOverride, double latitude,
            double longitude)
        {
            Promise<WalletRedeem> promise = new Promise<WalletRedeem>();
            JoyMonsterAPI.coroutineRunner.StartCoroutine(Wait(m_MockupDelay, () =>
            {
                WalletRedeem walletRedeem = new WalletRedeem();
                walletRedeem.code = Random.value >= 0.5f ? "" : "123456";
                walletRedeem.url = "https://joymonster.com/";
                walletRedeem.imageUrl = Random.value >= 0.5f
                    ? ""
                    : "https://timesofindia.indiatimes.com/thumb/msid-55738667,imgsize-12683,width-400,resizemode-4/55738667.jpg";
                walletRedeem.timer = Random.value >= 0.5f ? 0 : 15;

                bool withData = Random.value > 0.5f;
                if (JoyMonsterAPI.resultType == JoyMonsterAPI.ResultType.WITH_DATA ||
                    (JoyMonsterAPI.resultType == JoyMonsterAPI.ResultType.RANDOM && withData))
                {
                    walletRedeem.geo_error = false;
                }
                else
                {
                    walletRedeem.geo_error = true;
                }

                ResolveOrRejectPromise(promise, walletRedeem);
            }));
            return promise;
        }

        public static IPromise<FaqList> GetFaqList()
        {
            Promise<FaqList> promise = new Promise<FaqList>();
            JoyMonsterAPI.coroutineRunner.StartCoroutine(Wait(m_MockupDelay, () =>
            {
                FaqList faqList = new FaqList();

                bool withData = Random.value > 0.5f;
                if (JoyMonsterAPI.resultType == JoyMonsterAPI.ResultType.WITH_DATA ||
                    (JoyMonsterAPI.resultType == JoyMonsterAPI.ResultType.RANDOM && withData))
                {
                    faqList.faqs = new Faq[3];
                    faqList.faqs[0] = new Faq()
                    {
                        question = "Question #01",
                        answer = "This is the answer to question #1. There might be a fair amount of text here."
                    };
                    faqList.faqs[1] = new Faq()
                    {
                        question = "Question #2 is a bit longer in the main text field",
                        answer = "This is the answer to question #2."
                    };
                    faqList.faqs[2] = new Faq()
                    {
                        question = "Question #3",
                        answer =
                            "There is a larger answer here. This is a much bigger answer text. Also it might help the users with something. Mostly it's a space check. This is not the biggest one possible, but it should stay reasonable. Might even have a few more lines."
                    };
                }
                else
                {
                    faqList.faqs = new Faq[0];
                }

                ResolveOrRejectPromise(promise, faqList);
            }));
            return promise;
        }

        private static void ResolveOrRejectPromise<T>(Promise<T> promise, T data)
        {
            if (promise.CurState != PromiseState.Pending) return;

            if (JoyMonsterAPI.promiseResultType == JoyMonsterAPI.PromiseResultType.MOCKUP_ALWAYS_RESOLVE)
            {
                promise.Resolve(data);
            }
            else if (JoyMonsterAPI.promiseResultType == JoyMonsterAPI.PromiseResultType.MOCKUP_ALWAYS_EXCEPTION)
            {
                promise.Reject(new UnityException("Fake exception"));
            }
            else
            {
                if (Random.value > 0.5f)
                {
                    promise.Resolve(data);
                }
                else
                {
                    promise.Reject(new UnityException("Fake exception"));
                }
            }
        }

        private static IEnumerator Wait(float seconds, Action onComplete)
        {
            yield return new WaitForSeconds(seconds);
            if (onComplete != null)
            {
                onComplete();
            }
        }
    }
}
﻿using JoyMonster.API;
using JoyMonster.Core;
using RSG;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.Events;

[assembly: InternalsVisibleTo("JoyMonsterAPITests")]
namespace JoyMonster
{
    /// <summary>
    /// Public-facing API for JoyMonster plugin.
    /// </summary>
    public static class JoyMonsterPlugin
    {
        //  Version MUST be "major.minor.patch" format.
        /// <summary>
        /// The full JoyMonster Plugin version.
        /// </summary>
        public const string JOYMONSTER_PLUGIN_VERSION = "1.0.11";

        /// <summary>
        /// Called before the JoyMonster UI begins showing.
        /// </summary>
        public static UnityEvent onBeforeUiShow
        {
            get { return JoyMonsterManager.uiHandler.onBeforeUiShow; }
        }
        /// <summary>
        /// Called after the JoyMonster UI finishes showing.
        /// </summary>
        public static UnityEvent onAfterUiShow
        {
            get { return JoyMonsterManager.uiHandler.onAfterUiShow; }
        }

        /// <summary>
        /// Called before the JoyMonster UI begins hiding.
        /// </summary>
        public static UnityEvent onBeforeUiHide
        {
            get { return JoyMonsterManager.uiHandler.onBeforeUiHide; }
        }
        /// <summary>
        /// Called after the JoyMonster UI finishes hiding.
        /// </summary>
        public static UnityEvent onAfterUiHide
        {
            get { return JoyMonsterManager.uiHandler.onAfterUiHide; }
        }

        /// <summary>
        /// Gets a reference to the current JoyMonster settings file.
        /// <para></para>
        /// If no file exists, a default one will be created.
        /// <para></para>
        /// The default location is [JoyMonster]/Resources/JoyMonsterSettings.asset
        /// </summary>
        public static JoyMonsterSettings settings
        {
            get { return JoyMonsterManager.settings; }
        }

        /// <summary>
        /// Returns whether the JoyMonster Box has been collected since ResetBoxCollectionStatus has been last called.
        /// </summary>
        public static bool boxHasBeenCollected
        {
            get { return JoyMonsterManager.engagementHandler.hasBoxBeenCollected; }
        }

        /// <summary>
        /// Returns whether there are any engagements available to show.
        /// <para></para>
        /// This is regardless of whether the player has collected the box or not.
        /// </summary>
        public static bool engagementsAvailable
        {
            get { return JoyMonsterManager.engagementHandler.IsAtLeastOneEngagementAvailable(); }
        }

        /// <summary>
        /// Returns, in seconds, how long until new engagements will be available to collect via a Box.
        /// </summary>
        public static int secondsUntilNewEngagementsAvailable
        {
            get { return Mathf.FloorToInt(JoyMonsterManager.engagementHandler.cooldown); }
        }

        /// <summary>
        /// Returns whether the JoyMonster UI is currently showing.
        /// </summary>
        public static bool joyMonsterUiIsShowing
        {
            get { return JoyMonsterManager.uiHandler.uiIsShowing; }
        }

        /// <summary>
        /// Resets the collection status of the JoyMonster Box.
        /// <para></para>
        /// Call this every time a level instance ends, so that the plugin can attach boxes in the next level instance.
        /// <para></para>
        /// Be sure to call 'ShowRewardsIfAvailable' before calling this, so the player can select a reward if any were collected.
        /// </summary>
        public static void ResetBoxCollectedStatus()
        {
            JoyMonsterManager.engagementHandler.Reset();
        }

        /// <summary>
        /// Shows the JoyMonster UI and opens the wallet screen.
        /// <para></para>
        /// This is best called from a button in a main/pause menu.
        /// </summary>
        public static void ShowWallet()
        {
            JoyMonsterManager.ShowWallet();
        }

        /// <summary>
        /// This allows the JoyMonster UI to show and open to the reward selection screen, if any were collected.
        /// <para></para>
        /// Call this every time a level instance ends.
        /// <para></para>
        /// Be sure to call this before 'ResetBoxCollectionStatus'
        /// </summary>
        public static void ShowRewardsIfAvailable()
        {
            JoyMonsterManager.ShowRewards();
        }

        /// <summary>
        /// Returns the number of offers in the JoyMonster Wallet.
        /// <para></para>
        /// This is useful if you want a button that opens the wallet, to display whether or not there are any offers in the Wallet.
        /// </summary>
        /// <returns></returns>
        public static IPromise<int> GetOfferCount()
        {
            return JoyMonsterAPI.GetOfferCount();
        }

        /// <summary>
        /// Forces the wallet to refresh offers from the server next time the UI is shown.
        /// <para></para>
        /// The plugin automatically handles refreshes, so you shouldn't need to use this.
        /// </summary>
        public static void ForceWalletRefreshOnNextLoad()
        {
            JoyMonsterManager.walletHandler.MarkWalletDataDirty();
        }
    }
}